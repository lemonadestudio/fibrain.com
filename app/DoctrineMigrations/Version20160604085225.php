<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160604085225 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE kontakty2');
        $this->addSql('DROP TABLE kontrahenci2');
        $this->addSql('DROP TABLE newsletter_audit');
        $this->addSql('DROP TABLE subscription_audit');
        $this->addSql('DROP TABLE user_bak');
        $this->addSql('DROP TABLE zawartosc_str');
        $this->addSql('ALTER TABLE user_user_group DROP FOREIGN KEY FK_28657971A76ED395');
        $this->addSql('ALTER TABLE user_user_group DROP FOREIGN KEY FK_28657971FE54D947');
        $this->addSql('ALTER TABLE user_user_group ADD CONSTRAINT FK_28657971A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_user_group ADD CONSTRAINT FK_28657971FE54D947 FOREIGN KEY (group_id) REFERENCES user_group (id) ON DELETE CASCADE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE kontakty2 (kkid BIGINT AUTO_INCREMENT NOT NULL, kid BIGINT DEFAULT \'0\' NOT NULL, rk INT DEFAULT 0 NOT NULL, kontakt CHAR(60) DEFAULT \'0\' NOT NULL, do CHAR(100) DEFAULT \'\' NOT NULL, idu INT DEFAULT 0 NOT NULL, blokada TINYINT(1) DEFAULT \'0\' NOT NULL, INDEX rk_key (rk), PRIMARY KEY(kkid)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE kontrahenci2 (kid BIGINT AUTO_INCREMENT NOT NULL, kido BIGINT DEFAULT \'0\' NOT NULL, kidstatus TINYINT(1) DEFAULT \'0\' NOT NULL, nazwa CHAR(40) DEFAULT \'\' NOT NULL, ulica CHAR(65) DEFAULT \'\' NOT NULL, kod CHAR(6) DEFAULT \'\' NOT NULL, miasto CHAR(65) DEFAULT \'\' NOT NULL, wojewodztwo INT DEFAULT 0 NOT NULL, kraj CHAR(3) DEFAULT \'\' NOT NULL, nip CHAR(13) DEFAULT \'\' NOT NULL, regon CHAR(9) DEFAULT \'\' NOT NULL, krs CHAR(30) DEFAULT \'\' NOT NULL, data_rej_firmy CHAR(8) DEFAULT \'\' NOT NULL, forma_prawna TINYINT(1) DEFAULT \'0\' NOT NULL, data_rej_kontrahenta CHAR(8) DEFAULT \'\' NOT NULL, www CHAR(30) DEFAULT \'\' NOT NULL, bank CHAR(250) DEFAULT \'\' NOT NULL, nr_konta_b CHAR(32) DEFAULT \'0\' NOT NULL, nr_umowy CHAR(15) DEFAULT \'\' NOT NULL, def_dostawy TINYINT(1) DEFAULT \'0\' NOT NULL, status TINYINT(1) DEFAULT \'2\' NOT NULL, kontrolka CHAR(20) DEFAULT \'\' NOT NULL, PRIMARY KEY(kid)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE newsletter_audit (id INT NOT NULL, rev INT NOT NULL, tytul VARCHAR(255) DEFAULT NULL, tresc LONGTEXT DEFAULT NULL, tresc_txt LONGTEXT DEFAULT NULL, data_wysylki DATETIME DEFAULT NULL, status INT DEFAULT NULL, count_to_send INT DEFAULT NULL, count_sent INT DEFAULT NULL, created DATETIME DEFAULT NULL, updated DATETIME DEFAULT NULL, revtype VARCHAR(4) NOT NULL, tylko_z_kontem TINYINT(1) DEFAULT NULL, lista_reczna LONGTEXT DEFAULT NULL, oferta_swiatlowody TINYINT(1) DEFAULT NULL, oferta_akcesoria_swiatlowodowe TINYINT(1) DEFAULT NULL, oferta_mikrokanalizacja TINYINT(1) DEFAULT NULL, oferta_kable_napowietrzne_airtrack TINYINT(1) DEFAULT NULL, oferta_technologia_fttx TINYINT(1) DEFAULT NULL, oferta_technologia_gpon TINYINT(1) DEFAULT NULL, oferta_urzadzenia_aktywne TINYINT(1) DEFAULT NULL, oferta_sieci_miejskie TINYINT(1) DEFAULT NULL, oferta_osprzet_instalacyjny TINYINT(1) DEFAULT NULL, oferta_osprzet_pomiarowy TINYINT(1) DEFAULT NULL, oferta_okablowanie_strukturalne TINYINT(1) DEFAULT NULL, PRIMARY KEY(id, rev)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE subscription_audit (id INT NOT NULL, rev INT NOT NULL, email VARCHAR(255) DEFAULT NULL, confirmationToken VARCHAR(255) DEFAULT NULL, status INT DEFAULT NULL, created DATETIME DEFAULT NULL, updated DATETIME DEFAULT NULL, revtype VARCHAR(4) NOT NULL, PRIMARY KEY(id, rev)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_bak (id INT AUTO_INCREMENT NOT NULL, username VARCHAR(255) NOT NULL, username_canonical VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, email_canonical VARCHAR(255) NOT NULL, enabled TINYINT(1) NOT NULL, salt VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, last_login DATETIME DEFAULT NULL, locked TINYINT(1) NOT NULL, expired TINYINT(1) NOT NULL, expires_at DATETIME DEFAULT NULL, confirmation_token VARCHAR(255) DEFAULT NULL, password_requested_at DATETIME DEFAULT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', credentials_expired TINYINT(1) NOT NULL, credentials_expire_at DATETIME DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, date_of_birth DATETIME DEFAULT NULL, firstname VARCHAR(64) DEFAULT NULL, lastname VARCHAR(64) DEFAULT NULL, website VARCHAR(64) DEFAULT NULL, biography VARCHAR(255) DEFAULT NULL, gender VARCHAR(1) DEFAULT NULL, locale VARCHAR(8) DEFAULT NULL, timezone VARCHAR(64) DEFAULT NULL, phone VARCHAR(64) DEFAULT NULL, facebook_uid VARCHAR(255) DEFAULT NULL, facebook_name VARCHAR(255) DEFAULT NULL, facebook_data LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json)\', twitter_uid VARCHAR(255) DEFAULT NULL, twitter_name VARCHAR(255) DEFAULT NULL, twitter_data LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json)\', gplus_uid VARCHAR(255) DEFAULT NULL, gplus_name VARCHAR(255) DEFAULT NULL, gplus_data LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json)\', token VARCHAR(255) DEFAULT NULL, two_step_code VARCHAR(255) DEFAULT NULL, firma VARCHAR(255) DEFAULT NULL, ulica VARCHAR(255) DEFAULT NULL, kod_pocztowy VARCHAR(255) DEFAULT NULL, miasto VARCHAR(255) DEFAULT NULL, wojewodztwo VARCHAR(255) DEFAULT NULL, nip VARCHAR(255) DEFAULT NULL, telefon VARCHAR(255) DEFAULT NULL, fax VARCHAR(255) DEFAULT NULL, firma_integrator TINYINT(1) DEFAULT NULL, firma_instalator TINYINT(1) DEFAULT NULL, firma_handlowa TINYINT(1) DEFAULT NULL, firma_projektant TINYINT(1) DEFAULT NULL, firma_tv_kablowa TINYINT(1) DEFAULT NULL, firma_isp TINYINT(1) DEFAULT NULL, firma_inny VARCHAR(255) DEFAULT NULL, oferta_elektroenergetyka TINYINT(1) DEFAULT NULL, oferta_wireless TINYINT(1) DEFAULT NULL, oferta_urzadzenia_aktywne TINYINT(1) DEFAULT NULL, oferta_swiatlowody TINYINT(1) DEFAULT NULL, oferta_sieci TINYINT(1) DEFAULT NULL, oferta_szafy TINYINT(1) DEFAULT NULL, oferta_miejskie_sieci TINYINT(1) DEFAULT NULL, info_staly_klient TINYINT(1) DEFAULT NULL, info_wyszukiwarka TINYINT(1) DEFAULT NULL, info_reklamy TINYINT(1) DEFAULT NULL, info_prasa_codzienna TINYINT(1) DEFAULT NULL, info_prasa_specjalistyczna TINYINT(1) DEFAULT NULL, info_znajomi TINYINT(1) DEFAULT NULL, info_subskrypcja TINYINT(1) DEFAULT NULL, password_reset_token VARCHAR(1024) DEFAULT NULL, UNIQUE INDEX UNIQ_8D93D64992FC23A8 (username_canonical), UNIQUE INDEX UNIQ_8D93D649A0D96FBF (email_canonical), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE zawartosc_str (id INT UNSIGNED AUTO_INCREMENT NOT NULL, referencja VARCHAR(64) DEFAULT NULL, id_rodzic INT UNSIGNED DEFAULT 0 NOT NULL, id_rodzic2 INT UNSIGNED DEFAULT 0 NOT NULL, nazwa VARCHAR(255) DEFAULT \'produkt\' NOT NULL, opis_krotki VARCHAR(255) DEFAULT NULL, opis_pelny LONGTEXT DEFAULT NULL, tech_data LONGTEXT NOT NULL, zastosowania LONGTEXT NOT NULL, str_tytul VARCHAR(255) DEFAULT NULL, str_opis VARCHAR(255) DEFAULT NULL, str_slowa VARCHAR(250) DEFAULT \'\' NOT NULL, rodzaj_zawartosci TINYINT(1) DEFAULT \'1\' NOT NULL, add_ekspozycja TINYINT(1) DEFAULT \'0\' NOT NULL, lang CHAR(3) DEFAULT \'pl\' NOT NULL, id_wzorca_lang INT DEFAULT 0 NOT NULL, ranga_w_grupie DOUBLE PRECISION DEFAULT \'1\' NOT NULL, widocznosc TINYINT(1) DEFAULT \'1\' NOT NULL, w_sprzedazy TINYINT(1) DEFAULT \'1\' NOT NULL, cena NUMERIC(10, 2) DEFAULT NULL, vat INT DEFAULT 0 NOT NULL, jm VARCHAR(255) DEFAULT \'szt.\' NOT NULL, waga NUMERIC(10, 2) DEFAULT \'0.01\' NOT NULL, marka VARCHAR(64) DEFAULT \'0\' NOT NULL, img VARCHAR(255) DEFAULT NULL, tnimg VARCHAR(255) DEFAULT NULL, ogladalnosc INT DEFAULT 0 NOT NULL, active_of_pr TINYINT(1) DEFAULT \'0\' NOT NULL, r_show_subkat TINYINT(1) DEFAULT \'0\' NOT NULL, utworzony VARCHAR(30) DEFAULT \'\' NOT NULL, aktualizacja VARCHAR(30) DEFAULT \'\' NOT NULL, idu_aktualiz INT DEFAULT 0 NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE user_user_group DROP FOREIGN KEY FK_28657971A76ED395');
        $this->addSql('ALTER TABLE user_user_group DROP FOREIGN KEY FK_28657971FE54D947');
        $this->addSql('ALTER TABLE user_user_group ADD CONSTRAINT FK_28657971A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE user_user_group ADD CONSTRAINT FK_28657971FE54D947 FOREIGN KEY (group_id) REFERENCES user_group (id)');
    }
}
