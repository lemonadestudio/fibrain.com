<?php

namespace Cms\ElmatBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('cms_elmat');

        $rootNode
            ->addDefaultsIfNotSet()
            ->children()
            
            	 ->arrayNode('domains')
            	 	->prototype('array')
            	 		->children()
            	 			->scalarNode('domain')->end()
            	 			->scalarNode('symbol')->end()
            	 			->scalarNode('description')->end()
            	 			->variableNode('languages')->defaultValue(array())->end()
            				->booleanNode('is_main')->defaultValue(false)->end()	 			
            	 		->end()
            	 	->end()
            	 ->end()
            	 
            	 ->arrayNode('menu')
	             	->children()
	             		->arrayNode('modules')
	             			->prototype('array')
	             				->children()
		             				->scalarNode('label')->end()
		             				->scalarNode('route')->end()
		             				
		             				->variableNode('route_parameters')->defaultValue(array())->end()
		             				->scalarNode('get_elements_service')->defaultNull()->end()
	             				->end()
	             			->end()
	             		->end()
	             		->arrayNode('locations')
	             			->useAttributeAsKey('type')
	             			->prototype('array')
	             				->children()
	             					->scalarNode('label')->end()
	             					->scalarNode('cache')->defaultValue(0)->end()
	             				->end()
	             			->end()
	             		->end()
	             	->end()
	             ->end()
	             
	         ->end()
	     ;  
        // Here you should define the parameters that are allowed to
        // configure your bundle. See the documentation linked above for
        // more information on that topic.

        return $treeBuilder;

    }
}
