<?php

namespace Cms\ElmatBundle\Repository;

use Doctrine\ORM\EntityRepository;


class ProductRepository extends EntityRepository {

    public function loadMainPageBox($lang = "pl", $offer_group_slug = "") {

        $qb = $this->getEntityManager()->createQueryBuilder()
            ->select('a')
            ->from('CmsElmatBundle:Product', 'a')
            ->leftJoin('a.category', 'c')
            ->leftJoin('c.offer_group', 'og')
            ->andWhere('a.published = 1')
            ->andWhere('a.publishDate <= :date')
            ->andWhere('a.box_produkty_subpage = true')
            ->andWhere('a.image is not null')
            ->orderBy('a.publishDate', 'DESC')
            ->setMaxResults(6)
            ->setParameter(':date', new \DateTime());

        if ($offer_group_slug == 'live') {
            // chcieliby, aby były niewidoczne całkowicie dopóki dana strona nie będzie strona gotowa
            $qb
                ->andWhere('og.slug NOT IN (:slugs)')
                ->setParameter('slugs', array('fiber-optic-cables', 'fttx-systems-and-elements'));

        } elseif ($offer_group_slug) {
            $qb
                ->andWhere('og.slug = :slug')
                ->setParameter('slug', $offer_group_slug);

        } else {

            $qb
                ->andWhere('og.slug IS NULL');

        }


        return $qb->getQuery()->execute();

    }

}