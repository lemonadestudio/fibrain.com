<?php
namespace Cms\ElmatBundle\Repository;

use Doctrine\ORM\Query\ResultSetMapping;

use Doctrine\ORM\EntityRepository;

class NewsRepository extends EntityRepository
{
    public function loadArticle($slug, $lang="pl")
    {
        return $this->getEntityManager()
            ->createQuery("SELECT n FROM CmsElmatBundle:News n
                            where n.slug= :slug
                            and n.lang= :lang
                            and n.published=1
                            ORDER BY n.publishDate DESC")
            ->setParameter('slug', $slug)
            ->setParameter('lang', $lang)
            ->getSingleResult();
    }

    public function loadLatest($limit=10, $lang="pl", $offer_group_slug="", $filters = array())
    {
        $qb =  $this->getEntityManager()->createQueryBuilder()
            ->select('n')
            ->from('CmsElmatBundle:News', 'n')
            ->leftJoin('n.offer_group', 'og')
            ->leftJoin('n.category', 'c')
            ->where('n.lang = :lang')
            ->andWhere('n.published = 1')
            ->andWhere('n.publishDate <= :date')
            ->orderBy('n.publishDate', 'DESC')
            ->setParameters(array(
                    ':date' => new \DateTime('now')
                ))

//             ->createQuery("SELECT n FROM CmsElmatBundle:News n

//                             where n.lang= :lang
//                             and n.published='1'
//                             ORDER BY n.publishDate DESC")

            ->setMaxResults($limit)
            ->setParameter('lang', $lang)

        ;

        if($offer_group_slug) {
            $qb
                ->andWhere('og.slug = :slug')
                ->setParameter('slug', $offer_group_slug)
            ;

        } elseif( isset($filters['offer_groups']) && $filters['offer_groups'] == 'all') {
            
            $qb
                ->andWhere('og.slug IS NOT NULL')
            ;
            
        } else {

            $qb
                ->andWhere('og.slug IS NULL')
            ;
            
            if(is_array($filters['category']) && count($filters['category']) > 0) {
				$qb
					->andWhere('c.slug in (:categories)')
					->setParameter('categories', $filters['category'])
				;
			}

        }
        


        return $qb -> getQuery() -> execute();
    }

    public function loadByIDs($ids) {

    	if(!is_array($ids) || count($ids) == 0) {
    		$ids = array(0);
    	}

		 return $this->getEntityManager()
            ->createQuery("SELECT n
            			   FROM CmsElmatBundle:News n
            				WHERE n.id in (:ids)
                            ORDER BY n.publishDate DESC")
            ->setParameter(':ids', $ids)
            ->getResult();
    }

    public function loadMainPageHighlightsBox($type, $lang="", $filters = array())
    {
        
        $where_filters = '';
        $limit = 10;
        
        if(isset($filters['tagged_promocja']) && $filters['tagged_promocja'] == true) {
            $where_filters = ' and a.promocja = true ';
        } else {
            $where_filters = ' and a.highlights_box_mainpage = true ';
        }
        
        if(isset($filters['categories']) && is_array($filters['categories'])) {
            $where_filters .= ' and c.slug in (:categories) ';
        }
        
        if(isset($filters['only_offer_groups'] ) && $filters['only_offer_groups'] == true ) {
            $where_filters .= ' and og.id IS NOT NULL ';
        }
        
        
        if(isset($filters['limit']) && !empty($filters['limit'])) {
            $limit .= $filters['limit'];
        }
        
        
      
        
        $query = $this->getEntityManager()
        ->createQuery("SELECT a FROM CmsElmatBundle:News a
                            LEFT JOIN a.category c
                            LEFT JOIN a.offer_group og
                            where 	a.lang= :lang
	                            and a.published='1'
								and a.publishDate <= :date

	                           
								and a.image is not null
                                
                                $where_filters  

                            ORDER BY a.publishDate DESC")
                                ->setMaxResults($limit)
                                ->setParameter(':date', new \DateTime())
                                ->setParameter(':lang', $lang)
                                // ->setParameter(':type', $type)
                
       ;
        
        
        if(isset($filters['categories']) && is_array($filters['categories'])) {
            $query->setParameter('categories', $filters['categories']);
        }
        
        return $query ->getResult();
    }




    /**
     * pobiera ostatnie newsy z każdej kategorii
     *
     * @param string $lang
     * @param number $limit
     * @param array $category_ids IDki kategorii lub IDki grup ofertowych
     * @param array $exclude_ids
     * @param boolean $z_grup_ofertowych Jeżeli true - pobiera ostatnie newsy z grup ofertowych| false - z kategorii
     * @return array
     */
    public function loadLatestEachCategory(
            $lang="pl", $limit = 7, $category_ids = array(),
            $exclude_ids = array(), $z_grup_ofertowych = false) {

    	// todo: rozważyć mapowanie bezpośrednio na obiekty
    	$rsm = new ResultSetMapping();
    	$rsm->addScalarResult('id', 'id', 'integer');

    	$date = new \DateTime('now');
    	$date_format = $date->format('Y-m-d H:i:s');

    	if(!is_array($exclude_ids) || count($exclude_ids) == 0) {
    		$exclude_ids = array(0);
    	}

    	if(empty($category_ids)) {
    		return array();
    	}

    	$sql_arr = array();

    	foreach($category_ids as $cid) {
    		$sql_arr[] = "SELECT n.id
				FROM news n
				WHERE n.published = true
	    			AND n.publishDate <= '".$date_format."'
	    			". (
	    				$z_grup_ofertowych ?
	    					"
	    					AND n.offer_group_id = ".intval($cid)."
	    					"
	    					:
	    					"
	    					AND n.category_id = ".intval($cid)."
	    					AND n.offer_group_id IS NULL
    					")."
	    			AND n.id NOT IN (".implode(', ', $exclude_ids).")
                    AND n.deleted_at IS NULL
        		ORDER BY n.publishDate DESC
				LIMIT ".intval($limit)."
				";
    	}

    	if(!count($sql_arr)) {
    		return array();
    	}

    	$sql = '( ';
    	$sql .= implode(') UNION ALL ( ', $sql_arr);

    	$sql .= ' )';


		$ids_arr = $this->getEntityManager()
			->createNativeQuery($sql, $rsm)
			->execute();

		$ids = array();
		if(is_array($ids_arr)) {
			foreach($ids_arr as $ida) {
				$ids[] = $ida['id'];
			}
		}

		return $ids;

    }


    /**
     * pobiera IDki newsów do boxa highlights na stronie kategorii
     * lub grupy ofertowej
     * @param string $lang
     * @param $category_id
     * @param $z_grup_ofertowych
     */
    public function loadHighlightsCategoryIDs($lang = 'pl', $category_id, $z_grup_ofertowych = false) {


    	$rsm = new ResultSetMapping();
    	$rsm->addScalarResult('id', 'id', 'integer');

    	$date = new \DateTime('now');
    	$date_format = $date->format('Y-m-d H:i:s');

    	$ids_arr = $this->getEntityManager()->createNativeQuery("
				 SELECT n.id
				   FROM news n
				  WHERE n.published = true
    			    AND n.publishDate <= '".$date_format."'
				    AND n.highlight_category = true
    				AND n.lang = :lang
    			 ".($z_grup_ofertowych ?
    			  	"AND n.offer_group_id = :category_id "
    			  	:
    			  	"AND n.category_id = :category_id "
    			 	)."
    			LIMIT 1

    	", $rsm)
    	    	->setParameter(':lang', $lang)
    	    	->setParameter(':category_id', intval($category_id))
    	    	->execute();

    	$ids = array();
    	if(is_array($ids_arr)) {
    		foreach($ids_arr as $ida) {
    			$ids[] = $ida['id'];
    		}
    	}

    	return $ids;


    }

    /**
    * pobiera IDki newsów do boxa highlights na stronie głównej
    *
    * 1 news z każdej grupy ofertowej
    *
    * @param $lang
    * @param $tylko_z_ustawiana_grupa jeżeli true - wykluczy 1 news nie należący do żadnej grupy ofertowej
    *
    */
    public function loadHighlightsStronaGlownaIDs($lang = 'pl', $tylko_z_ustawiana_grupa = false) {


    	$rsm = new ResultSetMapping();
    	$rsm->addScalarResult('id', 'id', 'integer');

    	$date = new \DateTime('now');
    	$date_format = $date->format('Y-m-d H:i:s');

    	// id'ki newsów do boxu highlight na stronie głównej
    	// po jednym z każdej grupy ofertowej
    	$ids_arr = $this->getEntityManager()->createNativeQuery("
				SELECT n.id FROM (
				        SELECT MAX( publishDate ) as max_publish_date,
				      	CASE WHEN offer_group_id IS NULL THEN 0 ELSE offer_group_id END as og_id
				        FROM news n
				        WHERE 	n.published = true
    						AND n.publishDate <= '".$date_format."'
				            AND n.highlight_mainpage = true
    						AND n.lang = :lang
    						".($tylko_z_ustawiana_grupa ?
    								"AND n.offer_group_id > 0"
    								:
    								""
    						)."
				        GROUP BY offer_group_id
				) l
				LEFT JOIN news n
				    ON
				        (CASE WHEN n.offer_group_id IS NULL THEN 0 ELSE n.offer_group_id END) = l.og_id
				    AND n.publishDate = l.max_publish_date
                    
               --  // chcieliby, aby były niewidoczne całkowicie dopóki dana strona nie będzie strona gotowa
                WHERE n.offer_group_id NOT IN (2,3,6)
                
				GROUP BY n.offer_group_id
    	", $rsm)
    	    	->setParameter(':lang', $lang)
    	    	->execute();


    	$ids = array();
    	if(is_array($ids_arr)) {
    		foreach($ids_arr as $ida) {
    			$ids[] = $ida['id'];
    		}
    	}

    	return $ids;

    }




}
