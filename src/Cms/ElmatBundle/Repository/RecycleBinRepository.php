<?php

namespace Cms\ElmatBundle\Repository;

use Doctrine\ORM\EntityRepository;

class RecycleBinRepository extends EntityRepository {
    public function loadMainPageBox($lang = "pl", $offer_group_slug = "") {
        $qb = $this->getEntityManager()->createQueryBuilder()
            ->select('a')
            ->from('CmsElmatBundle:RecycleBin', 'a')
            ->setMaxResults(6);

        return $qb->getQuery()->execute();
    }
}