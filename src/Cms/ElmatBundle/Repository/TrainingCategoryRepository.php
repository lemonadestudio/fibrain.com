<?php
namespace Cms\ElmatBundle\Repository;

use Gedmo\Tree\Entity\Repository\NestedTreeRepository;

class TrainingCategoryRepository extends NestedTreeRepository
{
    public function loadArticle($slug, $lang="pl")
    {
        return $this->getEntityManager()
            ->createQuery("SELECT n FROM CmsElmatBundle:TrainingCategory n
                            where n.slug='".$slug."'
                            ")
            ->getSingleResult();
    }


}