<?php

namespace Cms\ElmatBundle\Controller;

use Cms\ElmatBundle\Entity\ContactFormMessage;
use Cms\ElmatBundle\Entity\Subscription;
use Cms\ElmatBundle\Form\Type\ContactZapytanieType;
use Cms\ElmatBundle\Helper\Cms;
use Cms\ElmatBundle\Repository\NewsRepository;
use Cms\ElmatBundle\Repository\UserGroupRepository;
use Cms\ElmatBundle\Repository\UserRepository;
use FOS\UserBundle\Doctrine\UserManager;
use Gedmo\Sluggable\Util\Urlizer;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Locale\Locale;

class DefaultController extends Controller {

    public function changelangAction() {
        return $this->redirect($this->generateUrl('cms_elmat_default_homepage'));
    }

    public function nolangAction() {
        return $this->redirect($this->generateUrl('cms_elmat_default_homepage'));
    }

    public function homepageAction() {
        if ($this->getRequest()->get('_domain_symbol_slug')) {
            $news_limit = 15;
        } else {
            $news_limit = 4;
        }

        $locale = $this->get('request')->getLocale();
        $em = $this->getDoctrine()->getManager();
        $news_prod = array();
        $highlights_news_realizacje = array();
        $categories = array();
        $categories_arr = array();
        $offer_groups = array();
        $offer_groups_arr = array();
        $resources = array();
        // $parent = $repo->findOneByRoute("CmsElmatBundle_aktualnosci");
        $og_symbol = $this->getRequest()->get('_domain_symbol_slug');

        if ($og_symbol) {
            // dane na podstrony produktowe
            $news = $this->getDoctrine()
                ->getRepository('CmsElmatBundle:News')
                ->loadLatest($news_limit, $this->get('request')->getLocale(), $this->getRequest()->get('_domain_symbol_slug'), array('category' => array('szkolenia', 'events', 'wydarzenia'))
                );

            $rozwiazania = array();
            $uslugi = array();
            $articles_highlights_box = array();
            $newses_highlights_box = array();
            $newses_highlights_nowosci = array();

            $box_systemy = $this->getDoctrine()->getRepository('CmsElmatBundle:Page')
                ->loadMainPageBox('systemy', $this->get('request')->getLocale(), $this->getRequest()->get('_domain_symbol_slug'));
            $box_szkolenia = $this->getDoctrine()->getRepository('CmsElmatBundle:Page')
                ->loadMainPageBox('szkolenia', $this->get('request')->getLocale(), $this->getRequest()->get('_domain_symbol_slug'));

//            $box_najnowsze_produkty = $this->getDoctrine()->getRepository('CmsElmatBundle:Product')
//                    ->loadMainPageBox( $this->get('request')->getLocale(), $this->getRequest()->get('_domain_symbol_slug'));

            $artykuly_techniczne = $this->getDoctrine()->getRepository('CmsElmatBundle:Zasob')->createQueryBuilder('z')
                ->leftJoin('z.category', 'c')
                ->leftJoin('c.offer_group', 'og')
                ->where('og.slug = :slug')
                ->andWhere('z.highlight_mainpage = true')
                ->addOrderBy('z.publishDate', 'desc')
                ->addOrderBy('z.created', 'desc')
                ->setMaxResults(8)
                ->setParameters(array(
                    'slug' => $this->getRequest()->get('_domain_symbol_slug')
                ))
                ->getQuery()->execute();

            // $this->getRequest()->get('_domain_symbol_slug')
//            $artykuly_techniczne  = $this->getDoctrine()->getRepository('CmsElmatBundle:Article')
//            ->loadMainPage('artykuly-techniczne', $this->get('request')->getLocale(), $this->getRequest()->get('_domain_symbol_slug'));
        } else {
            $categories = $this->getCategories($locale);
            $offer_groups = $this->getOfferGroups($locale);
            // dane na główną stronę
            $rozwiazania = $this->getDoctrine()->getRepository('CmsElmatBundle:Article')
                ->loadMainPage('rozwiazania', $this->get('request')->getLocale());

            $uslugi = $this->getDoctrine()->getRepository('CmsElmatBundle:Article')
                ->loadMainPage('uslugi', $this->get('request')->getLocale());

//            $articles_highlights_box = $this->getDoctrine()->getRepository('CmsElmatBundle:Article')
//            ->loadMainPageHighlightsBox('', $this->get('request')->getLocale());

            $articles_highlights_box = array();
            $newses_highlights_box = $this->getDoctrine()->getRepository('CmsElmatBundle:News')
                ->loadMainPageHighlightsBox('', $this->get('request')->getLocale(), array(
                    'limit' => 5,
                    //'only_offer_groups' => true,
                    //                'tagged_promocja' => true,
                    // 'categories_exclude' => array('realizacje')
                ));

            $newses_highlights_nowosci = $this->getDoctrine()->getRepository('CmsElmatBundle:News')
                ->loadMainPageHighlightsBox('', $this->get('request')->getLocale(), array(
                    'limit' => 5,
                    //                'only_offer_groups' => true,
                    'tagged_promocja' => true,
                    // 'categories_exclude' => array('realizacje')
                ));

            $highlights_news_realizacje = $this->getDoctrine()->getRepository('CmsElmatBundle:News')
                ->loadMainPageHighlightsBox('', $this->get('request')->getLocale(), array('limit' => 3, 'categories' => array('realizacje')));

            $resources = $this->getDoctrine()
                ->getRepository('CmsElmatBundle:Zasob')
                ->createQueryBuilder('z')
                ->leftJoin('z.category', 'c')
                ->leftJoin('c.offer_group', 'og')
                ->where('z.highlight_mainpage = true')
                ->addOrderBy('z.publishDate', 'desc')
                ->addOrderBy('z.created', 'desc')
                ->setMaxResults(6)
                ->getQuery()
                ->execute();

//            $news_prod = $this->getDoctrine()
//                ->getRepository('CmsElmatBundle:News')
//                ->loadLatest(7,
//                        $this->get('request')->getLocale(), '',
//                        array('offer_groups' => 'all')
//            );

            $box_systemy = array();
            $box_szkolenia = array();
            //$box_najnowsze_produkty = array();
            $artykuly_techniczne = array();

            $categories_ids = array();
            $categories_arr = array();
            foreach ($categories as $c) {
                if (in_array($c->getSlug(), array('wydarzenia', 'szkolenia', 'events',))) {
                    $categories_ids[] = $c->getId();
                    $categories_arr[] = array('id' => $c->getId(), 'slug' => $c->getSlug(), 'title' => $c->getTitle());
                }
            }

            $offer_group_ids = array();
            $offer_groups_arr = array();
            foreach ($offer_groups as $og) {

                $offer_group_ids[] = $og->getId();
                $offer_groups_arr[] = array('id' => $og->getId(), 'slug' => $og->getSlug(), 'title' => $og->getTitle(), 'count' => 0);
            }


            // ostatnie newsy z każdej kategorii z pominięciem newsów w highlights
            $latest_news_ids = $this->getNewsRepository()
                ->loadLatestEachCategory('pl', $news_limit, $categories_ids);

            $latest_news_offer_group_ids = $this->getNewsRepository()
                ->loadLatestEachCategory('pl', $news_limit, $offer_group_ids, array(), true);

            $news = $this->getNewsRepository()->loadByIDs($latest_news_ids);

            $news_prod = $this->getNewsRepository()->loadByIDs($latest_news_offer_group_ids);

            $offer_groups_arr_ok = array();
            foreach ($offer_groups_arr as $o => $oga) {
                foreach ($news_prod as $np) {
                    if ($np->getOfferGroup()->getId() == $oga['id']) {
                        $oga['count']++;
                    }
                }
                $offer_groups_arr[$o] = $oga;

                if ($oga['count']) {
                    $offer_groups_arr_ok[$o] = $oga;
                }
            }
            $offer_groups_arr = $offer_groups_arr_ok;
        }


        $idx_a = 0;
        $idx_n = 0;
        $count_a_n = 0;

        $max_a = count($articles_highlights_box);
        $max_n = count($newses_highlights_box);

        $highlights_news_articles = array();


        while (($idx_a < $max_a || $idx_n < $max_n) && $count_a_n < 3) {
            $curr_a = null;
            $curr_n = null;

            if ($idx_a < $max_a) {
                $curr_a = $articles_highlights_box[$idx_a];
            }

            if ($idx_n < $max_n) {
                $curr_n = $newses_highlights_box[$idx_n];
            }

            if ($curr_a && !$curr_n) {

                $highlights_news_articles[] = array('type' => 'article', 'obj' => $curr_a);
                $idx_a++;
                $count_a_n++;
            } elseif (!$curr_a && $curr_n) {

                $highlights_news_articles[] = array('type' => 'news', 'obj' => $curr_n);
                $idx_n++;
                $count_a_n++;
            } else { // ($curr_a && $curr_n)

                if ($curr_a->getPublishDate() >= $curr_n->getPublishDate()) {

                    $highlights_news_articles[] = array('type' => 'article', 'obj' => $curr_a);
                    $idx_a++;
                    $count_a_n++;
                } else {

                    $highlights_news_articles[] = array('type' => 'news', 'obj' => $curr_n);
                    $idx_n++;
                    $count_a_n++;
                }
            }
        }


        return $this->render('CmsElmatBundle:Default:homepage.html.twig', array(
            'categories_arr' => $categories_arr,
            'categories' => $categories,
            'offer_groups_arr' => $offer_groups_arr,
            'offer_groups' => $offer_groups,
            'news' => $news,
            'news_prod' => $news_prod,
            'rozwiazania' => $rozwiazania,
            'uslugi' => $uslugi,
            'artykuly_techniczne' => $artykuly_techniczne,
            'highlights_news_articles' => $highlights_news_articles,
            'news_highlights_nowosci' => $newses_highlights_nowosci,
            'highlights_news_realizacje' => $highlights_news_realizacje,
            'box_systemy' => $box_systemy,
            'box_szkolenia' => $box_szkolenia,
            'og_symbol' => $og_symbol,
            'resources' => $resources
            // 'box_najnowsze_produkty' => $box_najnowsze_produkty,
        ));
    }

    public function boxArtykulyTechniczneAction($is_live_elmat = false) {

        if ($is_live_elmat) {
            $domain_symbol = 'live';
        } else {
            $domain_symbol = $this->getRequest()->get('_domain_symbol_slug');
        }

        $artykuly_techniczne = $this->getDoctrine()->getRepository('CmsElmatBundle:Article')
            ->loadMainPage('artykuly-techniczne', $this->get('request')->getLocale(), $domain_symbol);


        return $this->render('CmsElmatBundle:Default:boxArtykulyTechniczne.html.twig', array(
            'artykuly_techniczne' => $artykuly_techniczne,
        ));
    }

    public function boxNajnowszeProduktyAction($is_live_elmat = false) {

        if ($is_live_elmat) {
            $domain_symbol = 'live';
        } else {
            $domain_symbol = $this->getRequest()->get('_domain_symbol_slug');
        }


        $box_najnowsze_produkty = $this->getDoctrine()->getRepository('CmsElmatBundle:Product')
            ->loadMainPageBox($this->get('request')->getLocale(), $domain_symbol);

        return $this->render('CmsElmatBundle:Default:boxNajnowszeProdukty.html.twig', array(
            'box_najnowsze_produkty' => $box_najnowsze_produkty,
        ));
    }

    /**
     */
    public function browseAction() {
        $_SESSION['KCFINDER'] = array();
        $_SESSION['KCFINDER']['disabled'] = false;
        $_SESSION['fold_type'] = 'pliki';
        $_SESSION['KCFINDER']['uploadURL'] = $this->get('request')
                ->getBasePath() . "/uploads";

        $get_parameters = $this->get('request')->query->all();

        $response = new RedirectResponse(
            $this->get('request')->getBasePath() . '/kcfinder/browse.php?'
            . http_build_query($get_parameters));

        return $response;
    }

    /**
     * @Route "/test12"
     * @return Response
     */
    public function testAction() {


        // Get the country names for a locale or get all country codes
        $countries = Locale::getDisplayCountries('en');
        $countryCodes = Locale::getCountries();

        // 	var_dump($countries, $countryCodes);
        // Get the language names for a locale or get all language codes
        $languages = Locale::getDisplayLanguages('pl');
        $languageCodes = Locale::getLanguages();

        // 	var_dump($languages, $languageCodes);
        // Get the locale names for a given code or get all locale codes
        $locales = Locale::getDisplayLocales('chr_US');
        $localeCodes = Locale::getLocales();

        var_dump($locales, $localeCodes);


        return new Response('<html>

				<body>
					<h1>Finder test ...</h1>
				</body>


				</html>');
    }

    public function main_sliderAction() {

        $offer_group_slug = $this->getRequest()->get('_domain_symbol_slug');


        $photos_qb = $this->getDoctrine()
            ->getRepository('CmsElmatBundle:SliderPhoto')
            ->createQueryBuilder('o')
            ->leftJoin('o.offer_group', 'og')
            ->orderBy('o.kolejnosc', 'ASC');

        if ($offer_group_slug) {

            $photos_qb
                ->where('og.slug = :slug')
                ->setParameter('slug', $offer_group_slug);
        } else {
            $photos_qb
                ->where('o.offer_group IS NULL');
        }

        $photos = $photos_qb->getQuery()->execute();

        return $this->render('CmsElmatBundle:Default:main_slider.html.twig', array(
            'photos' => $photos
        ));
    }

    public function menu_offer_groupsAction() {
        return $this->render('CmsElmatBundle:Default:menu_offer_groups.html.twig', array(
            'offer_groups' => $this->getOfferGroups('pl'),
        ));
    }

    public function domainsMenuAction() {
        return $this->render('CmsElmatBundle:Default:domainsMenu.html.twig', array(
            'offerGroups' => $this->getOfferGroups('pl'),
        ));
    }

    public function fileinfoAction() {

        $request = $this->getRequest();


        $file = rawurldecode($request->get('path'));

        $dir = $this->container->get('kernel')->getRootdir() . '/../web';

        $fileinfo = array(
            'size' => 0,
            'name' => '',
            'ext' => ''
        );

        try {
            $f = new File($dir . $file);

            $fileinfo['ext'] = mb_strtolower($f->getExtension());
            $fileinfo['path'] = $file;
            $fileinfo['size'] = $f->getSize();
            $fileinfo['name'] = $f->getBasename('.' . $fileinfo['ext']);
        } catch (FileNotFoundException $e) {

        }


        return new Response(json_encode($fileinfo));
    }

    public function pobierzAction($id) {

        $request = $this->getRequest();
        $doctrine = $this->getDoctrine();
        $em = $doctrine->getManager();

        $plik = $doctrine->getRepository('CmsElmatBundle:Plik')->find($id);

        try {


            $path = $plik->getPelnaSciezka();
            $file = new File($path);

            if ($file->isFile() && $file->isReadable()) {

                $response = new BinaryFileResponse($file);


                $nazwa = $plik->getNazwaRoz();
                $nazwa = Urlizer::utf8ToAscii($nazwa, ' ');
                $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, $nazwa);

                return $response;
            } else {
                throw new NotFoundHttpException();
            }
        } catch (FileNotFoundException $e) {
            throw new NotFoundHttpException();
        }
    }

    public function contactAction() {

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Home", $this->get("router")->generate("cms_elmat_default_homepage"));
        $breadcrumbs->addItem('Contact', $this->get('router')->generate('cms_elmat_default_contact'));

        $form = $this->createForm(new ContactZapytanieType($this->container));

        $request = $this->getRequest();
        $message_status = '';
        if ($request->getMethod() == 'POST') {

            $form->bind($request);

            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();

                $session = $this->get('session');
                $session->set('antyspam_kontakt', null);

                $data = $form->getData();
                $message_txt = '<h3>Wiadomość z formularza:</h3>';
                if (isset($data['tresc'])) {
                    $data['tresc'] = strip_tags($data['tresc'], '<br><p><b><strong>');
                    $message_txt .= $data['tresc'] . ' <hr />';
                }

                $message_txt .= '<h3>Pozostałe dane:</h3>';
                if (isset($data['firma'])) {
                    $message_txt .= 'Firma: ' . $data['firma'] . '<br />';
                }
                if (isset($data['imie_nazwisko'])) {
                    $message_txt .= 'Imię i nazwisko: ' . $data['imie_nazwisko'] . '<br />';
                }
                if (isset($data['imie'])) {
                    $message_txt .= 'Imię: ' . $data['imie'] . '<br />';
                }
                if (isset($data['nazwisko'])) {
                    $message_txt .= 'Nazwisko: ' . $data['nazwisko'] . '<br />';
                }
                if (isset($data['email'])) {
                    $message_txt .= 'E-mail: ' . $data['email'] . '<br />';
                }
                if (isset($data['telefon'])) {
                    $message_txt .= 'Telefon: ' . $data['telefon'] . '<br />';
                }
//                if (isset($data['typ'])) {
//                    $message_txt .= 'Kontakt z: ' . $wysylka_dzialy[$data['typ']] . '<br />';
//                }


                $message = new ContactFormMessage();
                $message->setSenderEmail(isset($data['email']) ? $data['email'] : 'brak e-mail');
                $message->setSenderMessage($message_txt);

                $sender_name = isset($data['imie']) ? $data['imie'] : '';
                $sender_name .= isset($data['nazwisko']) ? ' ' . $data['nazwisko'] : '';
                $sender_name .= isset($data['imie_nazwisko']) ? ' ' . $data['imie_nazwisko'] : '';


                $message->setSenderName($sender_name);
                $message->setSenderIP($request->getClientIp());
                $message->setSentAt(new \DateTime());

                $em->persist($message);
                $em->flush();

                $this->get('cms_config')->all();
                $email_to = 'info@fibrain.com';
                //$this->get('cms_config')->get('formularz_wysylka_email');

                $temat = $this->get('cms_config')->get('formularz_wysylka_temat');
                $wysylka_od = $this->get('cms_config')->get('formularz_wysylka_od');

                $message_email = \Swift_Message::newInstance()
                    ->setSubject($temat)
                    ->setFrom($this->container->getParameter('mailer_user'), $wysylka_od)
                    ->setTo($email_to)
                    ->setBody($message_txt, 'text/html')
                    ->addPart(strip_tags($message_txt), 'text/plain');

                if (isset($data['email'])) {
                    $message_email->setReplyTo($data['email']);
                }

                try {
                    $ret = $this->get('mailer')->send($message_email);
                } catch (Exception $e) {

                }

                $message_status = 'sent';
                $this->get('session')
                    ->setFlash('contact_form_sent', 'Wiadomość została wysłana. Dziękujemy za skorzystanie z formularza.');
            }
        }

        return $this->render('CmsElmatBundle:Default:contact.html.twig', array(
            'form' => $form->createView(),
            'message' => $message_status
        ));
    }

    /**
     * import klientów
     */
    public function import_oldAction() {

        $sql = "SELECT kont.kontakt, kont.do, k.* FROM kontrahenci2 k
                left join kontakty2 kont
                   on kont.kid = k.kid
                  and kont.kontakt like '%@%'
                where nazwa <> '' and nazwa is not null
                  and kont.kontakt like '%@%'
                  and kont.kontakt not like '__________@______.com'
                  and kont.kontakt not in ( select email from user )
                group by kid
                limit 1000";


        $stmt = $this->getDoctrine()->getConnection()->prepare($sql);
        $user_repo = $this->getUserRepository();
        $userManager = $this->getFOSUserManager();

        $em = $this->getDoctrine()->getManager();

        $group_klient = $this->getUserGroupRepository()->findOneBy(array('name' => 'Klient'));

        $stmt->execute();
        $klienci = $stmt->fetchAll();
        $i = 0;
        $ia = 0;

        $wojewodztwa = array(
            '0' => '',
            '1' => 'dolnośląskie',
            '2' => 'kujawsko-pomorskie',
            '3' => 'lubelskie',
            '4' => 'lubuskie',
            '5' => 'łódzkie',
            '6' => 'małopolskie',
            '7' => 'mazowieckie',
            '8' => 'opolskie',
            '9' => 'podkarpackie',
            '10' => 'podlaskie',
            '11' => 'pomorskie',
            '12' => 'śląskie',
            '13' => 'świętokrzyskie',
            '14' => 'warmińsko-mazurskie',
            '15' => 'wielkopolskie',
            '16' => 'zachodniopomorskie',
        );


        echo '<pre>';
        foreach ($klienci as $k) {

            $i++;
            $email = trim($k['kontakt']);


            $exists = $user_repo->findBy(array('email' => $email));
            echo $i;

            if ($exists) {
                echo ' Adres ' . $email . ' istnieje. pomijam ' . "\n";
                continue;
            } else {
                echo ' Dodaję klienta ' . $k['kontakt'] . ' ' . "\n";

                $user = $userManager->createUser();


                if ($group_klient) {
                    $user->addGroup($group_klient);
                } else {
                    $user->addRole('ROLE_KLIENT');
                }
                $user->setUserName($k['kontakt']);
                $user->setEnabled(true);
                $user->setEmail($k['kontakt']);
                $user->setFirma($k['nazwa']);
                $user->setUlica($k['ulica']);
                $user->setKodPocztowy($k['kod']);

                $user->setMiasto($k['miasto']);
                $user->setWojewodztwo($wojewodztwa[intval($k['wojewodztwo'])]);
                $user->setNip($k['nip']);

                $user->setPlainPassword(sha1($user->serialize() . time() . time()));
                $user->setConfirmationToken(sha1($user->serialize() . time()));

                $userManager->updateUser($user, true);

                $ia++;
            }

            usleep(1000);

            flush();

            if ($ia > 1000) {
                break;
            }
        }

        echo '</pre>';

        die;
    }

    /**
     * import klientów
     */
    public function importAction() {

        $user_repo = $this->getUserRepository();
        $userManager = $this->getFOSUserManager();
        $em = $this->getDoctrine()->getManager();

        $group_klient = $this->getUserGroupRepository()->findOneBy(array('name' => 'Klient'));

        $start_file = __DIR__ . '/../Resources/data/start.txt';

        $klienci_csv = file_get_contents(__DIR__ . '/../Resources/data/wszystkie_workin.csv');
        $klienci = array();
        $klienci_arr = array();
        foreach (explode(PHP_EOL, $klienci_csv) as $line) {
            $klienci_arr[] = str_getcsv($line, ';', '"', '"');
        }

        $start = intval(file_get_contents($start_file));

        $limit_once = 500;

        if ($start <= 0) {
            $start = 1;
        }

        echo 'Starting from ' . $start;

        for ($i = $start; $i < count($klienci_arr) && $i < $start + $limit_once && isset($klienci_arr[$i]); $i++) {


            $k = $klienci_arr[$i];


            $klient = array(
                'lp' => $i,
                'username' => trim($k[0]),
                'email' => trim($k[1]),
                'firma' => trim($k[2]),
                'ulica' => trim($k[3]),
                'kod_pocztowy' => trim($k[4]),
                'miasto' => trim($k[5]),
                'wojewodztwo' => trim($k[6]),
                'nip' => trim($k[8]),
                'telefon' => trim($k[9]),
                'fax' => trim($k[10]),
                'firma_isp' => (boolean)$k[12],
                'firma_tv_kablowa' => (boolean)$k[13],
                'firma_instalator' => (boolean)$k[14],
                'firma_instalator_sieci_niskopradowe' => (boolean)$k[15],
                'firma_samorzad_terytorialny' => (boolean)$k[16],
                'firma_handlowa' => (boolean)$k[17],
                'firma_administrator_sieci' => (boolean)$k[18],
                'firma_projektant' => (boolean)$k[18],
                'firma_integrator' => (boolean)$k[20],
                'firma_inny' => $k[21],
                'oferta_swiatlowody' => (boolean)$k[23],
                'oferta_akcesoria_swiatlowodowe' => (boolean)$k[24],
                'oferta_mikrokanalizacja' => (boolean)$k[25],
                'oferta_kable_napowietrzne_airtrack' => (boolean)$k[26],
                'oferta_technologia_fttx' => (boolean)$k[27],
                'oferta_technologia_gpon' => (boolean)$k[28],
                'oferta_urzadzenia_aktywne' => (boolean)$k[29],
                'oferta_sieci_miejskie' => (boolean)$k[30],
                'oferta_osprzet_instalacyjny' => (boolean)$k[31],
                'oferta_osprzet_pomiarowy' => (boolean)$k[32],
                'oferta_okablowanie_strukturalne' => (boolean)$k[33],
                'oferta_inne' => (boolean)$k[34],
                'info_staly_klient' => (boolean)$k[36],
                'info_wyszukiwarka' => (boolean)$k[37],
                'info_prasa_codzienna' => (boolean)$k[38],
                'info_reklamy' => (boolean)$k[39],
                'info_prasa_specjalistyczna' => (boolean)$k[40],
                'info_znajomi' => (boolean)$k[41],
                'info_subskrypcja' => (boolean)$k[42],
                'info_inne' => (boolean)$k[43],
            );

            $klienci[] = $klient;

            // var_dump($klient);
        }

        file_put_contents($start_file, $i);


        $i = 0;
        $ia = 0;

        echo '<pre>';
        foreach ($klienci as $k) {

            $i++;

            $email = $k['email'];

            $exists = $user_repo->findBy(array('email' => $email));
            echo ($i + $start) . ' ' . $i;

            if ($exists) {
                echo ' Adres ' . $email . ' istnieje. pomijam ' . "\n";
                continue;
            } elseif (!preg_match('/@/', $email)) {
                echo ' Adres ' . $email . ' to nie jest adres E-mail. pomijam ' . "\n";
                continue;
            } else {
                echo ' Dodaję klienta ' . $email . ' ' . "\n";

                $user = $userManager->createUser();

                if ($group_klient) {
                    $user->addGroup($group_klient);
                } else {
                    $user->addRole('ROLE_KLIENT');
                }

                foreach ($k as $f => $v) {
                    $user->set($f, $v);
                }
                $user->setUserName($email);
                $user->setEnabled(true);
                $user->setPlainPassword(sha1($user->serialize() . time() . time()));
                $user->setConfirmationToken(sha1($user->serialize() . time()));

                $userManager->updateUser($user, true);

                $ia++;

                /*
                  if(false ) {




                  $user->setUserName($k['kontakt']);

                  $user->setEmail($k['kontakt']);
                  $user->setFirma($k['nazwa']);
                  $user->setUlica($k['ulica']);
                  $user->setKodPocztowy($k['kod']);

                  $user->setMiasto($k['miasto']);
                  $user->setWojewodztwo($wojewodztwa[intval($k['wojewodztwo'])] );
                  $user->setNip($k['nip']);

                  $user->setPlainPassword(sha1($user->serialize().time().time()));
                  $user->setConfirmationToken(sha1($user->serialize().time()));

                  $userManager->updateUser($user, true);

                  $ia ++;

                  }
                 */
            }

            usleep(1000);

            flush();

            if ($ia > 1000) {
                break;
            }
        }

        echo '</pre>';

        die;
    }

    public function importKontaktyAction() {

        $sql = "SELECT * FROM kontakty2 where "
            . "blokada = 0 "
            . "and kontakt NOT LIKE '__________@______.com' "
            . "and kontakt like '%@%'"
            . "and kontakt not in ( select email from subscription)"
            . "LIMIT 100 ";

        $stmt = $this->getDoctrine()->getConnection()->prepare($sql);
        $sub_repo = $this->getDoctrine()->getRepository('CmsElmatBundle:Subscription');
        $em = $this->getDoctrine()->getManager();


        $stmt->execute();
        $kontakty2 = $stmt->fetchAll();
        $i = 0;
        $ia = 0;
        echo '<pre>';
        foreach ($kontakty2 as $k) {

            $i++;
            $email = trim($k['kontakt']);

            $sub = new Subscription();
            $sub->generateToken();
            $sub->setEmail($email);
            $sub->setStatus(Subscription::STATUS_AKTYWNY);


            $exists = $sub_repo->findBy(array('email' => $email));
            echo $i;

            if ($exists) {
                echo ' Adres ' . $email . ' istnieje. pomijam ' . "\n";
                continue;
            } else {
                echo ' Dodaję adres ' . $email . ' ' . "\n";

                $em->persist($sub);
                $em->flush();
                $ia++;
            }

            usleep(1000);

            flush();

            if ($ia > 10) {
                break;
            }
        }

        echo '</pre>';

        die;
    }

    /**
     * @return UserRepository
     */
    private function getUserRepository() {
        return $this->getDoctrine()->getRepository('CmsElmatBundle:User');
    }

    /**
     * @return UserManager
     */
    public function getFOSUserManager() {
        $userManager = $this->container->get('fos_user.user_manager');
        return $userManager;
    }

    /**
     * @return UserGroupRepository
     */
    private function getUserGroupRepository() {
        return $this->getDoctrine()->getRepository('CmsElmatBundle:UserGroup');
    }

    /**
     * @return NewsRepository
     */
    public function getNewsRepository() {

        return $this->getDoctrine()->getRepository('CmsElmatBundle:News');
    }

    public function editLinkScriptAction($type, $id, $og) {

        $security_context = $this->container->get('security.context');
        $router = $this->get('router');

        $admin_host = $this->container->getParameter('host_elmat');

        $link = false;


        $required_role = 'ROLE_GRUPA_ELMAT';

        if (!empty($og)) {

            foreach (Cms::getOfferGroups() as $offer_group) {

                if ($offer_group['slug'] == $og) {
                    $required_role = $offer_group['role'];
                }
            }
        }

        $admin_route = false;

        if ($security_context->isGranted('ROLE_SUPER_ADMIN') || ($security_context->isGranted('ROLE_ADMIN') && $security_context->isGranted($required_role))
        ) {

            $required_admin_role = null;

            switch ($type) {

                case 'news':
                    $required_admin_role = 'ROLE_CMS_ELMAT_ADMIN_NEWS_EDIT';
                    $admin_route = 'admin_cms_elmat_news_edit';
                    break;

                case 'article':
                    $required_admin_role = 'ROLE_CMS_ELMAT_ADMIN_ARTICLE_EDIT';
                    $admin_route = 'admin_cms_elmat_article_edit';
                    break;

                case 'page':
                    $required_admin_role = 'ROLE_CMS_ELMAT_ADMIN_PAGE_EDIT';
                    $admin_route = 'admin_cms_elmat_page_edit';
                    break;

                case 'product':
                    $required_admin_role = 'ROLE_CMS_ELMAT_ADMIN_PRODUCT_EDIT';
                    $admin_route = 'admin_cms_elmat_product_edit';
                    break;

                case 'productcategory':
                    $required_admin_role = 'ROLE_CMS_ELMAT_ADMIN_PRODUCT_CATEGORY_EDIT';
                    $admin_route = 'admin_cms_elmat_productcategory_edit';
                    break;
            }

            if ($required_admin_role && ($security_context->isGranted('ROLE_SUPER_ADMIN') || $security_context->isGranted($required_admin_role))) {
                $link = 'http://' . $admin_host . $router->generate($admin_route, array('id' => $id));
            }
        }


        $params = array(
            'link' => $link
        );

        $rendered = $this->renderView('CmsElmatBundle:Default:editLinkScript.js.twig', $params);
        $response = new Response($rendered);
        $response->headers->set('Content-Type', 'text/javascript');
        return $response;
    }

    /**
     *
     * @return \Knp\Component\Pager\Paginator
     */
    private function getPaginator() {
        return $this->get('knp_paginator');
    }

    public function szukajAction(Request $request, $fraza = '', $typ = '') {
        $em = $this->getDoctrine()->getManager();
        $router = $this->get('router');
        $cms_helper = new Cms($this->container);

        if ($request->isMethod('POST')) {
            $fraza = $request->request->get('fraza');
            $typ = $request->request->get('typ', '');

            if (!empty($typ) && !preg_match('/prod|news|pages/', $typ)) {
                $typ = '';
            }

            $fraza_url = urlencode($fraza);

            $params = array('fraza' => $fraza_url);

            $route = 'cms_elmat_default_szukaj';
            if (strlen($typ) > 0) {
                $params['typ'] = $typ;
                $route = 'cms_elmat_default_szukaj_typ';
            }

            return $this->redirect($router->generate($route, $params));
        }

        $words = explode(' ', urldecode($fraza));
        $slowa = array();

        foreach ($words as $word) {
            $word = trim($word);
            if (!in_array($word, $slowa)) {
                $slowa[] = $word;
            }
        }
        $fraza_clean = implode(' ', $slowa);

        if (!empty($typ) && !preg_match('/prod|news|pages/', $typ)) {
            return $this->redirect($router->generate('cms_elmat_default_szukaj', array('fraza' => $fraza)));
        }

        if (empty($fraza_clean) && 'cms_elmat_default_szukaj' == $request->get('_route')) {
            return $this->redirect($router->generate('cms_elmat_default_szukaj_0', array()));
        }

        $results = array();

        if (!empty($fraza_clean)) {
            $offer_groups = $cms_helper->getOfferGroups();

            if (empty($typ) || $typ == 'pages') {
                $qb = $em->createQueryBuilder();
                $qb->select('s');
                $qb->from('CmsElmatBundle:Page', 's');
                $qb->leftJoin('s.offer_group', 'og');
                $qb->where('s.publishDate <= :date');
                $qb->andWhere('s.published = true');
                $qb->andWhere($qb->expr()->orX(
                    $qb->expr()->like('LOWER(s.title)', ':search'),
                    $qb->expr()->like('LOWER(s.content)', ':search')
                ));
                $qb->setParameter('date', new \DateTime());
                $qb->setParameter('search', '%' . $fraza_clean . '%');
                $query = $qb->getQuery();
                $entities = $query->getResult();

                foreach ($entities as $entity) {
                    $title = $this->getFragmentForHighlight($entity->getTitle(), $fraza_clean);
                    $content = $this->getFragmentForHighlight($this->prepareContent($entity->getContent()), $fraza_clean);
                    $route = 'cms_elmat_page_show';
                    if ($entity->getOfferGroup()) {
                        $og = $entity->getOfferGroup();
                        foreach ($offer_groups as $g) {
                            if ($g['slug'] == $og->getSlug()) {
                                $route .= '_' . $g['symbol2'];
                                break;
                            }
                        }
                    }

                    $url = $router->generate($route, array('slug' => $entity->getSlug(), 'id' => $entity->getId()), true);

                    $results[] = array(
                        'title' => $title,
                        'content' => $content,
                        'url' => $url
                    );
                }
            }

            if (empty($typ) || $typ == 'news') {
                $qb = $em->createQueryBuilder();
                $qb->select('s');
                $qb->from('CmsElmatBundle:News', 's');
                $qb->where('s.publishDate <= :date');
                $qb->andWhere('s.published = true');
                $qb->andWhere($qb->expr()->orX(
                    $qb->expr()->like('LOWER(s.title)', ':search'),
                    $qb->expr()->like('LOWER(s.content)', ':search')
                ));
                $qb->setParameter('date', new \DateTime());
                $qb->setParameter('search', '%' . $fraza_clean . '%');
                $query = $qb->getQuery();
                $entities = $query->getResult();

                foreach ($entities as $entity) {
                    $title = $this->getFragmentForHighlight($entity->getTitle(), $fraza_clean);
                    $content = $this->getFragmentForHighlight($this->prepareContent($entity->getContent()), $fraza_clean);
                    $url = $router->generate('cms_elmat_live_shownews', array('slug' => $entity->getSlug(), 'id' => $entity->getId()), true);

                    $results[] = array(
                        'title' => $title,
                        'content' => $content,
                        'url' => $url
                    );
                }
            }

            if (empty($typ) || $typ == 'prod') {
                $qb = $em->createQueryBuilder();
                $qb->select('s');
                $qb->from('CmsElmatBundle:Product', 's');
                $qb->leftJoin('s.category', 'c');
                $qb->leftJoin('c.offer_group', 'cog');
                $qb->leftJoin('s.rows', 'r');
                $qb->leftJoin('s.tabs', 't');
                $qb->where('s.publishDate <= :date');
                $qb->andWhere('s.published = true');
                $qb->andWhere($qb->expr()->orX(
                    $qb->expr()->like('LOWER(s.title)', ':search'),
                    $qb->expr()->like('LOWER(s.code)', ':search'),
                    $qb->expr()->like('LOWER(s.content)', ':search'),
                    $qb->expr()->like('LOWER(r.name)', ':search'),
                    $qb->expr()->like('LOWER(r.description)', ':search'),
                    $qb->expr()->like('LOWER(r.value1)', ':search'),
                    $qb->expr()->like('LOWER(t.name)', ':search'),
                    $qb->expr()->like('LOWER(t.description)', ':search')
                ));
                $qb->setParameter('date', new \DateTime());
                $qb->setParameter('search', '%' . $fraza_clean . '%');
                $query = $qb->getQuery();
                $entities = $query->getResult();

                $domeny = $cms_helper->domains();
                $domain_symbol = 'elmat';

                foreach ($entities as $entity) {
                    $title = $this->getFragmentForHighlight($entity->getTitle(), $fraza_clean);
                    $content = $this->getFragmentForHighlight($this->prepareContent($entity->getContent()), $fraza_clean);
                    $category = $entity->getCategory();

                    $product_offer_group = null;
                    if ($category) {
                        $product_offer_group = $category->getOfferGroup();
                    }

                    $product_domain = $domeny['elmat']['domain'];

                    if ($product_offer_group) {
                        $product_offer_group_symbol = '';

                        foreach ($offer_groups as $og) {
                            if ($og['slug'] == $product_offer_group->getSlug()) {
                                $product_offer_group_symbol = $og['symbol'];
                            }
                        }

                        if ($domain_symbol != $product_offer_group_symbol) {
                            foreach ($domeny as $dm) {
                                if ($dm['symbol'] == $product_offer_group_symbol) {
                                    $product_domain = $dm['domain'];
                                }
                            }
                        }
                    }

                    $url = 'http://' . $product_domain . $router->generate('cms_elmat_product_show', array('id' => $entity->getId(), 'slug' => $entity->getSlug()));

                    $results[] = array(
                        'title' => $title,
                        'content' => $content,
                        'url' => $url
                    );
                }
            }
        }

        $page = intval($request->query->get('page', 1));
        $limit = 10;
        $paginator = $this->getPaginator();
        $paginated = $paginator->paginate($results, $page, $limit);

        return $this->render('CmsElmatBundle:Default:szukaj.html.twig', array(
            'typ' => $typ,
            'page' => $page,
            'limit' => $limit,
            'fraza' => $fraza_clean,
            'results' => $paginated
        ));
    }

    private function prepareContent($content) {
        $content = strip_tags($content);
        $content = html_entity_decode($content, ENT_COMPAT | ENT_HTML401, 'UTF-8');
        $content = str_replace("\xC2\xA0", ' ', $content);

        return $content;
    }

    private function getFragmentForHighlight($text, $words) {

        $text_highlighted = '';

        $text_search = str_replace(
            array('ą', 'ć', 'ę', 'ł', 'ń', 'ó', 'ś', 'ó', 'ż', 'ź', 'Ą', 'Ć', 'Ę', 'Ł', 'Ń', 'Ó', 'Ś', 'O', 'Ż', 'Ź'),
            array('a', 'c', 'e', 'l', 'n', 'o', 's', 'o', 'z', 'z', 'a', 'c', 'e', 'l', 'n', 'o', 's', 'o', 'z', 'z'),
            $text
        );

        $words = str_replace(
            array('ą', 'ć', 'ę', 'ł', 'ń', 'ó', 'ś', 'ó', 'ż', 'ź', 'Ą', 'Ć', 'Ę', 'Ł', 'Ń', 'Ó', 'Ś', 'O', 'Ż', 'Ź'),
            array('a', 'c', 'e', 'l', 'n', 'o', 's', 'o', 'z', 'z', 'a', 'c', 'e', 'l', 'n', 'o', 's', 'o', 'z', 'z'),
            $words
        );


        $slowa = explode(' ', $words);

        $length = mb_strlen($text, 'utf-8');

        $first_pos = -1;

        foreach ($slowa as $slowo) {

            $pos = stripos($text_search, $slowo, 0);
            if ($pos === 0) {
                $first_pos = $pos;
            }
            $pos = stripos($text_search, ' ' . $slowo, 0);

            if ($pos === false) {
                continue;
            }

            if ($pos < $first_pos || $first_pos == -1) {
                $first_pos = $pos;
            }
        }

        if ($length - $first_pos < 250) {
            $first_pos = $length - 250;
        }

        $first_pos -= 50;

        if ($first_pos < 0) {
            $first_pos = 0;
        }

        $text_highlighted = mb_substr($text, $first_pos, 300, 'utf-8');
        if ($first_pos > 0) {
            // usunięcie 1 słowa;
            $text_highlighted = '... ' . preg_replace('/^.*?\ /', '', $text_highlighted);
        }

        if (mb_strlen($text_highlighted, 'utf-8') > 250) {
            $text_highlighted = preg_replace('/(.*)\ .*?$/', '\1', $text_highlighted) . ' ... ';
        }

        foreach ($slowa as $slowo) {

            $slowo = preg_quote($slowo, '/');
            $slowo = str_replace(
                array('a', 'c', 'e', 'l', 'n', 'o', 's', 'z'), array('[Aaą]', '[Ccć]', '[Eeę]', '[Llł]', '[Nnń]', '[Ooó]', '[Ssś]', '[Zzźż]'), $slowo);


            $text_highlighted = preg_replace('/^(.*?)(' . $slowo . ')(.*?)$/iu', '\1<b class="hl">\2</b>\3', $text_highlighted);
        }


        return $text_highlighted;
    }

    private function getCategories($lang) {
        return $this->getDoctrine()
            ->getRepository('CmsElmatBundle:CategoryNews')
            ->findBy(array('lang' => $lang), array('kolejnosc' => 'ASC'));
    }

    private function getOfferGroups($lang) {
        return $this->getDoctrine()
            ->getRepository('CmsElmatBundle:OfferGroup')
            ->findBy(array('lang' => $lang), array('kolejnosc' => 'ASC'));
    }
}
