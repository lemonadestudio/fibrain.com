<?php

namespace Cms\ElmatBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Cms\ElmatBundle\Entity\OfferGroup;
use Cms\ElmatBundle\Repository\NewsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Cms\ElmatBundle\Entity\News;

class LiveController extends Controller {

    public function importAction() {
        $sql = "
				SELECT id, nazwa, opis_krotki, opis_pelny, utworzony, aktualizacja, ogladalnosc FROM zawartosc_str
				WHERE opis_krotki != ''
				ORDER BY utworzony ASC

				LIMIT 100000
		";

        $conn = $query = $this->getDoctrine()->getConnection();
        $stmt = $conn->prepare($sql);
        $stmt->execute();

        $em = $this->getDoctrine()->getManager();

        $ret = $stmt->fetchAll();

        foreach ($ret as $n) {
            if (empty($n['utworzony'])) {
                $n['utworzony'] = $n['aktualizacja'];
            }

            $n['utworzony'] = str_replace('.', '-', $n['utworzony']);
            $n['aktualizacja'] = str_replace('.', '-', $n['aktualizacja']);

            $news = new News();
            $news->setTitle($n['nazwa']);
            $news->setContent(stripslashes($n['opis_pelny']));
            $news->setDescription($n['opis_krotki']);
            $news->setAutomaticSeo(true);
            $news->setCreated(new \DateTime($n['utworzony']));
            $news->setUpdated(new \DateTime($n['aktualizacja']));
            $news->setPublishDate(new \DateTime($n['utworzony']));
            $news->setPublished(true);
            $news->setViewCount((int)$n['ogladalnosc']);

            $em->persist($news);
            $em->flush();
        }

        return new Response('');
    }

    /**
     * @Template()
     */
    public function shownewsAction($slug, $id) {

        $locale = $this->get('request')->getLocale();
        $news = $this->getDoctrine()
            ->getRepository('CmsElmatBundle:News')
            ->findOneBy(array(
                'id' => $id,
                'published' => true,
            ));


        if (!$news || $news->getPublishDate() > new \DateTime('now')) {
            return $this->redirect(
                $this->get('router')->generate('cms_elmat_live_main'));
        }
        // przekierowanie jeżeli inny slug lub lang
        if ($slug != $news->getSlug() || $locale != $news->getLang()) {
            return $this->redirect($this->get('router')->generate('cms_elmat_live_shownews', array(
                // '_locale' => $news->getLang(),
                'slug' => $news->getSlug(),
                'id' => $news->getId()
            )));
        }

        $query = $this->getDoctrine()->getManager()
            ->createQuery("
        					SELECT n
        					FROM CmsElmatBundle:News n
        					WHERE n.id != :id
						      AND n.published = true
        					  AND  n.publishDate <= :date
							" . ($news->getOfferGroup() ?
                    " AND n.offer_group = :offer_group_id"
                    : ($news->getCategory() ?
                        " AND n.category = :category_id
								   AND n.offer_group IS NULL "
                        : "AND n.category IS NULL ")) . "
        					ORDER BY n.publishDate DESC


        					");
        $query->setParameter(':date', new \DateTime('now'));
        $query->setParameter(':id', $id);

        if ($news->getOfferGroup()) {
            $query->setParameter(':offer_group_id', $news->getOfferGroup()->getId());
        } elseif ($news->getCategory()) {
            $query->setParameter(':category_id', $news->getCategory()->getId());
        }

        $query->setMaxResults(13);

        $newses = $query->execute();

        // newsy do boxa na dole - czytaj dalej
        $query = $this->getDoctrine()->getManager()
            ->createQuery("
        					SELECT n
        					FROM CmsElmatBundle:News n
        					WHERE n.id != :id
						      AND n.published = true
							 AND  n.publishDate <= :date
							" . ($news->getOfferGroup() ?
                    " AND n.offer_group != :offer_group_id"
                    : ($news->getCategory() ?
                        " AND n.category != :category_id"
                        : "AND n.category IS NOT NULL ")) . "
        					ORDER BY n.publishDate DESC


        					");

        $query->setParameter(':date', new \DateTime('now'));
        $query->setParameter(':id', $id);

        if ($news->getOfferGroup()) {
            $query->setParameter(':offer_group_id', $news->getOfferGroup()->getId());
        } elseif ($news->getCategory()) {
            $query->setParameter(':category_id', $news->getCategory()->getId());
        }

        $query->setMaxResults(8);
        $other_newses = $query->execute();

        $offer_group = $news->getOfferGroup();
        $category = $news->getCategory();

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Home", $this->get("router")->generate("cms_elmat_default_homepage"));
        $breadcrumbs->addItem("Live", $this->get("router")->generate("cms_elmat_live_main"));
        if ($offer_group) {
            $breadcrumbs->addItem($offer_group->getTitle(), $this->get("router")->generate("cms_elmat_live_offergroup", array('slug' => $offer_group->getSlug())));
        } elseif ($category) {
            $breadcrumbs->addItem($category->getTitle(), $this->get("router")->generate("cms_elmat_live_category", array('slug' => $category->getSlug())));
        }
        $breadcrumbs->addItem($news->getTitle(), $this->get("router")->generate("cms_elmat_live_shownews", array('slug' => $news->getSlug(), 'id' => $news->getId())));

        return array(
            'newses' => $newses,
            'news' => $news,
            'categories' => $this->getCategories($locale),
            'offer_group' => $offer_group,
            'category' => $category,
            'other_newses' => $other_newses
        );
    }

    private function getCategories($lang) {
        return $this->getDoctrine()
            ->getRepository('CmsElmatBundle:CategoryNews')
            ->findBy(array('lang' => $lang), array('kolejnosc' => 'ASC'));
    }

    private function getOfferGroups($lang) {
        return $this->getDoctrine()
            ->getRepository('CmsElmatBundle:OfferGroup')
            ->findBy(array('lang' => $lang), array('kolejnosc' => 'ASC'));
    }

    /**
     * @return NewsRepository
     */
    public function getNewsRepository() {
        return $this->getDoctrine()->getRepository('CmsElmatBundle:News');
    }

    /**
     * @Template
     */
    public function mainAction() {
        $locale = $this->get('request')->getLocale();
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Home", $this->get("router")->generate("cms_elmat_default_homepage"));
        $breadcrumbs->addItem("Live", $this->get("router")->generate("cms_elmat_live_main"));


        $categories = $this->getCategories($locale);
        $offer_groups = $this->getOfferGroups($locale);

        $categories_ids = array();
        foreach ($categories as $c) {
            $categories_ids[] = $c->getId();
        }
        $offer_group_ids = array();
        foreach ($offer_groups as $og) {
            // chcieliby, aby były niewidoczne całkowicie dopóki dana strona nie będzie strona gotowa
            if (in_array($og->getSlug(), array('fiber-optic-cables', 'fttx-systems-and-elements'))) {
                continue;
            }
            $offer_group_ids[] = $og->getId();
        }

        // newsy z highlights
//    	$highlight_news_ids = $this->getNewsRepository()->loadHighlightsStronaGlownaIDs($locale);
//    	$news_highlight = $this->getNewsRepository()->loadByIDs($highlight_news_ids);

        $highlight_news_ids = array();

        // ostatnie newsy z każdej kategorii z pominięciem newsów w highlights
        $latest_news_ids = $this->getNewsRepository()
            ->loadLatestEachCategory($locale, 11, $categories_ids, $highlight_news_ids);

        $newses_categories = $this->getNewsRepository()->loadByIDs($latest_news_ids);


        // grupowanie newsów po kategorii
        $news_groupped = array();
        foreach ($categories as $cat) {
            if (!array_key_exists($cat->getSlug(), $news_groupped)) {
                $news_groupped[$cat->getSlug()] = array();
            }
        };

        foreach ($newses_categories as $news) {
            if ($news->getCategory()) {
                $news_groupped[$news->getCategory()->getSlug()][] = $news;
            }
        }


        //  ostatnie newsy z każdej grupy ofertowej
        $latest_news_og_ids = $this->getNewsRepository()
            ->loadLatestEachCategory($locale, 15, $offer_group_ids, $highlight_news_ids, true);

        $newses_offergroups = $this->getNewsRepository()->loadByIDs($latest_news_og_ids);

        // grupowanie newsów po grupie ofertowej
        $news_groupped_og = array();
        foreach ($offer_groups as $og) {
            if (!array_key_exists($og->getSlug(), $news_groupped_og)) {
                $news_groupped_og[$og->getSlug()] = array();
            }
        };

        foreach ($newses_offergroups as $news) {
            if ($news->getOfferGroup()) {
                $news_groupped_og[$news->getOfferGroup()->getSlug()][] = $news;
            }
        }

        return array(
            'categories' => $categories,
            'news_groupped' => $news_groupped,
            'offer_groups' => $offer_groups,
            'news_groupped_og' => $news_groupped_og,
            // 'news_highlight' => $news_highlight

        );
    }

    /**
     * @Template()
     */
    public function categoryAction($slug, $page = 1) {
        $page = abs(intval($page)); 

        $locale = $this->get('request')->getLocale();

        $category = $this->getDoctrine()->getRepository('CmsElmatBundle:CategoryNews')->findOneBy(array('slug' => $slug));

        if (!$category) {
            return $this->redirect($this->get('router')->generate('cms_elmat_live_main'));
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Home", $this->get("router")->generate("cms_elmat_default_homepage"));
        $breadcrumbs->addItem("Live", $this->get("router")->generate("cms_elmat_live_main"));
        $breadcrumbs->addItem($category->getTitle(), $this->get("router")->generate("cms_elmat_live_category", array('slug' => $category->getSlug())));


        // newsy z highlights
        if (false && $category->getShowHighlights()) {
            $highlight_news_ids = $this->getNewsRepository()->loadHighlightsCategoryIDs($locale, $category->getId());
            $news_highlight = $this->getNewsRepository()->loadByIDs($highlight_news_ids);
        } else {
            $highlight_news_ids = array(0);
            $news_highlight = array();
        }

        if (!is_array($highlight_news_ids) || count($highlight_news_ids) == 0) {
            $highlight_news_ids = array(0);
        }


        $query = $this->getDoctrine()->getManager()
            ->createQuery("
        					SELECT n
        					FROM CmsElmatBundle:News n
        					WHERE
    							 n.published = true
							AND  n.publishDate <= :date
    						AND  n.category = :category_id
    						AND  n.offer_group is null

							ORDER BY n.publishDate DESC
        					");

        "

AND  n.id NOT IN (:ids_exclude)
        					";

//     	$query->setParameter(':ids_exclude', $highlight_news_ids);
        $query->setParameter(':date', date('Y-m-d H:i:s'));
        $query->setParameter(':category_id', $category->getId());

// 		$newses = $query->execute();

// 		$arr = array('bla', 'b', 'c', 'd', 'e', 'f');

// 		$t = $paginator->paginate(
// 				$arr,
// 				$page,  /*page number*/
// 				3 /*limit per page*/
// 		);
// 		var_dump($t);

        $paginator = $this->get('knp_paginator');
        $newses = $paginator->paginate(
            $query,
            $page,  /*page number*/
            5 /*limit per page*/
        );


        // $newses = $query->execute();


        return array(
            'categories' => $this->getCategories($locale),
            'category' => $category,
            'newses' => $newses,
            'news_highlight' => $news_highlight,
            'page' => $page,
        );

    }

    /**
     * @Template()
     */
    public function offergroupAction($slug, $page = 1) {

        $pelna_lista = false;
        $page = abs(intval($page));

        $locale = $this->get('request')->getLocale();

        if ($slug == 'aktualnosci-systemowe') {
            $pelna_lista = true;
            $offer_group = new OfferGroup();
            $offer_group->setTitle('Aktualności systemowe');
            $offer_group->setSlug('aktualnosci-systemowe');
            // todo: i18n
        } else {
            $offer_group = $this->getDoctrine()->getRepository('CmsElmatBundle:OfferGroup')->findOneBy(array('slug' => $slug));

        }


        // aktualnosci-systemowe - lista aktualności z wszystkich grup ofertowych
        if (!$offer_group) {
            return $this->redirect(
                $this->get('router')->generate('cms_elmat_live_main'));
        }


        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Home", $this->get("router")->generate("cms_elmat_default_homepage"));
        $breadcrumbs->addItem("Live", $this->get("router")->generate("cms_elmat_live_main"));
        $breadcrumbs->addItem($offer_group->getTitle(), $this->get("router")->generate("cms_elmat_live_offergroup", array('slug' => $offer_group->getSlug())));


        // newsy z highlights

        if (false && $pelna_lista) {
            // $highlight_news_ids = $this->getNewsRepository()->loadHighlightsStronaGlownaIDs($locale, true);
            $highlight_news_ids = $this->getNewsRepository()->loadHighlightsCategoryIDs($locale, $offer_group->getId(), true);
            $news_highlight = $this->getNewsRepository()->loadByIDs($highlight_news_ids);
        } elseif (false && $offer_group->getShowHighlights()) {
            $highlight_news_ids = $this->getNewsRepository()->loadHighlightsCategoryIDs($locale, $offer_group->getId(), true);
            $news_highlight = $this->getNewsRepository()->loadByIDs($highlight_news_ids);
        } else {
            $highlight_news_ids = array(0);
            $news_highlight = array();
        }

        if (empty($highlight_news_ids)) {
            $highlight_news_ids = array(0);
        }


        $query = $this->getDoctrine()->getManager()
            ->createQuery("
        					SELECT n
        					FROM CmsElmatBundle:News n
        					WHERE
    							 n.published = true
    						AND  n.publishDate <= :date
    						" . ($pelna_lista ?
                    "AND  n.offer_group IS NOT NULL"
                    // chcieliby, aby były niewidoczne całkowicie dopóki dana strona nie będzie strona gotowa
                    . " AND n.offer_group NOT IN (2,3,6) "
                    : " AND  n.offer_group = :offer_group_id "
                ) . "
							AND n.id NOT IN (:ids_exclude)
        					ORDER BY n.publishDate DESC

        					");

        // uwaga .. dziwne, ale kolejność przypisywania parametrów ma znaczenie dla paginatora! (jakiś bug)
        $query->setParameter(':date', date('Y-m-d H:i:s'));
        if (!$pelna_lista) {
            $query->setParameter(':offer_group_id', $offer_group->getId());
        }
        $query->setParameter(':ids_exclude', $highlight_news_ids);


        $paginator = $this->get('knp_paginator');
        $newses = $paginator->paginate(
            $query,
            $this->get('request')->query->get('page', $page)  /*page number*/,
            5 /*limit per page*/
        );

        return array(
            'categories' => $this->getCategories($locale),
            'offer_group' => $offer_group,
            'newses' => $newses,
            'news_highlight' => $news_highlight,
            'page' => $page,
        );
    }
}
