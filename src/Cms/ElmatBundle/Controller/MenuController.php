<?php

namespace Cms\ElmatBundle\Controller;

use Cms\ElmatBundle\Entity\Article;
use Cms\ElmatBundle\Entity\Page;
use Cms\ElmatBundle\Menu\Builder;
use Doctrine\ORM\Query;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class MenuController extends Controller {
    public function managedAction($location = 'menu_gorne1', $depth = 1) {
        // $menu = new Builder();
        $response = $this->render('CmsElmatBundle:Menu:' . ($location ? $location : "managed") . '.html.twig', array('location' => $location, 'depth' => $depth));

        $menu_cfg = $this->container->getParameter('cms_elmat.menu');

        $cache_time = intval($menu_cfg['locations'][$location]['cache']);

        return $response;
    }

    /**
     * generuje lewe menu - zawsze  - ot co !
     */
    public function leftmenuAction($options = array()) {
        $em = $this->getDoctrine()->getManager();

        $request = $options['request'];

        $links = array();

        $menu_builder = new Builder();
        $menu_builder->setContainer($this->container);

        $page = false;

        $route = $request->get('_route');
        $obj_id = $request->get('id');

        $page_route = 'cms_elmat_page_show';
        $article_route = 'cms_elmat_article_show';

        $page_route_parameters = array(
            'id' => $obj_id,
            'slug' => '',
        );


        switch ($route) {
            case 'cms_elmat_default_contact':
            case 'cms_elmat_default_szukaj':
            case 'cms_elmat_default_szukaj_0':
            case 'cms_elmat_default_szukaj_typ':
                $page = new Page();
                $page_route = 'cms_elmat_default_contact';
                $page_route_parameters = array();
                break;

            case 'cms_elmat_zasoby_show':
            case 'cms_elmat_zasoby_show_structural_cabling':
            case 'cms_elmat_zasoby_show_fiber_optic':
            case 'cms_elmat_zasoby_show_fttx_systems':
            case 'cms_elmat_zasoby_show_passive_optical':
            case 'cms_elmat_zasoby_show_active_elements':
            case 'cms_elmat_zasoby_show_connectivity':
            case 'cms_elmat_zasoby_show_distribution':
            case 'cms_elmat_zasoby_show_photonics':
                $page_route = $route;

                $page = $this->getZasobCategoryRepository()->find($obj_id);

                if ($page) {
                    $page_route_parameters['id'] = $page->getId();
                    $page_route_parameters['slug'] = $page->getSlug();
                }
                break;

            case 'cms_elmat_page_show':
            case 'cms_elmat_page_show_structural_cabling':
            case 'cms_elmat_page_show_fiber_optic':
            case 'cms_elmat_page_show_fttx_systems':
            case 'cms_elmat_page_show_passive_optical':
            case 'cms_elmat_page_show_active_elements':
            case 'cms_elmat_page_show_connectivity':
            case 'cms_elmat_page_show_distribution':
            case 'cms_elmat_page_show_photonics':
                $page_route = $route;

                $page = $this->getPageRepository()->find($obj_id);

                if ($page) {
                    $page_route_parameters['id'] = $page->getId();
                    $page_route_parameters['slug'] = $page->getSlug();
                }
                break;

            case 'cms_elmat_article_category_rozwiazania':
            case 'cms_elmat_article_category_uslugi':
            case 'cms_elmat_article_category_artykuly':
            case 'cms_elmat_article_category':
            case 'cms_elmat_article_show':
            case 'cms_elmat_article_show_structural_cabling':
            case 'cms_elmat_article_show_fiber_optic':
            case 'cms_elmat_article_show_fttx_systems':
            case 'cms_elmat_article_show_passive_optical':
            case 'cms_elmat_article_show_active_elements':
            case 'cms_elmat_article_show_connectivity':
            case 'cms_elmat_article_show_distribution':
            case 'cms_elmat_article_show_photonics':
                $type = $request->get('type');

                $types = array(
                    Article::TYPE_artykuly_techniczne,
                    Article::TYPE_rozwiazania,
                    Article::TYPE_uslugi
                );

                if ($request->get('_domain_symbol2')) {
                    $article_route = 'cms_elmat_article_show_' . $request->get('_domain_symbol2');
                }

                if (!in_array($type, $types)) {
                    return array(
                        'links' => $links
                    );
                }

                if ($obj_id == null) {
                    /* 1. sprawdzanie czy kategoria artykułu znajduje się w menu zasobów */
                    switch ($type) {
                        case Article::TYPE_artykuly_techniczne:
                            $category_route = 'cms_elmat_article_category_artykuly';
                            break;

                        case Article::TYPE_rozwiazania:
                            $category_route = 'cms_elmat_article_category_rozwiazania';
                            break;

                        case Article::TYPE_uslugi:
                            $category_route = 'cms_elmat_article_category_uslugi';
                            break;

                        default:
                            $category_route = '';
                            break;
                    }

                    $menu_gorne_location = 'menu_gorne1';

                    if ($request->get('_domain_symbol2')) {
                        $menu_gorne_location = 'menu_gorne_' . $request->get('_domain_symbol2');
                    }

                    if (!empty($category_route)) {
                        $qb = $em->createQueryBuilder();
                        $category_item = $qb->select('m, p')
                            ->from('CmsElmatBundle:MenuItem', 'm')
                            ->leftJoin('m.parentItem', 'p')
                            ->where('m.location = :location')
                            ->andWhere('m.route = :route')
                            ->andWhere($qb->expr()->gt('m.depth', 1))
                            ->andWhere($qb->expr()->isNotNull('m.anchor'))
                            ->setParameter('location', $menu_gorne_location)
                            ->setParameter('route', $category_route)
                            ->setMaxResults(1)
                            ->getQuery()
                            ->getOneOrNullResult();

                        if (null !== $category_item) {
                            $qb = $em->createQueryBuilder();
                            $menu_items = $qb->select('m, p')
                                ->from('CmsElmatBundle:MenuItem', 'm')
                                ->leftJoin('m.parentItem', 'p')
                                ->where('m.location = :location')
                                ->andWhere('m.parentItem = :parentItem')
                                ->andWhere($qb->expr()->gt('m.depth', 1))
                                ->setParameter('location', $menu_gorne_location)
                                ->setParameter('parentItem', $category_item->getParentItem()->getId())
                                ->getQuery()
                                ->getResult();

                            $zasoby_routes = array(
                                'cms_elmat_zasoby_show',
                                'cms_elmat_zasoby_show_structural_cabling',
                                'cms_elmat_zasoby_show_fiber_optic',
                                'cms_elmat_zasoby_show_fttx_systems',
                                'cms_elmat_zasoby_show_passive_optical',
                                'cms_elmat_zasoby_show_active_elements',
                                'cms_elmat_zasoby_show_connectivity',
                                'cms_elmat_zasoby_show_distribution',
                                'cms_elmat_zasoby_show_photonics'
                            );

                            $in_zasoby = false;
                            foreach ($menu_items as $menu_item) {
                                if (in_array($menu_item->getRoute(), $zasoby_routes)) {
                                    $in_zasoby = true;
                                }
                            }

                            if ($in_zasoby) {
                                $page = new Page();
                                $page_route = $category_route;
                                $page_route_parameters = array();
                            }
                        }
                    }

                    /* jeśli nie ad 1 wyświetl normalnie */
                    if (!$page) {
                        $links[] = array(
                            'current' => false,
                            'anchor' => $this->get('translator')->trans('label.main_page'),
                            'url' => "/"

                        );

                        foreach ($types as $_typ) {
                            $links[] = array(
                                'current' => $type == $_typ ? true : false,
                                'anchor' => $this->get('translator')->trans('label.' . $_typ),
                                'url' => $this->get('router')->generate('cms_elmat_article_category', array(
                                    'type' => $_typ
                                ))
                            );
                        }
                    }
                } else {
                    $links[] = array(
                        'current' => false,
                        'anchor' => $this->get('translator')->trans('label.' . $type),
                        'url' => $this->get('router')->generate('cms_elmat_article_category', array(
                            'type' => $type
                        ))
                    );

                    $articles = $this->getDoctrine()->getRepository('CmsElmatBundle:Article')->loadRecent(
                        10,
                        $type,
                        $this->get('request')->getLocale(),
                        $this->getRequest()->get('_domain_symbol_slug')
                    );

                    foreach ($articles as $art) {
                        $links[] = array(
                            'current' => $obj_id == $art->getId() ? true : false,
                            'anchor' => $art->getTitle(),
                            'url' => $this->get('router')->generate($article_route, array(
                                'type' => $art->getType(),
                                'id' => $art->getId(),
                                'slug' => $art->getSlug()
                            ))
                        );
                    }
                }
                break;

            case 'cms_elmat_aktualnosci_main':
            case 'cms_elmat_aktualnosci_show':
                $links[] = array(
                    'current' => false,
                    'anchor' => $this->get('translator')->trans('label.main_page'),
                    'url' => "/"
                );

                $links[] = array(
                    'current' => true,
                    'anchor' => 'News',
                    'url' => $this->get('router')->generate('cms_elmat_aktualnosci_main')
                );
                break;

            case 'cms_elmat_training_main':
            case 'cms_elmat_training_category_show':
            case 'cms_elmat_training_show':
            case 'cms_elmat_training_terminy':
            case 'cms_elmat_training_register':
                $category = null;
                $training = null;

                if ($route == 'cms_elmat_training_category_show') {
                    $category = $this->getDoctrine()->getRepository('CmsElmatBundle:TrainingCategory')
                        ->findOneBy(array('slug' => $request->get('slug')));
                }

                if ($route == 'cms_elmat_training_show') {
                    $training = $this->getDoctrine()->getRepository('CmsElmatBundle:Training')
                        ->findOneBy(array('slug' => $request->get('slug')));

                    if ($training) {
                        $category = $training->getCategory();
                    }
                }

                if ($route == 'cms_elmat_training_main'
                    || $route == 'cms_elmat_training_terminy'
                    || $route == 'cms_elmat_training_register'
                    || ($category && $category->getLvl() == 0)
                    || ($route == 'cms_elmat_training_show' && !$category)
                ) {

                    $links[] = array(
                        'current' => false,
                        'anchor' => $this->get('translator')->trans('label.trainings'),
                        'url' => "/"
                    );

                    $links[] = array(
                        'current' => $route == 'cms_elmat_training_main' ? true : false,
                        'anchor' => $this->get('translator')->trans('label.beginning'),
                        'url' => $this->get('router')->generate('cms_elmat_training_main')
                    );

                    $categories = $em->createQueryBuilder()
                        ->select('n, t, ch, cht')
                        ->from('CmsElmatBundle:TrainingCategory', 'n')
                        ->leftJoin('n.trainings', 't')
                        ->leftJoin('n.children', 'ch')
                        ->leftJoin('ch.trainings', 'cht')
                        ->where('n.lvl = 0')
                        ->orderBy('n.kolejnosc', 'ASC')
                        ->addOrderBy('n.lft', 'ASC')
                        ->addOrderBy('n.root', 'ASC')
                        ->addOrderBy('ch.lft')
                        ->getQuery()
                        ->getResult();

                    foreach ($categories as $cat) {
                        $current = $route == 'cms_elmat_training_category_show' && $request->get('slug') == $cat->getSlug() ? true : false;
                        $children = array();

                        if ($current) {
                            foreach ($cat->getChildren() as $child_cat) {
                                $children[] = array(
                                    'current' => false,
                                    'anchor' => $child_cat->getTitle(),
                                    'url' => $this->get('router')->generate('cms_elmat_training_category_show', array('slug' => $child_cat->getSlug())),
                                );
                            }
                        }


                        $links[] = array(
                            'current' => $current,
                            'anchor' => $cat->getTitle(),
                            'url' => $this->get('router')->generate('cms_elmat_training_category_show', array('slug' => $cat->getSlug())),
                            'children' => $children
                        );
                    }
                }


                if ($route == 'cms_elmat_training_show' || ($category && $category->getLvl() == 1)) {
                    if ($category && $category->getLvl() == 1) {
                        $links[] = array(
                            'current' => $route == 'cms_elmat_training_main' ? true : false,
                            'anchor' => $category->getParent()->getTitle(),
                            'url' => $this->get('router')->generate('cms_elmat_training_category_show', array('slug' => $category->getParent()->getSlug())),
                        );

                        $categories = $category->getParent()->getChildren();

                        foreach ($categories as $cat) {

                            $current = $route == 'cms_elmat_training_category_show' && $request->get('slug') == $cat->getSlug() ? true : false;

                            if ($route == 'cms_elmat_training_show' && $training && $training->getCategory()->getSlug() == $cat->getSlug()) {
                                $current = true;
                            }

                            $children = array();

                            if ($current) {
                                foreach ($cat->getTrainings() as $train) {
                                    $current_train = $route == 'cms_elmat_training_show' && $request->get('slug') == $train->getSlug() ? true : false;

                                    $children[] = array(
                                        'current' => $current_train,
                                        'anchor' => $train->getTitle(),
                                        'url' => $this->get('router')->generate('cms_elmat_training_show', array('slug' => $train->getSlug())),
                                    );
                                }
                            }

                            $links[] = array(
                                'current' => $current,
                                'anchor' => $cat->getTitle(),
                                'url' => $this->get('router')->generate('cms_elmat_training_category_show', array('slug' => $cat->getSlug())),
                                'children' => $children
                            );
                        }
                    }
                }

                $links[] = array(
                    'current' => $route == 'cms_elmat_training_terminy' || $route == 'cms_elmat_training_register' ? true : false,
                    'anchor' => 'Terminy i rejestracja',
                    'url' => $this->get('router')->generate('cms_elmat_training_terminy'),
                );
                break;

            case 'cms_elmat_userzone_main':
            case 'cms_elmat_userzone_cenniki':
            case 'cms_elmat_userzone_settings':
            case 'cms_elmat_userzone_logout':
            case 'cms_elmat_userzone_szkolenia':
            case 'cms_elmat_userzone_szkolenie_rejestracja':
                $links[] = array(
                    'current' => $route == 'cms_elmat_userzone_main' ? true : false,
                    'title' => 'Strefa klienta',
                    'anchor' => 'Strefa klienta',
                    'url' => $this->get('router')->generate('cms_elmat_userzone_main')
                );

//                $links[] = array(
//                    'current' => $route == 'cms_elmat_userzone_cenniki' ? true : false,
//                    'title' => 'Cenniki',
//                    'anchor' => 'Cenniki',
//                    'url' => $this->get('router')->generate('cms_elmat_userzone_cenniki')
//                );

                $links[] = array(
                    'current' => $route == 'cms_elmat_userzone_settings' ? true : false,
                    'title' => 'Ustawienia',
                    'anchor' => 'Ustawienia',
                    'url' => $this->get('router')->generate('cms_elmat_userzone_settings')
                );

                $links[] = array(
                    'current' => $route == 'cms_elmat_userzone_szkolenia'
                    || $route == 'cms_elmat_userzone_szkolenie_rejestracja'
                        ? true : false,
                    'title' => 'Moje szkolenia',
                    'anchor' => 'Moje szkolenia',
                    'url' => $this->get('router')->generate('cms_elmat_userzone_szkolenia')
                );

                $links[] = array(
                    'current' => $route == 'cms_elmat_userzone_logout' ? true : false,
                    'title' => 'Wyloguj',
                    'anchor' => 'Wyloguj',
                    'url' => $this->get('router')->generate('cms_elmat_userzone_logout')
                );
                break;
        }

        if ($page) {
            // liczniki zasobów
            $qb = $em->createQueryBuilder();
            $zasoby_count_arr = $qb->select('COUNT(z.id) AS ilosc, zg.slug, zgo.id AS og_id')
                ->from('CmsElmatBundle:Zasob', 'z')
                ->leftJoin('z.category', 'zg')
                ->leftJoin('zg.offer_group', 'zgo')
                ->where($qb->expr()->isNotNull('zg.slug'))
                ->groupBy('zg.slug')
                ->addGroupBy('zg.offer_group')
                ->getQuery()
                ->getArrayResult();

            $zasoby_count = array();

            foreach ($zasoby_count_arr as $zc) {

                if (!array_key_exists('cms_elmat_zasoby_show', $zasoby_count)) {
                    $zasoby_count['cms_elmat_zasoby_show'] = array();
                }
                if (!array_key_exists($zc['slug'], $zasoby_count['cms_elmat_zasoby_show'])) {
                    $zasoby_count['cms_elmat_zasoby_show'][$zc['slug']] = 0;
                }

                $zasoby_count['cms_elmat_zasoby_show'][$zc['slug']] += $zc['ilosc'];
                $z_og_route = '';
                switch ($zc['og_id']) {
                    case 1:
                        $z_og_route = 'cms_elmat_zasoby_show_structural_cabling';
                        break;
                    case 2:
                        $z_og_route = 'cms_elmat_zasoby_show_fiber_optic';
                        break;
                    case 3:
                        $z_og_route = 'cms_elmat_zasoby_show_fttx_systems';
                        break;
                    case 4:
                        $z_og_route = 'cms_elmat_zasoby_show_passive_optical';
                        break;
                    case 5:
                        $z_og_route = 'cms_elmat_zasoby_show_active_elements';
                        break;
                    case 6:
                        $z_og_route = 'cms_elmat_zasoby_show_connectivity';
                        break;
                    case 7:
                        $z_og_route = 'cms_elmat_zasoby_show_distribution';
                        break;
                    case 8:
                        $z_og_route = 'cms_elmat_zasoby_show_photonics';
                        break;
                }

                if ($z_og_route) {
                    if (!array_key_exists($z_og_route, $zasoby_count)) {
                        $zasoby_count[$z_og_route] = array();
                    }

                    if (!array_key_exists($zc['slug'], $zasoby_count[$z_og_route])) {
                        $zasoby_count[$z_og_route][$zc['slug']] = 0;
                    }

                    $zasoby_count[$z_og_route][$zc['slug']] += $zc['ilosc'];
                }
            }

            $links[] = array(
                'current' => false,
                'anchor' => $this->get('translator')->trans('label.main_page'),
                'url' => "/"
            );

            $menu_gorne_location = 'menu_gorne1';

            if ($request->get('_domain_symbol2')) {
                $menu_gorne_location = 'menu_gorne_' . $request->get('_domain_symbol2');
            }

            $qb = $em->createQueryBuilder();
            $qb->select('m, p');
            $qb->from('CmsElmatBundle:MenuItem', 'm');
            $qb->leftJoin('m.parentItem', 'p');
            $qb->leftJoin('p.parentItem', 'pp');
            $qb->where('m.location = :location');
            if (empty($page_route_parameters)) {
                $qb->andWhere($qb->expr()->andX(
                    $qb->expr()->orX(
                        $qb->expr()->eq('m.depth', 1),
                        $qb->expr()->eq('m.depth', 2),
                        $qb->expr()->eq('m.depth', 3),
                        $qb->expr()->eq('m.depth', 4)
                    ),
                    $qb->expr()->eq('m.route', ':route')
                ));
                $qb->setParameter('route', $page_route);
            } else {
                $qb->andWhere($qb->expr()->andX(
                    $qb->expr()->orX(
                        $qb->expr()->eq('m.depth', 1),
                        $qb->expr()->eq('m.depth', 2),
                        $qb->expr()->eq('m.depth', 3),
                        $qb->expr()->eq('m.depth', 4)
                    ),
                    $qb->expr()->eq('m.route', ':route'),
                    $qb->expr()->eq('m.routeParameters', ':routeParameters')
                ));
                $qb->setParameter('route', $page_route);
                $qb->setParameter('routeParameters', json_encode($page_route_parameters));
            }
            $qb->orderBy('m.depth', 'DESC');
            $qb->addOrderBy('m.lft', 'ASC');
            $qb->addOrderBy('m.title', 'ASC');
            $qb->setParameter('location', $menu_gorne_location);
            $qb->setMaxResults(1);
            $menu_item = $qb->getQuery()->getOneOrNullResult();

            if ($menu_item) {

                $_id_pp = -1; // id parent parent
                $_id_p = -1; // id parent
                $_id = -1; // id

                if ($menu_item->getDepth() == 1) {
                    // 1,2,3 poziom
                    $_id_pp = $menu_item->getId();
                    $_id_p = $menu_item->getId();
                    $_id = $menu_item->getId();
                }

                if ($menu_item->getDepth() == 2) {
                    // 2 i 3 poziom
                    $_id_p = $menu_item->getId();
                    $_id = $menu_item->getId();
                }

                if ($menu_item->getDepth() == 3) {
                    // 2 i 3 poziom
                    $_id_p = $menu_item->getParentItem()->getId();
                    $_id = $menu_item->getId();
                }

                if ($menu_item->getDepth() == 4) {
                    // 3, 4 przez join
                    $_id_p = $menu_item->getParentItem()->getParentItem()->getId();

                }

                if ($menu_item->getDepth() > 1) {
                    $qb = $em->createQueryBuilder();
                    $qb->select('m');
                    $qb->from('CmsElmatBundle:MenuItem', 'm');
                    $qb->where('m.location = :location');
                    $qb->andWhere('m.location = :location');
                    $qb->andWhere('m.lft < :lft');
                    $qb->orderBy('m.lft', 'DESC');
                    $qb->setParameter('location', $menu_gorne_location);
                    $qb->setParameter('lft', $menu_item->getLft());
                    $qb->setMaxResults(1);
                    $root_menu_item = $qb->getQuery()->getOneOrNullResult();

                    if ($root_menu_item) {
                        $params = $menu_builder->getMenuItemArray($root_menu_item);

                        $links_arr = array(
                            'current' => false,
                            'title' => $root_menu_item->getTitle(),
                            'anchor' => $root_menu_item->getAnchor(),
                            'url' => $params['uri'],
                            'children' => array(),
                        );

                        $links[0] = $links_arr;
                    }
                }

                $qb = $em->createQueryBuilder();
                $menu_items = $qb->select('m, m2, p')
                    ->from('CmsElmatBundle:MenuItem', 'm')
                    ->leftJoin('m.childItems', 'm2')
                    ->leftJoin('m.parentItem', 'p')
                    ->where('m.location = :location')
                    ->andWhere($qb->expr()->orX(
                        $qb->expr()->eq('m.id', ':id'),
                        $qb->expr()->eq('m.parentItem', ':id_p'),
                        $qb->expr()->eq('p.parentItem', ':id_pp')
                    ))
                    ->setParameter('location', $menu_gorne_location)
                    ->setParameter('id', $_id)
                    ->setParameter('id_p', $_id_p)
                    ->setParameter('id_pp', $_id_pp)
                    ->orderBy('m.lft', 'ASC')
                    ->addOrderBy('m.title', 'ASC')
                    ->getQuery()
                    ->getResult();

                foreach ($menu_items as $item) {
                    if (trim($item->getAnchor()) == '') {
                        continue;
                    }

                    $params = $menu_builder->getMenuItemArray($item);

                    if ($params) {
                        $childs = array();
                        $child_items = array();
                        $children_visible = false;
                        $search_routes = array(
                            'cms_elmat_default_szukaj',
                            'cms_elmat_default_szukaj_0',
                            'cms_elmat_default_szukaj_typ'
                        );


                        if ($item->getDepth() == 3) {
                            $child_items = $item->getChildItems();
                        }

                        if (is_array($child_items) || $child_items instanceof \Traversable) {

                            foreach ($child_items as $child_item) {
                                $child_params = $menu_builder->getMenuItemArray($child_item);
                                if ($child_params) {

//                                    if ($child_item->getId() == $menu_item->getId()) {
                                    $children_visible = true;
//                                    }

                                    $item_route = $child_item->getRoute();
                                    $count = 0;
                                    if ($item_route && array_key_exists($item_route, $zasoby_count)) {
                                        $item_route_params = $child_item->getRouteParametersArray();
                                        if (array_key_exists('slug', $item_route_params)) {
                                            if (array_key_exists($item_route_params['slug'], $zasoby_count[$item_route])) {
                                                $count = $zasoby_count[$item_route][$item_route_params['slug']];
                                            }
                                        }
                                    }

                                    $childs[] = array(
                                        'current' => $child_item->getId() == $menu_item->getId() && !in_array($route, $search_routes) ? true : false,
                                        'title' => $child_item->getTitle(),
                                        'anchor' => $child_item->getAnchor(),
                                        'url' => $child_params['uri'],
                                        'count' => $count,
                                    );
                                }
                            }
                        }

                        if (!$children_visible && $item->getId() != $menu_item->getId()) {
                            $childs = array();
                        }

                        $item_route = $item->getRoute();
                        $count = 0;
                        if ($item_route && array_key_exists($item_route, $zasoby_count)) {
                            $item_route_params = $item->getRouteParametersArray();
                            if (array_key_exists('slug', $item_route_params)) {
                                if (array_key_exists($item_route_params['slug'], $zasoby_count[$item_route])) {
                                    $count = $zasoby_count[$item_route][$item_route_params['slug']];
                                }
                            }
                        }

                        $links_arr = array(
                            'current' => $item->getId() == $menu_item->getId() && !in_array($route, $search_routes) ? true : false,
                            'title' => $item->getTitle(),
                            'anchor' => $item->getAnchor(),
                            'url' => $params['uri'],
                            'children' => $childs,
                            'count' => $count,
                        );

                        $links[] = $links_arr;
                    }
                }
            } else {
                $links[] = array(
                    'current' => true,
                    'anchor' => $page->getTitle(),
                    'url' => $this->get('router')->generate($page_route, $page_route_parameters)
                );
            }
        }

        return $this->render('CmsElmatBundle:Menu:leftmenu.html.twig', array(
            'links' => $links
        ));
    }

    public function getPageRepository() {
        return $this->getDoctrine()->getRepository('CmsElmatBundle:Page');
    }

    public function getZasobCategoryRepository() {
        return $this->getDoctrine()->getRepository('CmsElmatBundle:ZasobCategory');
    }
}
