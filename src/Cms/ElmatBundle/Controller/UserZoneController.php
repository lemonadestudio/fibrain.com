<?php

namespace Cms\ElmatBundle\Controller;
use Cms\ElmatBundle\Entity\Pricelist;
use Cms\ElmatBundle\Repository\ArticleRepository;
use Symfony\Component\Routing\Router;
use Cms\ElmatBundle\Entity\User;
use Cms\ElmatBundle\Form\Type\KlientRejestracjaKrok2Type;
use Cms\ElmatBundle\Form\Type\KlientRejestracjaType;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Cms\ElmatBundle\Repository\UserRepository;
use Cms\ElmatBundle\Repository\UserGroupRepository;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Cms\ElmatBundle\Form\Type\KlientZmianaHaslaType;
use Cms\ElmatBundle\Form\Type\KlientPrzypomnienieHaslaType;
use FOS\UserBundle\Doctrine\UserManager;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBag;


class UserZoneController extends Controller
{



	/**
	 * @Template()
	 */
    public function registerAction()
    {


    	$locale = $this->get('request')->getLocale();

    	$breadcrumbs = $this->get("white_october_breadcrumbs");
    	$breadcrumbs->addItem("Strona główna", "/");
    	$breadcrumbs->addItem("Strefa klienta", $this->get("router")->generate("cms_elmat_userzone_main"));
    	$breadcrumbs->addItem("Rejestracja", $this->get("router")->generate("cms_elmat_userzone_register"));

    	$request = $this->getRequest();

    	$krok = 1;
    	$krok_submit = 0;
    	$user = null;

    	$form = $this->createForm(new KlientRejestracjaType());


    	if($request->isMethod('post')) {

    	    $krok_submit = 1;
    	    $form->bind($request);

    	    $form_data = $request->request->get($form->getName());

    	    if(!is_array($form_data)) {
    	        return $this->redirect($this->get('router')->generate('cms_elmat_userzone_main'));
    	    }

    	    if(array_key_exists('krok', $form_data) && $form_data['krok'] == 3) {
    	        $krok_submit = 3;
    	    }

    	    if(array_key_exists('krok', $form_data) && $form_data['krok'] == 2) {
    	        $krok_submit = 2;
    	    }

    	    if($form->isValid() || $krok_submit >= 2) {
                $krok = 2;
    	        $form = $this->createForm(new KlientRejestracjaKrok2Type());

    	        $form_data['krok'] = 2;
    	        $request->request->set($form->getName(), $form_data);
    	        $form->bind($request);

    	    }



    	    if($form->isValid()) {

    	        if($krok_submit == 3) {

    	            // jeżeli formularz został ostatecznie wysłany

                    // zapisanie usera
    	            $user = $form->getData();

    	            $em = $this->getDoctrine()->getManager();



    	            // $user->setEnabled(false);

//     	            $new_user = new User();
//     	            $new_user
//     	                ->setEmail($user->getEmail())
//     	                ->setUsername($user->getUsername())
//     	                ->setPassword($user->getPassword())
//     	                ->setFirma($user->getFirma())
//     	                ->setUlica($user->getUlica())
//     	                ->setKodPocztowy($user->getKodPocztowy())
//     	                ->setMiasto($user->getMiasto())
//     	                ->setWojewodztwo($user->getWojewodztwo())
//     	                ->setNip($user->getNip())
//     	                ->setTelefon($user->getTelefon())
//     	                ->setFax($user->getFax())
//     	            ;




    	            $user->setConfirmationToken(sha1($user->serialize().time()));

                    $em->persist($user);
                    $em->flush();

                    $zmienne = array(
                            'AKTYWACJA_URL' => $this->get('router')->generate(
                                    'cms_elmat_userzone_confirm',
                                        array('id' => $user->getId(),
                                              'token' => $user->getConfirmationToken()
                                              ),
                                       true ),
                            'LOGIN' => $user->getUsername(),
                            'EMAIL' => $user->getEmail(),
                            'FIRMA' =>     $user->getFirma(),
                            'ULICA' =>     $user->getUlica(),
                            'KOD_POCZTOWY' => $user->getKodPocztowy(),
                            'MIASTO' =>     $user->getMiasto(),
                            'WOJEWODZTWO' => $user->getWojewodztwo(),
                            'NIP' =>     $user->getNip(),
                            'TELEFON' => $user->getTelefon(),
                            'FAX' =>     $user->getFax()


                    );

                    $template = $this->get('cms_email_templates')->getParsed('rejestracja_klienta_potwierdzenie_email', $zmienne);

                    $email_do = $user->getEmail();
                    $email_kopia = $this->get('cms_config')->get('ust_rejestracja_potwierdzenie_kopia_email');
                    $email_od = $this->get('cms_config')->get('ust_mailer_wiadomosc_od');

                    $message_email = \Swift_Message::newInstance()
                        ->setSubject($template->getTopic())
                        ->setFrom($this->container->getParameter('mailer_user'), $email_od)
                        ->setTo($email_do)
                        ->setBody($template->getContent(), 'text/html')
                        ->addPart(strip_tags($template->getContentTxt()), 'text/plain');

                    if($email_kopia) {
                        $message_email->setBcc(array_map('trim', explode(',', $email_kopia)));
                    }



                    try {
                        $ret = $this->get('mailer')->send($message_email);
                    } catch(Exception $e) {

                    }


                    $this->get('session')->getFlashBag()->set('notice',
                            $this->get('cms_config')->get('txt_strefa_klienta_rejestracja_podziekowanie'));

                    return $this->redirect($this->get('router')->generate('cms_elmat_userzone_login'));


                    // wysłanie potwierdzenia adresu email


    	        }

                if($krok == 2) {

                    $krok = 3;
                    $form = $this->createForm(new KlientRejestracjaKrok2Type());


                    $form_data['krok'] = 3;
                    $request->request->set($form->getName(), $form_data);
                    $form->bind($request);

                    $user = $form->getData();

                }


    	    }


    	    $data = $form->getData();



    	}

        return array(
            'krok_submit' => $krok_submit,
            'krok' => $krok,
            'form' => $form->createView(),
            'user' => $user,
        );


    }

    public function confirmAction($id, $token) {

        // $this->getSecurityContext()->isGranted('')
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUserRepository()->find($id);
        if(!$user) {
            throw new NotFoundHttpException();
        }

        

        if($user->getConfirmationToken() == $token) {
            
            if($user->isEnabled() == true) {
                
                $this->get('session')->getFlashBag()->set(
                    'notice',
                        $this->get('cms_config')->get('txt_strefa_klienta_rejestracja_potwierdzenie_email_link_wykorzystany')
                );
                
                return $this->redirect($this->get('router')->generate('cms_elmat_userzone_login'));

                
            }
            
            
            $user->setEnabled(true);
            
            $group_klient = $this->getUserGroupRepository()->findOneBy(array('name' => 'Klient'));
            
            if($group_klient) {
                $user->addGroup($group_klient);
            } else {
                $user->addRole('ROLE_KLIENT');
            }
            
            
            $em->flush();
            $this->get('session')->getFlashBag()->set(
                    'notice',
                    $this->get('cms_config')->get('txt_strefa_klienta_rejestracja_potwierdzenie_email')
            );

            $zmienne = array(

                    'EDYCJA_URL' => $this->get('router')->generate('admin_cms_elmat_user_edit', array('id' => $user->getId()), true),
                    'LOGIN' => $user->getUsername(),
                    'EMAIL' => $user->getEmail(),
                    'FIRMA' =>     $user->getFirma(),
                    'ULICA' =>     $user->getUlica(),
                    'KOD_POCZTOWY' => $user->getKodPocztowy(),
                    'MIASTO' =>     $user->getMiasto(),
                    'WOJEWODZTWO' => $user->getWojewodztwo(),
                    'NIP' =>     $user->getNip(),
                    'TELEFON' => $user->getTelefon(),
                    'FAX' =>     $user->getFax()

            );

            $template = $this->get('cms_email_templates')->getParsed('rejestracja_klienta_nowy_klient_powiadomienie', $zmienne);


            $email_do = $this->get('cms_config')->get('ust_rejestracja_powiadomienie_nowe_konto');
            $email_od = $this->get('cms_config')->get('ust_mailer_wiadomosc_od');

            $message_email = \Swift_Message::newInstance()
                ->setSubject($template->getTopic())
                ->setFrom($this->container->getParameter('mailer_user'), $email_od)
                ->setTo(array_map('trim', explode(',', $email_do)))
                ->setBody($template->getContent(), 'text/html')
                ->addPart(strip_tags($template->getContentTxt()), 'text/plain');


            try {
                $ret = $this->get('mailer')->send($message_email);
            } catch(Exception $e) {

            }

        }



        return $this->redirect($this->get('router')->generate('cms_elmat_userzone_login'));

    }

    /**
     * @return SecurityContext
     */
    private function getSecurityContext() {
        return $this->container->get('security.context');
    }

    /**
     * @return UserRepository
     */
    private function getUserRepository() {
        return $this->getDoctrine()->getRepository('CmsElmatBundle:User');
    }
    
    /**
     * @return UserGroupRepository
     */
    private function getUserGroupRepository() {
        return $this->getDoctrine()->getRepository('CmsElmatBundle:UserGroup');
    }


    /**
     * @Template()
     */
    public function loginAction() {


        $request = $this->container->get('request');

        $session = $request->getSession();


        // get the error if any (works with forward and redirect -- see below)
        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(SecurityContext::AUTHENTICATION_ERROR);
        } elseif (null !== $session && $session->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
            $session->remove(SecurityContext::AUTHENTICATION_ERROR);
        } else {
            $error = '';
        }

        if ($error) {

            $error = $error->getMessage();
        }
        // last username entered by the user
        $lastUsername = (null === $session) ? '' : $session->get(SecurityContext::LAST_USERNAME);

        if ($this->getSecurityContext()->isGranted('ROLE_KLIENT')) {

            $refererUri = $request->server->get('HTTP_REFERER');
            $main_uri = $this->container->get('router')->generate('cms_elmat_userzone_main');

            return new RedirectResponse($main_uri);
            // return new RedirectResponse($refererUri && $refererUri != $request->getUri() ? $refererUri : $this->container->get('router')->generate('cms_elmat_userzone_main'));
        }


        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Strona główna", "/");
        $breadcrumbs->addItem("Strefa klienta", $this->get("router")->generate("cms_elmat_userzone_main"));
        $breadcrumbs->addItem("Logowanie", $this->get("router")->generate("cms_elmat_userzone_login"));

        return array(
                'last_username' => $lastUsername,
                'error'         => $error,
        );

    }





    /**
     * @Template()
     */
    public function resetPasswordAction() {
        
        
        if ($this->getSecurityContext()->isGranted('ROLE_KLIENT')) {

            return $this->redirect($this->get('router')->generate('cms_elmat_userzone_main'));
        }


        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Strona główna", "/");
        $breadcrumbs->addItem("Strefa klienta", $this->get("router")->generate("cms_elmat_userzone_main"));
        $breadcrumbs->addItem("Resetowanie hasła", $this->get("router")->generate("cms_elmat_userzone_remind"));

        $form = $this->createForm(new KlientPrzypomnienieHaslaType());

        $request = $this->getRequest();

        if($request->isMethod('post')) {


            $form->bind($request);

           
            
           
            if( $form->isValid() ) {
                
                $form_data = $form->getData();

                
                

                $userManager = $this->getFOSUserManager();
                
                $user = $userManager->findUserByEmail($form_data['email']);
                
                if(!$user) {
                    
                    $form->get('email')->addError(new \Symfony\Component\Form\FormError('Brak konta dla podanego adresu. Sprawdź czy adres został wpisany poprawnie.'));
                    
                } else {
                    
                    
                   
                     
                     $reset_token = $user->getPasswordResetToken();
                     if(!$reset_token) {
                         $reset_token = sha1($user->serialize().time());
                         $user->setPasswordResetToken($reset_token);
                         $userManager->updateUser($user, true);
                     }
                     
                     
                     $zmienne = array(
                         'URL_ZMIANA_HASLA' => $this->generateUrl('cms_elmat_userzone_reset_password_change', 
                                 array('id' => $user->getId(), 'token' => $reset_token), 
                                 \Symfony\Component\Routing\Generator\UrlGeneratorInterface::ABSOLUTE_URL),
                         'IP' => $this->getRequest()->getClientIp()
                     );
                     
                      $template = $this->get('cms_email_templates')->getParsed('resetowanie_hasla_email', $zmienne);


                        $email_do = $user->getEmail();
                        $email_od = $this->get('cms_config')->get('ust_mailer_wiadomosc_od');

                        $message_email = \Swift_Message::newInstance()
                            ->setSubject($template->getTopic())
                            ->setFrom($this->container->getParameter('mailer_user'), $email_od)
                            ->setTo($email_do)
                            ->setBody($template->getContent(), 'text/html')
                            ->addPart(strip_tags($template->getContentTxt()), 'text/plain');


                        try {
                            $ret = $this->get('mailer')->send($message_email);
                            
                            $this->get('session')->getFlashBag()->add('notice', 'Wiadomość o zmianie hasła została wysłana na adres poczty elktronicznej. Prosimy postępować zgodnie z zawartymi w niej informacjami.');
                            
                        } catch(Exception $e) {
                            
                            $this->get('session')->getFlashBag()->add('notice', 'Wiadomość o zmianie hasła nie mogła zostać wysłana prosimy spróbować ponownie później');

                        }
                     
                     
                     
                     
                     

                    return $this->redirect($this->get('router')->generate('cms_elmat_userzone_remind'));
                     
                
//                $change_password = $form->getData();
//
//                $user = $this->get('security.context')->getToken()->getUser();
//
//                $user->setPlainPassword($change_password->getNewPassword());

                

                
                    
                    
                }
               

            }


            

        }

        return array(
                'form' => $form->createView()
        );

    }
    
    /**
     * 
     * @Template()
     */
    public function resetPasswordChangeAction($id, $token) {

        // $this->getSecurityContext()->isGranted('')
        $em = $this->getDoctrine()->getManager();
        $request = $this->getRequest();
        
        $user = $this->getUserRepository()->find($id);
        if(!$user) {
            throw new NotFoundHttpException();
        }


        if(strlen($token) > 10 && $user->getPasswordResetToken() == $token) {
            
            $form = $this->createForm(new KlientZmianaHaslaType(array('without_current_password' => true)));
            
            if($request->isMethod('post')) {
                
                $form->bind($request);
                
                
                
                if( $form->isValid() ) {

                    $userManager = $this->getFOSUserManager();
                    $change_password = $form->getData();
               
                    $user->setPlainPassword($change_password->getNewPassword());
                    $user->setPasswordResetToken(null);
                    $userManager->updateUser($user, true);

                    $this->get('session')->getFlashBag()->add('notice', 'Hasło zostało zmienione');

                    return $this->redirect($this->get('router')->generate('cms_elmat_userzone_login'));

                }

            }
            
            

        } else {
            
            $this->get('session')->getFlashBag()->add('notice', 'Niepoprawny link');
            
            return $this->redirect($this->get('router')->generate('cms_elmat_userzone_remind'));
            
        }


        return array(
            'id' => $user->getId(),
            'token' => $user->getPasswordResetToken(),
            'form' => $form->createView()
        );

        

    }
    
    
    /**
     * @return \FOS\UserBundle\Doctrine\UserManager
     */
    public function getFOSUserManager() {
        $userManager = $this->container->get('fos_user.user_manager');
        return $userManager;
    }

    /**
     * @Template
     */
    public function settingsAction() {

        if (!$this->getSecurityContext()->isGranted('ROLE_KLIENT')) {

            return $this->redirect($this->get('router')->generate('cms_elmat_userzone_login'));
        }


        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Strona główna", "/");
        $breadcrumbs->addItem("Strefa klienta", $this->get("router")->generate("cms_elmat_userzone_main"));
        $breadcrumbs->addItem("Ustawienia", $this->get("router")->generate("cms_elmat_userzone_settings"));

        $form_haslo = $this->createForm(new KlientZmianaHaslaType());

        $request = $this->getRequest();

        if($request->isMethod('post')) {

            if (true
                // dla formularza zmiany hasla
            ) {
                $form_haslo->bind($request);

                $form_data = $request->request->get($form_haslo->getName());

                if(!is_array($form_data)) {
                    return $this->redirect($this->get('router')->generate('cms_elmat_userzone_main'));
                }




                if( $form_haslo->isValid() ) {


                    $userManager = $this->container->get('fos_user.user_manager');
                    $change_password = $form_haslo->getData();

                    $user = $this->get('security.context')->getToken()->getUser();

                    $user->setPlainPassword($change_password->getNewPassword());

                    $userManager->updateUser($user, true);

                    $this->get('session')->getFlashBag()->add('notice', 'Hasło zostało zmienione');

                    return $this->redirect($this->get('router')->generate('cms_elmat_userzone_settings'));

                }


            }

        }

        return array(
                'form_haslo' => $form_haslo->createView()

        );

    }

    public function checkAction()
    {
        throw new \RuntimeException('You must configure the check path to be handled by the firewall using form_login in your security firewall configuration.');
    }

    public function logoutAction()
    {
        throw new \RuntimeException('You must activate the logout in your security firewall configuration.');
    }

    /**
     * @Template()
     */
    public function mainAction() {



        if ($this->getSecurityContext()->isGranted('ROLE_KLIENT')) {

            $szkolenia_uri = $this->get('router')->generate('cms_elmat_userzone_szkolenia');

            return new RedirectResponse($szkolenia_uri);
        }


        return $this->redirect($this->get('router')->generate('cms_elmat_userzone_login'));

        // jeżeli użytkownik nie zalogowany - przekierowanie do strony logowania

    }

    /**
     * @Template()
     */
    public function pricelistsAction() {

        $szkolenia_uri = $this->get('router')->generate('cms_elmat_userzone_szkolenia');

        return new RedirectResponse($szkolenia_uri);
        
        // tylko dla zalogowanych z rolą
        

        if (!$this->getSecurityContext()->isGranted('ROLE_KLIENT')) {

            return $this->redirect($this->get('router')->generate('cms_elmat_userzone_login'));
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Strona główna", "/");
        $breadcrumbs->addItem("Strefa klienta", $this->get("router")->generate("cms_elmat_userzone_main"));
        $breadcrumbs->addItem("Cenniki", $this->get("router")->generate("cms_elmat_userzone_cenniki"));

        $em = $this->getDoctrine()->getManager();

        $cenniki = $em ->createQuery("
                SELECT p
                FROM CmsElmatBundle:Pricelist p
                WHERE p.published = true
                ")
                ->execute();

        $cenniki_access = $this->getSecurityContext()->isGranted('ROLE_KLIENT_CENNIKI');

        return array(
                'cenniki' => $cenniki,
                'cenniki_access' => $cenniki_access,
                );

    }

    public function pricelistDownloadAction($id) {

        // sprawdzenie uprawnień

        if (!$this->getSecurityContext()->isGranted('ROLE_KLIENT_CENNIKI')) {

            return $this->redirect($this->get('router')->generate('cms_elmat_userzone_login'));

        }


        $cennik = $this->getDoctrine()
            ->getRepository('CmsElmatBundle:Pricelist')
            ->createQueryBuilder('p')->where('p.id = :id')
            ->andWhere('p.published = true')
            ->andWhere('p.cennik IS NOT NULL')
            ->getQuery()
            ->setParameters(array('id' => $id))
            ->getSingleResult();

            ;

        if($cennik) {

            $file_path = $cennik->getAbsolutePath('cennik');
            $file_name = $cennik->getCennikFilename();

            $response = new StreamedResponse();
            $response->setCallback(function () use ($file_path) {

                $fp = fopen($file_path, 'rb');
                fpassthru($fp);
            });
            $d = $response->headers->makeDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, $file_name);
            $response->headers->set('Content-Disposition', $d);

            return $response->send();

        } else {

            return $this->redirect($this->get('router')->generate('cms_elmat_userzone_pricelists'));

        }

    }
    
    
    /**
     * @Template
     * @param type $rejestracja_id
     */
    public function szkolenieRejestracjaAction($rejestracja_id) {
        
         if (!$this->getSecurityContext()->isGranted('ROLE_KLIENT')) {

            return $this->redirect($this->get('router')->generate('cms_elmat_userzone_login'));

        }
        
        $user = $this->get('security.context')->getToken()->getUser();
        
        $training_repo = $this->getDoctrine()->getRepository('CmsElmatBundle:TrainingRegistration');
        
        $registration = $training_repo->findOneBy(array(
            'user' => $user->getId(),
            'id' => $rejestracja_id
        ));
        
        if(!$registration ) {
             return $this->redirect($this->get('router')->generate('cms_elmat_userzone_szkolenia'));
        }
        
        $termin = $registration->getTrainingDate();
        $training = $termin->getTraining();
        
        return array(
            'user' => $user,
            'registration' => $registration,
            'termin' => $termin,
            'training' => $training,
        );
        
    }
    
    /**
     * @Template()
     */
    public function szkoleniaAction() {
        
        if (!$this->getSecurityContext()->isGranted('ROLE_KLIENT')) {

            return $this->redirect($this->get('router')->generate('cms_elmat_userzone_login'));

        }
        
          $user = $this->get('security.context')->getToken()->getUser();
         $training_repo = $this->getDoctrine()->getRepository('CmsElmatBundle:TrainingRegistration');
        
        $registrations = $training_repo->findBy(array(
            'user' => $user->getId(),
           
        ));
        
        
        return array(
            'registrations' => $registrations
        );
        
    }



}
