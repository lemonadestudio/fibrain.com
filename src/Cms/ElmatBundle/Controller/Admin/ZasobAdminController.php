<?php

namespace Cms\ElmatBundle\Controller\Admin;

use Sonata\AdminBundle\Controller\CRUDController as Controller;
use Cms\ElmatBundle\Entity\RecycleBin as RecycleBin;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class ZasobAdminController extends Controller
{
    /**
     * Moves item to recycle bin.
     * @param int $id
     * @return array
     * @throws NotFoundHttpException
     * @throws AccessDeniedException
     */
    public function recycleAction() {
        $id = $this->get('request')->get($this->admin->getIdParameter());
        $now = new \DateTime();

        $object = $this->admin->getObject($id);
        $Recycled = new RecycleBin();
        $Recycled->setTitle($object->getTitle());
        $Recycled->setItemId($id);
        $Recycled->setFromEntity('CmsElmatBundle:Zasob');
        $Recycled->setItem('Zasoby');
        $Recycled->setDeletedAt($now);
        
        $object->setDeletedAt($now);
        $this->admin->update($Recycled);
        $this->admin->update($object);
        
        $this->get('session')->setFlash('sonata_flash_success', 'flash_edit_success');

        if (!$object) {
            throw new NotFoundHttpException(sprintf('unable to find the object with id : %s', $id));
        }

        if (false === $this->admin->isGranted('EDIT', $object)) {
            throw new AccessDeniedException();
        }
        
        return $this->redirect($this->admin->generateUrl('list'));
    }
}
