<?php

namespace Cms\ElmatBundle\Controller\Admin;

use Sonata\AdminBundle\Controller\CRUDController as Controller;

class EmailTemplateAdminController extends Controller
{

    public function listAction()
    {
        $this->get('cms_email_templates')->all();

        return parent::listAction();
    }


}
