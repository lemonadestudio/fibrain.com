<?php

namespace Cms\ElmatBundle\Controller\Admin;

use Imagine\Gd\Imagine;
use Imagine\Image\Box;
use Imagine\Image\Point;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Sonata\AdminBundle\Controller\CRUDController as Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class ProductCategoryAdminController extends Controller {

    public function moveUpAction($id = null) {

        $id = $this->get('request')->get($this->admin->getIdParameter());

        $object = $this->admin->getObject($id);

        $product_category_repo = $this->getDoctrine()->getRepository('CmsElmatBundle:ProductCategory');

        if ($object->getLvl() > 0) {
            $product_category_repo->moveUp($object);
            //    $this->get('session')->setFlash('sonata_flash_success', 'pozycję przeniesiono w górę');
        }


        $request = $this->get('request');
        $referer = $request->headers->get('referer');
        if (!$referer) {
            $referer = $this->get('router')->generate('admin_cms_elmat_productcategory_list');
        }

        return new RedirectResponse($referer);


    }

    public function moveDownAction($id = null) {

        $id = $this->get('request')->get($this->admin->getIdParameter());

        $object = $this->admin->getObject($id);

        $product_category_repo = $this->getDoctrine()->getRepository('CmsElmatBundle:ProductCategory');

        if ($object->getLvl() > 0) {
            $product_category_repo->moveDown($object);
            //$this->get('session')->setFlash('sonata_flash_success', 'pozycję przeniesiono w dół');
        }


        $request = $this->get('request');
        $referer = $request->headers->get('referer');
        if (!$referer) {
            $referer = $this->get('router')->generate('admin_cms_elmat_productcategory_list');
        }

        return new RedirectResponse($referer);


    }


    public function kadrujAction($id = null) {

        $id = $this->get('request')->get($this->admin->getIdParameter());

        $object = $this->admin->getObject($id);

        if (!$object) {
            throw new NotFoundHttpException(sprintf('unable to find the object with id : %s', $id));
        }

        if (false === $this->admin->isGranted('EDIT', $object)) {
            throw new AccessDeniedException();
        }

        $this->admin->setSubject($object);

        $form = $this->admin->getForm();
        $form->setData($object);

        $crop = $this->getRequest()->request->get('crop');

        $imagine = new Imagine();

        try {


            if ($object->getAbsolutePath('image')) {

                $imagine->open($object->getAbsolutePath('image'));
                $file = new File($object->getAbsolutePath('image'));

                $old_name = $object->getImage();
                $new_name = 'image-' . uniqid() . '.' . $file->guessExtension();

                $file->move($object->getUploadRootDir(), $new_name);
                $object->setImage($new_name);
                $this->getDoctrine()->getManager()->flush();
                @unlink($object->getUploadRootDir() . '/crop-' . $old_name);

                // zapisanie nowego cropa
                $imagine->open($object->getAbsolutePath('image'))
                    ->crop(new Point($crop['x'], $crop['y']), new Box($crop['w'], $crop['h']))
                    ->save($object->getUploadRootDir() . '/crop-' . $object->getImage(), array('quality' => 98));

            }

        } catch (Exception $e) {

            return $this->render('CmsElmatBundle:Admin/ProductZdjecie:kadruj_blad.html.twig', array(
                'error' => $e->getMessage(),
            ));
        }


        return $this->render('CmsElmatBundle:Admin/ProductZdjecie:kadruj_sukces.html.twig', array(
            'object' => $object,
        ));
    }

}
