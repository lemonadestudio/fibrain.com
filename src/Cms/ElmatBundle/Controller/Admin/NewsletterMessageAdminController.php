<?php

namespace Cms\ElmatBundle\Controller\Admin;

use Sonata\AdminBundle\Controller\CRUDController as Controller;
use Cms\ElmatBundle\Entity\NewsletterMessageOfferType;
use Cms\ElmatBundle\Entity\NewsletterSubscriber;
use Cms\ElmatBundle\Entity\NewsletterMessageRecipient;
use Cms\ElmatBundle\Model\NewsletterMessageRecipientStatus;
use Symfony\Component\HttpFoundation\Response;

class NewsletterMessageAdminController extends Controller
{
    public function createAction()
    {
        // the key used to lookup the template
        $templateKey = 'edit';

        if (false === $this->admin->isGranted('CREATE')) {
            throw new AccessDeniedException();
        }

        $object = $this->admin->getNewInstance();

        $this->admin->setSubject($object);

        /** @var $form \Symfony\Component\Form\Form */
        $form = $this->admin->getForm();
        $form->setData($object);

        if ($this->get('request')->getMethod() == 'POST') {
            $form->bind($this->get('request'));

            $isFormValid = $form->isValid();

            // persist if the form was valid and if in preview mode the preview was approved
            if ($isFormValid && (!$this->isInPreviewMode() || $this->isPreviewApproved())) {
                $this->setOfferTypes($object, $form);
                $this->addRecipients($object);
                $this->admin->create($object);
                
                if ($this->isXmlHttpRequest()) {
                    return $this->renderJson(array(
                        'result' => 'ok',
                        'objectId' => $this->admin->getNormalizedIdentifier($object)
                    ));
                }

                $this->get('session')->setFlash('sonata_flash_success','flash_create_success');
                // redirect to edit mode
                return $this->redirectTo($object);
            }

            // show an error message if the form failed validation
            if (!$isFormValid) {
                if (!$this->isXmlHttpRequest()) {
                    $this->get('session')->setFlash('sonata_flash_error', 'flash_create_error');
                }
            } elseif ($this->isPreviewRequested()) {
                // pick the preview template if the form was valid and preview was requested
                $templateKey = 'preview';
            }
        }

        $view = $form->createView();

        // set the theme for the current Admin Form
        $this->get('twig')->getExtension('form')->renderer->setTheme($view, $this->admin->getFormTheme());

        return $this->render($this->admin->getTemplate($templateKey), array(
            'action' => 'create',
            'form'   => $view,
            'object' => $object,
        ));
    }
    
    private function getNewsletterSubscriberRepository() {
        return $this->getDoctrine()->getRepository('CmsElmatBundle:NewsletterSubscriber');
    }
    
    /*
     * Oferty uzytkownika sa strasznie (delikatnie powiedziane) zrobione. Kazdy rodzaj 
     * to oddzielna kolumna w tabeli user. Dobrym pomyslem byla by oddzielna tabela
     * na oferty i inne kategorie oraz odpowiednia relacja.
     */
    private function setOfferTypes($newsletterMessage, $form) {
        if (!$newsletterMessage->getSendOnlyToSubscribersWithAccount()) {
            return;
        }
        
        $allRequestData = $this->get('request')->request->all();
        $postData = $allRequestData[$form->getName()];
        $offerTypeIds = array_key_exists('offerTypeIds', $postData) ? $postData['offerTypeIds'] : array();
        
        foreach ($offerTypeIds as $offerTypeId) {
            $offerType = new NewsletterMessageOfferType();
            $offerType->setType(intval($offerTypeId));
            $offerType->setNewsletterMessage($newsletterMessage);
            $newsletterMessage->addOfferType($offerType);
        }
    }
    
    /*
     * Metoda najpierw pobiera wymagane id subskrybentow, a nastepnie dodaje je
     * do odbiorcow newslettera.
     * Problem w tym, ze doctrine nie obsluguje batch insert, wiec wykonuje
     * wszystkie inserty oddzielnie (przy aktualnej bazie np. 5000 zapytan).
     * Aktualnie nie sprawia to problemow, ale byc moze bedzie potrzebna optymalizacja, np.:
     * - batch insert jako plain sql
     * - wykonanie zapytan w oddzielnym skrypcie crona
     * - rozbicie dodawania odbiorcow na mniejsze "paczki"
     * - procedura skladowana
     */
    private function addRecipients($newsletterMessage) {
        $subscriberIds = $this->getNewsletterSubscriberRepository()
            ->getSubscriberIds(
                $newsletterMessage->getRecipientEmailsList(),
                $newsletterMessage->getSendOnlyToSubscribersWithAccount(),
                $newsletterMessage->getOfferTypes());

        $em = $this->getDoctrine()->getManager();
        
        foreach ($subscriberIds as $subscriberId) {
            $recipient = new NewsletterMessageRecipient();
            $recipient->setStatus(NewsletterMessageRecipientStatus::READY_TO_SEND);
            $recipient->setNewsletterMessage($newsletterMessage);
            $recipient->setNewsletterSubscriber($em->getReference('Cms\ElmatBundle\Entity\NewsletterSubscriber', $subscriberId));
            $newsletterMessage->addRecipient($recipient);
        }
    }
}
