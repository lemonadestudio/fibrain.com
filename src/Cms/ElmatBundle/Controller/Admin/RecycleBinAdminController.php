<?php

namespace Cms\ElmatBundle\Controller\Admin;

use Sonata\AdminBundle\Controller\CRUDController as Controller;
use Imagine\Gd\Imagine;
use Imagine\Image\Box;
use Imagine\Image\Point;
use Cms\ElmatBundle\Entity\Product as Product;
use Cms\ElmatBundle\Entity\ProductTab;
use Cms\ElmatBundle\Form\Type\ProductPlikiType;
use Cms\ElmatBundle\Form\Type\ProductZdjeciaType;
use Cms\ElmatBundle\Form\Type\ProductPowiazaneType;
use Cms\ElmatBundle\Form\Type\ProductRowsType;
use Cms\ElmatBundle\Form\Type\ProductTabsType;
use Cms\ElmatBundle\Form\Type\ProductRowsListType;
use Cms\ElmatBundle\Form\Type\ProductsBasedOnType;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Cms\ElmatBundle\Admin\RecycleBinAdmin as RecycleAdmin;

class RecycleBinAdminController extends Controller
{
        
    /**
     * 
     * @return object
     */
    public function restoreAction() {
        $templateKey = 'restore';

        $id = $this->get('request')->get($this->admin->getIdParameter());
        
        $em = $this->admin->getModelManager();
        $em->getEntityManager($this->admin->getClass())->getFilters()->disable('softdeleteable');
        
        $object = $em->getEntityManager($this->admin->getClass())->getRepository($this->admin->getClass())->find($id);
        $relatedRepo = $object->getFromEntity();
        $Related = $em->getEntityManager($relatedRepo)->getRepository($relatedRepo)->find($object->getItemId());
        
        $emRelated = $this->admin->getModelManager()->getEntityManager('CmsElmatBundle:Product');
        $Related->setDeletedAt(null);
        
        $this->admin->delete($object);
        $this->get('session')->setFlash('sonata_flash_success', 'flash_edit_success');
        
        return $this->redirect($this->admin->generateUrl('list'));
    }
    
}