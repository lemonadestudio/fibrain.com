<?php

namespace Cms\ElmatBundle\Controller\Admin;

use Symfony\Component\HttpFoundation\RedirectResponse;

use Cms\ElmatBundle\Form\Type\GaleriaZdjeciaType;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use Sonata\AdminBundle\Controller\CRUDController as Controller;

class GaleriaAdminController extends Controller
{
	public function editZdjeciaAction($id = null) {
	
		$id = $this->get('request')->get('id');
	
		$galeria = $this->getDoctrine()->getRepository('CmsElmatBundle:Galeria')->find($id);
	
		if(!$galeria) {
			throw new NotFoundHttpException();
		}
	
		if (false === $this->admin->isGranted('EDIT', $galeria)) {
			throw new AccessDeniedException();
		}
	
	
		foreach($galeria->getZdjecia() as $zdjecie) {
			$zdjecie->setUpdatedAt(new \DateTime('now'));
		}

		$form = $this->createForm(new GaleriaZdjeciaType(), $galeria);
	
	
		if ($this->get('request')->getMethod() == 'POST') {
			$form->bindRequest($this->get('request'));
	
	
	
			if ($form->isValid()) {
	
				$zdjecia_form = $form->get('zdjecia');
	
	
	
				//$form->get
				foreach($zdjecia_form->getChildren() as $zdjecie_form) {
					if ( get_class($zdjecie_form->getData()) == 'Cms\ElmatBundle\Entity\GaleriaZdjecie' ) {
						if ( $zdjecie_form['_delete']->getData() == true) {
							$this->getDoctrine()->getManager()->remove($zdjecie_form->getData());
						}
					}
				}
	
	
				$this->getDoctrine()->getManager()->flush();
				$this->get('session')->setFlash('sonata_flash_success', 'flash_edit_success');
	
				if ($this->isXmlHttpRequest()) {
					return $this->renderJson(array(
							'result'    => 'ok',
							'objectId'  => $galeria->getId()
					));
				}
	
				// przekierowanie
	
				$url = false;
	
				if ($this->get('request')->get('btn_update_and_list')) {
					$url = $this->admin->generateUrl('list');
				}
	
				if (!$url) {
					$url = $this->admin->generateObjectUrl('edit_zdjecia', $galeria);
				}
	
				return new RedirectResponse($url);
	
	
			}
	
			$this->get('session')->setFlash('sonata_flash_error', 'flash_edit_error');
		}
	
	
	
		// set the theme for the current Admin Form
		//  $this->get('twig')->getExtension('form')->setTheme($view, $this->admin->getFormTheme());
	
	
		$view = $form->createView();
	
	
		$response = $this->render('CmsElmatBundle:Admin/Galeria:edit_zdjecia.html.twig', array(
				'action' => 'edit_zdjecia',
				'form'   => $view,
				'object' => $galeria,
		));
	
		return $response;
	
	}
   
    
}
