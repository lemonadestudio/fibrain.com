<?php

namespace Cms\ElmatBundle\Controller\Admin;

use Cms\ElmatBundle\Form\Type\ArticlePlikiType;
use Cms\ElmatBundle\Form\Type\ArticleTabsType;
use Imagine\Gd\Imagine;
use Imagine\Image\Point;
use Imagine\Image\Box;
use Sonata\AdminBundle\Controller\CRUDController as Controller;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Cms\ElmatBundle\Entity\RecycleBin;

class ArticleAdminController extends Controller {

    public function editPlikiAction() {
        $id = $this->get('request')->get($this->admin->getIdParameter());

        $object = $this->admin->getObject($id);

        if (!$object) {
            throw new NotFoundHttpException();
        }

        if (false === $this->admin->isGranted('EDIT', $object)) {
            throw new AccessDeniedException();
        }

        foreach ($object->getPliki() as $_obj) {
            $_obj->setUpdated(new \DateTime('now'));
        }

        $form = $this->createForm(new ArticlePlikiType(), $object);

        if ($this->get('request')->getMethod() == 'POST') {
            $form->bind($this->get('request'));

            if ($form->isValid()) {
                $pliki_form = $form->get('pliki');

                foreach ($pliki_form->getChildren() as $plik_form) {
                    if (get_class($plik_form->getData()) == 'Cms\ElmatBundle\Entity\Plik') {
                        if ($plik_form['_delete']->getData() == true) {
                            $this->getDoctrine()->getManager()->remove($plik_form->getData());
                        }
                    }
                }

                $this->getDoctrine()->getManager()->flush();
                $this->get('session')->setFlash('sonata_flash_success', 'flash_edit_success');

                if ($this->isXmlHttpRequest()) {
                    return $this->renderJson(array(
                        'result' => 'ok',
                        'objectId' => $object->getId()
                    ));
                }

                // przekierowanie

                $url = false;

                if ($this->get('request')->get('btn_update_and_list')) {
                    $url = $this->admin->generateUrl('list');
                }

                if (!$url) {
                    $url = $this->admin->generateObjectUrl('edit_pliki', $object);
                }

                return new RedirectResponse($url);
            }

            $this->get('session')->setFlash('sonata_flash_error', 'flash_edit_error');
        }

        $view = $form->createView();

        return $this->render('CmsElmatBundle:Admin/Pliki:edit_pliki.html.twig', array(
            'preview_title' => 'Podgląd artykułu',
            'action' => 'edit_pliki',
            'form' => $view,
            'object' => $object,
        ));
    }

    public function editTabsAction() {
        // the key used to lookup the template
        $templateKey = 'edit_tabs';

        $id = $this->get('request')->get($this->admin->getIdParameter());

        $object = $this->admin->getObject($id);

        if (!$object) {
            throw new NotFoundHttpException(sprintf('unable to find the object with id : %s', $id));
        }

        if (false === $this->admin->isGranted('EDIT', $object)) {
            throw new AccessDeniedException();
        }

        $this->admin->setSubject($object);

        foreach ($object->getTabs() as $row) {
            $row->setUpdated(new \DateTime('now'));
        }

        $form = $this->createForm(new ArticleTabsType($this->admin), $object);

        if ($this->get('request')->getMethod() == 'POST') {
            $form->bind($this->get('request'));

            $isFormValid = $form->isValid();

            // persist if the form was valid and if in preview mode the preview was approved
            if ($isFormValid && (!$this->isInPreviewMode() || $this->isPreviewApproved())) {
                $rows_form = $form->get('tabs');

                foreach ($rows_form->getChildren() as $row) {
                    if (get_class($row->getData()) == 'Cms\ElmatBundle\Entity\ContentTab') {
                        if ($row['_delete']->getData() == true) {
                            $this->getDoctrine()->getManager()->remove($row->getData());
                        }
                    }
                }

                $this->getDoctrine()->getManager()->flush();

                $this->admin->update($object);
                $this->get('session')->setFlash('sonata_flash_success', 'flash_edit_success');

                if ($this->isXmlHttpRequest()) {
                    return $this->renderJson(array(
                        'result' => 'ok',
                        'objectId' => $this->admin->getNormalizedIdentifier($object)
                    ));
                }

                // redirect to edit mode

                return $this->redirect($this->admin->generateObjectUrl('edit_tabs', $object));
            }

            // show an error message if the form failed validation
            if (!$isFormValid) {
                if (!$this->isXmlHttpRequest()) {
                    $this->get('session')->setFlash('sonata_flash_error', 'flash_edit_error');
                }
            } elseif ($this->isPreviewRequested()) {
                // enable the preview template if the form was valid and preview was requested
                $templateKey = 'preview';
            }
        }

        $view = $form->createView();

        // set the theme for the current Admin Form
        $this->get('twig')->getExtension('form')->renderer->setTheme($view, $this->admin->getFormTheme());

        return $this->render($this->admin->getTemplate($templateKey), array(
            'action' => 'edit_tabs',
            'form' => $view,
            'object' => $object,
        ));
    }

    public function kadrujAction() {
        $id = $this->get('request')->get($this->admin->getIdParameter());

        $object = $this->admin->getObject($id);

        if (!$object) {
            throw new NotFoundHttpException(sprintf('unable to find the object with id : %s', $id));
        }

        if (false === $this->admin->isGranted('EDIT', $object)) {
            throw new AccessDeniedException();
        }

        $this->admin->setSubject($object);

        $form = $this->admin->getForm();
        $form->setData($object);

        $crop = $this->getRequest()->request->get('crop');

        $imagine = new Imagine();

        try {
            if ($object->getAbsolutePath('image')) {
                $imagine->open($object->getAbsolutePath('image'));
                $file = new File($object->getAbsolutePath('image'));

                $old_name = $object->getImage();
                $new_name = 'image-' . uniqid() . '.' . $file->guessExtension();

                $file->move($object->getUploadRootDir(), $new_name);
                $object->setImage($new_name);
                $this->getDoctrine()->getManager()->flush();
                @unlink($object->getUploadRootDir() . '/crop-' . $old_name);

                // zapisanie nowego cropa
                $imagine->open($object->getAbsolutePath('image'))
                    ->crop(new Point($crop['x'], $crop['y']), new Box($crop['w'], $crop['h']))
                    ->save($object->getUploadRootDir() . '/crop-' . $object->getImage(), array('quality' => 98));
            }
        } catch (Exception $e) {
            return $this->render('CmsElmatBundle:Admin/ProductZdjecie:kadruj_blad.html.twig', array(
                'error' => $e->getMessage(),
            ));
        }


        return $this->render('CmsElmatBundle:Admin/ProductZdjecie:kadruj_sukces.html.twig', array(
            'object' => $object,
        ));
    }

    public function previewAction() {
        $id = $this->get('request')->get($this->admin->getIdParameter());

        $object = $this->admin->getObject($id);

        if (!$object) {
            throw new NotFoundHttpException(sprintf('unable to find the object with id : %s', $id));
        }

        return $this->render('CmsElmatBundle:Admin/Article:preview.html.twig', array(
            'page' => $object
        ));
    }

    /**
     * Moves item to recycle bin.
     * @param int $id
     * @return array
     * @throws NotFoundHttpException
     * @throws AccessDeniedException
     */
    public function recycleAction() {
        $id = $this->get('request')->get($this->admin->getIdParameter());
        $now = new \DateTime();

        $object = $this->admin->getObject($id);
        $Recycled = new RecycleBin();
        $Recycled->setTitle($object->getTitle());
        $Recycled->setItemId($id);
        $Recycled->setFromEntity('CmsElmatBundle:Article');
        $Recycled->setItem('Artykuły');
        $Recycled->setDeletedAt($now);
        
        $this->admin->update($Recycled);
        $this->admin->update($object);
        
        $this->get('session')->setFlash('sonata_flash_success', 'flash_edit_success');

        if (!$object) {
            throw new NotFoundHttpException(sprintf('unable to find the object with id : %s', $id));
        }

        if (false === $this->admin->isGranted('EDIT', $object)) {
            throw new AccessDeniedException();
        }
        
        return $this->redirect($this->admin->generateUrl('list'));
    }
}
