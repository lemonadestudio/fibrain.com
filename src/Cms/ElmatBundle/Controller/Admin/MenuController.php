<?php

namespace Cms\ElmatBundle\Controller\Admin;

use Cms\ElmatBundle\Menu\Builder;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class MenuController extends Controller
{

	/**
	 * Renders managed CMS menu
	 */
    public function managedAction($location = '')
    {
        $menu = new Builder();
        $response = $this->render('CmsElmatBundle:Menu:managed.html.twig', array('location' => $location));
        
        $menu_cfg = $this->container->getParameter('cms_elmat.menu');
        
        $cache_time = intval($menu_cfg['locations'][$location]['cache']);
     
//         $response->setSharedMaxAge($cache_time);
//         $response->setMaxAge($cache_time);
               
        return $response;
        
    }

}
