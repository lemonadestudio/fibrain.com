<?php

namespace Cms\ElmatBundle\Controller\Admin;

use Cms\ElmatBundle\Entity\Product;
use Imagine\Gd\Imagine;
use Imagine\Image\Box;
use Imagine\Image\Point;
use Cms\ElmatBundle\Form\Type\ProductPlikiType;
use Cms\ElmatBundle\Form\Type\ProductZdjeciaType;
use Cms\ElmatBundle\Form\Type\ProductPowiazaneType;
use Cms\ElmatBundle\Entity\ProductTab;
use Cms\ElmatBundle\Form\Type\ProductRowsType;
use Cms\ElmatBundle\Form\Type\ProductTabsType;
use Cms\ElmatBundle\Form\Type\ProductRowsListType;
use Cms\ElmatBundle\Form\Type\ProductsBasedOnType;
use Sonata\AdminBundle\Controller\CRUDController as Controller;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Cms\ElmatBundle\Entity\RecycleBin as RecycleBin;

class ProductAdminController extends Controller {

    public function editAction($id = null) {
        // the key used to lookup the template
        $templateKey = 'edit';

        $id = $this->get('request')->get($this->admin->getIdParameter());

        $object = $this->admin->getObject($id);

        if ($object->getBaseProduct()) {
            return $this->redirectTo($object->getBaseProduct());
        }

        if (!$object) {
            throw new NotFoundHttpException(sprintf('unable to find the object with id : %s', $id));
        }

        if (false === $this->admin->isGranted('EDIT', $object)) {
            throw new AccessDeniedException();
        }

        $this->admin->setSubject($object);

        /** @var $form \Symfony\Component\Form\Form */
        $form = $this->admin->getForm();
        $form->setData($object);

        if ($this->get('request')->getMethod() == 'POST') {
            $form->bind($this->get('request'));

            $isFormValid = $form->isValid();

            // persist if the form was valid and if in preview mode the preview was approved
            if ($isFormValid && (!$this->isInPreviewMode() || $this->isPreviewApproved())) {

                foreach ($object->getProductsBasedOn() as $prod) {
                    $prod->setTitle($object->getTitle());
                    $prod->setPublishDate($object->getPublishDate());
                    $prod->setPublished($object->getPublished());
                }

                $this->admin->update($object);
                $this->get('session')->setFlash('sonata_flash_success', 'flash_edit_success');

                if ($this->isXmlHttpRequest()) {
                    return $this->renderJson(array(
                        'result' => 'ok',
                        'objectId' => $this->admin->getNormalizedIdentifier($object)
                    ));
                }

                // redirect to edit mode
                return $this->redirectTo($object);
            }

            // show an error message if the form failed validation
            if (!$isFormValid) {
                if (!$this->isXmlHttpRequest()) {
                    $this->get('session')->setFlash('sonata_flash_error', 'flash_edit_error');
                }
            } elseif ($this->isPreviewRequested()) {
                // enable the preview template if the form was valid and preview was requested
                $templateKey = 'preview';
            }
        }

        $view = $form->createView();

        // set the theme for the current Admin Form
        $this->get('twig')->getExtension('form')->renderer->setTheme($view, $this->admin->getFormTheme());

        return $this->render($this->admin->getTemplate($templateKey), array(
            'action' => 'edit',
            'form' => $view,
            'object' => $object,
        ));
    }

    public function editTabsAction() {
        // the key used to lookup the template
        $templateKey = 'edit_tabs';

        $id = $this->get('request')->get($this->admin->getIdParameter());

        $object = $this->admin->getObject($id);

        if (!$object) {
            throw new NotFoundHttpException(sprintf('unable to find the object with id : %s', $id));
        }

        if (false === $this->admin->isGranted('EDIT', $object)) {
            throw new AccessDeniedException();
        }

        $tab_cechy = null;
        $tab_dane = null;
        $tab_zastosowania = null;
        $tab_podobne_produkty = null;

        $tabs = $object->getTabs();
        $em = $this->getDoctrine()->getManager();

        foreach ($object->getTabs() as $tab) {
            if ($tab->getName() == 'Cechy') {
                $tab_cechy = $tab;
            }
            if ($tab->getName() == 'Dane techniczne') {
                $tab_dane = $tab;
            }
            if ($tab->getName() == 'Zastosowania') {
                $tab_zastosowania = $tab;
            }
            if ($tab->getName() == 'Produkty podobne') {
                $tab_podobne_produkty = $tab;
            }
        }

        if (!$tab_cechy && $tabs->count() < 4) {
            $tab_cechy = new ProductTab();
            $tab_cechy->setName('Cechy');
            $tab_cechy->setSort(1);
            $tab_cechy->setType(ProductTab::TYPE_normalny);
            $tab_cechy->setProduct($object);
            $tabs->add($tab_cechy);
            $em->persist($tab_cechy);
        }

        if (!$tab_dane && $tabs->count() < 4) {
            $tab_dane = new ProductTab();
            $tab_dane->setName('Dane techniczne');
            $tab_dane->setSort(2);
            $tab_dane->setType(ProductTab::TYPE_normalny);
            $tab_dane->setProduct($object);
            $tabs->add($tab_dane);
            $em->persist($tab_dane);
        }

        if (!$tab_zastosowania && $tabs->count() < 4) {
            $tab_zastosowania = new ProductTab();
            $tab_zastosowania->setName('Zastosowania');
            $tab_zastosowania->setSort(3);
            $tab_zastosowania->setType(ProductTab::TYPE_normalny);
            $tab_zastosowania->setProduct($object);
            $tabs->add($tab_zastosowania);
            $em->persist($tab_zastosowania);
        }

        if (!$tab_podobne_produkty && $tabs->count() < 4) {
            $tab_podobne_produkty = new ProductTab();
            $tab_podobne_produkty->setName('Podobne produkty');
            $tab_podobne_produkty->setSort(4);
            $tab_podobne_produkty->setType(ProductTab::TYPE_produkty_powiazane);
            $tab_podobne_produkty->setProduct($object);
            $tabs->add($tab_podobne_produkty);
            $em->persist($tab_podobne_produkty);
        }

        $em->flush();

        $this->admin->setSubject($object);

        foreach ($object->getTabs() as $row) {
            $row->setUpdated(new \DateTime('now'));
        }

        $form = $this->createForm(new ProductTabsType($this->admin), $object);

        if ($this->get('request')->getMethod() == 'POST') {
            $form->bind($this->get('request'));

            $isFormValid = $form->isValid();

            // persist if the form was valid and if in preview mode the preview was approved
            if ($isFormValid && (!$this->isInPreviewMode() || $this->isPreviewApproved())) {
                $rows_form = $form->get('tabs');

                foreach ($rows_form->getChildren() as $row) {
                    if (get_class($row->getData()) == 'Cms\ElmatBundle\Entity\ProductTab') {
                        if ($row['_delete']->getData() == true) {
                            $this->getDoctrine()->getManager()->remove($row->getData());
                        }
                    }
                }

                $this->getDoctrine()->getManager()->flush();

                $this->admin->update($object);
                $this->get('session')->setFlash('sonata_flash_success', 'flash_edit_success');

                if ($this->isXmlHttpRequest()) {
                    return $this->renderJson(array(
                        'result' => 'ok',
                        'objectId' => $this->admin->getNormalizedIdentifier($object)
                    ));
                }

                // redirect to edit mode

                return $this->redirect($this->admin->generateObjectUrl('edit_tabs', $object));
            }

            // show an error message if the form failed validation
            if (!$isFormValid) {
                if (!$this->isXmlHttpRequest()) {
                    $this->get('session')->setFlash('sonata_flash_error', 'flash_edit_error');
                }
            } elseif ($this->isPreviewRequested()) {
                // enable the preview template if the form was valid and preview was requested
                $templateKey = 'preview';
            }
        }

        $view = $form->createView();

        // set the theme for the current Admin Form
        $this->get('twig')->getExtension('form')->renderer->setTheme($view, $this->admin->getFormTheme());

        return $this->render($this->admin->getTemplate($templateKey), array(
            'action' => 'edit_tabs',
            'form' => $view,
            'object' => $object,
        ));
    }

    public function editZdjeciaAction() {

        $id = $this->get('request')->get($this->admin->getIdParameter());

        $object = $this->admin->getObject($id);

        if (!$object) {
            throw new NotFoundHttpException();
        }

        if (false === $this->admin->isGranted('EDIT', $object)) {
            throw new AccessDeniedException();
        }

        foreach ($object->getZdjecia() as $zdjecie) {
            $zdjecie->setUpdatedAt(new \DateTime('now'));
        }

        $form = $this->createForm(new ProductZdjeciaType(), $object);

        if ($this->get('request')->getMethod() == 'POST') {
            $form->bind($this->get('request'));

            if ($form->isValid()) {
                $zdjecia_form = $form->get('zdjecia');

                foreach ($zdjecia_form->getChildren() as $zdjecie_form) {
                    if (get_class($zdjecie_form->getData()) == 'Cms\ElmatBundle\Entity\ProductZdjecie') {
                        if ($zdjecie_form['_delete']->getData() == true) {
                            $this->getDoctrine()->getManager()->remove($zdjecie_form->getData());
                        }
                    }
                }

                $this->getDoctrine()->getManager()->flush();
                $this->get('session')->setFlash('sonata_flash_success', 'flash_edit_success');

                if ($this->isXmlHttpRequest()) {
                    return $this->renderJson(array(
                        'result' => 'ok',
                        'objectId' => $object->getId()
                    ));
                }

                // przekierowanie

                $url = false;

                if ($this->get('request')->get('btn_update_and_list')) {
                    $url = $this->admin->generateUrl('list');
                }

                if (!$url) {
                    $url = $this->admin->generateObjectUrl('edit_zdjecia', $object);
                }

                return new RedirectResponse($url);
            }

            $this->get('session')->setFlash('sonata_flash_error', 'flash_edit_error');
        }

        $view = $form->createView();

        return $this->render('CmsElmatBundle:Admin/Product:edit_zdjecia.html.twig', array(
            'action' => 'edit_zdjecia',
            'form' => $view,
            'object' => $object,
        ));
    }

    public function editPowiazaneAction() {

        $id = $this->get('request')->get($this->admin->getIdParameter());

        $object = $this->admin->getObject($id);

        if (!$object) {
            throw new NotFoundHttpException();
        }

        if (false === $this->admin->isGranted('EDIT', $object)) {
            throw new AccessDeniedException();
        }

        $product_powiazane_type = new ProductPowiazaneType();
        $product_powiazane_type->setContainer($this->container);

        $form = $this->createForm($product_powiazane_type, $object);

        if ($this->get('request')->getMethod() == 'POST') {
            $form->bind($this->get('request'));

            if ($form->isValid()) {
                $powiazane_form = $form->get('powiazane');

                //$form->get
                foreach ($powiazane_form->getChildren() as $powiazany_form) {
                    if (get_class($powiazany_form->getData()) == 'Cms\ElmatBundle\Entity\ProductPowiazany') {
                        if ($powiazany_form['_delete']->getData() == true) {
                            $this->getDoctrine()->getManager()->remove($powiazany_form->getData());
                        }
                    }
                }

                $this->getDoctrine()->getManager()->flush();
                $this->get('session')->setFlash('sonata_flash_success', 'flash_edit_success');

                if ($this->isXmlHttpRequest()) {
                    return $this->renderJson(array(
                        'result' => 'ok',
                        'objectId' => $object->getId()
                    ));
                }

                // przekierowanie

                $url = false;

                if ($this->get('request')->get('btn_update_and_list')) {
                    $url = $this->admin->generateUrl('list');
                }

                if (!$url) {
                    $url = $this->admin->generateObjectUrl('edit_powiazane', $object);
                }

                return new RedirectResponse($url);
            }

            $this->get('session')->setFlash('sonata_flash_error', 'flash_edit_error');
        }

        $view = $form->createView();

        return $this->render('CmsElmatBundle:Admin/Product:edit_powiazane.html.twig', array(
            'action' => 'edit_powiazane',
            'form' => $view,
            'object' => $object,
        ));
    }

    public function editPlikiAction() {

        $id = $this->get('request')->get($this->admin->getIdParameter());

        $object = $this->admin->getObject($id);

        if (!$object) {
            throw new NotFoundHttpException();
        }

        if (false === $this->admin->isGranted('EDIT', $object)) {
            throw new AccessDeniedException();
        }


        foreach ($object->getPliki() as $_obj) {
            $_obj->setUpdated(new \DateTime('now'));
        }

        $form = $this->createForm(new ProductPlikiType(), $object);

        if ($this->get('request')->getMethod() == 'POST') {
            $form->bind($this->get('request'));

            if ($form->isValid()) {
                $pliki_form = $form->get('pliki');

                foreach ($pliki_form->getChildren() as $plik_form) {
                    if (get_class($plik_form->getData()) == 'Cms\ElmatBundle\Entity\Plik') {
                        if ($plik_form['_delete']->getData() == true) {
                            $this->getDoctrine()->getManager()->remove($plik_form->getData());
                        }
                    }
                }

                $this->getDoctrine()->getManager()->flush();
                $this->get('session')->setFlash('sonata_flash_success', 'flash_edit_success');

                if ($this->isXmlHttpRequest()) {
                    return $this->renderJson(array(
                        'result' => 'ok',
                        'objectId' => $object->getId()
                    ));
                }

                // przekierowanie

                $url = false;

                if ($this->get('request')->get('btn_update_and_list')) {
                    $url = $this->admin->generateUrl('list');
                }

                if (!$url) {
                    $url = $this->admin->generateObjectUrl('edit_pliki', $object);
                }

                return new RedirectResponse($url);
            }

            $this->get('session')->setFlash('sonata_flash_error', 'flash_edit_error');
        }

        $view = $form->createView();

        return $this->render('CmsElmatBundle:Admin/Product:edit_pliki.html.twig', array(
            'action' => 'edit_pliki',
            'form' => $view,
            'object' => $object,
        ));
    }

    public function editBasedOnAction() {

        $id = $this->get('request')->get($this->admin->getIdParameter());

        $object = $this->admin->getObject($id);

        if (!$object) {
            throw new NotFoundHttpException();
        }

        if (false === $this->admin->isGranted('EDIT', $object)) {
            throw new AccessDeniedException();
        }

        $this->admin->setSubject($object);

        $form = $this->createForm(new ProductsBasedOnType($this->admin), $object);

        if ($this->get('request')->getMethod() == 'POST') {
            $form->bind($this->get('request'));

            if ($form->isValid()) {
                $products_form = $form->get('products_based_on');

                foreach ($products_form->getChildren() as $product_form) {
                    $sub_prod = $product_form->getData();
                    if (get_class($sub_prod) == 'Cms\ElmatBundle\Entity\Product') {
                        if ($product_form['_delete']->getData() == true) {
                            $this->getDoctrine()->getManager()->remove($product_form->getData());
                        }
                        $sub_prod->setTitle($object->getTitle());
                        $sub_prod->setBaseProduct($object);
                    }
                }

                $this->getDoctrine()->getManager()->flush();
                $this->get('session')->setFlash('sonata_flash_success', 'flash_edit_success');

                if ($this->isXmlHttpRequest()) {
                    return $this->renderJson(array(
                        'result' => 'ok',
                        'objectId' => $object->getId()
                    ));
                }

                // przekierowanie

                $url = false;

                if ($this->get('request')->get('btn_update_and_list')) {
                    $url = $this->admin->generateUrl('list');
                }

                if (!$url) {
                    $url = $this->admin->generateObjectUrl('edit_based_on', $object);
                }

                return new RedirectResponse($url);
            }

            $this->get('session')->setFlash('sonata_flash_error', 'flash_edit_error');
        }

        $view = $form->createView();

        return $this->render('CmsElmatBundle:Admin/Product:edit_based_on.html.twig', array(
            'action' => 'edit_based_on',
            'form' => $view,
            'object' => $object,
        ));
    }

    public function editListAction() {
        // the key used to lookup the template
        $templateKey = 'edit_list';

        $id = $this->get('request')->get($this->admin->getIdParameter());

        $object = $this->admin->getObject($id);

        if (!$object) {
            throw new NotFoundHttpException(sprintf('unable to find the object with id : %s', $id));
        }

        if (false === $this->admin->isGranted('EDIT', $object)) {
            throw new AccessDeniedException();
        }

        $this->admin->setSubject($object);

        foreach ($object->getRows() as $row) {
            $row->setUpdated(new \DateTime('now'));
        }

        $form = $this->createForm(new ProductRowsListType($this->admin), $object);

        if ($this->get('request')->getMethod() == 'POST') {
            $form->bind($this->get('request'));

            $isFormValid = $form->isValid();

            // persist if the form was valid and if in preview mode the preview was approved
            if ($isFormValid && (!$this->isInPreviewMode() || $this->isPreviewApproved())) {
                $rows_form = $form->get('rows');

                foreach ($rows_form->getChildren() as $row) {
                    if (get_class($row->getData()) == 'Cms\ElmatBundle\Entity\ProductRow') {
                        if ($row['_delete']->getData() == true) {
                            $this->getDoctrine()->getManager()->remove($row->getData());
                        }
                    }
                }

                $this->getDoctrine()->getManager()->flush();

                $this->admin->update($object);
                $this->get('session')->setFlash('sonata_flash_success', 'flash_edit_success');

                if ($this->isXmlHttpRequest()) {
                    return $this->renderJson(array(
                        'result' => 'ok',
                        'objectId' => $this->admin->getNormalizedIdentifier($object)
                    ));
                }

                // redirect to edit mode

                return $this->redirect($this->admin->generateObjectUrl('edit_list', $object));
            }

            // show an error message if the form failed validation
            if (!$isFormValid) {
                if (!$this->isXmlHttpRequest()) {
                    $this->get('session')->setFlash('sonata_flash_error', 'flash_edit_error');
                }
            } elseif ($this->isPreviewRequested()) {
                // enable the preview template if the form was valid and preview was requested
                $templateKey = 'preview';
            }
        }

        $view = $form->createView();

        // set the theme for the current Admin Form
        $this->get('twig')->getExtension('form')->renderer->setTheme($view, $this->admin->getFormTheme());

        return $this->render($this->admin->getTemplate($templateKey), array(
            'action' => 'edit_list',
            'form' => $view,
            'object' => $object,
        ));
    }

    public function editRowsAction() {
        // the key used to lookup the template
        $templateKey = 'edit_rows';

        $id = $this->get('request')->get($this->admin->getIdParameter());

        $object = $this->admin->getObject($id);

        if (!$object) {
            throw new NotFoundHttpException(sprintf('unable to find the object with id : %s', $id));
        }

        if (false === $this->admin->isGranted('EDIT', $object)) {
            throw new AccessDeniedException();
        }

        $this->admin->setSubject($object);

        foreach ($object->getRows() as $row) {
            $row->setUpdated(new \DateTime('now'));
        }

        $form = $this->createForm(new ProductRowsType($this->admin), $object);

        if ($this->get('request')->getMethod() == 'POST') {
            $form->bind($this->get('request'));

            $isFormValid = $form->isValid();

            // persist if the form was valid and if in preview mode the preview was approved
            if ($isFormValid && (!$this->isInPreviewMode() || $this->isPreviewApproved())) {
                $rows_form = $form->get('rows');

                foreach ($rows_form->getChildren() as $row) {
                    if (get_class($row->getData()) == 'Cms\ElmatBundle\Entity\ProductRow') {
                        if ($row['_delete']->getData() == true) {
                            $this->getDoctrine()->getManager()->remove($row->getData());
                        }
                    }
                }

                $this->getDoctrine()->getManager()->flush();

                $this->admin->update($object);
                $this->get('session')->setFlash('sonata_flash_success', 'flash_edit_success');

                if ($this->isXmlHttpRequest()) {
                    return $this->renderJson(array(
                        'result' => 'ok',
                        'objectId' => $this->admin->getNormalizedIdentifier($object)
                    ));
                }

                // redirect to edit mode

                return $this->redirect($this->admin->generateObjectUrl('edit_rows', $object));
            }

            // show an error message if the form failed validation
            if (!$isFormValid) {
                if (!$this->isXmlHttpRequest()) {
                    $this->get('session')->setFlash('sonata_flash_error', 'flash_edit_error');
                }
            } elseif ($this->isPreviewRequested()) {
                // enable the preview template if the form was valid and preview was requested
                $templateKey = 'preview';
            }
        }

        $view = $form->createView();

        $this->get('twig')->getExtension('form')->renderer->setTheme($view, $this->admin->getFormTheme());

        return $this->render($this->admin->getTemplate($templateKey), array(
            'action' => 'edit_rows',
            'form' => $view,
            'object' => $object,
        ));
    }

    public function kadrujAction() {

        $id = $this->get('request')->get($this->admin->getIdParameter());

        $object = $this->admin->getObject($id);

        if (!$object) {
            throw new NotFoundHttpException(sprintf('unable to find the object with id : %s', $id));
        }

        if (false === $this->admin->isGranted('EDIT', $object)) {
            throw new AccessDeniedException();
        }

        $this->admin->setSubject($object);

        $form = $this->admin->getForm();
        $form->setData($object);

        $crop = $this->getRequest()->request->get('crop');

        $imagine = new Imagine();

        try {
            if ($object->getAbsolutePath('image')) {

                $imagine->open($object->getAbsolutePath('image'));
                $file = new File($object->getAbsolutePath('image'));

                $old_name = $object->getImage();
                $new_name = 'image-' . uniqid() . '.' . $file->guessExtension();

                $file->move($object->getUploadRootDir(), $new_name);
                $object->setImage($new_name);
                $this->getDoctrine()->getManager()->flush();
                @unlink($object->getUploadRootDir() . '/crop-' . $old_name);

                // zapisanie nowego cropa
                $imagine->open($object->getAbsolutePath('image'))
                    ->crop(new Point($crop['x'], $crop['y']), new Box($crop['w'], $crop['h']))
                    ->save($object->getUploadRootDir() . '/crop-' . $object->getImage(), array('quality' => 98));
            }
        } catch (Exception $e) {
            return $this->render('CmsElmatBundle:Admin/ProductZdjecie:kadruj_blad.html.twig', array(
                'error' => $e->getMessage(),
            ));
        }


        return $this->render('CmsElmatBundle:Admin/ProductZdjecie:kadruj_sukces.html.twig', array(
            'object' => $object,
        ));
    }

    public function kadruj2Action() {

        $id = $this->get('request')->get($this->admin->getIdParameter());

        $object = $this->admin->getObject($id);

        if (!$object) {
            throw new NotFoundHttpException(sprintf('unable to find the object with id : %s', $id));
        }

        if (false === $this->admin->isGranted('EDIT', $object)) {
            throw new AccessDeniedException();
        }

        $this->admin->setSubject($object);

        $form = $this->admin->getForm();
        $form->setData($object);

        $crop = $this->getRequest()->request->get('crop');

        $imagine = new Imagine();

        try {
            if ($object->getAbsolutePath('image_thumb')) {

                $imagine->open($object->getAbsolutePath('image_thumb'));
                $file = new File($object->getAbsolutePath('image_thumb'));

                $old_name = $object->getImageThumb();
                $new_name = 'image_thumb-' . uniqid() . '.' . $file->guessExtension();

                $file->move($object->getUploadRootDir(), $new_name);
                $object->setImageThumb($new_name);
                $this->getDoctrine()->getManager()->flush();
                @unlink($object->getUploadRootDir() . '/crop-' . $old_name);

                // zapisanie nowego cropa
                $imagine->open($object->getAbsolutePath('image_thumb'))
                    ->crop(new Point($crop['x'], $crop['y']), new Box($crop['w'], $crop['h']))
                    ->save($object->getUploadRootDir() . '/crop-' . $object->getImageThumb(), array('quality' => 98));

            }
        } catch (Exception $e) {
            return $this->render('CmsElmatBundle:Admin/ProductZdjecie:kadruj_blad.html.twig', array(
                'error' => $e->getMessage(),
            ));
        }

        return $this->render('CmsElmatBundle:Admin/ProductZdjecie:kadruj_sukces.html.twig', array(
            'object' => $object,
        ));
    }

    public function previewAction() {
        $id = $this->get('request')->get($this->admin->getIdParameter());

        $object = $this->admin->getObject($id);

        if (!$object) {
            throw new NotFoundHttpException(sprintf('unable to find the object with id : %s', $id));
        }

        switch ($object->getType()) {
            case Product::TYPE_grupa_lista:
                $view = 'preview_grupa_lista.html.twig';
                break;

            case Product::TYPE_grupa_tabelka:
                $view = 'preview_grupa_tabelka.html.twig';
                break;

            case Product::TYPE_normalny:
            default:
                $view = 'preview_normalny.html.twig';
                break;
        }

        return $this->render('CmsElmatBundle:Admin/Product:' . $view, array(
            'product' => $object
        ));
    }

    /**
     * Moves item to recycle bin.
     * @param int $id
     * @return array
     * @throws NotFoundHttpException
     * @throws AccessDeniedException
     */
    public function recycleAction() {
        $id = $this->get('request')->get($this->admin->getIdParameter());
        $now = new \DateTime();

        $object = $this->admin->getObject($id);
        
        $Recycled = new RecycleBin();
        $Recycled->setTitle($object->getTitle());
        $Recycled->setItem('Produkty');
        $Recycled->setItemId($id);
        $Recycled->setFromEntity('CmsElmatBundle:Product');
        $Recycled->setDeletedAt($now);
        
        $object->setDeletedAt($now);
        $this->admin->update($object);
        $this->admin->update($Recycled);
        
        $this->get('session')->setFlash('sonata_flash_success', 'flash_edit_success');

        if (!$object) {
            throw new NotFoundHttpException(sprintf('unable to find the object with id : %s', $id));
        }

        if (false === $this->admin->isGranted('EDIT', $object)) {
            throw new AccessDeniedException();
        }
        
        return $this->redirect($this->admin->generateUrl('list'));
    }
}
