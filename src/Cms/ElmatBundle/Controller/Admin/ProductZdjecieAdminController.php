<?php

namespace Cms\ElmatBundle\Controller\Admin;

use Sonata\AdminBundle\Controller\CRUDController as Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;

use Symfony\Bundle\FrameworkBundle\Controller\RedirectController;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use Symfony\Component\HttpFoundation\File\File;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Imagine\Image\Box;

use Imagine\Image\Point;

use Imagine\Gd\Imagine;

class ProductZdjecieAdminController extends Controller
{

	public function kadrujAction($id = null)
	{

		$id = $this->get('request')->get($this->admin->getIdParameter());

		$object = $this->admin->getObject($id);

		if (!$object) {
			throw new NotFoundHttpException(sprintf('unable to find the object with id : %s', $id));
		}

		if (false === $this->admin->isGranted('EDIT', $object)) {
			throw new AccessDeniedException();
		}

		$this->admin->setSubject($object);

		$form = $this->admin->getForm();
		$form->setData($object);

		$crop = $this->getRequest()->request->get('crop');

		$imagine = new Imagine();

		try {



			if ($object->getAbsolutePath()) {

				$imagine->open($object->getAbsolutePath());
				$file = new File($object->getAbsolutePath());

				$old_name = $object->getObrazek();
				$new_name = $object->generateObrazekName().'.'.$file->guessExtension();

				$file->move($object->getUploadRootDir(), $new_name);
				$object->setObrazek($new_name);
				$this->getDoctrine()->getManager()->flush();
				@unlink($object->getUploadRootDir() .'/crop-'.$old_name);

				// zapisanie nowego cropa
				$imagine->open($object->getAbsolutePath())
				->crop(new Point($crop['x'], $crop['y']), new Box($crop['w'], $crop['h']))
				->save($object->getUploadRootDir().'/crop-'.$object->getObrazek(), array('quality' => 98) );

			}

		} catch (Exception $e) {

			return $this->render('CmsElmatBundle:Admin/ProductZdjecie:kadruj_blad.html.twig', array(
					'error' => $e->getMessage(),
			));
		}


		return $this->render('CmsElmatBundle:Admin/ProductZdjecie:kadruj_sukces.html.twig', array(
				'object' => $object,
		));
	}

}
