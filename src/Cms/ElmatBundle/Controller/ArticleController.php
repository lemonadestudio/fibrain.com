<?php

namespace Cms\ElmatBundle\Controller;

use Symfony\Component\Translation\IdentityTranslator;

use Cms\ElmatBundle\Repository\ArticleRepository;
use Cms\ElmatBundle\Entity\Article;
use Cms\ElmatBundle\Repository\ProductCategoryRepository;
use Cms\ElmatBundle\Entity\ProductCategory;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\DBAL\Statement;
use Symfony\Component\Routing\Router;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;


class ArticleController extends Controller
{


    private function getOfferGroups($lang) {


    	return $this->getDoctrine()
	    	->getRepository('CmsElmatBundle:OfferGroup')
	    	->findBy(array('lang' => $lang), array('kolejnosc' => 'ASC'));
    }

    /**
     * @return ArticleRepository
     */
	public function getArticleRepository() {

		return $this->getDoctrine()->getRepository('CmsElmatBundle:Article');

	}

	/**
	 * @Template()
	 * @param unknown $slug
	 * @param unknown $id
	 * @return \Symfony\Component\HttpFoundation\RedirectResponse|multitype:NULL Ambigous <object, NULL>
	 */
    public function categoryAction($type)
    {


    	$locale = $this->get('request')->getLocale();

    	if(!in_array($type, array(
    			Article::TYPE_artykuly_techniczne,
    			Article::TYPE_rozwiazania,
    			Article::TYPE_uslugi
    			))) {

    		return $this->redirect("/");
    	}


    	$offer_group_slug = $this->getRequest()->get('_domain_symbol_slug');



    	$breadcrumbs = $this->get("white_october_breadcrumbs");
    	$breadcrumbs->addItem("Home", "/");
    	$breadcrumbs->addItem(
    			$this->get('translator')->trans('label.'.$type),

    			$this->get("router")->generate("cms_elmat_article_category", array(
    					'type' => $type)));

    	$qb = $this->getArticleRepository()->createQueryBuilder('a')
    	                ->leftJoin('a.offer_group', 'og')
    					->where('a.published = true')
    					->andWhere('a.publishDate <= :date')
    					->andWhere('a.type = :type')
    					->orderBy('a.publishDate', 'DESC')
    					->setParameter('type', $type)
    					->setParameter('date', new \DateTime())
    					;

    	if($offer_group_slug) {
    	    $qb
    	    ->andWhere('og.slug = :slug')
    	    ->setParameter('slug', $offer_group_slug)
    	    ;

    	} else {

    	    $qb
    	    ->andWhere('og.slug IS NULL')
    	    ;

    	}

    	$articles = $qb->getQuery()->execute();

        return array(
        		'type' => $type,
        		'articles' => $articles
        );


    }

    /**
     *
     * @param unknown $slug
     * @param unknown $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|multitype:NULL Ambigous <object, NULL>
     */
    public function showAction($type, $slug, $id)
    {


    	$locale = $this->get('request')->getLocale();

    	if(!in_array($type, array(
    			Article::TYPE_artykuly_techniczne,
    			Article::TYPE_rozwiazania,
    			Article::TYPE_uslugi
    	))) {

    		return $this->redirect("/");
    	}


    	$article = $this->getArticleRepository()->findOneBy(array('id' => $id));

    	if(!$article) {
    		return $this->redirect("/");
    	}

    	if($article->getSlug() != $slug || $type != $article->getType()) {
    		return $this->redirect(
    				$this->get('router')->generate('cms_elmat_article_show',
    						array('id' => $article->getId(),
    								'slug' => $article->getSlug(),
    								'type' => $article->getType())));
    	}
        
        
        // sprawdzenie czy jesteśmy na dobrej domenie
        // jeżeli nie - przekirowanie na prawidłową.
        // skonfigurowane domeny
		$cms_helper = $this->get('cms.elmat.helper');
        $domeny = $cms_helper->domains();
        $domain_symbol = $this->getRequest()->get('_domain_symbol');
        $offer_groups = $cms_helper->getOfferGroups();
        $article_offer_group = $article->getOfferGroup();
        
        $force_new_route = false;
        
        if($article_offer_group) {
            
            $product_offer_group_symbol = '';
            $product_offer_gorup_article_route_symbol = '';
            
            foreach($offer_groups as $og) {
                
                if($og['slug'] == $article_offer_group->getSlug()) {
                    $product_offer_group_symbol = $og['symbol'];
                    $product_offer_gorup_article_route_symbol = $og['symbol2'];
                }
                
            }
            
            if($domain_symbol != $product_offer_group_symbol) {
                
                $force_new_route = 'cms_elmat_article_show_' . $product_offer_gorup_article_route_symbol;
                
            }
            
        } else {
            
             // jeżeli jesteśmy na domenie live przekirowanie na główną stronę.
            if($domain_symbol != 'elmat') {
           
                $force_new_route = 'cms_elmat_article_show';
                
            }
            
        }
    
        
        if($force_new_route) {
    		return $this->redirect(
    				$this->get('router')->generate($force_new_route,
    						array('id' => $article->getId(),
    								'slug' => $article->getSlug(),
    								'type' => $article->getType())));
    	}
        
        
        


    	$breadcrumbs = $this->get("white_october_breadcrumbs");
    	$breadcrumbs->addItem("Home", "/");

    	$breadcrumbs->addItem($this->get('translator')->trans('label.'.$article->getType()),
    			$this->get("router")->generate("cms_elmat_article_category", array(
    					'type' => $article->getType())));



    	$breadcrumbs->addItem($article->getTitle(), $this->get('router')->generate('cms_elmat_article_show',
    			array('id' => $article->getId(), 'slug' => $article->getSlug(), 'type'=>$article->getType())));


       $parameters =  array(
        	'article' => $article
        );

       switch($article->getType()) {


	       	default:
	       		$view = 'CmsElmatBundle:Article:show.html.twig';
				break;

       }

       return $this->render($view, $parameters);


    }




}
