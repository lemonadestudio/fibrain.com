<?php

namespace Cms\ElmatBundle\Controller;

use Doctrine\ORM\Query\Expr\OrderBy;

use Symfony\Component\HttpFoundation\Response;

use Doctrine\DBAL\Statement;
use Cms\ElmatBundle\Entity\OfferGroup;
use Cms\ElmatBundle\Repository\NewsRepository;
use Symfony\Component\Routing\Router;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Cms\ElmatBundle\Entity\News;


class AktualnosciController extends Controller
{


	/**
	 * @Template()
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
    public function showAction($slug, $id)
    {


    	$locale = $this->get('request')->getLocale();



        $og_slug = $this->getRequest()->get('_domain_symbol_slug');

        if(!$og_slug) {
            return $this->redirect($this->get('router')->generate('cms_elmat_live_main'));
        }

        $news = $this->getDoctrine()
        ->getRepository('CmsElmatBundle:News')
        ->findOneBy(array(
                'id' => $id,
                'published' => true,
        ));



        if(!$news || $news->getPublishDate() > new \DateTime('now')) {
        	return $this->redirect(
        			$this->get('router')->generate('cms_elmat_aktualnosci_main'));
        }

        // przekierowanie jeżeli inny slug lub lang
        if($slug != $news->getSlug() || $locale != $news->getLang()) {
        	return $this->redirect($this->get('router')
        				->generate('cms_elmat_aktualnosci_show',
        					array(
        						// '_locale' => $news->getLang(),
        						'slug' => $news->getSlug(),
        						'id' => $news->getId())
        				)
        			);
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Home", "/");
        $breadcrumbs->addItem("News", $this->get("router")->generate("cms_elmat_aktualnosci_main"));
        $breadcrumbs->addItem($news->getTitle(), $this->get("router")->generate("cms_elmat_aktualnosci_show", array('id' => $news->getId(),  'slug' => $news->getSlug())));





        return array(

        		'news' => $news,

        		);
    }



    /**
     * @return NewsRepository
     */
	public function getNewsRepository() {

		return $this->getDoctrine()->getRepository('CmsElmatBundle:News');

	}

    /**
     * @Template
     * @return multitype:NULL
     */
    public function mainAction($page)
    {

    	$locale = $this->get('request')->getLocale();

    	$og_slug = $this->getRequest()->get('_domain_symbol_slug');
    	$og_id = $this->getRequest()->get('_domain_symbol_id');

    	if(!$og_slug) {
    	    return $this->redirect($this->get('router')->generate('cms_elmat_live_main'));
    	}



    	$breadcrumbs = $this->get("white_october_breadcrumbs");
    	$breadcrumbs->addItem("Home", "/");
    	$breadcrumbs->addItem("News", $this->get("router")->generate("cms_elmat_aktualnosci_main"));

    	// $highlight_news_ids = $this->getNewsRepository()->loadHighlightsCategoryIDs($locale, $og_id, true);
    	// $news_highlight = $this->getNewsRepository()->loadByIDs($highlight_news_ids);
        $highlight_news_ids = array();
        $news_highlight = array();
        
        if(empty($highlight_news_ids)) {
    		$highlight_news_ids = array(0);
    	}

    	$qb = $this->getNewsRepository()->createQueryBuilder('n')
    	->leftJoin('n.offer_group', 'og')
    	->where('   n.publishDate <= :date
    	        AND n.published = true
    	        AND og.slug = :slug
    	        AND  n.id NOT IN (:ids_exclude)
    	        '

    	        )
    	->orderBy('n.publishDate', 'DESC')
    	->setParameters(array(
    	        'date' => new \DateTime('now'),
    	        'slug' => $og_slug,
    	        'ids_exclude' => $highlight_news_ids,
    	        ))
    	;


    	$paginator = $this->get('knp_paginator');
    	$news = $paginator->paginate(
    	        $qb->getQuery(),
    	        $page,
    	        6
    	);

    	return array(
    		'news' => $news,
    	    'news_highlight' => $news_highlight,

    	);

    }


}
