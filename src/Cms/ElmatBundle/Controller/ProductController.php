<?php

namespace Cms\ElmatBundle\Controller;

use Cms\ElmatBundle\Entity\Product;
use Cms\ElmatBundle\Repository\ProductCategoryRepository;
use Cms\ElmatBundle\Repository\ProductRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;


class ProductController extends Controller {
    private function getOfferGroups($lang) {
        return $this->getDoctrine()
            ->getRepository('CmsElmatBundle:OfferGroup')
            ->findBy(array('lang' => $lang), array('kolejnosc' => 'ASC'));
    }

    /**
     * @return ProductRepository
     */
    public function getProductRepository() {

        return $this->getDoctrine()->getRepository('CmsElmatBundle:Product');

    }

    /**
     * @return ProductCategoryRepository
     */
    public function getProductCategoryRepository() {

        return $this->getDoctrine()->getRepository('CmsElmatBundle:ProductCategory');

    }

    /**
     * @Template
     */
    public function mainAction() {

        $locale = $this->get('request')->getLocale();

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Home", "/");
        $breadcrumbs->addItem("Products", $this->get("router")->generate("cms_elmat_product_main"));


        $offer_group_slug = $this->getRequest()->get('_domain_symbol_slug');

        $qb = $this->getProductCategoryRepository()
            ->createQueryBuilder('c')
            ->where('c.lvl = 0')
            ->leftJoin('c.offer_group', 'og')
            ->orderBy('c.kolejnosc', 'ASC')
            ->addOrderBy('c.root', 'ASC');

        if ($offer_group_slug) {
            $qb->andWhere('og.slug = :og_slug')
                ->setParameter('og_slug', $offer_group_slug);
        }


        $categories = $qb->getQuery()->execute();

        // ->findBy(array('lvl' => 0), array('root' => 'asc'));

        $offer_groups = $this->getOfferGroups($locale);

        return array(
            'categories' => $categories,
            'offer_groups' => $offer_groups,
        );

    }

    /**
     * @Template()
     */
    public function categoryAction($slug, $id) {
        $locale = $this->get('request')->getLocale();

        $category = $this->getProductCategoryRepository()->findOneBy(array('id' => $id));

        if (!$category) {
            return $this->redirect("/");
        }

        if ($category->getSlug() != $slug) {
            return $this->redirect(
                $this->get('router')->generate('cms_elmat_product_category',
                    array('id' => $id, 'slug' => $category->getSlug())));
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Home", "/");
        $breadcrumbs->addItem("Products", $this->get("router")->generate("cms_elmat_product_main"));

        $parent_categories = $this->getProductCategoryRepository()
            ->createQueryBuilder('c')
            ->where('c.lft < :lft')
            ->andWhere('c.rgt > :rgt')
            ->andWhere('c.root = :root')
            ->orderBy('c.lft', 'asc')
            ->setParameters(array(
                ':lft' => $category->getLft(),
                ':rgt' => $category->getRgt(),
                ':root' => $category->getRoot()
            ))
            ->getQuery()->execute();

        foreach ($parent_categories as $cat) {
            $breadcrumbs->addItem($cat->getName(), $this->get('router')->generate('cms_elmat_product_category',
                array('id' => $cat->getId(), 'slug' => $cat->getSlug())));
        }


        $breadcrumbs->addItem($category->getName(), $this->get('router')->generate('cms_elmat_product_category',
            array('id' => $category->getId(), 'slug' => $category->getSlug())));


        return array(
            'category' => $category,
            'categories' => $category->getChildren(),
            'products' => $category->getProducts()

        );

    }

    public function showAction($slug, $id) {
        $locale = $this->get('request')->getLocale();

        $product = $this->getProductRepository()->findOneBy(array('id' => $id));

        if (!$product) {
            return $this->redirect("/");
        }

        if ($product->getSlug() != $slug) {
            return $this->redirect($this->get('router')->generate('cms_elmat_product_show', array(
                'id' => $product->getId(),
                'slug' => $product->getSlug()
            )));
        }

        // sprawdzenie czy jesteśmy na dobrej domenie
        // jeżeli nie - przekirowanie na prawidłową.
        // skonfigurowane domeny
        $cms_helper = $this->get('cms.elmat.helper');
        $domeny = $cms_helper->domains();
        $domain_symbol = $this->getRequest()->get('_domain_symbol');
        $offer_groups = $cms_helper->getOfferGroups();
        $category = $product->getCategory();
        $product_offer_group = $category->getOfferGroup();

        $force_new_domain = false;

        if ($product_offer_group) {
            $product_offer_group_symbol = '';

            foreach ($offer_groups as $og) {
                if ($og['slug'] == $product_offer_group->getSlug()) {
                    $product_offer_group_symbol = $og['symbol'];
                }
            }

            if ($domain_symbol != $product_offer_group_symbol) {
                foreach ($domeny as $dm) {
                    if ($dm['symbol'] == $product_offer_group_symbol) {
                        $force_new_domain = $dm['domain'];
                    }
                }
            }
        } else {
            // jeżeli jesteśmy na domenie live przekirowanie na główną stronę.
            if ($domain_symbol != 'elmat') {
                $force_new_domain = $domeny['elmat']['domain'];
            }
        }

        if ($force_new_domain) {
            return $this->redirect('http://' . $force_new_domain . $this->get('router')->generate('cms_elmat_product_show', array(
                    'id' => $product->getId(),
                    'slug' => $product->getSlug()
                )));
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Home", "/");
        $breadcrumbs->addItem("Products", $this->get("router")->generate("cms_elmat_product_main"));

        $parent_categories = array();

        if ($category) {
            $parent_categories = $this->getProductCategoryRepository()->createQueryBuilder('c')
                ->where('c.lft < :lft')
                ->andWhere('c.rgt > :rgt')
                ->andWhere('c.root = :root')
                ->orderBy('c.lft', 'asc')
                ->setParameters(array(
                    ':lft' => $category->getLft(),
                    ':rgt' => $category->getRgt(),
                    ':root' => $category->getRoot()
                ))
                ->getQuery()->execute();

            foreach ($parent_categories as $cat) {
                $breadcrumbs->addItem($cat->getName(), $this->get('router')->generate('cms_elmat_product_category', array(
                    'id' => $cat->getId(),
                    'slug' => $cat->getSlug()
                )));
            }

            $breadcrumbs->addItem($category->getName(), $this->get('router')->generate('cms_elmat_product_category', array(
                'id' => $category->getId(),
                'slug' => $category->getSlug()
            )));

        }

        $breadcrumbs->addItem($product->getTitle(), $this->get('router')->generate('cms_elmat_product_show', array(
            'id' => $product->getId(),
            'slug' => $product->getSlug()
        )));


        $parameters = array(
            'category' => $category,
            'categories' => $category->getChildren(),
            'product' => $product
        );

        switch ($product->getType()) {
            case Product::TYPE_grupa_lista:
                $view = 'CmsElmatBundle:Product:show_grupa_lista.html.twig';
                break;

            case Product::TYPE_grupa_tabelka:
                $view = 'CmsElmatBundle:Product:show_grupa_tabelka.html.twig';
                break;

            case Product::TYPE_normalny:
            default:
                $view = 'CmsElmatBundle:Product:show_normalny.html.twig';
                break;

        }

        return $this->render($view, $parameters);
    }

    /**
     * @Template()
     */
    public function leftmenuAction($category_id, $request, $prod_id = null) {
        $offer_group_slug = $request->get('_domain_symbol_slug');

        $category = $this->getProductCategoryRepository()->findOneBy(array('id' => $category_id));

        $root = array(
            'current' => false,
            'title' => 'Products',
            'url' => '',
            'children' => null
        );

        $children = null;
        $parent_id = null;

        if ($category) {
            while ($category !== null) {
                $temp_children = array();

                $categories = $category->getChildren();
                $products = $category->getProducts();

                foreach ($categories as $cat) {
                    $item = array(
                        'current' => $category_id == $cat->getId() && $prod_id == null ? true : false,
                        'title' => $cat->getName(),
                        'url' => $this->get('router')->generate('cms_elmat_product_category', array(
                            'id' => $cat->getId(),
                            'slug' => $cat->getSlug()
                        )),
                        'children' => null
                    );

                    if ($cat->getId() == $parent_id) {
                        $item['children'] = $children;
                    }

                    $temp_children[] = $item;
                }

                foreach ($products as $prod) {
                    $temp_children[] = array(
                        'current' => $prod_id == $prod->getId() ? true : false,
                        'title' => $prod->getTitle(),
                        'url' => $this->get('router')->generate('cms_elmat_product_show', array(
                            'id' => $prod->getId(),
                            'slug' => $prod->getSlug()
                        )),
                        'children' => null
                    );
                }

                $children = $temp_children;

                $parent_id = $category->getId();
                $category = $category->getParent();
            }
        }

        /* listowanie głównych kategorii */
        $qb = $this->getProductCategoryRepository()
            ->createQueryBuilder('c')
            ->where('c.lvl = 0')
            ->leftJoin('c.offer_group', 'og')
            ->orderBy('c.kolejnosc', 'ASC');

        if ($offer_group_slug) {
            $qb->andWhere('og.slug = :og_slug');
            $qb->setParameter('og_slug', $offer_group_slug);
        }

        $categories = $qb->getQuery()->execute();

        if (count($categories) > 0) {
            $root['children'] = array();

            foreach ($categories as $cat) {
                $item = array(
                    'current' => $category_id == $cat->getId() && $prod_id == null ? true : false,
                    'title' => $cat->getName(),
                    'url' => $this->get('router')->generate('cms_elmat_product_category', array(
                        'id' => $cat->getId(),
                        'slug' => $cat->getSlug()
                    )),
                    'children' => null
                );

                if ($cat->getId() == $parent_id) {
                    $item['children'] = $children;
                }

                $root['children'][] = $item;
            }
        }

        $links[] = $root;

        return array(
            'links' => $links
        );
    }
}
