<?php

namespace Cms\ElmatBundle\Controller;

use Cms\ElmatBundle\Entity\TrainingParticipant;
use Cms\ElmatBundle\Entity\TrainingRegistration;
use Cms\ElmatBundle\Form\Type\KlientRejestracjaKrok2Type;
use Cms\ElmatBundle\Form\Type\TrainingRejestracjaKrok2Type;
use Doctrine\ORM\NoResultException;

use Cms\ElmatBundle\Entity\Page;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Cms\ElmatBundle\Repository\TrainingDateRepository;
use Cms\ElmatBundle\Form\Type\TrainingRejestracjaType;

class TrainingController extends Controller
{

    public function mainAction()
    {

        $page = $this->getDoctrine()->getRepository('CmsElmatBundle:Page')
            ->findOneBy(array('slug' => 'szkolenia'));

        if (!$page) {


            $page = new Page();
            $page->setTitle('Szkolenia');

            $page->setContent('<p>Szkolenia opis wstęp</p>');
            $page->setPublished(false);
            $page->setSlug('szkolenia');

            $this->getDoctrine()->getManager()->persist($page);
            $this->getDoctrine()->getManager()->flush();

        }

        return $this->render('CmsElmatBundle:Training:main.html.twig', array(
            'page' => $page,
        ));

    }

    public function showAction($slug)
    {
        $training = $this->getDoctrine()
            ->getRepository('CmsElmatBundle:Training')
            ->loadArticle($slug, $this->get('request')->getLocale());


        if (!$training) {
            return $this->redirect($this->get('router')->generate('cms_elmat_training_main'));
        }

        return $this->render('CmsElmatBundle:Training:show.html.twig', array('training' => $training));
    }

    public function categoryShowAction($slug)
    {
        $em = $this->getDoctrine()->getManager();

        try {
            $trainingcategory = $em->createQueryBuilder()
                ->select('n, t, ch, cht')
                ->from('CmsElmatBundle:TrainingCategory', 'n')
                ->leftJoin('n.trainings', 't')
                ->leftJoin('n.children', 'ch')
                ->leftJoin('ch.trainings', 'cht')
                ->where('n.slug = :slug')
                ->setParameter('slug', $slug)
                ->getQuery()
                ->getSingleResult();
        } catch (NoResultException $e) {
            return $this->redirect($this->get('router')->generate('cms_elmat_training_main'));
        }

        return $this->render('CmsElmatBundle:Training:category_show.html.twig', array(
            'trainingcategory' => $trainingcategory
        ));
    }


    /**
     *
     * @return TrainingDateRepository
     */
    public function getTrainingDateRepository()
    {

        $repo = $this->getDoctrine()
            ->getRepository('CmsElmatBundle:TrainingDate');

        return $repo;
    }

    public function registerAction($slug, $termin_id)
    {

        $termin = $this->getTrainingDateRepository()->find($termin_id);

        if (!$termin || $termin->getStartDate() < new \DateTime('today') || $termin->getBrakMiejsc()) {
            return $this->redirect($this->get('router')->generate('cms_elmat_training_terminy'));
        }

        $szkolenie = $termin->getTraining();
        $krok = 1;

        if ($slug != $szkolenie->getSlug()) {
            return $this->redirect($this->get('router')->generate(
                'cms_elmat_training_register', array(
                'termin_id' => $termin->getId(),
                'slug' => $szkolenie->getSlug()
            )));
        }

        $request = $this->getRequest();

        $krok = 1;
        $krok_submit = 0;


        if ($this->get('security.context')->isGranted('ROLE_KLIENT')) {
            $user = $this->get('security.context')->getToken()->getUser();
        } else {
            return $this->render('CmsElmatBundle:Training:register.html.twig', array(
                'termin' => $termin,
                'training' => $szkolenie,
                'user' => null,
            ));
        }

        $krok = 1;
        $krok_submit = 0;

        $training_registration_init = new TrainingRegistration();
        $training_registration_init->addTrainingParticipant(new TrainingParticipant());
        $training_registration_init->setTrainingDate($termin);
        $training_registration_init->setUser($user);

        $form = $this->createForm(new TrainingRejestracjaType(), $training_registration_init);


        $training_registration = null;

        if ($request->isMethod('post')) {

            $krok_submit = 1;
            $krok_return = 0;

            $form_data = $request->request->get($form->getName());

            if (!is_array($form_data)) {
                return $this->redirect($this->get('router')->generate('cms_elmat_training_terminy'));
            }


            if (array_key_exists('krok', $form_data) && $form_data['krok'] == 2) {
                $krok_submit = 2;
            }

            if (array_key_exists('krok', $form_data) && $form_data['krok'] == 1) {
                unset($form_data['uwagi']);
                unset($form_data['krok']);
                $request->request->set($form->getName(), $form_data);
                $krok_return = 1;
            }

            $form->bind($request);


            if (!$krok_return && ($form->isValid() || $krok_submit == 2)) {
                $krok = 2;
                $form = $this->createForm(new TrainingRejestracjaKrok2Type(), $training_registration_init);

                $form_data['krok'] = 2;
                $request->request->set($form->getName(), $form_data);
                $form->bind($request);

            }


            if ($form->isValid()) {


                $training_registration = $form->getData();

                if ($krok_submit == 2) {

                    $this->getDoctrine()->getManager()->persist($training_registration);
                    $this->getDoctrine()->getManager()->flush();

                    $user = $training_registration->getUser();
                    $termin = $training_registration_init->getTrainingDate();
                    $uczestnicy = $training_registration_init->getTrainingParticipants();
                    $szkolenie = $termin->getTraining();

                    $uczestnicy_txt = '<ol>';
                    foreach ($uczestnicy as $ucz) {
                        $uczestnicy_txt .= '<li>';
                        $uczestnicy_txt .= $ucz->getNazwa() . ' ' . $ucz->getEmail() . ' ' . $ucz->getTelefon();
                        $uczestnicy_txt .= '</li>';
                    }
                    $uczestnicy_txt .= '</ol>';


                    $zmienne = array(
                        'EDYCJA_URL' => $this->get('router')->generate(
                            'admin_cms_elmat_trainingregistration_edit',
                            array(
                                'id' => $training_registration->getId()
                            ),
                            true),
                        'FIRMA' => $user->getFirma(),
                        'ULICA' => $user->getUlica(),
                        'KOD_POCZTOWY' => $user->getKodPocztowy(),
                        'MIASTO' => $user->getMiasto(),
                        'NIP' => $user->getNip(),
                        'FAX' => $user->getFax(),
                        'TELEFON' => $user->getTelefon(),
                        'EMAIL' => $user->getEmail(),
                        'SZKOLENIE' => $szkolenie->getTitle(),
                        'TERMIN' => $termin->getStartDate()->format('d-m-Y') . ($termin->getStartDate() != $termin->getEndDate() ? ' - ' . $termin->getEndDate()->format('d-m-Y') : ''),
                        'LICZBA_UCZESTNIKOW' => count($uczestnicy),
                        'LISTA_UCZESTNIKOW' => $uczestnicy_txt,
                        'KOSZT_OSOBA' => intval($termin->getPrice()),
                        'KOSZT_CALKOWITY' => intval($termin->getPrice()) * count($uczestnicy),
                        'UWAGI' => $training_registration->getUwagi(),

                    );

                    $template = $this->get('cms_email_templates')->getParsed('rejestracja_klienta_na_szkolenie_powiadomienie', $zmienne);

                    $email_do = $this->get('cms_config')->get('ust_rejestracja_szkolenie_email');
                    $email_od = $this->get('cms_config')->get('ust_mailer_wiadomosc_od');

                    if ($email_do) {
                        $message_email = \Swift_Message::newInstance()
                            ->setSubject($template->getTopic())
                            ->setFrom($this->container->getParameter('mailer_user'), $email_od)
                            ->setTo(array_map('trim', explode(',', $email_do)))
                            ->setBody($template->getContent(), 'text/html')
                            ->addPart(strip_tags($template->getContentTxt()), 'text/plain');

                        try {
                            $ret = $this->get('mailer')->send($message_email);
                        } catch (Exception $e) {

                        }
                    }


                    return $this->redirect($this->get('router')->generate(
                            'cms_elmat_userzone_szkolenie_rejestracja', array(
                            'rejestracja_id' => $training_registration->getId(),
                        )) . '?print=1');
                }
            }
        }

        if (false && $request->isMethod('post')) {
            if ($form->isValid()) {
                if ($krok_submit == 3) {

                    // jeżeli formularz został ostatecznie wysłany

                    // zapisanie usera
                    $user = $form->getData();

                    $em = $this->getDoctrine()->getManager();

                    $user->setConfirmationToken(sha1($user->serialize() . time()));

                    $em->persist($user);
                    $em->flush();


                    $this->get('session')->getFlashBag()->set('notice',
                        $this->get('cms_config')->get('txt_strefa_klienta_rejestracja_podziekowanie'));

                    return $this->redirect($this->get('router')->generate('cms_elmat_userzone_login'));
                }

                if ($krok == 2) {
                    $krok = 3;
                    $form = $this->createForm(new KlientRejestracjaKrok2Type());


                    $form_data['krok'] = 3;
                    $request->request->set($form->getName(), $form_data);
                    $form->bind($request);

                    $user = $form->getData();
                }
            }

            $data = $form->getData();
        }


        return $this->render('CmsElmatBundle:Training:register.html.twig', array(
            'krok' => $krok,
            'form' => $form->createView(),
            'termin' => $termin,
            'training' => $szkolenie,
            'registration' => $training_registration,
            'user' => $user,
        ));
    }

    public function terminyAction()
    {
        $doctrine = $this->getDoctrine();
        $translator = $this->get('translator');
        $request = $this->get('request');
        $session = $this->get('session');

        $_all = $request->query->get('all');


        if (isset($_all)) {
            $session->set('terminy_filters', array());
            return $this->redirect($this->get('router')->generate('cms_elmat_training_terminy'));
        }

        $qb = $this->getTrainingDateRepository()->createQueryBuilder('td')
            ->leftJoin('td.training', 't')
            ->leftJoin('t.category', 'tc')
            ->leftJoin('td.city', 'tdc')
            ->where('td.published = true')
            ->andWhere('td.publishDate <= :today')
            ->orderBy('td.startDate', 'DESC');


        $qb->setParameter('today', date('Y-m-d H:i:s'));

        // $qb->where();

        $terminy = $qb->getQuery()->execute();

        $form_filters_builder = $this->getTerminyFormBuilder($terminy);

        $form_filters = $form_filters_builder->getForm();

        if ($request->query->get('setfilter') == 1) {
            $_to_bind = array(
                'data' => date('Y-m', strtotime($request->query->get('date'))),
                'data_prec' => date('Y-m-d', strtotime($request->query->get('date'))),
                // 'szkolenie' =>  $request->query->get('szkolenie')
            );
            $form_filters->bind($_to_bind);
            if ($form_filters->isValid()) {
                $filters_data = $form_filters->getData();
                $session->set('terminy_filters', $filters_data);
            } else {

            }
            return $this->redirect($this->get('router')->generate('cms_elmat_training_terminy'));


        }

        if ($request->isMethod('POST')) {

            $form_filters->bind($request);
            if ($form_filters->isValid()) {
                $prev_filters_data = $session->get('terminy_filters');
                if (!isset($prev_filters_data['data'])) {
                    $prev_filters_data['data'] = '';
                }
                $filters_data = $form_filters->getData();
                if ($prev_filters_data['data'] != $filters_data['data']) {
                    $filters_data['data_prec'] = '';
                }
                $session->set('terminy_filters', $filters_data);
            }


        }

        $filters_session = (array)$session->get('terminy_filters');


        if (!empty($filters_session['rodzaj'])) {
            $qb->andWhere('tc.id = :rodzaj');
            $qb->setParameter('rodzaj', $filters_session['rodzaj']);
        }

        if (!empty($filters_session['szkolenie'])) {
            $qb->andWhere('t.id = :szkolenie');
            $qb->setParameter('szkolenie', $filters_session['szkolenie']);
        }

        if (!empty($filters_session['miasto'])) {
            $qb->andWhere('tdc.id = :miasto');
            $qb->setParameter('miasto', $filters_session['miasto']);
        }

        if (!empty($filters_session['data'])) {
            $qb->andWhere('td.startDate >= :start_date');
            $qb->andWhere('td.startDate < :end_date');
            $start_date = date('Y-m-d', strtotime($filters_session['data']));
            $end_date = date('Y-m-d', strtotime($start_date . ' +1 month'));
            $qb->setParameter('start_date', $start_date);
            $qb->setParameter('end_date', $end_date);
        }

        if (!empty($filters_session['data_prec'])) {
            $qb->andWhere('td.startDate = :start_date');
            $start_date = date('Y-m-d', strtotime($filters_session['data_prec']));
            $qb->setParameter('start_date', $start_date);
        }

        $terminy = $qb->getQuery()->execute();

        $form_filters_builder->setData($filters_session);


        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Home", $this->get("router")->generate("cms_elmat_default_homepage"));
        //
        $em = $this->getDoctrine()->getManager();

        $breadcrumbs->addItem("Kalendarz szkoleń", $this->get("router")->generate("cms_elmat_training_terminy"));


        return $this->render('CmsElmatBundle:Training:terminy.html.twig', array(
            'terminy' => $terminy,
            'form_filters' => $form_filters_builder->getForm()->createView(),
            'terminy_filters' => $session->get('terminy_filters'),
            'date_now' => new \DateTime(),
        ));
    }


    /**
     *
     * @return $form FormBuilder
     */
    private function getTerminyFormBuilder($terminy)
    {

        $translator = $this->get('translator');

        $rodzaj_choices = array(
            '' => 'Rodzaj'
        );
        $szkolenie_choices = array(
            '' => 'Temat szkolenia'
        );
        $miasto_choices = array(
            '' => 'Miasto'
        );
        $data_choices = array(
            '' => 'Data'
        );

        $data_prec_choices = array(
            '' => 'Data prec'
        );

        $cena_choices = array(
            '' => 'Cena',
        );

        $cena_choices_tmp1 = array();
        for ($i = 0; $i <= 4000; $i += 1000) {
            $cena_choices_tmp1[] = $i . '_' . ($i + 999);
        }

        $cena_choices_tmp = array();

        foreach ($terminy as $t) {

            if ($t->getTraining()->getCategory()) {
                $rodzaj_choices[$t->getTraining()->getCategory()->getId()] = $t->getTraining()->getCategory();
            }

            $szkolenie_choices[$t->getTraining()->getId()] = $t->getTraining();
            $miasto_choices[$t->getCity()->getId()] = $t->getCity();


            $date = $t->getStartDate();
            $date_y = $date->format('Y');
            $date_m = $date->format('m');
            $date_d = $date->format('d');
            $data_choices[$date_y . '-' . $date_m] = $translator->trans('miesiac_' . $date_m) . ' ' . $date_y;

            $data_prec_choices[$date_y . '-' . $date_m . '-' . $date_d] = $date_y . '-' . $date_m . '-' . $date_d;

            $price_low = floor($t->getPrice() / 1000) * 1000;
            if ($price_low > 4000) {
                $price_low = 4000;
            }
            $price_high = $price_low + 999;

            $cena_choices_tmp[$price_low . '_' . $price_high] = $translator->trans('ceny_przedzial_' . $price_low . '_' . $price_high);

        }

        foreach ($cena_choices_tmp1 as $_tmp) {
            if (array_key_exists($_tmp, $cena_choices_tmp)) {
                $cena_choices[$_tmp] = $cena_choices_tmp[$_tmp];
            }
        }

        $form_filters_builder = $this->createFormBuilder(null, array('csrf_protection' => false));


        $form_filters_builder->add('rodzaj', 'choice', array(
                'choices' => $rodzaj_choices
            )
        );

        $form_filters_builder->add('szkolenie', 'choice', array(
                'choices' => $szkolenie_choices
            )
        );
        $form_filters_builder->add('miasto', 'choice', array(
                'choices' => $miasto_choices
            )
        );

        $form_filters_builder->add('data', 'choice', array(
                'choices' => $data_choices
            )
        );

        $form_filters_builder->add('data_prec', 'choice', array(
                'choices' => $data_prec_choices
            )
        );

        $form_filters_builder->add('cena', 'choice', array(
                'choices' => $cena_choices
            )
        );

        return $form_filters_builder;

    }

    public function calendarAction($options = array())
    {
        $default_options = array(
            'header' => true
        );

        $options = array_merge($default_options, $options);

        $doctrine = $this->getDoctrine();
        $translator = $this->get('translator');
        $request = $this->get('request');
        $session = $this->get('session');

        $terminy_filters = $session->get('terminy_filters');

        $def_data = null;
        if (is_array($terminy_filters) && !empty($terminy_filters['data'])) {
            $def_data = $terminy_filters['data'];
        }

        $date = $request->get('date', $def_data ? $def_data : date('Y-m-d'));
        $year = date('Y', strtotime($date));
        $month = date('m', strtotime($date));


        if ($year < date('Y') || $year > 2050) {
            $year = date('Y');
        }

        if ($month < 1 || $month > 12) {
            $month = date('m');
        }

        $start_date = $year . '-' . $month . '-01';
        $end_date = date('Y-m-d', strtotime($start_date . ' +1 month'));


        $qb = $this->getTrainingDateRepository()->createQueryBuilder('td')
            ->leftJoin('td.training', 't')
            ->leftJoin('t.category', 'tc')
            ->leftJoin('td.city', 'tdc')
            ->where('td.published = true')
            ->andWhere('td.publishDate <= :today')
            ->andWhere('td.startDate >= :today')
            ->andWhere('td.startDate >= :start_date')
            ->andWhere('td.startDate < :end_date')
            ->orderBy('td.startDate', 'ASC');
        $qb->setParameter('today', date('Y-m-d H:i:s'));
        $qb->setParameter('start_date', $start_date);
        $qb->setParameter('end_date', $end_date);

        $terminy = $qb->getQuery()->execute();

        $qb->setParameter('start_date', date('Y-m-d'));
        $qb->setParameter('end_date', date('Y-m-d', strtotime('now +1 year')));
        $qb->setMaxResults(6);
        $terminy_najblizsze = $qb->getQuery()->execute();


        $day_last = date('t', strtotime($start_date)); // t - Ilość dni w danym miesiącu
        $first_dow = date('N', strtotime($start_date)); // dzień tygodnia 1 dnia miesiąca

        $days = array();
        for ($d = 0; $d < $day_last; $d++) {
            $day_date = date('Y-m-d', strtotime($start_date . ' +' . $d . ' days'));
            $days[$day_date] = array(
                'day' => $d + 1,
                'dow' => (($first_dow + $d - 1) % 7) + 1, // dzień tygodnia
                'terminy' => array()
            );
        }

        foreach ($terminy as $t) {
            $_d = $t->getStartDate()->format('Y-m-d');
            $days[$_d]['terminy'][] = $t;
        }

        $prev_date = date('Y-m', strtotime($start_date . ' -1 month'));
        if ($prev_date < date('Y-m')) {
            $prev_date = null;
        }

        $next_date = date('Y-m', strtotime($start_date . ' +1 month'));


        return $this->render('CmsElmatBundle:Training:calendar.html.twig', array(
            'terminy' => $terminy,
            'terminy_najblizsze' => $terminy_najblizsze,
            'year' => $year,
            'month' => $month,
            'days' => $days,
            'first_dow' => $first_dow,
            'prev_date' => $prev_date,
            'next_date' => $next_date,
            'is_ajax' => $request->isXmlHttpRequest(),
            'options' => $options
        ));
    }
}
