<?php

namespace Cms\ElmatBundle\Controller;

use Cms\ElmatBundle\Repository\ZasobCategoryRepository;
use Cms\ElmatBundle\Repository\ZasobRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;


class ZasobController extends Controller {
    private function getOfferGroups($lang) {
        return $this->getDoctrine()
            ->getRepository('CmsElmatBundle:OfferGroup')
            ->findBy(array('lang' => $lang), array('kolejnosc' => 'ASC'));
    }

    /**
     * @return ZasobRepository
     */
    public function getZasobRepository() {
        return $this->getDoctrine()->getRepository('CmsElmatBundle:Zasob');
    }


    /**
     * @return ZasobCategoryRepository
     */
    public function getZasobCategoryRepository() {
        return $this->getDoctrine()->getRepository('CmsElmatBundle:ZasobCategory');
    }


    public function categoryAction($slug, $id, $page = 1) {


        $locale = $this->get('request')->getLocale();

        $em = $this->get('doctrine')->getManager();


        $zasob_kat = $this->getZasobCategoryRepository()->findOneBy(array('id' => $id));

        if (!$zasob_kat) {
            return $this->redirect("/");

        }

        if ($zasob_kat->getSlug() != $slug) {
            return $this->redirect(
                $this->get('router')->generate('cms_elmat_zasoby_show',
                    array(
                        'id' => $zasob_kat->getId(),
                        'slug' => $zasob_kat->getSlug(),
                    )));
        }

        $offer_group = $zasob_kat->getOfferGroup();


        $zasob_category_query = $em->createQuery(
            "
				SELECT z
				FROM   CmsElmatBundle:Zasob z
                LEFT JOIN z.category zc
                LEFT JOIN zc.offer_group og
		        
				WHERE  z.published = true
                and z.publishDate <= :date
		        " . ($offer_group ?
                "AND zc.id = :category_id"
                :
                "AND zc.slug = :category_slug "
            )
            . "
				ORDER BY z.publishDate DESC
				"
        )
            ->setParameter('date', new \DateTime('now'));


        if ($offer_group) {
            $zasob_category_query->setParameter('category_id', $zasob_kat->getId());
        } else {
            $zasob_category_query->setParameter('category_slug', $zasob_kat->getSlug());
        }

        $paginator = $this->get('knp_paginator');
        $zasoby = $paginator->paginate(
            $zasob_category_query,
            $this->get('request')->query->get('page', $page),
            8
        );

        // $zasoby = $zasob_category_query->execute();


//    	$breadcrumbs = $this->get("white_october_breadcrumbs");
//    	$breadcrumbs->addItem("Strona główna", "/");


//    	$breadcrumbs->addItem($zasob_kat->getTitle(), $this->get('router')->generate('cms_elmat_zasoby_show',
//    			array('id' => $zasob_kat->getId(), 'slug' => $zasob_kat->getSlug())));


        $parameters = array(
            'category' => $zasob_kat,
            'zasoby' => $zasoby,
            'offer_groups_labels' => $offer_group ? false : true,
        );


        $view = 'CmsElmatBundle:Zasob:category.html.twig';


        return $this->render($view, $parameters);


    }


}
