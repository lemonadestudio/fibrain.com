<?php

namespace Cms\ElmatBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Cms\ElmatBundle\Entity\Slider;


class SliderController extends Controller
{
    public function showAction($id)
    {
        $slider = $this->getDoctrine()
                ->getRepository('CmsElmatBundle:Slider')
                ->loadArticle($id, $this->get('request')->getLocale());
        
        return $this->render('CmsElmatBundle:Slider:slider.html.twig', array('slider' => $slider));
    }

}
