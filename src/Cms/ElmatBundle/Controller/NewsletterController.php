<?php

namespace Cms\ElmatBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Doctrine\ORM\Query;
use Cms\ElmatBundle\Repository\NewsletterMessageRepository;
use Cms\ElmatBundle\Repository\NewsletterSubscriberRepository;
use Cms\ElmatBundle\Entity\NewsletterSubscriber;
use Cms\ElmatBundle\Entity\NewsletterMessage;
use Cms\ElmatBundle\Entity\NewsletterMessageRecipient;
use Cms\ElmatBundle\Entity\User;
use Cms\ElmatBundle\Model\NewsletterMessageRecipientStatus;
use Cms\ElmatBundle\Model\NewsletterMessageComposer;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Cms\ElmatBundle\Form\Type\NewsletterSubscriberType;
use Symfony\Component\Form\FormError;
use Cms\ElmatBundle\Util\Config;
use Cms\ElmatBundle\Repository\UserRepository;

class NewsletterController extends Controller {

    public function testsendAction($id) {
        if (!$this->container->get('security.context')->isGranted('ROLE_ADMIN')) {
            throw new NotFoundHttpException();
        }
        
        $redirectUrl = $this->get('router')->generate('admin_cms_elmat_newslettermessage_list');
        $email = $this->getRequest()->get('email');
        $senderMail = $this->container->getParameter('mailer_user');
        $senderName = $this->get('cms_config')->get('ust_newsletter_wysylka_od');
        $newsletterMessage = $this->getNewsletterMessageRepository()->findOneById($id);
        
        if (!$newsletterMessage || !$email) {
            throw new NotFoundHttpException();
        }
        
        $messageComposer = new NewsletterMessageComposer(
                $this->createFakeRecipientForTestMessage($email, $newsletterMessage), 
                $this->container);

        if ($newsletterMessage->getSendOnlyToSubscribersWithAccount()) {
            $messageComposer->setMatchingUser(array($this->createFakeUserForTestMessage($email)));
        }

        if (!$messageComposer->canCompose()) {
            $this->get('session')->getFlashBag()->add('sonata_flash_error', 'Nie można utworzyć wiadomości dla tego adresu email.');
            return $this->redirect($redirectUrl);
        }

        $composedMessage = $messageComposer->composeMessage();

        if (!$composedMessage) {
            $this->get('session')->getFlashBag()->add('sonata_flash_error', 'Błąd podczas tworzenia wiadomości.');
            return $this->redirect($redirectUrl);
        }

        if ($this->sendTestMail($senderMail, $senderName, $composedMessage)) {
            $this->get('session')->getFlashBag()->add('sonata_flash_info', 'Wysłano testowy newsletter.');
        } else {
            $this->get('session')->getFlashBag()->add('sonata_flash_error', 'Błąd podczas wysyłania emaila.');
        }

        return $this->redirect($redirectUrl);
    }

    public function unsubscribeAction($id, $token) {
        $subscriber = $this->getNewsletterSubscriberRepository()
                ->findOneBy(array(
            'id' => $id,
            'token' => $token,
            'isActive' => true
        ));

        if ($subscriber) {
            $subscriber->setIsActive(false);
            $unsubscribeMessage = $this->get('cms_config')->get('ust_newsletter_dezaktywacja_komunikat');
            $this->getDoctrine()->getManager()->flush();
            $this->get('session')->getFlashBag()->add('popup', $unsubscribeMessage);
        }

        return $this->redirect($this
                                ->get('router')
                                ->generate('cms_elmat_default_homepage'));
    }
    
    public function confirmAction($id, $token) {
        $subscriber = $this->getNewsletterSubscriberRepository()
                ->findOneBy(array(
            'id' => $id,
            'token' => $token,
            'isActive' => false
        ));

        if ($subscriber) {
            $message = $this->get('cms_config')->get('ust_newsletter_aktywacja_komunikat');
            $subscriber->setIsActive(true);
            $this->getDoctrine()->getManager()->flush();
            $this->get('session')->getFlashBag()->add('popup', $message);
        }

        return $this->redirect($this
                                ->get('router')
                                ->generate('cms_elmat_default_homepage'));
    }
    
    public function subscribeAction() {
        $createdSubscriptionData = $this->createSubscription();

        return $this->render('CmsElmatBundle:Newsletter:subscribe.html.twig', $createdSubscriptionData);
    }
    
    public function sidebarNewsletterAction() {
        $form = $this->createForm(new NewsletterSubscriberType());
        
        return $this->render('CmsElmatBundle:Newsletter:sidebarNewsletter.html.twig', array(
            'isSubscriptionCreated' => false,
            'form' => $form->createView()
        ));
    }
    
    public function sidebarNewsletterSubscribeAction() {
        $createdSubscriptionData = $this->createSubscription();
        
        return $this->render('CmsElmatBundle:Newsletter:sidebarNewsletterForm.html.twig', $createdSubscriptionData);
    }
    
    private function sendActivationMessageToSubscriber($subscriber) {
        $messageVariables = array(
            'AKTYWACJA_URL' => $this->get('router')->generate(
                    'cms_elmat_newsletter_confirm', array(
                        'id' => $subscriber->getId(),
                        'token' => $subscriber->getToken()
                    ), true)
        );

        $template = $this->get('cms_email_templates')
                ->getParsed('newsletter_aktywacja_email', $messageVariables);

        $emailCopyTo = $this->get('cms_config')->get('ust_newsletter_aktywacja_kopia_email');
        $emailFrom = $this->get('cms_config')->get('ust_mailer_wiadomosc_od');

        $message = \Swift_Message::newInstance()
                ->setSubject($template->getTopic())
                ->setFrom($this->container->getParameter('mailer_user'), $emailFrom)
                ->setTo($subscriber->getEmail())
                ->setBody($template->getContent(), 'text/html')
                ->addPart(strip_tags($template->getContentTxt()), 'text/plain');

        if ($emailCopyTo) {
            $message->setBcc(array_map('trim', explode(',', $emailCopyTo)));
        }

        try {
            $this->get('mailer')->send($message);
        } catch (Exception $e) {}
    }
    
    private function sendTestMail($senderMail, $senderName, $composedMessage) {
        $email = \Swift_Message::newInstance()
                ->setSubject($composedMessage->getTitle())
                ->setFrom($senderMail, $senderName)
                ->setTo($composedMessage->getRecipientMail())
                ->setBody($composedMessage->getHtmlMessage(), 'text/html');

        if ($composedMessage->getPlainTextMessage()) {
            $email->addPart(strip_tags($composedMessage->getPlainTextMessage()), 'text/plain');
        }

        try {
            return (bool)$this->get('mailer')->send($email);
        } catch (Exception $e) {
            return false;
        }
    }
    
    private function createFakeRecipientForTestMessage($email, $newsletterMessage) {
        $fakeSubscriber = new NewsletterSubscriber();
        $fakeSubscriber->setEmail($email);
        $fakeSubscriber->setIsActive(true);
        $fakeSubscriber->setToken($fakeSubscriber->generateToken($email));
        $this->setPrivateField($fakeSubscriber, 'id', 0);
        
        $fakeRecipient = new NewsletterMessageRecipient();
        $fakeRecipient->setNewsletterMessage($newsletterMessage);
        $fakeRecipient->setNewsletterSubscriber($fakeSubscriber);
        $fakeRecipient->setStatus(NewsletterMessageRecipientStatus::READY_TO_SEND);
        $this->setPrivateField($fakeRecipient, 'id', 0);
        
        return $fakeRecipient;
    }
    
    private function createFakeUserForTestMessage($email) {
        $fakeUser = new User();
        $fakeUser->setEmail($email);
        $fakeUser->setConfirmationToken('1fc90646fd79ccfb8c94ff816926643f9ea5b45e');
        $fakeUser->setPasswordResetToken('1fc90646fd79ccfb8c94ff816926643f9ea5b45e');
        $fakeUser->setFax('999 999 999');
        $fakeUser->setFirma('Testowa firma');
        $fakeUser->setNip('1882286736');
        $fakeUser->setTelefon('111 111 111');
        $fakeUser->setUlica('Warszawaska 99/99');
        $fakeUser->setKodPocztowy('99-999');
        $fakeUser->setMiasto('Warszawa');
        $this->setPrivateField($fakeUser, 'id', 0);
        
        return $fakeUser;
    }
    
    private function getNewsletterMessageRepository() {
        return $this->getDoctrine()->getRepository('CmsElmatBundle:NewsletterMessage');
    }

    private function getNewsletterSubscriberRepository() {
        return $this->getDoctrine()->getRepository('CmsElmatBundle:NewsletterSubscriber');
    }

    private function getUserRepository() {
        return $this->getDoctrine()->getRepository('CmsElmatBundle:User');
    }

    private function setPrivateField($classInstance, $fieldNama, $newValue) {
        $reflector = new \ReflectionClass($classInstance);
        $property = $reflector->getProperty($fieldNama);
        $property->setAccessible(true);
        $property->setValue($classInstance, $newValue);
    }
    
    private function createSubscription() {
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new NewsletterSubscriberType());
        $isSubscriptionCreated = false;

        if ($request->isXmlHttpRequest()) {
            $form->bind($request);

            if ($form->isValid()) {
                $existingSubscriber = $this->getNewsletterSubscriberRepository()
                        ->findOneBy(array(
                            'email' => $form->get('email')->getData()
                        ));

                $subscriberIsAlreadyActive = $existingSubscriber && $existingSubscriber->getIsActive();
                $subscriberIsInactive = $existingSubscriber && !$existingSubscriber->getIsActive();
                $subscriberDoesNotExist = !$existingSubscriber;
                
                if ($subscriberIsAlreadyActive) {
                    $form->get('email')->addError(new FormError('Ten adres jest już w naszej bazie'));
                } else if ($subscriberIsInactive) {
                    $this->sendActivationMessageToSubscriber($existingSubscriber);
                    $isSubscriptionCreated = true;
                } else if ($subscriberDoesNotExist) {
                    $newSubscriber = new NewsletterSubscriber();
                    $newSubscriber->setEmail($form->get('email')->getData());
                    $newSubscriber->setIsActive(false);
                    $em->persist($newSubscriber);
                    $em->flush();
                    $this->sendActivationMessageToSubscriber($newSubscriber);
                    $isSubscriptionCreated = true;
                }
            }
        }

        return array(
            'isSubscriptionCreated' => $isSubscriptionCreated,
            'form' => $form->createView()
        );
    }
}
