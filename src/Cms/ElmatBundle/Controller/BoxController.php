<?php

namespace Cms\ElmatBundle\Controller;


use Cms\ElmatBundle\Entity\Box;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Template()
 */
class BoxController extends Controller
{

	/**
	 * @Template()
	 */
	public function showAction($slug) {

	    $slug = trim($slug);

		$box = $this->getDoctrine()->getRepository('CmsElmatBundle:Box')
		->findOneBy(array('slug' => $slug));

		if(!$box) {


			$box = new Box();
			$box->setTitle($slug);

			$box->setContent('<p>Box '.$slug.'</p><p>edytuj w panelu administracyjnym</p>');
			$box->setPublished(true);
			$box->setSlug($slug);
			$this->getDoctrine()->getManager()->persist($box);
			$this->getDoctrine()->getManager()->flush();

		}

		return array(
				'box' => $box,
		);

	}

	/**
	 * @Template()
	 */
	public function showfullAction($slug) {

	    return $this->showAction($slug);

	}

}
