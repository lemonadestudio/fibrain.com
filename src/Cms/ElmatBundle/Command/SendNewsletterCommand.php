<?php

namespace Cms\ElmatBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Cms\ElmatBundle\Entity\NewsletterSubscriber;
use Cms\ElmatBundle\Entity\NewsletterMessage;
use Cms\ElmatBundle\Repository\NewsletterMessageRecipientRepository;
use Cms\ElmatBundle\Model\NewsletterMessageRecipientStatus;
use Cms\ElmatBundle\Model\NewsletterMessageComposer;
use Cms\ElmatBundle\Model\NewsletterComposedMessage;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class SendNewsletterCommand extends ContainerAwareCommand
{    
    private $doctrine;
    private $em;
    private $newsletterMessageRecipientRepo;
    private $cmsConfig;
    private $mailer;
    private $logs;
    private $sentMailsCount;
    
    protected function configure()
    {
        $this
            ->setName('newsletter:send')
            ->setDescription('Wysyłanie newslettera.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->initDependencies();
        $mailsCountToSend = intval($this->cmsConfig->get('ust_newsletter_wysylka_maks_ilosc'));
        $this->logs[] = "Maksymalna ilość maili do wysłania: {$mailsCountToSend}";
        
        if ($mailsCountToSend <= 0) {
            return;
        }
        
        $recipients = $this->newsletterMessageRecipientRepo->getRecipientsToSendMessage($mailsCountToSend);
        $recipientUsers = $this->newsletterMessageRecipientRepo->getUsersByRecipientsMail($recipients);
        $senderMail = $this->getContainer()->getParameter('mailer_user');
        $senderName = $this->cmsConfig->get('ust_newsletter_wysylka_od');
        
        foreach ($recipients as $recipient) {
            $this->sendMessage($recipient, $recipientUsers, $senderMail, $senderName);
            $this->em->persist($recipient);
        }
        
        $this->em->flush();
        $this->logs[] = "Ilość wysłanych maili: {$this->sentMailsCount}.";
        $this->printLogs($output);
    }
    
    private function sendMessage($recipient, $recipientUsers, $senderMail, $senderName) {
        $messageComposer = new NewsletterMessageComposer($recipient, $this->getContainer());

        if ($recipient->getNewsletterMessage()->getSendOnlyToSubscribersWithAccount()) {
            $messageComposer->setMatchingUser($recipientUsers);
        }

        if (!$messageComposer->canCompose()) {
            $recipient->setStatus($messageComposer->getError());
            return;
        }

        $composedMessage = $messageComposer->composeMessage();

        if (!$composedMessage) {
            $recipient->setStatus(NewsletterMessageRecipientStatus::MESSAGE_COMPOSE_ERROR);
            return;
        }

        if ($this->sendMail($senderMail, $senderName, $composedMessage)) {
            $this->sentMailsCount++;

            $recipient->setSentDate(new \DateTime());
            $recipient->setStatus(NewsletterMessageRecipientStatus::SENT);
        } else {
            $recipient->setStatus(NewsletterMessageRecipientStatus::MAILER_ERROR);
        }
    }
    
    private function sendMail($senderMail, $senderName, $composedMessage) {
        $email = \Swift_Message::newInstance()
                ->setSubject($composedMessage->getTitle())
                ->setFrom($senderMail, $senderName)
                ->setTo($composedMessage->getRecipientMail())
                ->setBody($composedMessage->getHtmlMessage(), 'text/html');
        
        if ($composedMessage->getPlainTextMessage()) {
            $email->addPart(strip_tags($composedMessage->getPlainTextMessage()), 'text/plain');
        }

        try {
            if ($this->mailer->send($email)) {
                //http://stackoverflow.com/questions/11393667/symfony2-swift-mailer-command-email-body-displayed-into-the-console
                //http://stackoverflow.com/questions/13122096/unable-to-send-e-mail-from-within-custom-symfony2-command-but-can-from-elsewhere
                //wykonuje sie po analizie kazdego elementu w petli, zeby odrazu wylapac wyjatki np. dlugi czas dostepu do serwera
                $spool = $this->mailer->getTransport()->getSpool();
                $transport = $this->getContainer()->get('swiftmailer.transport.real');
                $spool->flushQueue($transport);
                
                $this->logs[] = "Wysłane od {$senderMail} do {$composedMessage->getRecipientMail()}";
                
                return true;
            } else {
                $this->logs[] = "Błąd wysyłania maila od {$senderMail} do {$composedMessage->getRecipientMail()}.";
                
                return false;
            }
        } catch (Exception $e) {
            $this->logs[] = "Błąd wysyłania maila od {$senderMail} do {$composedMessage->getRecipientMail()}. Treść błędu: {$e->toString()}";
            
            return false;
        }
    }
    
    // przypisywanie obiektow z kontenera w tej metodzie, bo
    // symfony sypie bledami jezeli robi sie to bezposrednio w konstruktorze
    // lub w configure()
    private function initDependencies() {
        $this->doctrine = $this->getContainer()->get('doctrine');
        $this->em = $this->doctrine->getManager();
        $this->newsletterMessageRecipientRepo = $this->doctrine
                ->getRepository('CmsElmatBundle:NewsletterMessageRecipient');
        $this->cmsConfig = $this->getContainer()->get('cms_config');
        $this->mailer = $this->getContainer()->get('mailer');
        $this->logs = array();
        $this->sentMailsCount = 0;
    }
    
    private function printLogs($output) {
        foreach ($this->logs as $log) {
            $output->writeln($log);
        }
    }
}
