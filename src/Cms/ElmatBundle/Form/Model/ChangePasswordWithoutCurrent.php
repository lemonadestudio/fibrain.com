<?php

namespace Cms\ElmatBundle\Form\Model;
use Symfony\Component\Security\Core\Validator\Constraints as SecurityAssert;
use Symfony\Component\Validator\Constraints as Assert;

class ChangePasswordWithoutCurrent
{
    

    /**
     * @Assert\Length(
     *     min = 6,
     *     minMessage = "Hasło powinno mieć przynajmniej 6 znaków"
     * )
     */
    protected $newPassword;


    public function getNewPassword()
    {
        return $this->newPassword;
    }

    public function setNewPassword($newPassword)
    {
        $this->newPassword = $newPassword;
    }

}
