<?php

namespace Cms\ElmatBundle\Form\Type;

use Cms\ElmatBundle\Helper\Cms;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ProductPowiazanyType extends AbstractType {

    private $container = null;

    public function setContainer($container = null) {
        $this->container = $container;
    }

    public function buildForm(FormBuilderInterface $builder, array $options) {

        if ($this->container) {
            $products_qb = $this->getProductsQB();
        } else {
            $products_qb = function (EntityRepository $er) {
                $qb = $er->createQueryBuilder('p')
                    ->leftJoin('p.category', 'u')
                    ->leftJoin('u.offer_group', 'og')
                    ->orderBy('u.root', 'ASC')
                    ->addOrderBy('u.lft', 'ASC');

                return $qb;
            };
        }

        $builder
            ->add('kolejnosc')
            ->add('powiazany', null, array(
                'group_by' => 'CategoryNametreeFull',
                'query_builder' => $products_qb,
            ))
            ->add('_delete', 'checkbox', array('required' => false, 'mapped' => false));

    }

    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Cms\ElmatBundle\Entity\ProductPowiazany',
        ));
    }

    public function getName() {
        return 'product_powiazany';
    }

    private function getOfferGroupsAccess() {
        $s = $this->container->get('security.context');

        $offer_groups = Cms::getOfferGroups();

        $slugs = array('__');
        foreach ($offer_groups as $og) {
            if ($s->isGranted($og['role'])) {
                $slugs[] = $og['slug'];
            }
        }

        return array(
            'slugs' => $slugs,
            'grupa_elmat' => $s->isGranted('ROLE_GRUPA_ELMAT')
        );
    }


    private function getProductsQB() {
        $og_access = $this->getOfferGroupsAccess();

        $products_qb = function (EntityRepository $er) use ($og_access) {
            $qb = $er->createQueryBuilder('p')
                ->leftJoin('p.category', 'u')
                ->leftJoin('u.offer_group', 'og')
                ->where('og.slug IN (:slugs)')
                ->setParameter('slugs', $og_access['slugs'])
                ->orderBy('u.root', 'ASC')
                ->addOrderBy('u.lft', 'ASC');

            if ($og_access['grupa_elmat']) {
                $qb->orWhere('og.slug IS NULL');
            }

            return $qb;
        };

        return $products_qb;
    }
}