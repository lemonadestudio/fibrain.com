<?php

namespace Cms\ElmatBundle\Form\Type;

use Cms\ElmatBundle\Entity\ProductTab;
use Sonata\AdminBundle\Admin\AdminInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;


class ProductTabType extends AbstractType {

    private $admin;

    public function __construct(AdminInterface $admin) {
        $this->admin = $admin;
    }

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('name', null, array())
            ->add('sort', null, array())
            ->add('description', null, array('required' => false))
            ->add('type', 'choice', array('choices' => array(ProductTab::TYPE_normalny => 'Zwykła zakładka', ProductTab::produkty_powiazane => 'Powiązane produkty'), 'required' => false))
            ->add('_delete', 'checkbox', array('required' => false, 'mapped' => false))
            ->add('updated', null, array('with_seconds' => 'true', 'attr' => array('style' => 'display:none;')));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Cms\ElmatBundle\Entity\ProductTab',
        ));
    }

    public function getName() {
        return 'product_tab';
    }
}
