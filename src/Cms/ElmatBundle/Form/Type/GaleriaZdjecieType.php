<?php

namespace Cms\ElmatBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class GaleriaZdjecieType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('_file', 'file', array('required' => false))
            ->add('podpis')
            ->add('kolejnosc')
            ->add('_delete', 'checkbox', array('required' => false, 'mapped' => false))
            ->add('updated_at', null, array('with_seconds' => 'true', 'attr' => array('style' => 'display:none;')));;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Cms\ElmatBundle\Entity\GaleriaZdjecie',
        ));
    }

    public function getName() {
        return 'galeria_zdjecie';
    }
}