<?php

namespace Cms\ElmatBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;

class KlientPrzypomnienieHaslaType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('email', 'text', array(
            'required' => true,
            'label' => 'Adres e-mail',
            'constraints' => array(
                new Email(),
                new NotBlank()
            )
        ));
    }

    public function getName() {
        return 'przypomnienie';
    }
}