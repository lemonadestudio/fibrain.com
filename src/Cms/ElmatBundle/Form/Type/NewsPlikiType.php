<?php

namespace Cms\ElmatBundle\Form\Type;

use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class NewsPlikiType extends PlikiType {

    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Cms\ElmatBundle\Entity\News',
        ));
    }

    public function getName() {
        return 'news_pliki';
    }
}