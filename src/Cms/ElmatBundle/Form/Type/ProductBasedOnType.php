<?php

namespace Cms\ElmatBundle\Form\Type;

use Sonata\AdminBundle\Admin\AdminInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ProductBasedOnType extends AbstractType {

    private $admin;

    public function __construct(AdminInterface $admin) {
        $this->admin = $admin;
    }

    public function buildForm(FormBuilderInterface $builder, array $options) {

        $offer_group_required = $this->admin->getOfferGroupRequired();
        $category_qb = $this->admin->getCategoryQB();

        $builder
            ->add('category', null, array(
                'property' => 'nametree',
                'query_builder' => $category_qb,
                'required' => $offer_group_required,
                'label' => 'label.category'
            ))
            ->add('_delete', 'checkbox', array('required' => false, 'mapped' => false));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Cms\ElmatBundle\Entity\Product',
        ));
    }

    public function getName() {
        return 'product_based_on';
    }
}
