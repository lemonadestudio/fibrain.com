<?php

namespace Cms\ElmatBundle\Form\Type;

use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints as Constraints;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Collection;

class KlientRejestracjaKrok2Type extends KlientRejestracjaType {

    public function buildForm(FormBuilderInterface $builder, array $options) {

        parent::buildForm($builder, $options);

        $zgoda_no_blank = new Constraints\NotBlank();
        $zgoda_no_blank->message = 'Należy wyrazić zgodę na przetwarzanie danych';

        $builder
            ->add('firma_isp', null, array('label' => 'ISP'))
            ->add('firma_tv_kablowa', null, array('label' => 'Operator telewizji kablowej'))
            ->add('firma_instalator', null, array('label' => 'Instalator sieci światłowodowych'))
            ->add('firma_instalator_sieci_niskopradowe', null, array('label' => 'Instalator sieci niskoprądowych'))
            ->add('firma_samorzad_terytorialny', null, array('label' => 'Samorząd terytorialny'))
            ->add('firma_handlowa', null, array('label' => 'Firma handlowa'))
            ->add('firma_administrator_sieci', null, array('label' => 'Administrator sieci'))
            ->add('firma_projektant', null, array('label' => 'Projektant/biuro projektowe'))
            ->add('firma_integrator', null, array('label' => 'Integrator'))
            ->add('firma_inny', null, array('label' => 'Inny (jaki)'))
            ->add('oferta_swiatlowody', null, array('label' => 'Kable światłowodowe'))
            ->add('oferta_akcesoria_swiatlowodowe', null, array('label' => 'Akcesoria światłowodowe'))
            ->add('oferta_mikrokanalizacja', null, array('label' => 'Mikrokanalizacja MetroJet'))
            ->add('oferta_kable_napowietrzne_airtrack', null, array('label' => 'Kable napowietrzne AirTrack'))
            ->add('oferta_technologia_fttx', null, array('label' => 'Technologia FTTx'))
            ->add('oferta_technologia_gpon', null, array('label' => 'Technologia GPON'))
            ->add('oferta_urzadzenia_aktywne', null, array('label' => 'Urządzenia aktywne'))
            ->add('oferta_sieci_miejskie', null, array('label' => 'Rozwiązania dla sieci miejskich'))
            ->add('oferta_osprzet_instalacyjny', null, array('label' => 'Osprzęt instalacyjny'))
            ->add('oferta_osprzet_pomiarowy', null, array('label' => 'Osprzęt pomiarowy'))
            ->add('oferta_okablowanie_strukturalne', null, array('label' => 'Okablowanie strukturalne'))
            ->add('oferta_inne', null, array('label' => 'Inne'))
            ->add('info_staly_klient', null, array('label' => 'Jestem stałym klientem'))
            ->add('info_wyszukiwarka', null, array('label' => 'Wyszukiwarka'))
            ->add('info_prasa_codzienna', null, array('label' => 'Prasa codzienna'))
            ->add('info_reklamy', null, array('label' => 'Reklamy w internecie'))
            ->add('info_prasa_specjalistyczna', null, array('label' => 'Prasa specjalistyczna'))
            ->add('info_znajomi', null, array('label' => 'Znajomi, z polecenia'))
            ->add('info_subskrypcja', null, array('label' => 'Subskrybcja ze strony'))
            ->add('info_inne', null, array('label' => 'Inne'))
            ->add('zgoda_przetwarzanie', 'checkbox', array('required' => true, 'mapped' => false, 'constraints' => array($zgoda_no_blank)))
            ->add('krok', 'hidden', array('data' => '2', 'mapped' => false));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Cms\ElmatBundle\Entity\User',
        ));
    }

    public function getName() {
        return 'klient';
    }
}