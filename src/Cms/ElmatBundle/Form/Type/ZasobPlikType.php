<?php

namespace Cms\ElmatBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ZasobPlikType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('sciezka', null, array('attr' => array('readonly' => 'readonly', 'onclick' => 'openKCFinder(this)')))
            ->add('nazwa', null, array('attr' => array('class' => 'input-nazwa')))
            ->add('rozszerzenie', null, array('attr' => array('class' => 'input-rozszerzenie')))
            ->add('rozmiar', null, array('attr' => array('class' => 'input-rozmiar')));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Cms\ElmatBundle\Entity\Plik',
        ));
    }

    public function getName() {
        return 'zasob_plik';
    }
}