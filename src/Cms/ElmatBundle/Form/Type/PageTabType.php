<?php

namespace Cms\ElmatBundle\Form\Type;

use Sonata\AdminBundle\Admin\AdminInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PageTabType extends AbstractType {

    private $admin;

    public function __construct(AdminInterface $admin) {
        $this->admin = $admin;
    }

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('name', null, array())
            ->add('sort', null, array())
            ->add('description', null, array('required' => false))
            ->add('_delete', 'checkbox', array('required' => false, 'mapped' => false))
            ->add('updated', null, array('with_seconds' => 'true', 'attr' => array('style' => 'display:none;')));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Cms\ElmatBundle\Entity\ContentTab',
        ));
    }

    public function getName() {
        return 'page_tab';
    }
}
