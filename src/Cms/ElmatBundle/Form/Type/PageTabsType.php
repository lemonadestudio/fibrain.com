<?php

namespace Cms\ElmatBundle\Form\Type;

use Sonata\AdminBundle\Admin\AdminInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PageTabsType extends AbstractType {
    private $admin;

    public function __construct(AdminInterface $admin) {
        $this->admin = $admin;
    }

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('tabs', 'collection', array(
            'type' => new PageTabType($this->admin),
            'allow_add' => true,
            'by_reference' => false,
        ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Cms\ElmatBundle\Entity\Page',
        ));
    }

    public function getName() {
        return 'page_tabs';
    }
}
