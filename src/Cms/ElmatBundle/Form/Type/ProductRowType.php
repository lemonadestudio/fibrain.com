<?php

namespace Cms\ElmatBundle\Form\Type;

use Sonata\AdminBundle\Admin\AdminInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ProductRowType extends AbstractType {

    private $admin;

    public function __construct(AdminInterface $admin) {
        $this->admin = $admin;
    }

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('name', null, array('required' => false))
            ->add('sort', null, array())
            ->add('_file_image', 'file', array('required' => false))
            ->add('_file_doc_pl', 'file', array('required' => false))
            ->add('_file_doc_en', 'file', array('required' => false))
            ->add('_delete_file_image', 'checkbox', array('required' => false, 'label' => 'Usunąć zdjęcie?'))
            ->add('_delete_file_doc_pl', 'checkbox', array('required' => false, 'label' => 'Usunąć plik?'))
            ->add('_delete_file_doc_en', 'checkbox', array('required' => false, 'label' => 'Usunąć plik?'))
            ->add('value1', null, array('required' => false))
            ->add('value2', null, array('required' => false))
            ->add('value3', null, array('required' => false))
            ->add('value4', null, array('required' => false))
            ->add('value5', null, array('required' => false))
            ->add('value6', null, array('required' => false))
            ->add('value7', null, array('required' => false))
            ->add('_delete', 'checkbox', array('required' => false, 'mapped' => false))
            ->add('updated', null, array('with_seconds' => 'true', 'attr' => array('style' => 'display:none;')))
            ->add('type', 'hidden')
            ->add('separator_bgcolor')
            ->add('separator_text1', null, array('required' => false))
            ->add('separator_text2', null, array('required' => false))
            ->add('height', null, array('required' => false));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Cms\ElmatBundle\Entity\ProductRow',
        ));
    }

    public function getName() {
        return 'product_row';
    }
}
