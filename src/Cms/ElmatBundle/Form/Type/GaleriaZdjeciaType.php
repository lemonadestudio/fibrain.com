<?php


namespace Cms\ElmatBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class GaleriaZdjeciaType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('zdjecia', 'collection', array(
            'type' => new GaleriaZdjecieType(),
            'allow_add' => true,
            'by_reference' => false,
        ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Cms\ElmatBundle\Entity\Galeria',
        ));
    }

    public function getName() {
        return 'galeria_zdjecia';
    }
}