<?php

namespace Cms\ElmatBundle\Form\Type;

use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PagePlikiType extends PlikiType {

    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Cms\ElmatBundle\Entity\Page',
        ));
    }

    public function getName() {
        return 'page_pliki';
    }
}