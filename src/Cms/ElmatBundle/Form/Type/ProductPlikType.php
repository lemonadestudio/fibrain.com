<?php

namespace Cms\ElmatBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ProductPlikType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('sciezka')
            ->add('nazwa')
            ->add('rozszerzenie')
            ->add('rozmiar')
            ->add('kolejnosc')
            ->add('_delete', 'checkbox', array('required' => false, 'mapped' => false))
            ->add('updated', null, array('with_seconds' => 'true', 'attr' => array('style' => 'display:none;')));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Cms\ElmatBundle\Entity\Plik',
        ));
    }

    public function getName() {
        return 'product_plik';
    }
}