<?php

namespace Cms\ElmatBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Validator\Constraints as Constraints;

class NewsletterSubscriberType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('email', 'text', array(
            'required' => true,
            'constraints' => array(
                new Constraints\Email(),
                new Constraints\NotBlank()
            )
        ));
    }

    public function getName() {
        return 'NewsletterSubscriber';
    }
}
