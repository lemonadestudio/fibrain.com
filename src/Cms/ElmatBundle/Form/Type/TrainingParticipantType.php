<?php

namespace Cms\ElmatBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints as Constraints;

class TrainingParticipantType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('nazwa', null, array('constraints' => array(new Constraints\NotBlank())))
            ->add('email', null, array('constraints' => array(new Constraints\Email())))
            ->add('telefon');
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Cms\ElmatBundle\Entity\TrainingParticipant',
        ));
    }

    public function getName() {
        return 'training_participant';
    }
}