<?php

namespace Cms\ElmatBundle\Form\Type;

use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints as Constraints;
use Symfony\Component\Form\FormBuilderInterface;

class TrainingRejestracjaKrok2Type extends TrainingRejestracjaType {

    public function buildForm(FormBuilderInterface $builder, array $options) {

        parent::buildForm($builder, $options);

        $builder
            ->add('uwagi', 'textarea')
            ->add('krok', 'hidden', array('data' => '2', 'mapped' => false));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Cms\ElmatBundle\Entity\TrainingRegistration',
        ));
    }

    public function getName() {
        return 'training_registration';
    }
}