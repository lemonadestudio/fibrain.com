<?php

namespace Cms\ElmatBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ProductPlikiType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('pliki', 'collection', array(
            'type' => new ProductPlikType(),
            'allow_add' => true,
            'by_reference' => false,
        ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Cms\ElmatBundle\Entity\Product',
        ));
    }

    public function getName() {
        return 'product_pliki';
    }
}