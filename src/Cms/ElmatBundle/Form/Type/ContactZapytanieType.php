<?php

namespace Cms\ElmatBundle\Form\Type;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\MaxLength;
use Symfony\Component\Form\AbstractType;
use Cms\ElmatBundle\Validator\Constraints\GoogleRecaptcha;

class ContactZapytanieType extends AbstractType {
    /**
     *
     * @var ContainerInterface
     */
    private $container;

    public function __construct(ContainerInterface $container) {
        $this->container = $container;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('firma', 'text', array('required' => false, 'label' => 'Company'))
            ->add('imie_nazwisko', 'text', array('label' => 'Full name'))
            ->add('email', 'text', array('label' => 'E-mail address'))
            ->add('telefon', 'text', array('required' => false, 'label' => 'Phone number'))
            ->add('tresc', 'textarea', array('label' => 'Message'))
            ->add('google_recaptcha', new GoogleRecaptchaType($this->container), array('label' => 'Anti-spam security'))
            ->add('g-recaptcha-response', null, array('mapped' => false));
    }

    /**
     * @param array $options
     * @return array
     */
    public function getDefaultOptions(array $options) {
        $collectionConstraint = new Collection(array(
            'firma' => array(new MaxLength(100)),
            'imie_nazwisko' => array(new NotBlank(array('message' => 'Imię: Uzupełnij to pole')), new MaxLength(100)),
            'email' => array(new NotBlank(array('message' => 'Email: Uzupełnij to pole')), new Email(array('message' => 'Email: Nieprawidłowy adres e-mail'))),
            'telefon' => array(new MaxLength(100)),
            'tresc' => array(new NotBlank(array('message' => 'Treść: Uzupełnij to pole')), new MaxLength(100)),
            'google_recaptcha' => array(new GoogleRecaptcha())
        ));

        return array('validation_constraint' => $collectionConstraint);
    }

    public function getName() {
        return '';
    }

}
