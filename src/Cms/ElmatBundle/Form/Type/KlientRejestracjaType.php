<?php

namespace Cms\ElmatBundle\Form\Type;

use Cms\ElmatBundle\Entity\User;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints as Constraints;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Form\AbstractType;

class KlientRejestracjaType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {

        $wojewodztwa_choices = array_merge(array('' => ' -- Wybierz --'), User::$wojewodztwa);

        $builder
            ->add('username', null, array('label' => 'Login:', 'constraints' => array(new Constraints\NotBlank())))
            ->add('plainPassword', 'repeated', array(
                'type' => 'password',
                'invalid_message' => 'Hasła nie pasują do siebie',
                'options' => array('attr' => array('class' => 'password-field')),
                'required' => true,
                'first_options' => array('label' => 'Hasło:', 'always_empty' => false),
                'second_options' => array('label' => 'Powtórz hasło:', 'always_empty' => false),
                'constraints' => array(new Constraints\NotBlank())
            ))
            ->add('email', null, array('label' => 'Adres E-mail:', 'constraints' => array(new Constraints\NotBlank())))
            ->add('firma', null, array('label' => 'Nazwa firmy:'))
            ->add('ulica', null, array('label' => 'Ulica:'))
            ->add('kod_pocztowy', null, array('label' => 'Kod pocztowy:'))
            ->add('miasto', null, array('label' => 'Miasto:'))
            ->add('wojewodztwo', 'choice', array('choices' => $wojewodztwa_choices, 'label' => 'Województwo:'))
            ->add('nip', null, array('label' => 'NIP:'))
            ->add('telefon', null, array('label' => 'Telefon:'))
            ->add('fax', null, array('label' => 'Fax:'));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Cms\ElmatBundle\Entity\User',
        ));
    }

    public function getName() {
        return 'klient';
    }
}