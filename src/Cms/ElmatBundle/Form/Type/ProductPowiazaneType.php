<?php


namespace Cms\ElmatBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ProductPowiazaneType extends AbstractType {

    private $container = null;

    public function setContainer($container = null) {
        $this->container = $container;
    }

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $produkt_powiazany_type = new ProductPowiazanyType();
        $produkt_powiazany_type->setContainer($this->container);

        $builder->add('powiazane', 'collection', array(
            'type' => $produkt_powiazany_type,
            'allow_add' => true,
            'by_reference' => false,
        ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Cms\ElmatBundle\Entity\Product',
        ));
    }

    public function getName() {
        return 'product_powiazane';
    }
}