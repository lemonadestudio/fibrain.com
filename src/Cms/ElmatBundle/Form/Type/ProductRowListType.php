<?php

namespace Cms\ElmatBundle\Form\Type;

use Sonata\AdminBundle\Admin\AdminInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ProductRowListType extends AbstractType {

    private $admin;

    public function __construct(AdminInterface $admin) {
        $this->admin = $admin;
    }

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('name', null, array())
            ->add('sort', null, array())
            ->add('_file_image', 'file', array('required' => false))
            ->add('_file_doc_pl', 'file', array('required' => false))
            ->add('_file_doc_en', 'file', array('required' => false))
            ->add('_delete_file_image', 'checkbox', array('required' => false, 'label' => 'Usunąć zdjęcie?'))
            ->add('_delete_file_doc_pl', 'checkbox', array('required' => false, 'label' => 'Usunąć plik?'))
            ->add('_delete_file_doc_en', 'checkbox', array('required' => false, 'label' => 'Usunąć plik?'))
            ->add('description', null, array('required' => false))
            ->add('_delete', 'checkbox', array('required' => false, 'mapped' => false))
            ->add('updated', null, array('with_seconds' => 'true', 'attr' => array('style' => 'display:none;')))
            ->add('type', 'hidden')
            ->add('separator_bgcolor')
            ->add('separator_text1')
            ->add('separator_text2');
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Cms\ElmatBundle\Entity\ProductRow',
        ));
    }

    public function getName() {
        return 'product_row_list';
    }
}
