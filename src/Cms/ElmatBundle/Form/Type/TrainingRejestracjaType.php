<?php

namespace Cms\ElmatBundle\Form\Type;

use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints as Constraints;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Form\AbstractType;

class TrainingRejestracjaType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('training_participants', 'collection', array(
            'type' => new TrainingParticipantType(),
            'allow_add' => true,
            'error_bubbling' => false,
            'by_reference' => false,
        ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Cms\ElmatBundle\Entity\TrainingRegistration',
        ));
    }

    public function getName() {
        return 'training_registration';
    }
}