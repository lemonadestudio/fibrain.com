<?php

namespace Cms\ElmatBundle\Form\Type;

use Sonata\AdminBundle\Admin\AdminInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ProductRowsType extends AbstractType {

    private $admin;

    public function __construct(AdminInterface $admin) {
        $this->admin = $admin;
    }

    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder->add('rows', 'collection', array(
            'type' => new ProductRowType($this->admin),
            'allow_add' => true,
            'by_reference' => false,
        ))
            ->add('col1')
            ->add('col2')
            ->add('col3')
            ->add('col4')
            ->add('col5')
            ->add('col6')
            ->add('col7')
            ->add('col_name')
            ->add('col_image')
            ->add('col_doc')
            ->add('col1_width')
            ->add('col2_width')
            ->add('col3_width')
            ->add('col4_width')
            ->add('col5_width')
            ->add('col6_width')
            ->add('col7_width')
            ->add('col_name_width')
            ->add('col_image_width')
            ->add('col_doc_width');
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Cms\ElmatBundle\Entity\Product',
        ));
    }

    public function getName() {
        return 'product_rows';
    }
}
