<?php

namespace Cms\ElmatBundle\Form\Type;

use Symfony\Component\Validator\Constraints as Constraints;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class KlientZmianaHaslaType extends AbstractType {

    private $_options = array();

    public function __construct($options = array()) {
        $def_options = array(
            'without_current_password' => false
        );

        $this->_options = array_merge($def_options, $options);
    }

    public function buildForm(FormBuilderInterface $builder, array $options) {
        if (!$this->_options['without_current_password']) {

            $builder->add('oldPassword', 'password', array(
                'required' => true,
                'label' => 'Aktualne hasło',
                'attr' => array('class' => 'password-field')
            ));

        };

        $builder->add('newPassword', 'repeated', array(
            'type' => 'password',
            'invalid_message' => 'Hasła nie pasują do siebie',
            'options' => array('attr' => array('class' => 'password-field')),
            'required' => true,
            'first_options' => array('label' => 'Nowe hasło:', 'always_empty' => false),
            'second_options' => array('label' => 'Powtórz hasło:', 'always_empty' => false),
            'constraints' => array(new Constraints\NotBlank())
        ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        if (!$this->_options['without_current_password']) {
            $resolver->setDefaults(array(
                'data_class' => 'Cms\ElmatBundle\Form\Model\ChangePassword',
            ));
        } else {
            $resolver->setDefaults(array(
                'data_class' => 'Cms\ElmatBundle\Form\Model\ChangePasswordWithoutCurrent',
            ));
        }
    }

    public function getName() {
        return 'klient_haslo';
    }
}