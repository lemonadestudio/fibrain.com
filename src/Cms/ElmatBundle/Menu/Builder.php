<?php

namespace Cms\ElmatBundle\Menu;

use Cms\ElmatBundle\Entity\MenuItem;

use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Symfony\Component\DependencyInjection\ContainerAware;

class Builder extends ContainerAware {
    var $depth      = 1;
    var $curr_depth = 1;

    public function managedMenu(FactoryInterface $factory, $options = array()) {
        $menu = $factory->createItem('root');

        if (array_key_exists('REDIRECT_URL', $_SERVER)) {
            $menu->setCurrentUri($_SERVER['REDIRECT_URL']);
        } else {
            $current_uri = $this->container->get('request')->getRequestUri();
        }

        $em = $this->container->get('doctrine')->getManager();

        $location = $options['location'];
        $this->depth = array_key_exists('depth', $options) ? $options['depth'] : 1;

        $menu_items = $em->createQueryBuilder()
            ->select('m', 'm2', 'm3')
            ->from('CmsElmatBundle:MenuItem', 'm')
            ->leftJoin('m.childItems', 'm2')
            ->leftJoin('m2.childItems', 'm3')
            ->where('m.location = :location')
            ->andWhere('m.depth = 1')
            ->orderBy('m.lft', 'ASC')
            ->addOrderBy('m.title', 'ASC')
            ->setParameter('location', $location)
            ->getQuery()
            ->getResult();

        foreach ($menu_items as $item) {
            $this->buildMenu($item, $menu);
        }

        return $menu;
    }

    /**
     *
     * @param MenuItem $item
     * @param  ItemInterface $menu
     */
    public function buildMenu($item, &$menu) {
        $item_arr = $this->getMenuItemArray($item);

        if (!$item_arr) {
            return;
        }

        $name = $menu->addChild($item_arr['anchor'], array(
            'uri' => $item_arr['uri'],
            'linkAttributes' => array_merge(array('title' => $item_arr['title']), $item_arr['linkAttributes'])
        ))->getName();;


        /*
         * wczytywanie drzeewka w dol
         *
         */
        if ($this->curr_depth < $this->depth) {
            if ($item->getChildItems()) {
                $this->curr_depth++;
                $ch = $menu->getChild($name);
                foreach ($item->getChildItems() as $item1) {
                    $this->buildMenu($item1, $ch);
                }
                $this->curr_depth--;
            }
        }
    }


    public function getMenuItemArray($item) {
        $router = $this->container->get('router');

        if (!$item->getUrl() && !$item->getRoute()) {
            return false;
        }

        $uri = false;

        switch ($item->getType()) {
            case 'url':
                $uri = $item->getUrl();
                break;

            case 'route':
                if ($item->getRoute()) {
                    $route = $router->getRouteCollection()->get($item->getRoute());

                    if ($route) {
                        $parameters = array();

                        foreach ($item->getRouteParametersArray() as $k => $param) {
                            if (preg_match('@\{' . $k . '\}@', $route->getPath())) {
                                $parameters[$k] = $param;
                            }
                        }

                        try {
                            $uri = $router->generate($item->getRoute(), $parameters);
                        } catch (\Exception $e) {
                            // return false;
                        }
                    }
                }
                break;

            default:
                break;
        }

        if ($uri === false) {
            return false;
        }

        return array(
            'anchor' => $item->getAnchor(),
            'title' => $item->getTitle(),
            'uri' => $uri,
            'linkAttributes' => array()
        );
    }
}
