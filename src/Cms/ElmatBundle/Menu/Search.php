<?php

namespace Cms\ElmatBundle\Menu;

use Cms\ElmatBundle\Entity\Article;

use Symfony\Component\DependencyInjection\ContainerInterface;

class Search {


    private $container;
    private $method;

    public function __construct(ContainerInterface $container, $method = '') {
        $this->container = $container;
        $this->method = $method;
    }

    public function search($search) {
        switch ($this->method) {
            case 'podstrona' :
            case 'podstrona_structural_cabling':
            case 'podstrona_fiber_optic':
            case 'podstrona_fttx_systems':
            case 'podstrona_passive_optical':
            case 'podstrona_active_elements':
            case 'podstrona_connectivity':
            case 'podstrona_distribution':
            case 'podstrona_photonics':
                return $this->podstrona('', $this->method);
                break;

            case 'zasob' :
            case 'zasob_structural_cabling':
            case 'zasob_fiber_optic':
            case 'zasob_fttx_systems':
            case 'zasob_passive_optical':
            case 'zasob_active_elements':
            case 'zasob_connectivity':
            case 'zasob_distribution':
            case 'zasob_photonics':
                return $this->zasob('', $this->method);
                break;

            case Article::TYPE_artykuly_techniczne:
            case Article::TYPE_rozwiazania:
            case Article::TYPE_uslugi:
                return $this->artykul_rozwiazanie_usluga('', $this->method);
                break;

            case 'training-category':
                return $this->training_category('');
                break;

            default:
                break;
        }

        return array();
    }

    public function artykul_rozwiazanie_usluga($search = '', $type = '') {

        $em = $this->container->get('doctrine')->getManager();

        $search = mb_strtolower($search);

        $objects = $em->createQueryBuilder()
            ->select('s')
            ->from('CmsElmatBundle:Article', 's')
            ->where('LOWER(s.title) LIKE :search')
            ->andWhere('s.type = :type')
            ->orderBy('s.title', 'ASC')
            ->setParameter('search', '%' . $search . '%')
            ->setParameter('type', $type)
            ->getQuery()
            ->execute();

        $ret_arr = array();
        $ret_arr[] = array(
            'parameters' => array(),
            'title' => ''
        );
        foreach ($objects as $obj) {
            $ret_arr[] = array(
                'parameters' => array('id' => $obj->getId(), 'slug' => $obj->getSlug(), 'type' => $obj->getType()),
                'title' => $obj->getTitle()
            );
        }

        return $ret_arr;

    }

    public function podstrona($search = '', $method = '') {
        $em = $this->container->get('doctrine')->getManager();

        $search = mb_strtolower($search);

        $offer_group = '';
        $matches = array();
        if (preg_match('@podstrona_(.*)@i', $method, $matches)) {
            $offer_group = $matches[1];

            if ($offer_group == 'structural_cabling') {
                $offer_group = 'structural-cabling';
            }
            if ($offer_group == 'fiber_optic') {
                $offer_group = 'fiber-optic-cables';
            }
            if ($offer_group == 'fttx_systems') {
                $offer_group = 'fttx-systems-and-elements';
            }
            if ($offer_group == 'passive_optical') {
                $offer_group = 'passive-optical-technology';
            }
            if ($offer_group == 'active_elements') {
                $offer_group = 'active-elements-and-systems';
            }
            if ($offer_group == 'connectivity') {
                $offer_group = 'connectivity';
            }
            if ($offer_group == 'distribution') {
                $offer_group = 'distribution';
            }
            if ($offer_group == 'photonics') {
                $offer_group = 'photonics';
            }
        }

        $strony_query = $em->createQueryBuilder();
        $strony_query->select('s');
        $strony_query->from('CmsElmatBundle:Page', 's');
        $strony_query->leftJoin('s.offer_group', 'og');
        $strony_query->where('LOWER(s.title) LIKE :search');
        if ($offer_group) {
            $strony_query->andWhere('og.slug = :offer_group');
            $strony_query->setParameter('offer_group', $offer_group);
        } else {
            $strony_query->andWhere('s.offer_group IS NULL');
        }
        $strony_query->orderBy('s.title', 'ASC');
        $strony_query->setParameter('search', '%' . $search . '%');

        $strony = $strony_query->getQuery()->execute();

        $ret_arr = array();
        $ret_arr[] = array(
            'parameters' => array(),
            'title' => ''
        );
        foreach ($strony as $strona) {
            $ret_arr[] = array(
                'parameters' => array('id' => $strona->getId(), 'slug' => $strona->getSlug()),
                'title' => $strona->getTitle()
            );
        }

        return $ret_arr;
    }


    public function zasob($search = '', $method = '') {
        $em = $this->container->get('doctrine')->getManager();

        $search = mb_strtolower($search);

        $offer_group = '';
        $matches = array();
        if (preg_match('@zasob_(.*)@i', $method, $matches)) {
            $offer_group = $matches[1];

            if ($offer_group == 'structural_cabling') {
                $offer_group = 'structural-cabling';
            }
            if ($offer_group == 'fiber_optic') {
                $offer_group = 'fiber-optic-cables';
            }
            if ($offer_group == 'fttx_systems') {
                $offer_group = 'fttx-systems-and-elements';
            }
            if ($offer_group == 'passive_optical') {
                $offer_group = 'passive-optical-technology';
            }
            if ($offer_group == 'active_elements') {
                $offer_group = 'active-elements-and-systems';
            }
            if ($offer_group == 'connectivity') {
                $offer_group = 'connectivity';
            }
            if ($offer_group == 'distribution') {
                $offer_group = 'distribution';
            }
            if ($offer_group == 'photonics') {
                $offer_group = 'photonics';
            }
        }

        $zasob_category_query = $em->createQueryBuilder();
        $zasob_category_query->select('s');
        $zasob_category_query->from('CmsElmatBundle:ZasobCategory', 's');
        $zasob_category_query->leftJoin('s.offer_group', 'og');
        $zasob_category_query->where('LOWER(s.title) LIKE :search');
        if ($offer_group) {
            $zasob_category_query->andWhere('og.slug = :offer_group');
            $zasob_category_query->setParameter('offer_group', $offer_group);
        } else {
            $zasob_category_query->andWhere('s.offer_group IS NULL');
        }
        $zasob_category_query->orderBy('s.title', 'ASC');
        $zasob_category_query->setParameter('search', '%' . $search . '%');

        $categories = $zasob_category_query->getQuery()->execute();

        $ret_arr = array();
        $ret_arr[] = array(
            'parameters' => array(),
            'title' => ''
        );
        foreach ($categories as $zasob_category) {
            $ret_arr[] = array(
                'parameters' => array('id' => $zasob_category->getId(), 'slug' => $zasob_category->getSlug()),
                'title' => ($zasob_category->getOfferGroup() ? $zasob_category->getOfferGroup()->getTitle() . ' - ' : '') . $zasob_category->getTitle()
            );
        }

        return $ret_arr;
    }


    public function training_category($search = '') {

        $em = $this->container->get('doctrine')->getManager();

        $search = mb_strtolower($search);

        $objects = $em->createQueryBuilder()
            ->select('s')
            ->from('CmsElmatBundle:TrainingCategory', 's')
            ->where('LOWER(s.title) LIKE :search')
            ->orderBy('s.title', 'ASC')
            ->setParameter('search', '%' . $search . '%')
            ->getQuery()
            ->execute();

        $ret_arr = array();
        $ret_arr[] = array(
            'parameters' => array(),
            'title' => ''
        );
        foreach ($objects as $obj) {
            $ret_arr[] = array(
                'parameters' => array('slug' => $obj->getSlug()),
                'title' => $obj->getTitle()
            );
        }

        return $ret_arr;
    }


}