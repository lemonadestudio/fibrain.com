<?php

namespace Cms\ElmatBundle\Admin;

use Cms\ElmatBundle\Helper\Cms;
use Cms\ElmatBundle\Entity\ProductCategory;
use Doctrine\ORM\EntityRepository;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\DoctrineORMAdminBundle\Datagrid\ProxyQuery;
use Knp\Menu\ItemInterface as MenuItemInterface;

class ProductCategoryAdmin extends Admin {

    public function createQuery($context = 'list') {
        $query = parent::createQuery($context);

        $og_access = $this->getOfferGroupsAccess();

        $query = $query
            ->leftJoin($query->getRootAlias() . '.offer_group', 'og')
            ->leftJoin($query->getRootAlias() . '.parent', 'p')
            ->where('og.slug IN (:slugs)')
            ->setParameter('slugs', $og_access['slugs']);

        if ($og_access['grupa_elmat']) {
            $query = $query->orWhere('og.slug IS NULL');
        }

        $query->orderBy('og.id', 'ASC');
        $query->addOrderBy('o.kolejnosc', 'ASC');
        $query->addOrderBy('o.lft', 'ASC');

        $query = new ProxyQuery($query);

        return $query;
    }

    protected $translationDomain = 'CmsElmatBundle';

    protected $datagridValues = array(
        '_page' => 1,
        '_per_page' => 25,
    );

    protected function configureSideMenu(MenuItemInterface $menu, $action, AdminInterface $childAdmin = null) {
        if (!$childAdmin && !in_array($action, array('edit'))) {
            return;
        }
        $admin = $this->isChild() ? $this->getParent() : $this;
        $id = $admin->getRequest()->get('id');

        $menu->addChild('Edytuj', array('uri' => $this->generateUrl('edit', array('id' => $id))));
    }

    protected function configureRoutes(RouteCollection $collection) {
        $collection->add('move_up', $this->getRouterIdParameter() . '/move_up');
        $collection->add('move_down', $this->getRouterIdParameter() . '/move_down');
        $collection->add('kadruj', $this->getRouterIdParameter() . '/kadruj');

    }

    public function getTemplate($name) {
        switch ($name) {
            case 'edit':
                return 'CmsElmatBundle:Admin\ProductCategory:edit.html.twig';
                break;

            default:
                return parent::getTemplate($name);
                break;
        }
    }

    protected function configureFormFields(FormMapper $formMapper) {
        $offer_group_qb = $this->getOfferGroupQB();
        $category_qb = $this->getCategoryQB();

        $id = $this->getRequest()->get($this->getIdParameter());

        $formMapper->with('label.mainoptions');
        $formMapper->add('name', null, array('required' => true));
        $formMapper->add('content', null, array('required' => false, 'attr' => array('class' => 'sonata-medium wysiwyg')));
        $formMapper->add('content_short', null, array('required' => false, 'attr' => array('class' => 'sonata-medium wysiwyg')));
        $formMapper->add('offer_group', null, array(
            'query_builder' => $offer_group_qb,
            'required' => true
        ));
        $formMapper->add('template', 'choice', array(
            'required' => false,
            'choices' => array(
                ProductCategory::TEMPLATE_DEFAULT => 'Widok domyślny',
                ProductCategory::TEMPLATE_LIST => 'Widok listy'
            )
        ));
        $formMapper->add('parent', null, array(
            'property' => 'nametree',
            'query_builder' => $category_qb,
            'required' => false,
            'label' => 'label.parent_category'
        ));

        if ($id) {
            $obj = $this->getObject($id);
            if ($obj->getLvl() == 0) {
                $formMapper->add('kolejnosc');
            }
        }

        $formMapper->add('_file_image', 'file', array('required' => false));
        $formMapper->add('_delete_file_image', 'checkbox', array('required' => false, 'label' => 'Usunąć zdjęcie?'));
        $formMapper->end();
        $formMapper->with('label.seo');
        $formMapper->add('slug', null, array('required' => false));
        $formMapper->add('updated', null, array('with_seconds' => 'true', 'label' => ' ', 'attr' => array('style' => 'display:none;')));
        $formMapper->end();
        $formMapper->setHelps(array(
            'slug' => $this->trans('help.slug'),
            'content_short' => 'Krótki opis widoczny na listowaniu kategorii obok zdjęcia',
            'offer_group' => 'Zmiana grupy ofertowej jest możliwa tylko dla kategorii na najwyższym poziomie. Po zmianie grupy dla nadrzędnej kategorii, grupa ofertowa zostanie ustawiona dla wszystkich elementów podrzędnych',
            'kolejnosc' => 'Kolejność - dotyczy tylko kategorii na najwyższym poziomie'
        ));
    }

    public function configureShowFields(ShowMapper $showMapper) {
        $showMapper
            ->add('name')
            ->add('content', null, array('safe' => true))
            ->add('slug')
            ->add('created')
            ->add('updated');
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $offer_group_qb = $this->getOfferGroupQB();
        $offer_group_required = $this->getOfferGroupRequired();
        $category_qb = $this->getCategoryQB();

        $datagridMapper
            ->add('name')
            ->add('offer_group', null, array(), null, array(
                'query_builder' => $offer_group_qb,
                'required' => $offer_group_required
            ))
            ->add('parent', null, array(), null, array(
                'property' => 'nametree',
                'query_builder' => $category_qb,
                'required' => false,
                'label' => 'label.parent_category'
            ));
    }

    protected function configureListFields(ListMapper $listMapper) {
        $listMapper->add('offer_group');
        $listMapper->addIdentifier('nametree');
        $listMapper->add('moveupdown', 'string', array('label' => 'Kolejność', 'template' => 'CmsElmatBundle:Admin\ProductCategory:list_moveupdown.html.twig'));
        $listMapper->add('photo', 'string', array('template' => 'CmsElmatBundle:Admin\List:obrazek.html.twig'));
        $listMapper->add('slug', null, array('sortable' => false));
        $listMapper->add('updated', null, array('sortable' => false));
        $listMapper->add('parent');
        $listMapper->add('_action', 'actions', array(
            'actions' => array(
                'view' => array(),
                'edit' => array(),
                'delete' => array(),

            )
        ));
    }


    public function postUpdate($object) {
        $this->postPersist($object);
    }

    public function postPersist($object) {

        $doctrine = $this->getConfigurationPool()->getContainer()->get('doctrine');
        $em = $doctrine->getManager();

        // po aktualizacji kategorii aktualizacja grupy ofertowej dla całego drzewa kategorii

        $root = $object->getRoot();
        $lvl = $object->getLvl();

        $offer_group_set = false;
        $offer_group = null;

        $update_kolejnosc = null;
        $kolejnosc = null;

        if ($lvl > 0) {
            $root_object = $doctrine->getRepository('CmsElmatBundle:ProductCategory')->findOneBy(array(
                'lvl' => 0,
                'root' => $root
            ));

            if ($root_object) {
                $offer_group = $root_object->getOfferGroup();
                $offer_group_set = true;

                $update_kolejnosc = true;
                $kolejnosc = $root_object->getKolejnosc();
            }
        } else {
            $offer_group = $object->getOfferGroup();
            $offer_group_set = true;

            $update_kolejnosc = true;
            $kolejnosc = $object->getKolejnosc();

            if (null === $kolejnosc) {
                $kolejnosc = $this->getMaxKolejnosc($offer_group);
            }
        }

        if ($offer_group_set) {
            $qb = $em->createQueryBuilder();
            $query = $qb->update('CmsElmatBundle:ProductCategory', 'pc')
                ->set('pc.offer_group', ':offer_group')
                ->where('pc.root = :root')
                ->setParameter('offer_group', $offer_group)
                ->setParameter('root', $root)
                ->getQuery();

            $query->execute();
        }

        if ($update_kolejnosc) {
            $qb = $em->createQueryBuilder();
            $query = $qb->update('CmsElmatBundle:ProductCategory', 'pc')
                ->set('pc.kolejnosc', ':kolejnosc')
                ->where('pc.root = :root')
                ->setParameter('kolejnosc', $kolejnosc)
                ->setParameter('root', $root)
                ->getQuery();

            $query->execute();
        }
    }

    private function getMaxKolejnosc($offer_group) {
        $doctrine = $this->getConfigurationPool()->getContainer()->get('doctrine');
        $em = $doctrine->getManager();

        $query = $em->createQueryBuilder()
            ->select('COUNT(c.id)')
            ->from('CmsElmatBundle:ProductCategory', 'c')
            ->where('c.offer_group = :offer_group')
            ->andWhere('c.lvl = 0')
            ->setParameter('offer_group', $offer_group->getId())
            ->getQuery();

        return $query->getSingleScalarResult();
    }

    private function getOfferGroupsAccess() {
        $s = $this->getConfigurationPool()->getContainer()->get('security.context');

        $offer_groups = Cms::getOfferGroups();

        $slugs = array('__');
        foreach ($offer_groups as $og) {
            if ($s->isGranted($og['role'])) {
                $slugs[] = $og['slug'];
            }
        }

        return array(
            'slugs' => $slugs,
            'grupa_elmat' => $s->isGranted('ROLE_GRUPA_ELMAT')
        );
    }

    private function getOfferGroupQB() {

        $og_access = $this->getOfferGroupsAccess();

        $offer_group_qb = function (EntityRepository $er) use ($og_access) {
            $qb = $er->createQueryBuilder('og')
                ->where('og.slug IN (:slugs)')
                ->setParameter('slugs', $og_access['slugs']);
            return $qb;

        };

        return $offer_group_qb;

    }


    private function getOfferGroupRequired() {

        $og_access = $this->getOfferGroupsAccess();

        return !$og_access['grupa_elmat'];
    }


    private function getCategoryQB() {

        $og_access = $this->getOfferGroupsAccess();

        $category_qb = function (EntityRepository $er) use ($og_access) {
            $qb = $er->createQueryBuilder('u')
                ->leftJoin('u.offer_group', 'og')
                ->where('og.slug IN (:slugs)')
                ->setParameter('slugs', $og_access['slugs'])
                ->orderBy('u.root', 'ASC')
                ->addOrderBy('u.lft', 'ASC');

            if ($og_access['grupa_elmat']) {
                $qb->orWhere('og.slug IS NULL');
            }

            return $qb;
        };

        return $category_qb;

    }


}