<?php

namespace Cms\ElmatBundle\Admin;

use Sonata\AdminBundle\Route\RouteCollection;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Knp\Menu\ItemInterface as MenuItemInterface;

class GaleriaZdjecieAdmin extends Admin {


	protected $translationDomain = 'CmsElmatBundle';

	protected function configureRoutes(RouteCollection $collection)
	{
		$collection->add('kadruj', '{id}/kadruj');
	}



	public function getTemplate($name)
	{
	    switch ($name) {
	        case 'edit':
	           return 'CmsElmatBundle:Admin\\GaleriaZdjecie:edit.html.twig';
	            break;
	        default:
	            return parent::getTemplate($name);
	            break;
	    }
	}

    public function configureShowFields(ShowMapper $showMapper) {

        $showMapper
                ->add('podpis')
	        	->add('obrazek', null, array('template' => 'CmsElmatBundle:Slider:obrazek.html.twig'))
	        	->add('kolejnosc')
	        	->add('galeria')


         ;
    }


    public function configureListFields(ListMapper $listMapper) {

        $listMapper
        	->addIdentifier('podpis', null, array())
        	->add('obrazek', null, array('template' => 'CmsElmatBundle:Slider:obrazek.html.twig'))
        	->add('kolejnosc')
        	->add('galeria')


                ;

                $listMapper->add('_action', 'actions', array(
                		$this->trans('actions') => array(
                				'edit' => array(),
                				'delete' => array(),
                				'view' => array(),
                		)
                	)
				);

    }



    public function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('galeria')
        ;
    }

    public function configureFormFields(FormMapper $formMapper)
    {

    	$this->getSubject()->setUpdatedAt( new \DateTime('now'));

        $formMapper
        	->add('podpis')
        	->add('_file', 'file', array('required' => false,  'property_path' => null, 'label' => 'File'))
        	->add('kolejnosc')
        	// ->add('galeria')

			->add('updated_at', null, array( 'with_seconds' => 'true',  'attr'=>array('style'=>'display:none;')))
        ;

        $formMapper->setHelps(array(

        		));


    }

}