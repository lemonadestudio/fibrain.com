<?php

namespace Cms\ElmatBundle\Admin;

use Sonata\AdminBundle\Route\RouteCollection;

use Sonata\AdminBundle\Show\ShowMapper;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Validator\ErrorElement;
use Sonata\AdminBundle\Form\FormMapper;

class OfferGroupAdmin extends Admin {

    protected $translationDomain = 'CmsElmatBundle';

    protected $datagridValues = array(
    		'_page'       => 1,
    		'_per_page'   => 25,
    		'_sort_by' => 'kolejnosc',
    		'_sort_order' => 'ASC',

    );

    public function getTemplate($name)
    {
        switch ($name) {
            case 'edit':
                return 'CmsElmatBundle:Admin\OfferGroup:edit.html.twig';
                break;
            default:
                return parent::getTemplate($name);
                break;
        }
    }

    protected function configureRoutes(RouteCollection $collection)
    {
    	$collection
	    	->remove('create')
	    	->remove('delete')
// 	    	->remove('edit')
//	    	->remove('history')
    	;

    }

    protected function configureFormFields(FormMapper $formMapper)
    {

    	if($this->getObjectIdentifier())

        $formMapper
        ->with('label.mainoptions')
       //         ->add('title', null, array('required' => true))
       //          ->add('slug', null, array('required' => true))
                ->add('kolejnosc', null, array('required' => false))
                ->add('_file_image', 'file', array('required' => false, 'label' => 'label.main_photo'))
                ->add('_delete_file_image', 'checkbox', array('required'=> false, 'label' => 'Usunąć zdjęcie?'))

        ->end()
        ->with('label.highlights')
        		->add('show_highlights', null, array('label' => 'label.show_highlights'))
        		// ->add('title', null, array('required' => true))
        ->end()

        ->add('updated', null, array('data' => new \DateTime(), 'required' => false, 'with_seconds' => 'true', 'label' => ' ', 'attr'=>array('style'=>'display:none;')))
        
        ->setHelps(array(
        		'show_highlights' => $this->trans('help.show_highlights')
        		))
//                 ->add('lang', 'choice', array(
//                 		'choices' => array("pl"=>"Polski", "en"=>"English")))

           ;
    }

    public function configureShowFields(ShowMapper $showMapper) {

    	$showMapper
    	->add('title')
    	->add('slug')
    	->add('kolejnosc')
    	->add('lang')

    	->add('created')
    	->add('updated')

    	;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
        	->add('lang', 'doctrine_orm_choice', array(), 'choice', array(
        		'choices' => array("pl"=>"Polski", "en"=>"English")))
            ->add('title')


        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('title')
            ->add('image', 'string', array('template' => 'CmsElmatBundle:Admin\List:obrazek.html.twig'))
            ->add('slug')
            ->add('kolejnosc')
            ->add('lang')



        	->add('_action', 'actions', array(
        		'actions' => array(
        				'view' => array(),
        				'edit' => array(),
        				'delete' => array(),

        	)
        ))

        ;
    }


}