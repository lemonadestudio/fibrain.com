<?php

namespace Cms\ElmatBundle\Admin;

use Sonata\AdminBundle\Show\ShowMapper;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Validator\ErrorElement;
use Sonata\AdminBundle\Form\FormMapper;

class CategoryNewsAdmin extends Admin {

    protected $translationDomain = 'CmsElmatBundle';

    protected $datagridValues = array(
    		'_page'       => 1,
    		'_per_page'   => 25,
    		'_sort_by' => 'kolejnosc',
    		'_sort_order' => 'ASC',

    );

    protected function configureFormFields(FormMapper $formMapper)
    {

    	if($this->getObjectIdentifier())

        $formMapper
        ->with('label.mainoptions')
                ->add('title', null, array('required' => true))
                ->add('slug', null, array('required' => true))
                ->add('kolejnosc', null, array('required' => false))
                ->add('lang', 'choice', array(
                		'choices' => array("pl"=>"Polski", "en"=>"English")))
        ->end()
        ->with('label.highlights')
                	->add('show_highlights', null, array('label' => 'label.show_highlights'))
                		// ->add('title', null, array('required' => true))
         ->end()

          ->setHelps(array(
                				'show_highlights' => $this->trans('help.show_highlights')
           ))

           ;
    }

    public function configureShowFields(ShowMapper $showMapper) {

    	$showMapper
    	->add('title')
    	->add('slug')
    	->add('kolejnosc')
    	->add('lang')
    	->add('created')
    	->add('updated')

    	;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
        	->add('lang', 'doctrine_orm_choice', array(), 'choice', array(
        		'choices' => array("pl"=>"Polski", "en"=>"English")))
            ->add('title')


        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('title')
            ->add('slug')
            ->add('kolejnosc')
            ->add('lang')


        	->add('_action', 'actions', array(
        		'actions' => array(
        				'view' => array(),
        				'edit' => array(),
        				'delete' => array()
        		)
        ))

        ;
    }


}