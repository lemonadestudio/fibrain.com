<?php

namespace Cms\ElmatBundle\Admin;

use Doctrine\ORM\EntityRepository;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Validator\ErrorElement;
use Sonata\AdminBundle\Form\FormMapper;

class TrainingAdmin extends Admin {

    protected $translationDomain = 'CmsElmatBundle';

    protected $datagridValues = array(

            '_page'       => 1,
            '_per_page'   => 25,
            '_sort_by' => 'publishDate',
            '_sort_order' => 'DESC',

    );

    public function getTemplate($name)
    {

        switch ($name) {
            case 'edit':
                return 'CmsElmatBundle:Admin\Training:edit.html.twig';
                break;
            default:
                return parent::getTemplate($name);
                break;
        }
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
                ->add('lang', 'choice', array(
                        'choices' => array("pl"=>"Polski", "en"=>"English"),

                ))
                ->add('category', null, array(
                        'property' => 'nametree',
                        'query_builder' => function(EntityRepository $er) {
                            return $er->createQueryBuilder('u')
                            ->orderBy('u.root', 'ASC')
                            ->addOrderBy('u.lft', 'ASC');},
                            'required' => false,
                            'label' => 'label.category'))

                ->add('title', null, array('required' => true))
                ->add('content', null, array('required' => false, 'attr' => array('class' => 'sonata-medium wysiwyg')))

                ->add('published', 'checkbox', array('required' => false))
                ->add('publishDate', 'datetime')

                ->add('_file_image', 'file', array('required' => false, 'label' => 'label.main_photo'))
                ->add('_delete_file_image', 'checkbox', array('required'=> false, 'label' => 'Usunąć zdjęcie?'))

             	->add('automaticSeo', 'checkbox', array('required' => false))
                ->add('slug', null, array('required' => false))
                ->add('keywords', null, array('required' => false))
                ->add('description', 'textarea', array('required' => false))




        -> setHelps(array(
                    'title' => $this->trans('Enter page title'),
                    'automaticSeo' => $this->trans('help.automatic_seo'),
                    'slug' => $this->trans('help.slug'),
                    'publishDate' => $this->trans('help.publish_date'),

           ));
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('title')
            ->add('category')
            ->add('published')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('title')
            ->add('image', 'string', array('template' => 'CmsElmatBundle:Admin\List:obrazek.html.twig'))
            ->add('category')
            ->add('published')
            ->add('publishDate')

            ->add('_action', 'actions', array(
                    'actions' => array(
                    				// 'view' => array(),
                    				'edit' => array(),
                    				'delete' => array(),
                    )
            ))
        ;


    }


}