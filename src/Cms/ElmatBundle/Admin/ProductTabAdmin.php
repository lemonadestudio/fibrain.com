<?php

namespace Cms\ElmatBundle\Admin;

use Cms\ElmatBundle\Entity\ProductTab;

use Sonata\AdminBundle\Admin\AdminInterface;

use Sonata\AdminBundle\Route\RouteCollection;

use Sonata\AdminBundle\Show\ShowMapper;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Validator\ErrorElement;
use Sonata\AdminBundle\Form\FormMapper;

use Knp\Menu\FactoryInterface as MenuFactoryInterface;
use Knp\Menu\ItemInterface as MenuItemInterface;
use Knp\Menu\MenuItem;

class ProductTabAdmin extends Admin {

    protected $translationDomain = 'CmsElmatBundle';

    protected $datagridValues = array(
    		'_page'       => 1,
    		'_per_page'   => 25,
    		'_sort_by' => 'publishDate',
    		'_sort_order' => 'DESC',

    );

    public function showIn($context) {
    	// return false;
    }

    protected function configureSideMenu(MenuItemInterface $menu, $action, AdminInterface $childAdmin = null)
    {


    }

    protected function configureRoutes(RouteCollection $collection)
    {


    }

	public function getTemplate($name)
	{
		switch ($name) {
// 			case 'edit':
// 				return 'CmsElmatBundle:Admin\Product:edit.html.twig';
// 				break;

// 			case 'show':
// 				return 'CmsElmatBundle:Admin\Aktualnosci:show.html.twig';
// 				break;

			default:
				return parent::getTemplate($name);
				break;
		}
	}


    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper

        	->add('name', null, array())
			->add('sort', null, array())

			->add('description', null, array('required' => false))
			->add('type', 'choice', array('choices' => array(ProductTab::TYPE_normalny => 'Zwykła zakładka', ProductTab::produkty_powiazane => 'Powiązane produkty'), 'required' => false))

			->add('_delete', 'checkbox', array('required' => false, 'property_path' => false))
			->add('updated', null, array( 'with_seconds' => 'true',  'attr'=>array('style'=>'display:none;')));

      ;

    }

    public function configureShowFields(ShowMapper $showMapper) {

    	$showMapper



	    	->add('name')


    	;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper



        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper

            ->addIdentifier('name')


        ;
    }




}