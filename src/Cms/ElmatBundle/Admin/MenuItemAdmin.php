<?php

namespace Cms\ElmatBundle\Admin;

use Cms\ElmatBundle\Helper\Cms;

use Sonata\AdminBundle\Validator\ErrorElement;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class MenuItemAdmin extends Admin {

    protected $translationDomain = 'CmsElmatBundle';

    protected $formOptions = array(
    		'validation_groups' => array()
    );

	public function getTypeChoices() {

		 return array(
		 		'url' => $this->trans('Manual URL'),
		 		'route' => $this->trans('Module')
		 		);
	}

	public function getModuleChoices() {

		$config_menu = $this->getConfigurationPool()->getContainer()->getParameter('cms_elmat.menu');

		$og_access = $this->getOfferGroupsAccess();

		$modules = array('' => '');

		foreach($config_menu['modules'] as $mod_name => $module) {

		    $add = true;
		    $matches = array();
		    if(preg_match('/(cms_elmat_default_homepage|cms_elmat_page_show)_?(.*)/', $module['route'], $matches)) {
		        $add = false;

                if($matches[2] && in_array($matches[2], $og_access['symbols2'])) {
                    $add = true;
                }
                if(!$matches[2] && $og_access['grupa_elmat']) {
                    $add = true;
                }
		    }

		    if($add)
			    $modules[$module['route']] = $module['label'];

		}

		return $modules;

	}

	public function getSearchServicesJson() {


		return json_encode($this->getSearchServices());

	}

	public function getSearchServices() {

		$config_menu = $this->getConfigurationPool()->getContainer()->getParameter('cms_elmat.menu');
		$services = array();
		foreach($config_menu['modules'] as $mod_name => $module) {

			$services[$module['route']] = $module['get_elements_service'];

		}

		return $services;

	}



	public function getBatchActions() {
		return array();

	}

	protected function configureRoutes(RouteCollection $collection) {
		$collection->add('saveorder');
		$collection->add('routeparameters');
	}

	/**
	 * wczytuje konfigurację dostępnych lokacji
	 */
	public function getLocationChoices() {

		$config_menu = $this->getConfigurationPool()->getContainer()->getParameter('cms_elmat.menu');

		$locations = array();

		foreach($config_menu['locations'] as $loc_name => $location ) {
			$locations[$loc_name] = $this->trans($location['label']);
		}

		return $locations;


	}


	public function getTemplate($name)
	{
		switch ($name) {
			case 'edit':
				return 'CmsElmatBundle:CRUD:menuitem_edit.html.twig';
				break;
			case 'list':
				return 'CmsElmatBundle:CRUD:menuitem_list.html.twig';
				break;


			default:
				return parent::getTemplate($name);
				break;
		}
	}




    public function configureShowFields(ShowMapper $showMapper) {

        $showMapper
        		->add('location')
                ->add('title')
                ;
    }


    public function configureListFields(ListMapper $listMapper) {

        $listMapper->addIdentifier('title')
        ->add('location')
        ->add('currentUrl', null, array('template' => 'CmsElmatBundle:CRUD:menuitem_currenturl.html.twig'))
        ;

    }

    public function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
        ;
    }

    public function configureFormFields(FormMapper $formMapper)
    {


        $formMapper
            ->with('General')
	            ->add('anchor')
	            ->add('title', null, array('required' => false))
	            // ->add('location', 'choice', array('choices' => $this->getLocationChoices(), 'expanded' => false))
        		->add('location', 'hidden', array())
        		->add('type', 'choice', array('choices' => $this->getTypeChoices()))
	            ->add('url', 'text')
	            ->add('route', 'choice', array('required' => false,  'choices' => $this->getModuleChoices()))
	            // ->add('routeParameters_choice', 'choice', array('mapped' => false, 'required' => false, 'choices' => array(), 'label' => 'Wybierz'))
	            ->add('routeParameters', 'hidden', array('required' => false))
       		    ->add('searchServices', 'hidden', array('data' => $this->getSearchServicesJson(), 'mapped' => false), array('style'=>'') )
	        ->end()
	        // ->with('Advanced')
	        	// ->add('attributes', 'text', array('required' => false))
	        // ->end()

            ->setHelps(array(
            		'anchor' => $this->trans('help.menuitem.anchor'),
            		'title' => $this->trans('help.menuitem.title'),
            		'attributes' => $this->trans('help.menuitem.attributes')
            	)
           	)

        ;


    }

    public function validate(ErrorElement $errorElement, $object)
    {
    	$errorElement

    		->with('routeParameters')


    		->end()
    	;


    }


    private function getOfferGroupsAccess() {

        $s = $this->getConfigurationPool()->getContainer()->get('security.context');

        $offer_groups = Cms::getOfferGroups();
        $symbols2 = array('__');
        $slugs = array('__');
        foreach($offer_groups as $og) {
            if($s->isGranted($og['role'])) {
                $slugs[] = $og['slug'];
                $symbols2[] = $og['symbol2'];
            }
        }

        return array(
                'slugs' => $slugs,
                'symbols2' => $symbols2,
                'grupa_elmat' => $s->isGranted('ROLE_GRUPA_ELMAT')
        );
    }









}