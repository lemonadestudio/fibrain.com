<?php

namespace Cms\ElmatBundle\Admin;

use Doctrine\ORM\EntityRepository;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Validator\ErrorElement;
use Sonata\AdminBundle\Form\FormMapper;

use Knp\Menu\FactoryInterface as MenuFactoryInterface;
use Knp\Menu\ItemInterface as MenuItemInterface;
use Knp\Menu\MenuItem;

class PricelistAdmin extends Admin {

    protected $translationDomain = 'CmsElmatBundle';

    protected $datagridValues = array(
    		'_page'       => 1,
    		'_per_page'   => 25,
    		'_sort_by' => 'publishDate',
    		'_sort_order' => 'DESC',

    );

    protected function configureSideMenu(MenuItemInterface $menu, $action, AdminInterface $childAdmin = null)
    {
		if (!$childAdmin && !in_array($action, array('edit'))) {
        	return;
    	}
		    $admin = $this->isChild() ? $this->getParent() : $this;
		    $id = $admin->getRequest()->get('id');
		    // $_object = $this->getObject($id);
		    $menu->addChild('Edytuj', array('uri' => $this->generateUrl('edit', array('id' => $id))));






    }

    protected function configureRoutes(RouteCollection $collection)
    {

//     	$collection->add('edit_rows', '{id}/edit_rows');


    }

	public function getTemplate($name)
	{
		switch ($name) {
			case 'edit':
				return 'CmsElmatBundle:Admin\Pricelist:edit.html.twig';
				break;
// 			case 'show':
// 				return 'CmsElmatBundle:Admin\Aktualnosci:show.html.twig';
// 				break;

			default:
				return parent::getTemplate($name);
				break;
		}
	}


    protected function configureFormFields(FormMapper $formMapper)
    {



        $formMapper

	        	->with('label.mainoptions')


	        		->add('title', null, array('required' => true))
	        		->add('content', null, array('required' => false, 'label' => 'Krótki opis', 'attr' => array('class' => 'sonata-medium wysiwyg')))
					->add('published')
					->add('_file_cennik', 'file', array('required' => false, 'label' => 'Plik z cennikiem'))


					->add('_file_image', 'file', array('required' => false, 'label' => 'label.main_photo'))

					->add('updated', null, array( 'property_path' => 'updated', 'with_seconds' => 'true', 'label' => ' ', 'attr'=>array('style'=>'display:none;')))



      	-> setHelps(array(
        				'title' => $this->trans('help.title'),

        		))
         ;




      	if($this->getSubject() && $this->getSubject()->getId()) {

      	    if($this->getSubject()->getWebPath('image')) {
      	        $formMapper
      	        ->add('_delete_file_image', 'checkbox', array('required'=> false, 'label' => 'Usunąć zdjęcie?'))
      	        ;
      	    }

      	    if($this->getSubject()->getWebPath('cennik')) {
      	        $formMapper
      	            ->add('_delete_file_cennik', 'checkbox', array('required'=> false, 'label' => 'Usunąć cennik?'))
      	            ->add('cennik_filename', null, array('required'=> false, 'label' => 'Nazwa pliku'))

      	        ;
      	    }

      	    $this->getSubject()->setUpdated(new \DateTime());



      	}



    }

    public function configureShowFields(ShowMapper $showMapper) {

    	$showMapper

	    	->add('title')
	    	->add('content', null, array('safe' => true))
	    	->add('published')

	    	->add('created')
	    	->add('updated')

    	;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper

            ->add('title')



        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper

            ->addIdentifier('title')
            ->add('photo', 'string', array('template' => 'CmsElmatBundle:Admin\List:obrazek.html.twig'))
            ->add('cennik', 'string', array('template' => 'CmsElmatBundle:Admin\Pricelist:list_cennik.html.twig'))
            ->add('published')
            ->add('updated')
            ->add('_action', 'actions', array(
            		'actions' => array(
            				'view' => array(),
            				'edit' => array(),
            				'delete' => array(),

            				)
            	))

        ;
    }




}