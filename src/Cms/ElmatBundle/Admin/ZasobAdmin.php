<?php

namespace Cms\ElmatBundle\Admin;

use Cms\ElmatBundle\Entity\Article;
use Cms\ElmatBundle\Form\Type\ZasobPlikType;
use Cms\ElmatBundle\Helper\Cms;
use Doctrine\ORM\EntityRepository;
use Knp\Menu\FactoryInterface as MenuFactoryInterface;
use Knp\Menu\ItemInterface as MenuItemInterface;
use Knp\Menu\MenuItem;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Validator\ErrorElement;
use Sonata\DoctrineORMAdminBundle\Datagrid\ProxyQuery;

class ZasobAdmin extends Admin {

    protected $translationDomain = 'CmsElmatBundle';

    protected $datagridValues = array(
    		'_page'       => 1,
    		'_per_page'   => 25,
    		'_sort_by' => 'publishDate',
    		'_sort_order' => 'DESC',

    );

    protected function configureRoutes(RouteCollection $collection) {
        $collection->add('recycle', $this->getRouterIdParameter() . '/przenies-do-kosza');
    }

	public function getTemplate($name)
	{
		switch ($name) {
			case 'edit':
				return 'CmsElmatBundle:Admin\Zasob:edit.html.twig';
				break;

			default:
				return parent::getTemplate($name);
				break;
		}
	}


    protected function configureFormFields(FormMapper $formMapper)
    {

        $category_qb = $this->getCategoryQB();
       
        $formMapper

	        	->with('label.mainoptions')
//	        		->add('lang', 'choice', array('choices' => array(
//	        				'pl' => 'Polski', 'en' => 'Angielski')))
	        		// ->add('offer_group', null, array('required' => false))
//	        		->add('offer_group', null, array(
//	        		        'query_builder' => $offer_group_qb,
//	        		        'required' => $offer_group_required
//	        		))
	        		->add('category', null, array(
                        'property' => 'namefull',
                        'query_builder' => $category_qb,
	        		    'required' => true
                    ))
	        		->add('title', null, array('required' => true))
                    ->add('description')
	        		// ->add('content', null, array('required' => false, 'attr' => array('class' => 'sonata-medium wysiwyg')))
					->add('published')
					->add('publishDate')
					
					->add('_file_image', 'file', array('required' => false, 'label' => 'label.main_photo'))
					->add('_delete_file_image', 'checkbox', array('required'=> false, 'label' => 'Usunąć zdjęcie?'))
                    ->add('plik', new ZasobPlikType())
					// ->add('slug', null, array('required' => false))
					// ->add('automaticSeo')
					->add('highlight_mainpage', null, array('label' => 'label.highlight_mainpage'))
					// ->add('highlights_box_mainpage', null, array('label' => 'label.highlights_box_mainpage'))


      	-> setHelps(array(
        				'title' => $this->trans('help.title'),
        				'automaticSeo' => $this->trans('help.automatic_seo'),
        				'slug' => $this->trans('help.slug'),
        				'publishDate' => $this->trans('help.publish_date'),
      					'lang'	=> $this->trans('help.lang')
        		))
      ;

    }
    
    

    public function configureShowFields(ShowMapper $showMapper) {

    	$showMapper


	    	->add('title')
	    	->add('content', null, array('safe' => true))

	    	->add('published')
	    	->add('publishDate')

	    	->add('keywords')
	    	->add('description')
	    	->add('created')
	    	->add('updated')

    	;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {

        $category_qb = $this->getCategoryQB();
        

        $datagridMapper
            // ->add('offer_group')

            ->add('category', null, array(), null, array(
                    'property' => 'namefull',
                    'query_builder' => $category_qb,
                    'required' => true
            ))

            ->add('title')
            
	       ->add('highlight_mainpage', null, array('label' => 'label.highlight_mainpage'))
	    
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            //
            // ->add('offer_group')
            ->add('category')
            ->addIdentifier('title')

            ->add('photo', 'string', array('template' => 'CmsElmatBundle:Admin\List:obrazek.html.twig'))
//            ->add('slug')
                ->add('plik')
            
            ->add('published')
            ->add('publishDate')
            ->add('updated')

            ->add('_action', 'actions', array(
            		'actions' => array(
            				'view' => array(),
            				'edit' => array(),
                            'recycle' => array(
                                'template' => 'SonataAdminBundle:CRUD:base_list_action_recycle.html.twig'
                            ),

                        )
            	));
    }


    public function createQuery($context = 'list')
    {

        $og_access = $this->getOfferGroupsAccess();

        $query = parent::createQuery($context);

        $query = $query
        ->leftJoin( $query->getRootAlias().'.category', 'c')
        ->leftJoin( 'c.offer_group', 'og')
        ->where('og.slug IN (:slugs)')
        ->setParameter('slugs', $og_access['slugs'])
        ;

        if($og_access['grupa_elmat']) {
            $query = $query->orWhere('og.slug IS NULL');
        }

        $query = new ProxyQuery($query);

        return $query;


    }



    private function getOfferGroupsAccess() {


        $s = $this->getConfigurationPool()->getContainer()->get('security.context');

        $offer_groups = Cms::getOfferGroups();

        $slugs = array('__');
        foreach($offer_groups as $og) {
            if($s->isGranted($og['role'])) {
                $slugs[] = $og['slug'];
            }
        }

        return array(
                'slugs' => $slugs,
                'grupa_elmat' => $s->isGranted('ROLE_GRUPA_ELMAT')
        );
    }


    private function getCategoryQB() {

        $og_access = $this->getOfferGroupsAccess();
        
        

        $category_qb = function(EntityRepository $er) use ($og_access) {
            $qb =  
                    $er->createQueryBuilder('c')
                    ->leftJoin('c.offer_group', 'og')
            ->where('og.slug IN (:slugs)')

            ->setParameter('slugs', $og_access['slugs'])
                     
            ;
            
            if($og_access['grupa_elmat']) {
                $qb->orWhere('og.slug is null');
            }
            
            return $qb;

        }
        ;

        return $category_qb;

    }


//    private function getOfferGroupRequired() {
//
//        $og_access = $this->getOfferGroupsAccess();
//
//        return !$og_access['grupa_elmat'];
//    }




}