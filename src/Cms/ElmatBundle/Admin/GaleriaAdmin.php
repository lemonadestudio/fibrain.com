<?php

namespace Cms\ElmatBundle\Admin;

use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Knp\Menu\ItemInterface as MenuItemInterface;

class GaleriaAdmin extends Admin {

	protected $translationDomain = 'CmsElmatBundle';

	protected function configureRoutes(RouteCollection $collection)
	{

		$collection->add('edit_zdjecia', '{id}/edit_zdjecia');

	}


	protected function configureSideMenu(MenuItemInterface $menu, $action, AdminInterface $childAdmin = null)
	{

		if (!$childAdmin && !in_array($action, array('edit', 'edit_zdjecia'))) {
			return;
		}
		$admin = $this->isChild() ? $this->getParent() : $this;
		$id = $admin->getRequest()->get('id');

		$menu->addChild(
				$this->trans('Edycja'),
				array('uri' => $admin->generateUrl('edit', array('id' => $id)))
		);

		$menu->addChild(
				$this->trans('Edytuj zdjęcia'),
				array('uri' => $admin->generateUrl('edit_zdjecia', array('id' => $id)))
		);


// 		$menu->addChild(
// 				'comments',
// 				array('uri' => $admin->generateUrl('acme_comment.admin.comment.list', array('id' => $id)))
// 		);
	}



	public function getTemplate($name)
	{
	    switch ($name) {
	        case 'edit':
	            return 'CmsElmatBundle:Admin\\Galeria:edit.html.twig';
	            break;
	        default:
	            return parent::getTemplate($name);
	            break;
	    }
	}

    public function configureShowFields(ShowMapper $showMapper) {

        $showMapper
                ->add('nazwa')
	        	->add('opis')
	        	->add('obrazek', null, array('template' => 'CmsElmatBundle:Slider:obrazek.html.twig'))
	        	->add('zdjecia')

         ;
    }


    public function configureListFields(ListMapper $listMapper) {

        $listMapper
        	->addIdentifier('nazwa', null, array())
        	->add('opis')
        	->add('obrazek', null, array( 'template' => 'CmsElmatBundle:Slider:obrazek.html.twig'))
        	->add('zdjecia')


                ;

                $listMapper->add('_action', 'actions', array(
                		$this->trans('actions') => array(
                				'edit' => array(),
                				'delete' => array(),
                				'view' => array(),
                		)
                	)
				);

    }



    public function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('nazwa')

        ;
    }

    public function configureFormFields(FormMapper $formMapper)
    {

    	$this->getSubject()->setUpdatedAt( new \DateTime('now'));

        $formMapper
        	->add('nazwa')
        	->add('opis', null, array('required' => false, 'attr' => array('class' => 'sonata-medium wysiwyg')))
        	->add('_file', 'file', array('required' => false, 'mapped' => false, 'label' => 'File'))
			->add('updated_at', null, array( 'with_seconds' => 'true',  'attr'=>array('style'=>'display:none;')))

        ;

        $formMapper->setHelps(array(

        ));


    }

}