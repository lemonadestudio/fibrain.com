<?php

namespace Cms\ElmatBundle\Admin;

use Sonata\AdminBundle\Route\RouteCollection;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Cms\ElmatBundle\Entity\Subscription;

class SubscriptionAdmin extends Admin {

	protected $translationDomain = 'CmsElmatBundle';

// 	public function  showIn($context) {
// 		return false;
// 	}

	protected $datagridValues = array(
			'_page'       => 1,
			'_sort_order' => 'ASC', // sort direction
			'_sort_by' => 'name' // field name
	);




	protected function configureRoutes(RouteCollection $collection) {

// 				$collection->remove('create');
// 				$collection->remove('edit');
// 				$collection->remove('batch');
				$collection->remove('show');
// 				$collection->remove('delete');

	}


    public function configureListFields(ListMapper $listMapper) {



        $listMapper->addIdentifier('email')
            ->add('statusTxt')
            ->add('created')
            ->add('updated')


       ;

       $listMapper->add('_action', 'actions', array(
       		$this->trans('actions') => array(
//        				'view' => array(),
       				'edit' => array(),
       				'delete' => array()
       		)
       ));

    }



    public function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('email')
            ->add('status', 'doctrine_orm_choice', array(), 'choice', array('choices' => Subscription::getStatusChoices() ))

        ;
    }

    public function configureFormFields(FormMapper $formMapper)
    {



		$formMapper
    		->add('email')
    		->add('status', 'choice', array('choices' => Subscription::getStatusChoices() ))
		;



    }





}