<?php

namespace Cms\ElmatBundle\Admin;

use Cms\ElmatBundle\Helper\Cms;
use Cms\ElmatBundle\Entity\Article;
use Doctrine\ORM\EntityRepository;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\DoctrineORMAdminBundle\Datagrid\ProxyQuery;
use Knp\Menu\ItemInterface as MenuItemInterface;

class ArticleAdmin extends Admin {

    protected $translationDomain = 'CmsElmatBundle';

    protected $datagridValues = array(
        '_page' => 1,
        '_per_page' => 25,
        '_sort_by' => 'publishDate',
        '_sort_order' => 'DESC',
    );

    protected function configureSideMenu(MenuItemInterface $menu, $action, AdminInterface $childAdmin = null) {
        if (!$childAdmin && !in_array($action, array('edit', 'edit_pliki', 'edit_tabs'))) {
            return;
        }
        $admin = $this->isChild() ? $this->getParent() : $this;
        $id = $admin->getRequest()->get('id');

        $menu->addChild('Edytuj', array('uri' => $this->generateUrl('edit', array('id' => $id))));
        $menu->addChild(
            $this->trans('Pliki do pobrania'), array('uri' => $admin->generateUrl('edit_pliki', array('id' => $id)))
        );
        $menu->addChild('Zakładki', array('uri' => $this->generateUrl('edit_tabs', array('id' => $id))));
    }

    protected function configureRoutes(RouteCollection $collection) {
        $collection->add('edit_pliki', $this->getRouterIdParameter() . '/edit_pliki');
        $collection->add('edit_tabs', $this->getRouterIdParameter() . '/edit_tabs');
        $collection->add('kadruj', $this->getRouterIdParameter() . '/kadruj');
        $collection->add('preview', $this->getRouterIdParameter() . '/preview');
        $collection->add('recycle', $this->getRouterIdParameter() . '/przenies-do-kosza');
    }

    public function getTemplate($name) {
        switch ($name) {
            case 'edit':
                return 'CmsElmatBundle:Admin\Article:edit.html.twig';
                break;

            case 'edit_tabs':
                return 'CmsElmatBundle:Admin\Article:edit_tabs.html.twig';
                break;

            default:
                return parent::getTemplate($name);
                break;
        }
    }


    protected function configureFormFields(FormMapper $formMapper) {

        $offer_group_qb = $this->getOfferGroupQB();
        $offer_group_required = $this->getOfferGroupRequired();

        $formMapper
            ->with('label.mainoptions')
            ->add('lang', 'choice', array(
                'choices' => array(
                    'pl' => 'Polski', 'en' => 'Angielski'
                )
            ))
            ->add('offer_group', null, array(
                'query_builder' => $offer_group_qb,
                'required' => $offer_group_required
            ))
            ->add('type', 'choice', array(
                'choices' => array(
                    Article::TYPE_artykuly_techniczne => 'Artykuły techniczne',
                    Article::TYPE_rozwiazania => 'Rozwiązania',
                    Article::TYPE_uslugi => 'Usługi'
                ),
                'label' => $this->trans('label.article_type')
            ))
            ->add('title', null, array('required' => true))
            ->add('content', null, array('required' => false, 'attr' => array('class' => 'sonata-medium wysiwyg')))
            ->add('published')
            ->add('publishDate')
            ->add('keywords', 'textarea', array('required' => false, 'label' => 'label.keywords', 'attr' => array('class' => 'span8', 'rows' => 3)))
            ->add('description', 'textarea', array('required' => false, 'label' => 'label.description', 'attr' => array('class' => 'span10', 'rows' => 10)))
            ->add('galeria', null, array('required' => false, 'label' => 'label.galeria'))
            ->add('_file_image', 'file', array('required' => false, 'label' => 'label.main_photo'))
            ->add('_delete_file_image', 'checkbox', array('required' => false, 'label' => 'Usunąć zdjęcie?'))
            ->add('slug', null, array('required' => false))
            ->add('automaticSeo')
            ->add('highlight_mainpage', null, array('label' => 'label.highlight_mainpage'))
            ->add('highlights_box_mainpage', null, array('label' => 'label.highlights_box_mainpage'))
            ->setHelps(array(
                'title' => $this->trans('help.title'),
                'automaticSeo' => $this->trans('help.automatic_seo'),
                'slug' => $this->trans('help.slug'),
                'publishDate' => $this->trans('help.publish_date'),
                'lang' => $this->trans('help.lang')
            ));
    }

    public function configureShowFields(ShowMapper $showMapper) {
        $showMapper
            ->add('title')
            ->add('content', null, array('safe' => true))
            ->add('published')
            ->add('publishDate')
            ->add('keywords')
            ->add('description')
            ->add('created')
            ->add('updated');
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {

        $offer_group_qb = $this->getOfferGroupQB();
        $offer_group_required = $this->getOfferGroupRequired();

        $datagridMapper
            ->add('offer_group', null, array(), null, array(
                'query_builder' => $offer_group_qb,
                'required' => $offer_group_required
            ))
            ->add('title')
            ->add('type', 'doctrine_orm_choice', array(), 'choice', array(
                'choices' => array(
                    Article::TYPE_artykuly_techniczne => 'Artykuł techniczny',
                    Article::TYPE_rozwiazania => 'Rozwiązania',
                    Article::TYPE_uslugi => 'Usługi'
                ),
                'label' => $this->trans('label.article_type')
            ))
            ->add('highlight_mainpage', null, array('label' => 'label.highlight_mainpage'))
            ->add('highlights_box_mainpage', null, array('label' => 'label.highlights_box_mainpage'));
    }

    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
            //
            ->add('offer_group')
            ->addIdentifier('title')
            ->add('photo', 'string', array('template' => 'CmsElmatBundle:Admin\List:obrazek.html.twig'))
            ->add('slug')
            ->add('type')
            ->add('published')
            ->add('publishDate')
            ->add('updated')
            ->add('_action', 'actions', array(
                'actions' => array(
                    'view' => array(),
                    'edit' => array(),
                    'recycle' => array(
                        'template' => 'SonataAdminBundle:CRUD:base_list_action_recycle.html.twig'
                    ),

                )
            ));
    }


    public function createQuery($context = 'list') {
        $og_access = $this->getOfferGroupsAccess();

        $query = parent::createQuery($context);

        $query = $query
            ->leftJoin($query->getRootAlias() . '.offer_group', 'og')
            ->where('og.slug IN (:slugs)')
            ->setParameter('slugs', $og_access['slugs']);

        if ($og_access['grupa_elmat']) {
            $query = $query->orWhere('og.slug IS NULL');
        }

        $query = new ProxyQuery($query);

        return $query;
    }


    private function getOfferGroupsAccess() {
        $s = $this->getConfigurationPool()->getContainer()->get('security.context');

        $offer_groups = Cms::getOfferGroups();

        $slugs = array('__');
        foreach ($offer_groups as $og) {
            if ($s->isGranted($og['role'])) {
                $slugs[] = $og['slug'];
            }
        }

        return array(
            'slugs' => $slugs,
            'grupa_elmat' => $s->isGranted('ROLE_GRUPA_ELMAT')
        );
    }


    private function getOfferGroupQB() {
        $og_access = $this->getOfferGroupsAccess();

        $offer_group_qb = function (EntityRepository $er) use ($og_access) {
            $qb = $er->createQueryBuilder('og')
                ->where('og.slug IN (:slugs)')
                ->setParameter('slugs', $og_access['slugs']);
            return $qb;

        };

        return $offer_group_qb;
    }


    private function getOfferGroupRequired() {
        $og_access = $this->getOfferGroupsAccess();

        return !$og_access['grupa_elmat'];
    }
}