<?php

namespace Cms\ElmatBundle\Admin;

use Sonata\AdminBundle\Route\RouteCollection;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class EmailTemplateAdmin extends Admin {

	protected $translationDomain = 'CmsElmatBundle';

// 	public function  showIn($context) {
// 		return false;
// 	}

	protected $datagridValues = array(
			'_page'       => 1,
			'_sort_order' => 'ASC', // sort direction
			'_sort_by' => 'name' // field name
	);

	public function getTemplate($name)
	{
	    switch ($name) {
	        case 'edit':
	            return 'CmsElmatBundle:Admin\EmailTemplate:edit.html.twig';
	            break;
	            // 			case 'show':
	            // 				return 'CmsElmatBundle:Admin\Aktualnosci:show.html.twig';
	            // 				break;

	        default:
	            return parent::getTemplate($name);
	            break;
	    }
	}


	protected function configureRoutes(RouteCollection $collection) {

				$collection->remove('create');
// 				$collection->remove('edit');
// 				$collection->remove('batch');
				$collection->remove('show');
				$collection->remove('delete');

	}


    public function configureListFields(ListMapper $listMapper) {



        $listMapper
            ->addIdentifier('name')
            ->add('topic')
            ->add('description')
       ;

       $listMapper->add('_action', 'actions', array(
       		$this->trans('actions') => array(
//        				'view' => array(),
       				'edit' => array(),
//         				'delete' => array()
       		)
       ));

    }

    public function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name')
            ->add('description')
            ->add('content')

        ;
    }

    public function configureFormFields(FormMapper $formMapper)
    {

        $id = $this->getRequest()->get($this->getIdParameter());
        $obj = null;

    	if(!$id) {
    		$formMapper->add('name')
    		->add('description');
    	} else {

    	    $obj = $this->getObject($id);
    	}

        $formMapper

             // ->add('name', null, array('attr' => array('readonly' => true, 'disabled' => true)), array())
             ->add('topic', null, array('label' => 'Temat'))
             ->add('content', 'textarea', array('required' => false, 'label' => 'Treść HTML', 'attr' => array('class' => 'sonata-medium wysiwyg')))
             ->add('content_txt', 'textarea', array('required' => false, 'label' => 'Treść TXT'))

        ;


            $formMapper->setHelps(
                    array(
                     'topic' => 'Temat wiadomości.',
                     'content' => 'Treść e-mail w formacie HTML',
                     'content_txt' => 'Treść e-mail w formacie TXT. Treść przeznaczona dla czytników nie obsługujących HTML. Aby to pole zostało automatycznie uzupełnione na podstawie pola Treść HTML nalezy usunąć jego zawartość.',
           ));


    }





}