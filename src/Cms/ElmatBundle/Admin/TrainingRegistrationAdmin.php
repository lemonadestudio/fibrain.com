<?php

namespace Cms\ElmatBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Validator\ErrorElement;
use Sonata\AdminBundle\Form\FormMapper;
use Cms\ElmatBundle\Entity\TrainingRegistration;
use Sonata\DoctrineORMAdminBundle\Datagrid\ProxyQuery;
use Sonata\AdminBundle\Route\RouteCollection;

class TrainingRegistrationAdmin extends Admin {

    protected $translationDomain = 'CmsElmatBundle';
    
     protected $datagridValues = array(

    		'_page'       => 1,
    		'_per_page'   => 50,
    		'_sort_by' => 'created',
    		'_sort_order' => 'DESC',

    );
     
     protected function configureRoutes(RouteCollection $collection) {

				$collection->remove('create');

				// $collection->remove('edit');
// 				$collection->remove('batch');
// 				$collection->remove('show');
// 				$collection->remove('delete');

	}
    
    
    public function createQuery($context = 'list')
    {

        $query = parent::createQuery($context);

        $query = $query
            ->addSelect('tp')
            ->addSelect('us')
            ->addSelect('td')
            ->addSelect('t')
                
               
            ->leftJoin( $query->getRootAlias().'.user', 'us')
            ->leftJoin( $query->getRootAlias().'.training_participants', 'tp')
            ->leftJoin( $query->getRootAlias().'.training_date', 'td')
            ->leftJoin( 'td.training', 't')
           
        
        
        ;


        $query = new ProxyQuery($query);

        return $query;


    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
               
                ->add('uwagi', 'textarea', array('required' => false))
                ->add('uwagi_administracyjne', 'textarea', array('required' => false))
                
                ->add('status', 'choice', array(
                    'choices' => array(
                    TrainingRegistration::STATUS_NIEPOTWIERDZONE=> "Niepotwierdzone", 
                    TrainingRegistration::STATUS_POTWIERDZONE=> "Potwierdzone", 
                    TrainingRegistration::STATUS_ANULOWANE=> "Anulowane", 
                        
                        )))
                    
                 ->add('status_platnosci', 'choice', array(
                    'choices' => array(
                        TrainingRegistration::STATUS_PLATNOSCI_NIEOPLACONE=> "Nieopłacone", 
                        TrainingRegistration::STATUS_PLATNOSCI_OPLACONE=> "Opłacone", 
                        TrainingRegistration::STATUS_PLATNOSCI_OPLACONE_CZESCIOWO=> "Opłacone częściowo", 
                    
                        )))
                
                // ->add('training_participants')

                ;


    }

   protected function configureDatagridFilters(DatagridMapper $datagridMapper)
   {
       
          
       $datagridMapper
               
                ->add('user.firma', null, array( 'label' => 'Klient (firma)'  ), null, array( ))
                ->add('user.email', null, array( 'label' => 'Klient (email)'  ), null, array( ))
                ->add('training_date.training', null, array( 'label' => 'Szkolenie'  ), null, array( ))
                ->add('training_date.startDate', 'doctrine_orm_date_range', array( 'label' => 'Data szkolenia',   ), null, array( ))
                
                ->add('status', 'doctrine_orm_choice', array(), 'choice', array(
                   'multiple' => true,
                    'expanded' => true,
                   'choices' => array(
                    TrainingRegistration::STATUS_NIEPOTWIERDZONE=> "Niepotwierdzone", 
                    TrainingRegistration::STATUS_POTWIERDZONE=> "Potwierdzone", 
                    TrainingRegistration::STATUS_ANULOWANE=> "Anulowane", 
                        
                        ),
                        'label' => 'Status'))
                ->add('status_platnosci', 'doctrine_orm_choice', array(), 'choice', array(
                    'multiple' => true,
                    'expanded' => true,
                    'choices' => array(
                        TrainingRegistration::STATUS_PLATNOSCI_NIEOPLACONE=> "Nieopłacone", 
                        TrainingRegistration::STATUS_PLATNOSCI_OPLACONE=> "Opłacone", 
                        TrainingRegistration::STATUS_PLATNOSCI_OPLACONE_CZESCIOWO=> "Opłacone częściowo", 
                    
                        )))
               
          
       ;
   }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id', null)
            ->add('user', null, array('label' => 'Klient', 'template' => 'CmsElmatBundle:Admin\TrainingRegistration:list_user.html.twig'))
            ->add('training_date.training', null, array('label' => 'Szkolenie'))
            ->add('created', null, array('label' => 'Data zgłoszenia'))
            ->add('training_date.startDate', null, array('label' => 'Termin szkolenia') )
            ->add('statusTxt', null, array('label' => 'Status'))
            ->add('statusPlatnosciTxt', null, array('label' => 'Status płatności'))
            ->add('uwagi')
            ->add('uwagi_administracyjne')
            ->add('participantsTxt', null, array('label' => 'Uczestnicy', 'template' => 'CmsElmatBundle:Admin\TrainingRegistration:list_participants.html.twig'))
            

            ->add('_action', 'actions', array(
                    'actions' => array(
                            // 'view' => array(),
                            'edit' => array(),
                           //  'delete' => array(),
                    )
            ))
        ;
    }


}