<?php

namespace Cms\ElmatBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Validator\ErrorElement;
use Sonata\AdminBundle\Form\FormMapper;

class TrainingDateAdmin extends Admin {

    protected $translationDomain = 'CmsElmatBundle';

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
                ->add('training', null, array('required' => false))
                ->add('city', null, array('required' => false))
                ->add('price', null, array('required' => false))
                ->add('startDate', 'date', array('required' => false))
                ->add('endDate', 'date', array('required' => false))
                ->add('published', 'checkbox', array('required' => false))
                ->add('publishDate', 'datetime')
                ->add('brak_miejsc')

                ->add('lang', 'choice', array(
                    'choices' => array("pl"=>"Polski", "en"=>"English"),

                ));


    }

   protected function configureDatagridFilters(DatagridMapper $datagridMapper)
   {
       $datagridMapper
           ->add('training')
           ->add('city')
       ;
   }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('training', null)
            ->add('city', null)
            ->add('price')
            ->add('startDate', null)
            ->add('endDate', null)
            ->add('brak_miejsc')

            ->add('_action', 'actions', array(
                    'actions' => array(
                            // 'view' => array(),
                            'edit' => array(),
                            'delete' => array(),
                    )
            ))
        ;
    }


}