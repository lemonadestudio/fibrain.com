<?php

namespace Cms\ElmatBundle\Admin;

use Doctrine\ORM\EntityRepository;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Validator\ErrorElement;
use Sonata\AdminBundle\Form\FormMapper;
use Knp\Menu\FactoryInterface as MenuFactoryInterface;
use Knp\Menu\ItemInterface as MenuItemInterface;

class SliderPhotoAdmin extends Admin {

    protected $translationDomain = 'CmsElmatBundle';
    protected $datagridValues = array(
        '_page' => 1,
        '_per_page' => 25,
        '_sort_by' => 'publishDate',
        '_sort_order' => 'DESC',
    );

    protected function configureSideMenu(MenuItemInterface $menu, $action, AdminInterface $childAdmin = null) {
        if (!$childAdmin && !in_array($action, array('edit'))) {
            return;
        }
        $admin = $this->isChild() ? $this->getParent() : $this;
        $id = $admin->getRequest()->get('id');
        // $_object = $this->getObject($id);
        $menu->addChild('Edytuj', array('uri' => $this->generateUrl('edit', array('id' => $id))));
    }

    protected function configureRoutes(RouteCollection $collection) {

//     	$collection->add('edit_rows', '{id}/edit_rows');
    }

    public function getTemplate($name) {
        switch ($name) {
            case 'edit':
                return 'CmsElmatBundle:Admin\SliderPhoto:edit.html.twig';
                break;
// 			case 'show':
// 				return 'CmsElmatBundle:Admin\Aktualnosci:show.html.twig';
// 				break;

            default:
                return parent::getTemplate($name);
                break;
        }
    }

    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
                ->with('label.mainoptions')
                ->add('lang', 'choice', array(
                    'choices' => array(
                        'pl' => 'Polski', 
                        'en' => 'Angielski')))
                ->add('offer_group', null, array('required' => false))
                ->add('_file_image', 'file', array('required' => false, 'label' => 'label.main_photo'))
                ->add('_delete_file_image', 'checkbox', array('required' => false, 'label' => 'Usunąć zdjęcie?'))
                ->add('kolejnosc')
                ->add('title')
                ->add('link')
                ->add('description')
                ->add('textColor')
                ->end()
                ->add('updated', null, array('data' => new \DateTime(), 'required' => false, 'with_seconds' => 'true', 'label' => ' ', 'attr'=>array('style'=>'display:none;')))
                ->setHelps(array(
                    'lang' => $this->trans('help.lang'),
                    'textColor' => 'Należy wpisać albo nazwę koloru np. red, albo kod w formacie hex np. #ffffff'
                ))
        ;
    }

    public function configureShowFields(ShowMapper $showMapper) {

        $showMapper
                ->add('id')
                ->add('image')
                ->add('kolejnosc')
                ->add('link')
                ->add('created')
                ->add('updated')
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
                ->add('offer_group')
        ;
    }

    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                ->addIdentifier('id')
                ->add('photo', 'string', array('template' => 'CmsElmatBundle:Admin\List:obrazek.html.twig'))
                ->add('offer_group')
                ->add('kolejnosc')
                ->add('link')
                ->add('_action', 'actions', array(
                    'actions' => array(
                        // 'view' => array(),
                        'edit' => array(),
                        'delete' => array(),
                    )
                ))
        ;
    }

}
