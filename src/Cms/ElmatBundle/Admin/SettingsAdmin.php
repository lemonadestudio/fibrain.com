<?php

namespace Cms\ElmatBundle\Admin;

use Sonata\AdminBundle\Route\RouteCollection;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class SettingsAdmin extends Admin {

	protected $translationDomain = 'CmsElmatBundle';

// 	public function  showIn($context) {
// 		return false;
// 	}

	protected $datagridValues = array(
			'_page'       => 1,
			'_sort_order' => 'ASC', // sort direction
			'_sort_by' => 'name' // field name
	);




	protected function configureRoutes(RouteCollection $collection) {

				$collection->remove('create');
// 				$collection->remove('edit');
// 				$collection->remove('batch');
				$collection->remove('show');
				$collection->remove('delete');

	}


    public function configureListFields(ListMapper $listMapper) {



        $listMapper->addIdentifier('name')
        ->add('description')
        ->add('value', null, array('template' => 'LmCmsBundle:Admin\Setting:list_value.html.twig'))

       ;

       $listMapper->add('_action', 'actions', array(
       		$this->trans('actions') => array(
//        				'view' => array(),
       				'edit' => array(),
//        				'delete' => array()
       		)
       ));

    }

    public function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name')
            ->add('description')
            ->add('value')

        ;
    }

    public function configureFormFields(FormMapper $formMapper)
    {

        $id = $this->getRequest()->get($this->getIdParameter());
        $obj = null;

    	if(!$id) {
    		$formMapper->add('name')
    		->add('description');
    	} else {

    	    $obj = $this->getObject($id);
    	}

        $formMapper

             // ->add('name', null, array('attr' => array('readonly' => true, 'disabled' => true)), array())
             ->add('value', 'textarea', array('required' => false, 'attr' => array('col' => 200, 'rows' => 15)))
//               ->add('section')



        ;

        if($obj) {
            $formMapper->setHelps(array('value' => $obj->getDescription()));
        }

    }





}