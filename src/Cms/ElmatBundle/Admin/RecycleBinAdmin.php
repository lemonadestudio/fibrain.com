<?php

namespace Cms\ElmatBundle\Admin;

use Cms\ElmatBundle\Helper\Cms;
use Sonata\DoctrineORMAdminBundle\Datagrid\ProxyQuery;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Cms\ElmatBundle\Entity\Product;

class RecycleBinAdmin extends Admin {
    
    protected $translationDomain = 'CmsElmatBundle';

    protected $datagridValues = array(
        '_page' => 1,
        '_per_page' => 25,
        '_sort_by' => 'publishDate',
        '_sort_order' => 'DESC',

    );

    public function getTemplate($name)
    {
        return parent::getTemplate($name);
    }

    protected function configureRoutes(RouteCollection $collection)
    {
    	$collection
            ->add('list')
            ->add('restore', $this->getRouterIdParameter() . '/przywroc')
            ->remove('create')
            ->remove('show')
    	;

    }
   
    
    public function createQuery($context = 'list') {

        $og_access = $this->getOfferGroupsAccess();
        
        $query = parent::createQuery($context);
        
        $em = $query->getQueryBuilder()->getEntityManager();
        $em->getFilters()->disable('softdeleteable');
        
        $query = $query
                ->select('r')
                ->from('CmsElmatBundle:RecycleBin', 'r')
                ->where('r.deleted_at != :null')
                ->setParameter('null', serialize(null));
        
        $query = new ProxyQuery($query);
        
        return $query;
    }
    
    private function getOfferGroupsAccess() {
        $s = $this->getConfigurationPool()->getContainer()->get('security.context');

        $offer_groups = Cms::getOfferGroups();

        $slugs = array('__');
        foreach ($offer_groups as $og) {
            if ($s->isGranted($og['role'])) {
                $slugs[] = $og['slug'];
            }
        }

        return array(
            'slugs' => $slugs,
            'grupa_elmat' => $s->isGranted('ROLE_GRUPA_ELMAT')
        );
    }

    public function configureShowFields(ShowMapper $showMapper) {

    	$showMapper
    	->add('title')
    	->add('item')
    	->add('lang')

    	->add('created')
    	->add('updated')

    	;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
        	->add('lang', 'doctrine_orm_choice', array(), 'choice', array(
        		'choices' => array("pl"=>"Polski", "en"=>"English")))
            ->add('title')


        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('title')
            ->add('item')
            ->add('deleted_at')
            ->add('lang')


        	->add('_action', 'actions', array(
        		'actions' => array(
                        'view' => array(),
                        'restore' => array('template' => 'CmsElmatBundle:Admin\RecycleBin:restore.html.twig'),
                )
            ));
    }
    
}