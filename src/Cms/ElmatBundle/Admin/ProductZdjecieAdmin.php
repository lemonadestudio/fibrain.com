<?php

namespace Cms\ElmatBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;

class ProductZdjecieAdmin extends Admin {
    protected $translationDomain = 'CmsElmatBundle';

    protected function configureRoutes(RouteCollection $collection) {
        $collection->add('kadruj', $this->getRouterIdParameter() . '/kadruj');
    }

    public function getTemplate($name) {
        switch ($name) {
            case 'edit':
                return 'CmsElmatBundle:Admin\ProductZdjecie:edit.html.twig';
                break;

            default:
                return parent::getTemplate($name);
                break;
        }
    }

    public function configureShowFields(ShowMapper $showMapper) {
        $showMapper->add('podpis');
        $showMapper->add('obrazek', null, array('template' => 'CmsElmatBundle:Slider:obrazek.html.twig'));
        $showMapper->add('kolejnosc');
    }

    public function configureListFields(ListMapper $listMapper) {
        $listMapper->addIdentifier('podpis', null, array());
        $listMapper->add('obrazek', null, array('template' => 'CmsElmatBundle:Slider:obrazek.html.twig'));
        $listMapper->add('kolejnosc');
        $listMapper->add('_action', 'actions', array(
            $this->trans('actions') => array(
                'edit' => array(),
                'delete' => array(),
                'view' => array(),
            )
        ));
    }

    public function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper->add('podpis');
    }

    public function configureFormFields(FormMapper $formMapper) {
        $this->getSubject()->setUpdatedAt(new \DateTime('now'));

        $formMapper->add('podpis');
        $formMapper->add('_file', 'file', array('required' => false, 'property_path' => null, 'label' => 'File'));
        $formMapper->add('kolejnosc');
        $formMapper->add('updated_at', null, array('with_seconds' => 'true'));
        $formMapper->setHelps(array());
    }
}