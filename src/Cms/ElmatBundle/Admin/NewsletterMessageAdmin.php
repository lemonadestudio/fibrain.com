<?php

namespace Cms\ElmatBundle\Admin;

use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Cms\ElmatBundle\Model\OfferType;

class NewsletterMessageAdmin extends Admin {

    protected $translationDomain = 'CmsElmatBundle';
    protected $datagridValues    = array(
        '_page' => 1,
        '_sort_order' => 'DESC', // sort direction
        '_sort_by' => 'id' // field name
    );

    public function getTemplate($name) {
        switch ($name) {
            case 'edit':
                return 'CmsElmatBundle:Admin\NewsletterMessage:edit.html.twig';

            case 'show':
                return 'CmsElmatBundle:Admin\NewsletterMessage:show.html.twig';

            default:
                return parent::getTemplate($name);
        }
    }

    public function configureListFields(ListMapper $listMapper) {
        $listMapper->addIdentifier('title')
            ->add('startSendDateText')
            ->add('wyslij_testowy', null, array('template' => 'CmsElmatBundle:Admin\NewsletterMessage:wyslij_testowy.html.twig'))
            ->add('sendOnlyToSubscribersWithAccount');

        $listMapper->add('_action', 'actions', array(
            $this->trans('actions') => array(
                'view' => array(),
                'edit' => array(),
                'delete' => array()
            )
        ));
    }

    public function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
            ->add('title')
            ->add('sendOnlyToSubscribersWithAccount');
    }

    public function configureFormFields(FormMapper $formMapper) {
        // edit
        if ($this->id($this->getSubject())) {
            $formMapper
                ->add('title')
                ->add('message', null, array('attr' => array('class' => 'sonata-medium wysiwyg')))
                ->add('plainTextMessage', null, array('attr' => array('style' => 'width:800px', 'cols' => 220, 'rows' => 6)))
                ->setHelps(array(
                    'title' => 'Tytuł maila',
                    'message' => 'Treść maila wersja HTML<br />Dostępne zmienne:<br /> - <em>{URL_REZYGNACJA}</em> - link do wypisania się z newsletera',
                    'plainTextMessage' => 'Treść maila - wersja dla klientów obsługujących tylko tryb tekstowy. Uzupełnienie nie jest konieczne, ale zapewnia kompatybilność z klientami tekstowymi.'
                ));
        } // create
        else {
            $formMapper
                ->add('title')
                ->add('startSendDate')
                ->add('message', null, array('attr' => array('class' => 'sonata-medium wysiwyg')))
                ->add('plainTextMessage', null, array('attr' => array('style' => 'width:800px', 'cols' => 220, 'rows' => 6)))
                ->add('sendOnlyToSubscribersWithAccount', null, array(
                    'required' => false
                ))
                ->add('recipientEmails', null, array('attr' => array('style' => 'width:300px', 'cols' => 100, 'rows' => 6)))
                ->add('offerTypeIds', 'choice', array(
                    'required' => false,
                    'empty_value' => '',
                    'choices' => OfferType::getAllTypes(),
                    'multiple' => true,
                    'expanded' => true,
                    'mapped' => false
                ))
                ->setHelps(array(
                    'title' => 'Tytuł maila',
                    'message' => 'Treść maila wersja HTML<br />Dostępne zmienne:<br /> - <em>{URL_REZYGNACJA}</em> - link do wypisania się z newsletera',
                    'plainTextMessage' => 'Treść maila - wersja dla klientów obsługujących tylko tryb tekstowy. Uzupełnienie nie jest konieczne, ale zapewnia kompatybilność z klientami tekstowymi.',
                    'sendOnlyToSubscribersWithAccount' => 'Newsletter wysyłany tylko do użytkowników posiadających konto z tym samym adresem email. '
                        . 'Po wybraniu tej opcji dostępne są nowe zmienne w szablonie:<br />'
                        . ' - <em>{URL_ZMIANA_HASLA}</em> - link do resetowania hasła <br /> '
                        . ' - <em>{FIRMA}</em> - nazwa firmy<br />'
                        . ' - <em>{NIP}</em> - nip<br />'
                        . ' - <em>{EMAIL}</em> - email<br />'
                        . ' - <em>{TELEFON}</em> - tel.<br />'
                        . ' - <em>{ULICA}, {KOD_POCZTOWY}, {MIASTO}</em> - elementy adresu firmy<br />',
                    'recipientEmails' => 'Manualna lista adresów email (w nowych linijkach) na które ma zostać wysłany Newsletter. 
                        <br>UWAGA: Newsletter zostanie wysłany na dany adres email, tyklo w przypadku gdy dany adres jest dodany jako subskrypcja.',
                    'offerTypes' => 'Gdy nie są zaznaczone żadne pola domyślnie Newsletter wysyłany jest do wszystkich subskrybentów'
                ));
        }
    }

    public function getNewInstance() {
        $object = parent::getNewInstance();

        $defaultTemplate = $this->configurationPool->getContainer()->get('cms_email_templates')->get('newsletter_domyslny_szablon');

        if ($defaultTemplate) {
            $object->setTitle($defaultTemplate->getTopic());
            $object->setMessage($defaultTemplate->getContent());
        }

        return $object;
    }

}
