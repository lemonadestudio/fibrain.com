<?php

namespace Cms\ElmatBundle\Admin;

use Cms\ElmatBundle\Helper\Cms;
use Cms\ElmatBundle\Entity\Product;
use Doctrine\ORM\EntityRepository;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\DoctrineORMAdminBundle\Datagrid\ProxyQuery;
use Knp\Menu\ItemInterface as MenuItemInterface;

class ProductAdmin extends Admin {
    protected $translationDomain = 'CmsElmatBundle';

    protected $datagridValues = array(
        '_page' => 1,
        '_per_page' => 25,
        '_sort_by' => 'publishDate',
        '_sort_order' => 'DESC',
    );

    protected function configureSideMenu(MenuItemInterface $menu, $action, AdminInterface $childAdmin = null) {
        $actions = array(
            'edit',
            'edit_rows',
            'edit_list',
            'edit_tabs',
            'edit_zdjecia',
            'edit_powiazane',
            'edit_pliki',
            'edit_based_on'
        );

        if (!$childAdmin && !in_array($action, $actions)) {
            return;
        }

        $admin = $this->isChild() ? $this->getParent() : $this;
        $id = $admin->getRequest()->get('id');

        $menu->addChild('Edytuj', array('uri' => $this->generateUrl('edit', array('id' => $id))));

        if ($this->getObject($id)->getType() == Product::TYPE_grupa_tabelka) {
            $menu->addChild('Produkty składowe / tabelka', array('uri' => $this->generateUrl('edit_rows', array('id' => $id))));
        }

        if ($this->getObject($id)->getType() == Product::TYPE_grupa_lista) {
            $menu->addChild('Produkty składowe / lista', array('uri' => $this->generateUrl('edit_list', array('id' => $id))));
        }

        if ($this->getObject($id)->getType() == Product::TYPE_normalny) {
            $menu->addChild('Zakładki', array('uri' => $this->generateUrl('edit_tabs', array('id' => $id))));
            $menu->addChild(
                $this->trans('Edytuj zdjęcia'),
                array('uri' => $admin->generateUrl('edit_zdjecia', array('id' => $id)))
            );
            $menu->addChild(
                $this->trans('Produkty powiązane'),
                array('uri' => $admin->generateUrl('edit_powiazane', array('id' => $id)))
            );
        }

        $menu->addChild(
            $this->trans('Pliki do pobrania'),
            array('uri' => $admin->generateUrl('edit_pliki', array('id' => $id)))
        );

        $menu->addChild(
            $this->trans('Podpięte w innych kategoriach'),
            array('uri' => $admin->generateUrl('edit_based_on', array('id' => $id)))
        );
    }

    public function createQuery($context = 'list') {
        $og_access = $this->getOfferGroupsAccess();

        $query = parent::createQuery($context);

        $query = $query
            ->leftJoin($query->getRootAlias() . '.category', 'c')
            ->leftJoin('c.offer_group', 'og')
            ->where('og.slug IN (:slugs)')
            ->setParameter('slugs', $og_access['slugs']);

        if ($og_access['grupa_elmat']) {
            $query = $query->orWhere('og.slug IS NULL');
        }

        $query = new ProxyQuery($query);

        return $query;
    }

    protected function configureRoutes(RouteCollection $collection) {
        $collection->add('edit_tabs', $this->getRouterIdParameter() . '/edit_tabs');
        $collection->add('edit_zdjecia', $this->getRouterIdParameter() . '/edit_zdjecia');
        $collection->add('edit_powiazane', $this->getRouterIdParameter() . '/edit_powiazane');
        $collection->add('edit_pliki', $this->getRouterIdParameter() . '/edit_pliki');
        $collection->add('edit_based_on', $this->getRouterIdParameter() . '/edit_based_on');
        $collection->add('edit_list', $this->getRouterIdParameter() . '/edit_list');
        $collection->add('edit_rows', $this->getRouterIdParameter() . '/edit_rows');
        $collection->add('kadruj', $this->getRouterIdParameter() . '/kadruj');
        $collection->add('kadruj2', $this->getRouterIdParameter() . '/kadruj2');
        $collection->add('preview', $this->getRouterIdParameter() . '/preview');
        $collection->add('recycle', $this->getRouterIdParameter() . '/przenies-do-kosza');
    }

    public function getTemplate($name) {
        switch ($name) {
            case 'edit':
                return 'CmsElmatBundle:Admin\Product:edit.html.twig';
                break;
            case 'edit_rows':
                return 'CmsElmatBundle:Admin\Product:edit_rows.html.twig';
                break;
            case 'edit_list':
                return 'CmsElmatBundle:Admin\Product:edit_list.html.twig';
                break;
            case 'edit_tabs':
                return 'CmsElmatBundle:Admin\Product:edit_tabs.html.twig';
                break;
            case 'edit_based_on':
                return 'CmsElmatBundle:Admin\Product:edit_based_on.html.twig';
                break;

            default:
                return parent::getTemplate($name);
                break;
        }
    }

    private function getOfferGroupsAccess() {
        $s = $this->getConfigurationPool()->getContainer()->get('security.context');

        $offer_groups = Cms::getOfferGroups();

        $slugs = array('__');
        foreach ($offer_groups as $og) {
            if ($s->isGranted($og['role'])) {
                $slugs[] = $og['slug'];
            }
        }

        return array(
            'slugs' => $slugs,
            'grupa_elmat' => $s->isGranted('ROLE_GRUPA_ELMAT')
        );
    }


    private function getOfferGroupQB() {

        $og_access = $this->getOfferGroupsAccess();

        $offer_group_qb = function (EntityRepository $er) use ($og_access) {
            $qb = $er->createQueryBuilder('og')
                ->where('og.slug IN (:slugs)')
                ->setParameter('slugs', $og_access['slugs']);
            return $qb;

        };

        return $offer_group_qb;

    }


    public function getOfferGroupRequired() {

        $og_access = $this->getOfferGroupsAccess();

        return !$og_access['grupa_elmat'];
    }


    public function getCategoryQB() {

        $og_access = $this->getOfferGroupsAccess();

        $category_qb = function (EntityRepository $er) use ($og_access) {
            $qb = $er->createQueryBuilder('u')
                ->leftJoin('u.offer_group', 'og')
                ->where('og.slug IN (:slugs)')
                ->setParameter('slugs', $og_access['slugs'])
                ->orderBy('u.root', 'ASC')
                ->addOrderBy('u.lft', 'ASC');

            if ($og_access['grupa_elmat']) {
                $qb->orWhere('og.slug IS NULL');
            }

            return $qb;
        };

        return $category_qb;

    }


    protected function configureFormFields(FormMapper $formMapper) {
        $offer_group_required = $this->getOfferGroupRequired();
        $category_qb = $this->getCategoryQB();

        $formMapper
            ->with('label.mainoptions')
            ->add('title', null, array('required' => true))
            ->add('code', null, array('label' => 'label.product_code'))
            ->add('content', null, array('required' => false, 'attr' => array('class' => 'sonata-medium wysiwyg')))
            ->add('category', null, array(
                'property' => 'nametree',
                'query_builder' => $category_qb,
                'required' => $offer_group_required,
                'label' => 'label.category'
            ))
            ->add('type', 'choice', array(
                'choices' => array(
                    'normalny' => 'Normalny widok',
                    'grupa_tabelka' => 'Grupa produktów: Tabelka',
                    'grupa_lista' => 'Grupa produktów: Lista'
                ), 'label' => $this->trans('label.product_type')
            ))
            ->add('box_produkty_subpage', null, array('label' => 'Wyświetlić w box-ie "najnowsze produkty"?'))
            ->add('_file_image', 'file', array('required' => false, 'label' => 'label.main_photo'))
            ->add('_delete_file_image', 'checkbox', array('required' => false, 'label' => 'Usunąć zdjęcie?'))
            ->add('_file_image_thumb', 'file', array('required' => false, 'label' => 'Miniaturka'))
            ->add('_delete_file_image_thumb', 'checkbox', array('required' => false, 'label' => 'Usunąć miniaturkę?'))
            ->end()
            ->with('label.publikacja')
            ->add('published', 'checkbox', array('required' => false))
            ->add('publishDate', 'datetime', array())
            ->end()
            ->with('label.seo')
            ->add('automaticSeo', 'checkbox', array('required' => false, 'label' => 'label.automatic_seo'))
            ->add('slug', null, array('required' => false, 'label' => 'label.slug'))
            ->add('keywords', 'textarea', array('required' => false, 'label' => 'label.keywords', 'attr' => array('class' => 'span8', 'rows' => 3)))
            ->add('description', 'textarea', array('required' => false, 'label' => 'label.description', 'attr' => array('class' => 'span10', 'rows' => 10)))
            ->add('updated', null, array('with_seconds' => 'true', 'label' => ' ', 'attr' => array('style' => 'display:none;')))
            ->end()
            ->setHelps(array(
                'title' => $this->trans('help.title'),
                'automaticSeo' => $this->trans('help.automatic_seo'),
                'slug' => $this->trans('help.slug'),
                'publishDate' => $this->trans('help.publish_date'),
                'lang' => $this->trans('help.lang'),
                '_file_image' => 'Zdjęcie główne w galerii. Jeżeli nie ma osobno zdefiniowanej miniatury - zostanie również użyte jako miniaturka',
                '_file_image_thumb' => 'Miniaturka na listowaniu kategorii',
            ));
    }

    public function configureShowFields(ShowMapper $showMapper) {

        $showMapper
            ->add('title')
            ->add('content', null, array('safe' => true))
            ->add('published')
            ->add('publishDate')
            ->add('keywords')
            ->add('description')
            ->add('created')
            ->add('updated');
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {

        $category_qb = $this->getCategoryQB();

        $datagridMapper
            ->add('title')
            ->add('category', null, array(), null, array(
                'property' => 'nametree',
                'query_builder' => $category_qb,
                'required' => false,
                'label' => 'label.category'
            ));
    }

    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
            ->add('category')
            ->add('title', null, array('template' => 'CmsElmatBundle:Admin\Product:list_title.html.twig'))
            ->add('photo', 'string', array('template' => 'CmsElmatBundle:Admin\List:obrazek.html.twig'))
            ->add('slug')
            ->add('type')
            ->add('published')
            ->add('publishDate')
            ->add('updated')
            ->add('_action', 'actions', array(
                'actions' => array(
                    'view' => array(),
                    'edit' => array(),
                    'recycle' => array(
                        'template' => 'SonataAdminBundle:CRUD:base_list_action_recycle.html.twig'
                    ),
                )
            ));
    }
}