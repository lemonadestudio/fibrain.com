<?php

namespace Cms\ElmatBundle\Admin;

use Doctrine\ORM\EntityRepository;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Validator\ErrorElement;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;   


use Cms\ElmatBundle\Helper\Cms;

use Sonata\DoctrineORMAdminBundle\Datagrid\ProxyQuery;

class ZasobCategoryAdmin extends Admin {

    protected $translationDomain = 'CmsElmatBundle';

    public function getTemplate($name)
    {
        switch ($name) {
            case 'edit':
                return 'CmsElmatBundle:Admin\ZasobCategory:edit.html.twig';
                break;
            default:
                return parent::getTemplate($name);
                break;
        }
    }

    protected function configureRoutes(RouteCollection $collection)
    {

    }

//    public function createQuery($context = 'list')
//    {
//        $query = parent::createQuery($context);
//
//        // rozważyć przeniesienie sortowania z productCategory (jeżeli będzie połączenie do OfferGroup)
//
//        $_alias = $query->getRootAlias();
//
//        $query->orderBy($_alias.'.kolejnosc', 'ASC');
//
//        return $query;
//    }

    protected function configureFormFields(FormMapper $formMapper)
    {

        $this->getSubject()->setUpdated( new \DateTime('now'));
        $id = $this->getRequest()->get($this->getIdParameter());
        
         $offer_group_qb = $this->getOfferGroupQB();
        

        $formMapper
                ->add('title', null, array('required' => true))
                ->add('offer_group',  null, array(
                    'query_builder' => $offer_group_qb,
                    'required' => false))

                ->add('content', null, array('required' => false, 'attr' => array('class' => 'sonata-medium wysiwyg')))

               
                ->add('automaticSeo', 'checkbox', array('required' => false))
                ->add('slug', null, array('required' => false))
                ->add('keywords', null, array('required' => false))
                ->add('description', 'textarea', array('required' => false))
                ->add('updated', null, array( 'with_seconds' => 'true', 'label' => ' ', 'attr'=>array('style'=>'display:none;')))

           ;

       
                $formMapper->add('kolejnosc');
         

        $formMapper
        -> setHelps(array(
              
        ));
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $offer_group_qb = $this->getOfferGroupQB();
        
        $datagridMapper
            ->add('title')
            ->add('offer_group', null, array(), null, array(
                    'query_builder' => $offer_group_qb,
                    'required' => true))
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('title')
            ->add('offer_group')
            ->add('kolejnosc')

            ->add('_action', 'actions', array(
                    'actions' => array(
                            // 'view' => array(),
                            'edit' => array(),
                            'delete' => array(),

                    )
            ))
        ;
    }

    public function postUpdate($object) {

        $this->postPersist($object);

    }

    public function postPersist($object)
    {

       
    }
    
    public function createQuery($context = 'list')
    {

        $og_access = $this->getOfferGroupsAccess();

        $query = parent::createQuery($context);

        $query = $query
        ->leftJoin( $query->getRootAlias().'.offer_group', 'og')
        ->where('og.slug IN (:slugs)')
        ->setParameter('slugs', $og_access['slugs'])
        ;

        if($og_access['grupa_elmat']) {
            $query = $query->orWhere('og.slug IS NULL');
        }

        $query = new ProxyQuery($query);

        return $query;


    }



    private function getOfferGroupsAccess() {


        $s = $this->getConfigurationPool()->getContainer()->get('security.context');

        $offer_groups = \Cms\ElmatBundle\Helper\Cms::getOfferGroups();

        $slugs = array('__');
        foreach($offer_groups as $og) {
            if($s->isGranted($og['role'])) {
                $slugs[] = $og['slug'];
            }
        }

        return array(
                'slugs' => $slugs,
                'grupa_elmat' => $s->isGranted('ROLE_GRUPA_ELMAT')
        );
    }


    private function getOfferGroupQB() {

        $og_access = $this->getOfferGroupsAccess();

        $offer_group_qb = function(EntityRepository $er) use ($og_access) {
            $qb =  $er->createQueryBuilder('og')
            ->where('og.slug IN (:slugs)')

            ->setParameter('slugs', $og_access['slugs'])
            ;
            return $qb;

        }
        ;

        return $offer_group_qb;

    }


    private function getOfferGroupRequired() {

        $og_access = $this->getOfferGroupsAccess();

        return !$og_access['grupa_elmat'];
    }


}