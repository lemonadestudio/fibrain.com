<?php

namespace Cms\ElmatBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpFoundation\Request;
use Cms\ElmatBundle\Util\Config;

/**
 * @Annotation
 */
class GoogleRecaptchaValidator extends ConstraintValidator
{
    const RECAPTCHA_VERIFY_SERVER = 'https://www.google.com';
    
    private $secretKey;
    private $request;
    
    public function __construct(Config $cmsConfig, Request $request)
    {
        $this->request = $request;
        $this->secretKey = $cmsConfig->get('google_recaptcha_secret_key');
    }
    
    public function validate($value, Constraint $constraint)
    {
//        $this->context->addViolation($constraint->message . " - " . $this->secretKey);
//        
//        var_dump($this->request->server->get('REMOTE_ADDR'));
//        var_dump($this->secretKey);
//        
//        die;
        
        $isValid = $this->checkAnswer();
        
        if (!$isValid) {
            $this->context->addViolation($constraint->message);
        }
    }
    
    private function checkAnswer()
    {
        $remoteIp = $this->request->server->get('REMOTE_ADDR');
        $recaptchaResponse = $this->request->get('g-recaptcha-response');
        
        if ($remoteIp == null || $remoteIp == '') {
            throw new ValidatorException('For security reasons, you must pass the remote ip to reCAPTCHA');
        }
        
        // discard spam submissions
        if ($recaptchaResponse == null || strlen($recaptchaResponse) == 0) {
            return false;
        }
        
        $verifyServerResponse = $this->httpGet(self::RECAPTCHA_VERIFY_SERVER, '/recaptcha/api/siteverify', array(
            'secret'   => $this->secretKey,
            'remoteip' => $remoteIp,
            'response' => $recaptchaResponse
        ));
        
        $decodedVerifyServerResponse = json_decode($verifyServerResponse, true);
        
        if ($decodedVerifyServerResponse['success'] == true) {
            return true;
        }
        
        return false;
    }
    
    private function httpGet($host, $path, $data)
    {
        $host = sprintf('%s%s?%s', $host, $path, http_build_query($data));
        return file_get_contents($host);
    }
}