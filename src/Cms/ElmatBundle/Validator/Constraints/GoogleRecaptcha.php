<?php

namespace Cms\ElmatBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class GoogleRecaptcha extends Constraint
{
    public $message = 'Invalid captcha.';
    
    public function validatedBy()
    {
        return 'google_recaptcha';
    }
}