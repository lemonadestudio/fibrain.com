<?php
namespace Cms\ElmatBundle\Entity;

use Cms\ElmatBundle\Helper\Cms;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="zasob_category")
 * @ORM\Entity(repositoryClass="Cms\ElmatBundle\Repository\ZasobCategoryRepository")
 * @ORM\HasLifecycleCallbacks();
 */
class ZasobCategory {
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(name="slug", type="string", length=255, nullable=true)
     */
    private $slug;

    /**
     * @ORM\Column(type="string", length=1024, nullable=true)
     * @Assert\Length(max = 1024)
     */
    private $keywords;

    /**
     * @ORM\Column(type="string", length=2048, nullable=true)
     * @Assert\Length(max = 2048)
     */
    private $description;

    /**
     * @ORM\Column(type="boolean", nullable=true );
     */
    private $automaticSeo;

    /**
     * @ORM\Column(type="text", nullable=true )
     */
    private $content;

    /**
     * @var \DateTime $updated
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated;

    /**
     * kolejność sortowania
     * @ORM\Column(type="integer", nullable=true);
     */
    private $kolejnosc;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="OfferGroup",
     *     inversedBy="zasob_categories"
     * )
     * @ORM\JoinColumn(onDelete="RESTRICT")
     */
    private $offer_group;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Zasob",
     *     mappedBy="category"
     * )
     * @ORM\OrderBy({"publishDate" = "DESC"})
     */
    protected $zasoby;

    /**
     * @ORM\PostPersist();
     */
    public function setPostCreatedValue() {
    }

    /**
     * @ORM\PrePersist();
     * @ORM\PreUpdate();
     */
    public function setCreatedValue() {
        if ($this->getAutomaticSeo()) {
            // urla
            $cms_helper = new Cms();
            $this->setSlug($cms_helper->make_url($this->getTitle()));

            // description
            $description = strip_tags($this->getContent());
            $description = mb_substr(html_entity_decode($description), 0, 2048);

            // usunięcie ostatniego, niedokończonego zdania

            $description = preg_replace('@(.*)\..*@', '\1', $description);

            $this->setDescription($description);

            // keywords
            $keywords_arr = explode(' ', $this->getTitle() . ' ' . $this->getDescription());

            $keywords = array();
            if (is_array($keywords_arr)) {
                foreach ($keywords_arr as $kw) {
                    $kw = trim($kw);
                    $kw = preg_replace('@\.,;\'\"@', '', $kw);
                    if (strlen($kw) >= 4 && !in_array($kw, $keywords)) {
                        $keywords[] = $kw;
                    }
                    if (count($keywords) >= 10) {
                        break;
                    }
                }
            }

            $this->setKeywords(implode(', ', $keywords));
            $this->setAutomaticSeo(false);
        }
    }


    public function __toString() {
        return '' . $this->getTitle();
    }


    public function __construct() {
        $this->automaticSeo = true;
        $this->updated = new \DateTime();
        $this->zasoby = new ArrayCollection();
        $this->kolejnosc = 0;
    }

    public function getNamefull() {
        $n = '';

        if ($this->getOfferGroup()) {
            $n .= $this->getOfferGroup()->__toString() . ' - ';
        }

        $n .= $this->getTitle();

        return $n;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     */
    public function setTitle($title) {
        $this->title = $title;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * Set slug
     *
     * @param string $slug
     */
    public function setSlug($slug) {
        $this->slug = $slug;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug() {
        return $this->slug;
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function prepareUploadedNames() {

    }


    /**
     * Set keywords
     *
     * @param string $keywords
     * @return ZasobCategory
     */
    public function setKeywords($keywords) {
        $this->keywords = $keywords;

        return $this;
    }

    /**
     * Get keywords
     *
     * @return string
     */
    public function getKeywords() {
        return $this->keywords;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return ZasobCategory
     */
    public function setDescription($description) {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return ZasobCategory
     */
    public function setContent($content) {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent() {
        return $this->content;
    }

    /**
     * Set automaticSeo
     *
     * @param boolean $automaticSeo
     * @return ZasobCategory
     */
    public function setAutomaticSeo($automaticSeo) {
        $this->automaticSeo = $automaticSeo;

        return $this;
    }

    /**
     * Get automaticSeo
     *
     * @return boolean
     */
    public function getAutomaticSeo() {
        return $this->automaticSeo;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return ZasobCategory
     */
    public function setUpdated($updated) {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated() {
        return $this->updated;
    }

    /**
     * Set kolejnosc
     *
     * @param integer $kolejnosc
     * @return ZasobCategory
     */
    public function setKolejnosc($kolejnosc) {
        $this->kolejnosc = $kolejnosc;

        return $this;
    }

    /**
     * Get kolejnosc
     *
     * @return integer
     */
    public function getKolejnosc() {
        return $this->kolejnosc;
    }

    /**
     * Set offer_group
     *
     * @param OfferGroup $offerGroup
     * @return News
     */
    public function setOfferGroup(OfferGroup $offerGroup = null) {
        $this->offer_group = $offerGroup;

        return $this;
    }

    /**
     * Get offer_group
     *
     * @return OfferGroup
     */
    public function getOfferGroup() {
        return $this->offer_group;
    }

    /**
     *
     * @param Zasob $zasob
     */
    public function addZasoby(Zasob $zasob) {
        $this->zasoby[] = $zasob;
    }

    /**
     * Remove zasob
     *
     * @param Zasob $zasob
     */
    public function removeZasoby(Zasob $zasob) {
        $this->zasoby->removeElement($zasob);
    }

    /**
     * Get zasob
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getZasoby() {
        return $this->zasoby;
    }
}