<?php

namespace Cms\ElmatBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="contact_message")
 * @ORM\HasLifecycleCallbacks()
 */
class ContactFormMessage {
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true);
     */
    private $senderName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true);
     */
    private $senderEmail;

    /**
     * @ORM\Column(type="text",  nullable=true);
     */
    private $senderMessage;

    /**
     * @ORM\Column(type="datetime", nullable=true);
     */
    private $sentAt;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set senderName
     *
     * @param string $senderName
     */
    public function setSenderName($senderName) {
        $this->senderName = $senderName;
    }

    /**
     * Get senderName
     *
     * @return string
     */
    public function getSenderName() {
        return $this->senderName;
    }

    /**
     * Set senderEmail
     *
     * @param string $senderEmail
     */
    public function setSenderEmail($senderEmail) {
        $this->senderEmail = $senderEmail;
    }

    /**
     * Get senderEmail
     *
     * @return string
     */
    public function getSenderEmail() {
        return $this->senderEmail;
    }

    /**
     * Set senderMessage
     *
     * @param string $senderMessage
     */
    public function setSenderMessage($senderMessage) {
        $this->senderMessage = $senderMessage;
    }

    /**
     * Get senderMessage
     *
     * @return string
     */
    public function getSenderMessage() {
        return $this->senderMessage;
    }

    /**
     * Set sentAt
     *
     * @param \DateTime $sentAt
     */
    public function setSentAt($sentAt) {
        $this->sentAt = $sentAt;
    }

    /**
     * Get sentAt
     *
     * @return \DateTime
     */
    public function getSentAt() {
        return $this->sentAt;
    }

    /**
     * @var string $senderIP
     */
    private $senderIP;


    /**
     * Set senderIP
     *
     * @param string $senderIP
     */
    public function setSenderIP($senderIP) {
        $this->senderIP = $senderIP;
    }

    /**
     * Get senderIP
     *
     * @return string
     */
    public function getSenderIP() {
        return $this->senderIP;
    }
}