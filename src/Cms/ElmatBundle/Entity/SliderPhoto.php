<?php

namespace Cms\ElmatBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Table(name="slider_photo");
 * @ORM\Entity();
 * @ORM\HasLifecycleCallbacks();
 */
class SliderPhoto {
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $obrazek
     *
     * @ORM\Column(name="obrazek", type="string", length=255, nullable=true)
     */
    private $image;

    /** @Assert\File(maxSize="6000000") */
    public $_file_image;

    public $_delete_file_image;

    /**
     * @var string $kolejnosc
     *
     * @ORM\Column(name="kolejnosc", type="integer", nullable=true)
     */
    private $kolejnosc;

    /**
     * @ORM\Column(type="string", length=2, nullable=false )
     */
    private $lang;

    /**
     * @ORM\ManyToOne(targetEntity="OfferGroup", inversedBy="slider_photos")
     * @ORM\JoinColumn(onDelete="RESTRICT")
     */
    protected $offer_group;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $link;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $textColor;

    /**
     *
     * @Assert\File(maxSize="6000000")
     */
    public $_file;

    /**
     * @var \DateTime $created
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @var \DateTime $updated
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updated;


    public function __construct() {
        $this->kolejnosc = 0;
    }

    public function __toString() {
        return '' . $this->getId();
    }

    /**
     * @ORM\PrePersist();
     * @ORM\PreUpdate();
     */
    public function setCreatedValue() {

    }


    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function prepareUploadedNames() {


        foreach (array('image') as $_fi) {
            $_file = '_file_' . $_fi;

            // 			var_dump($_file, $this->{$_file});

            if (null !== $this->{$_file}) {

                // do whatever you want to generate a unique name
                $this->{'old' . $_file} = $this->getAbsolutePath($_fi);
                $this->{$_fi} = $_fi . '-' . uniqid() . '.' . $this->{$_file}->guessExtension();

            } elseif (true == $this->{'_delete' . $_file}) {

                @unlink($this->getAbsolutePath($_fi));
                $this->{$_fi} = null;

            }

        }
        // 		die;
    }

    /**
     *
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function storeUploadedFiles() {

        foreach (array('image') as $_fi) {

            $_file = '_file_' . $_fi;

            if (null === $this->{$_file}) {
                continue;
            }

            $this->{$_file}->move($this->getUploadRootDir(), $this->{$_fi});

            if ($this->{'old' . $_file}) {
                @unlink($this->{'old' . $_file});
            }

            unset($this->{$_file});


        }

    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUploadedFiles() {
        if ($file = $this->getAbsolutePath('image')) {
            @unlink($file);
        }

    }


    public function getAbsolutePath($_fil) {
        $file = null;
        switch ($_fil) {

            case 'image':
            default:
                $file = $this->getImage();
        }
        return !$file ? null : $this->getUploadRootDir() . '/' . $file;
    }

    public function getWebPath($_fil = 'image') {
        $file = null;
        switch ($_fil) {

            case 'image':
            default:
                $file = $this->getImage();
        }
        return !$file ? null : $this->getUploadDir() . '/' . $file;
    }

    protected function getUploadRootDir() {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__ . '/../../../../web/' . $this->getUploadDir();
    }

    protected function getUploadDir() {
        return 'uploads/slider_photo/' . $this->getId();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }


    /**
     * Set image
     *
     * @param string $image
     * @return SliderPhoto
     */
    public function setImage($image) {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage() {
        return $this->image;
    }

    /**
     * Set kolejnosc
     *
     * @param integer $kolejnosc
     * @return SliderPhoto
     */
    public function setKolejnosc($kolejnosc) {
        $this->kolejnosc = $kolejnosc;

        return $this;
    }

    /**
     * Get kolejnosc
     *
     * @return integer
     */
    public function getKolejnosc() {
        return $this->kolejnosc;
    }

    /**
     * Set lang
     *
     * @param string $lang
     * @return SliderPhoto
     */
    public function setLang($lang) {
        $this->lang = $lang;

        return $this;
    }

    /**
     * Get lang
     *
     * @return string
     */
    public function getLang() {
        return $this->lang;
    }

    /**
     * Set link
     *
     * @param string $link
     * @return SliderPhoto
     */
    public function setLink($link) {
        $this->link = $link;

        return $this;
    }

    /**
     * Get link
     *
     * @return string
     */
    public function getLink() {
        return $this->link;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return SliderPhoto
     */
    public function setCreated($created) {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated() {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return SliderPhoto
     */
    public function setUpdated($updated) {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated() {
        return $this->updated;
    }

    /**
     * Set offer_group
     *
     * @param \Cms\ElmatBundle\Entity\OfferGroup $offerGroup
     * @return SliderPhoto
     */
    public function setOfferGroup(OfferGroup $offerGroup = null) {
        $this->offer_group = $offerGroup;

        return $this;
    }

    /**
     * Get offer_group
     *
     * @return \Cms\ElmatBundle\Entity\OfferGroup
     */
    public function getOfferGroup() {
        return $this->offer_group;
    }

    public function getTitle() {
        return $this->title;
    }

    public function getDescription() {
        return $this->description;
    }

    public function setTitle($title) {
        $this->title = $title;
        return $this;
    }

    public function setDescription($description) {
        $this->description = $description;
        return $this;
    }


    /**
     * Set textColor
     *
     * @param string $textColor
     * @return SliderPhoto
     */
    public function setTextColor($textColor) {
        $this->textColor = $textColor;

        return $this;
    }

    /**
     * Get textColor
     *
     * @return string
     */
    public function getTextColor() {
        return $this->textColor;
    }
}