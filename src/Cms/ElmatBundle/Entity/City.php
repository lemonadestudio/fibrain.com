<?php
namespace Cms\ElmatBundle\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as Orm;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @Orm\Entity(repositoryClass="Cms\ElmatBundle\Repository\CityRepository")
 * @Orm\Table(name="city")
 * @ORM\HasLifecycleCallbacks();
 */
class City {
    /**
     * @Orm\Id
     * @Orm\Column(type="integer")
     * @Orm\GeneratedValue
     */
    private $id;

    /**
     * @var string $title
     *
     * @ORM\Column(type="string", length=255);
     * @Assert\NotNull();
     * @Assert\Length(max = 255);
     */
    private $title;

    /**
     * @Gedmo\Slug(fields={"title"})
     * @ORM\Column(type="string", length=255, unique=true);
     */
    private $slug;

    /**
     * @ORM\Column(type="string", length=2, nullable=false )
     */
    private $lang;

    /**
     * @ORM\OneToMany(
     *     targetEntity="TrainingDate",
     *     mappedBy="city"
     * )
     */
    protected $training_dates;

    public function __construct() {
        $this->training_dates = new ArrayCollection();
        $this->lang = "pl";
    }

    public function __toString() {
        return '' . $this->title;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     */
    public function setTitle($title) {
        $this->title = $title;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * Set slug
     *
     * @param string $slug
     */
    public function setSlug($slug) {
        $this->slug = $slug;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug() {
        return $this->slug;
    }

    /**
     * Set lang
     *
     * @param string $lang
     */
    public function setLang($lang) {
        $this->lang = $lang;
    }

    /**
     * Get lang
     *
     * @return string
     */
    public function getLang() {
        return $this->lang;
    }

    /**
     * Add training_date
     *
     * @param \Cms\ElmatBundle\Entity\TrainingDate $trainingDate
     */
    public function addTrainingDate(TrainingDate $trainingDate) {
        $this->training_dates[] = $trainingDate;
    }

    /**
     * Remove training_date
     *
     * @param \Cms\ElmatBundle\Entity\TrainingDate $trainingDate
     */
    public function removeTrainingDate(TrainingDate $trainingDate) {
        $this->training_dates->removeElement($trainingDate);
    }

    /**
     * Get training_dates
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTrainingDates() {
        return $this->training_dates;
    }
}