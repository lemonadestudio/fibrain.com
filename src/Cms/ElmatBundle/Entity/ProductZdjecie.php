<?php

namespace Cms\ElmatBundle\Entity;

use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;
use Symfony\Component\HttpFoundation\File\File;
use Cms\ElmatBundle\Helper\Cms;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;


/**
 *
 * @ORM\Table(name="product_zdjecie");
 * @ORM\Entity();
 * @ORM\HasLifecycleCallbacks();
 */
class ProductZdjecie {
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=1024, nullable=true)
     */
    private $podpis;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $kolejnosc;

    /**
     * @ORM\Column(name="obrazek", type="string", length=255, nullable=true)
     */
    private $obrazek;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @ORM\ManyToOne(targetEntity="Product", inversedBy="zdjecia")
     */
    private $product;

    /**
     *
     * @Assert\File(maxSize="6000000")
     */
    public $_file;

    public $_prev_podpis;


    public function __construct() {
        $this->kolejnosc = 0;
        $this->updated_at = new \DateTime();

    }

    public function __toString() {
        return $this->getPodpis() ? $this->getPodpis() : '' . $this->getId();
    }

    /**
     *
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload() {

        $this->updated_at = new \DateTime("now");

        if (null === $this->_file) {


            $_file = $this->getAbsolutePath();

            if ($_file
                && ($this->_prev_podpis
                    // || ( !$this->_prev_podpis && $this->getPodpis() )
                )
            ) {
                try {

                    $_file_opened = new File($_file, true);
                    $this->_file = $_file_opened;

                } catch (FileNotFoundException $e) {

                }
            }

        }

        if (null !== $this->_file) {

            // do whatever you want to generate a unique name
            $this->oldFile = $this->getObrazek();

            $ext = $this->_file->guessExtension();
            $ext = str_replace('jpeg', 'jpg', $ext);

            if (!$this->getPodpis() && $this->_file instanceof UploadedFile) {

                $orig_name = $this->_file->getClientOriginalName();
                $orig_ext = $this->_file->getClientOriginalExtension();

                $this->setPodpis(preg_replace('@\.' . preg_quote($orig_ext, '@') . '$@', '', $orig_name));

            }

            $this->obrazek = $this->generateObrazekName() . '.' . $ext;


        }

    }

    /**
     *
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload() {
        if (null === $this->_file) {
            return;
        }

        // if there is an error when moving the file, an exception will
        // be automatically thrown by move(). This will properly prevent
        // the entity from being persisted to the database on error
        $this->_file->move($this->getUploadRootDir(), $this->obrazek);

        if ($this->oldFile && $this->oldFile != $this->obrazek) {
            @unlink($this->getUploadRootDir() . '/' . $this->oldFile);
            @unlink($this->getUploadRootDir() . '/crop-' . $this->oldFile);
        }

        unset($this->_file);
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload() {
        if ($file = $this->getAbsolutePath()) {
            @unlink($file);
        }
    }


    public function getAbsolutePath() {
        return !$this->getObrazek() ? null : $this->getUploadRootDir() . '/' . $this->getObrazek();
    }

    public function getWebPath() {
        return !$this->getObrazek() ? null : $this->getUploadDir() . '/' . $this->getObrazek();
    }

    public function getWebPathCrop() {
        if (file_exists($this->getUploadRootDir() . '/crop-' . $this->getObrazek())) {
            return $this->getUploadDir() . '/crop-' . $this->getObrazek();
        } else {
            return $this->getWebPath();
        }
    }

    public function generateObrazekName() {

        $cms = new Cms();
        $name = $this->getId() . ($this->getPodpis() ? '-' . $this->getPodpis() : '');
        if (!$name) {
            $name = uniqid();
        }

        return $cms->make_url($name);
    }

    public function getUploadRootDir() {

        // the absolute directory path where uploaded documents should be saved
        return __DIR__ . '/../../../../web/' . $this->getUploadDir();
    }

    public function getUploadDir() {
        // get rid of the __DIR__ so it doesn't screw when displaying uploaded doc/image in the view.
        $prod_id = $this->getProduct() ? $this->getProduct()->getId() : '0';
        return 'uploads/product_zdjecie/' . $prod_id;
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set podpis
     *
     * @param string $podpis
     */
    public function setPodpis($podpis) {

        if ($this->_prev_podpis != $this->podpis
            && $podpis != $this->podpis
        ) {

            $this->_prev_podpis = $this->podpis;
        }


        $this->podpis = $podpis;

    }

    /**
     * Get podpis
     *
     * @return string
     */
    public function getPodpis() {
        return $this->podpis;
    }

    /**
     * Set kolejnosc
     *
     * @param integer $kolejnosc
     */
    public function setKolejnosc($kolejnosc) {
        $this->kolejnosc = $kolejnosc;
    }

    /**
     * Get kolejnosc
     *
     * @return integer
     */
    public function getKolejnosc() {
        return $this->kolejnosc;
    }

    /**
     * Set obrazek
     *
     * @param string $obrazek
     */
    public function setObrazek($obrazek) {
        $this->obrazek = $obrazek;
    }

    /**
     * Get obrazek
     *
     * @return string
     */
    public function getObrazek() {
        return $this->obrazek;
    }

    /**
     * Set updated_at
     *
     * @param \DateTime $updatedAt
     */
    public function setUpdatedAt($updatedAt) {
        $this->updated_at = $updatedAt;
    }

    /**
     * Get updated_at
     *
     * @return \DateTime
     */
    public function getUpdatedAt() {
        return $this->updated_at;
    }


    /**
     * Set product
     *
     * @param \Cms\ElmatBundle\Entity\Product $product
     * @return ProductZdjecie
     */
    public function setProduct(Product $product = null) {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return \Cms\ElmatBundle\Entity\Product
     */
    public function getProduct() {
        return $this->product;
    }
}