<?php
namespace Cms\ElmatBundle\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @Gedmo\Tree(type="nested")
 * @ORM\Entity(repositoryClass="Cms\ElmatBundle\Repository\ProductCategoryRepository")
 * @ORM\Table(name="product_category")
 * @ORM\HasLifecycleCallbacks()
 */
class ProductCategory {

    const TEMPLATE_DEFAULT = '';
    const TEMPLATE_LIST    = 'lista';

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255);
     * @Gedmo\Translatable
     * @Assert\NotNull();
     * @Assert\Length(max = 255);
     */
    private $name;

    /**
     * @Gedmo\Slug(fields={"name"})
     * @Gedmo\Translatable
     * @ORM\Column(type="string", length=255, unique=false);
     */
    private $slug;

    /**
     * @var string $content
     *
     * @Gedmo\Translatable
     * @ORM\Column(type="text", nullable=true )
     */
    private $content;

    /**
     * @var string $content
     *
     * @Gedmo\Translatable
     * @ORM\Column(type="text", nullable=true )
     */
    private $content_short;

    /**
     * @var \DateTime $created
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created;

    /**
     * @var \DateTime $updated
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated;

    /** @Assert\File(maxSize="6000000") */
    public $_file_image;

    public $_delete_file_image;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @ORM\OneToMany(targetEntity="Product", mappedBy="category", cascade={"persist"})
     * @ORM\OrderBy({"publishDate" = "DESC"});
     */
    private $products;

    /**
     * @Gedmo\TreeLeft
     * @ORM\Column(name="lft", type="integer")
     */
    private $lft;

    /**
     * @Gedmo\TreeLevel
     * @ORM\Column(name="lvl", type="integer")
     */
    private $lvl;

    /**
     * @Gedmo\TreeRight
     * @ORM\Column(name="rgt", type="integer")
     */
    private $rgt;

    /**
     * @Gedmo\TreeRoot
     * @ORM\Column(name="root", type="integer", nullable=true)
     */
    private $root;

    /**
     * @Gedmo\TreeParent
     * @ORM\ManyToOne(targetEntity="ProductCategory", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $parent;

    /**
     * @ORM\OneToMany(targetEntity="ProductCategory", mappedBy="parent", cascade={"persist"} )
     * @ORM\OrderBy({"root" = "ASC", "lft" = "ASC"})
     */
    private $children;

    /**
     * @ORM\ManyToOne(targetEntity="OfferGroup", inversedBy="product_categories")
     * @ORM\JoinColumn(onDelete="RESTRICT")
     */
    private $offer_group;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Choice(choices={"", "lista"})
     */
    private $template;

    /**
     * kolejność sortowania
     * @ORM\Column(type="integer", nullable=true);
     */
    private $kolejnosc;

    public function __construct() {
        $this->products = new ArrayCollection();
        $this->childItems = new ArrayCollection();
        $this->children = new ArrayCollection();

        $this->lvl = 1;
        $this->lft = 400;
        $this->rgt = 400;
    }

    public function __toString() {
        return '' . $this->getName();
    }

    public function getNametree($full = false) {
        $pref = '';
        for ($i = 0; $i < $this->lvl; $i++) {
            $pref .= '-';
        }
        $pref = preg_replace('/-$/', ' ↳', $pref);
        $pref = preg_replace('/-/', " · · ·", $pref);

        $name = $pref . $this->getName();

        if ($this->getLvl() == 0 || $full) {
            if ($this->getOfferGroup()) {
                $name .= ' (grupa ' . $this->getOfferGroup() . ')';
            }
        }

        return $name;
    }

    public function getNametreeFull() {
        return $this->getNametree(true);
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function updateOfferGroups() {
        return;
    }


    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function prepareUploadedNames() {
        foreach (array('image') as $_fi) {
            $_file = '_file_' . $_fi;

            if (null !== $this->{$_file}) {
                // do whatever you want to generate a unique name
                $this->{'old' . $_file} = $this->getAbsolutePath($_fi);
                $this->{'oldcrop' . $_file} = $this->getAbsolutePath($_fi, true);
                $this->{$_fi} = $_fi . '-' . uniqid() . '.' . $this->{$_file}->guessExtension();
            } elseif (true == $this->{'_delete' . $_file}) {
                @unlink($this->getAbsolutePath($_fi));
                @unlink($this->getAbsolutePath($_fi, true));
                $this->{$_fi} = null;
            }
        }
    }

    /**
     *
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function storeUploadedFiles() {
        foreach (array('image') as $_fi) {
            $_file = '_file_' . $_fi;

            if (null === $this->{$_file}) {
                continue;
            }

            $this->{$_file}->move($this->getUploadRootDir(), $this->{$_fi});

            if ($this->{'old' . $_file}) {
                @unlink($this->{'old' . $_file});
            }

            if ($this->{'oldcrop' . $_file}) {
                @unlink($this->{'oldcrop' . $_file});
            }

            unset($this->{$_file});
        }
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUploadedFiles() {
        if ($file = $this->getAbsolutePath('image')) {
            @unlink($file);
        }
    }


    public function getAbsolutePath($_fil, $crop = false) {
        $file = null;

        switch ($_fil) {
            case 'image':
            default:
                $file = $this->getImage();
                break;
        }

        return !$file ? null : $this->getUploadRootDir() . '/' . ($crop ? 'crop-' : '') . $file;
    }

    public function getWebPath($_fil = 'image') {
        $file = null;

        switch ($_fil) {
            case 'image':
            default:
                $file = $this->getImage();
                break;
        }

        return !$file ? null : $this->getUploadDir() . '/' . $file;
    }

    public function getWebPathCrop() {
        if (file_exists($this->getUploadRootDir() . '/crop-' . $this->getImage())) {
            return $this->getUploadDir() . '/crop-' . $this->getImage();
        } else {
            return $this->getWebPath();
        }
    }

    public function getUploadRootDir() {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__ . '/../../../../web/' . $this->getUploadDir();
    }

    public function getUploadDir() {
        return 'uploads/produkty_kategorie/' . $this->getId();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return ProductCategory
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return ProductCategory
     */
    public function setContent($content) {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent() {
        return $this->content;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return ProductCategory
     */
    public function setCreated($created) {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated() {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return ProductCategory
     */
    public function setUpdated($updated) {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated() {
        return $this->updated;
    }

    /**
     * Set image
     *
     * @param string $image
     * @return ProductCategory
     */
    public function setImage($image) {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage() {
        return $this->image;
    }

    /**
     * Add product
     *
     * @param \Cms\ElmatBundle\Entity\Product $product
     * @return ProductCategory
     */
    public function addProduct(Product $product) {
        $product->setCategory($this);
        $this->products->add($product);

        return $this;
    }

    public function setProducts($products) {
        foreach ($products as $product) {
            $this->addProduct($product);
        }
    }

    /**
     * Remove products
     *
     * @param \Cms\ElmatBundle\Entity\Product $products
     */
    public function removeProduct($products) {
        $this->products->removeElement($products);
    }

    /**
     * Get products
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProducts() {
        return $this->products;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return ProductCategory
     */
    public function setSlug($slug) {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug() {
        return $this->slug;
    }

    /**
     * Set parent
     *
     * @param \Cms\ElmatBundle\Entity\ProductCategory $parent
     * @return ProductCategory
     */
    public function setParent($parent = null) {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \Cms\ElmatBundle\Entity\ProductCategory
     */
    public function getParent() {
        return $this->parent;
    }

    /**
     * Add child
     *
     * @param \Cms\ElmatBundle\Entity\ProductCategory $child
     * @return ProductCategory
     */
    public function addChildren($child) {
        $this->children[] = $child;
        $child->setParent($this);

        return $this;
    }

    /**
     * Remove child
     *
     * @param \Cms\ElmatBundle\Entity\ProductCategory $child
     */
    public function removeChildren($child) {
        $this->children->removeElement($child);
    }

    /**
     * Get children
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChildren() {
        return $this->children;
    }

    /**
     * Set lft
     *
     * @param integer $lft
     * @return ProductCategory
     */
    public function setLft($lft) {
        $this->lft = $lft;

        return $this;
    }

    /**
     * Get lft
     *
     * @return integer
     */
    public function getLft() {
        return $this->lft;
    }

    /**
     * Set lvl
     *
     * @param integer $lvl
     * @return ProductCategory
     */
    public function setLvl($lvl) {
        $this->lvl = $lvl;

        return $this;
    }

    /**
     * Get lvl
     *
     * @return integer
     */
    public function getLvl() {
        return $this->lvl;
    }

    /**
     * Set rgt
     *
     * @param integer $rgt
     * @return ProductCategory
     */
    public function setRgt($rgt) {
        $this->rgt = $rgt;

        return $this;
    }

    /**
     * Get rgt
     *
     * @return integer
     */
    public function getRgt() {
        return $this->rgt;
    }

    /**
     * Set root
     *
     * @param integer $root
     * @return ProductCategory
     */
    public function setRoot($root) {
        $this->root = $root;

        return $this;
    }

    /**
     * Get root
     *
     * @return integer
     */
    public function getRoot() {
        return $this->root;
    }

    /**
     * Set offer_group
     *
     * @param \Cms\ElmatBundle\Entity\OfferGroup $offerGroup
     * @return ProductCategory
     */
    public function setOfferGroup(OfferGroup $offerGroup = null) {
        $this->offer_group = $offerGroup;

        return $this;
    }

    /**
     * Get offer_group
     *
     * @return \Cms\ElmatBundle\Entity\OfferGroup
     */
    public function getOfferGroup() {
        return $this->offer_group;
    }

    /**
     * Set template
     *
     * @param string $template
     * @return ProductCategory
     */
    public function setTemplate($template) {
        $this->template = $template;

        return $this;
    }

    /**
     * Get template
     *
     * @return string
     */
    public function getTemplate() {
        return $this->template;
    }

    /**
     * Set content_short
     *
     * @param string $contentShort
     * @return ProductCategory
     */
    public function setContentShort($contentShort) {
        $this->content_short = $contentShort;

        return $this;
    }

    /**
     * Get content_short
     *
     * @return string
     */
    public function getContentShort() {
        return $this->content_short;
    }

    /**
     * Set kolejnosc
     *
     * @param integer $kolejnosc
     * @return ProductCategory
     */
    public function setKolejnosc($kolejnosc) {
        $this->kolejnosc = $kolejnosc;

        return $this;
    }

    /**
     * Get kolejnosc
     *
     * @return integer
     */
    public function getKolejnosc() {
        return $this->kolejnosc;
    }
}