<?php

namespace Cms\ElmatBundle\Entity;

use Cms\ElmatBundle\Helper\Cms;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 *
 * @ORM\Table(name="galeria");
 * @ORM\Entity();
 * @ORM\HasLifecycleCallbacks();
 */
class Galeria {
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotNull();
     */
    private $nazwa;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $opis;

    /**
     * @ORM\Column(type="string", length=255);
     * @Assert\NotNull();
     */
    private $slug;


    /**
     * @ORM\Column(name="obrazek", type="string", length=255, nullable=true)
     */
    private $obrazek;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @ORM\OneToMany(
     *     targetEntity="GaleriaZdjecie",
     *     mappedBy="galeria",
     *     cascade={"persist", "remove"}
     * )
     * @ORM\OrderBy({"kolejnosc" = "ASC"})
     */
    private $zdjecia;

    /**
     * @ORM\OneToMany(
     *   targetEntity="Cms\ElmatBundle\Entity\Article",
     *   mappedBy="galeria"
     * )
     * @var \Doctrine\Common\Collections\Collection
     */
    private $articles;

    /**
     * @ORM\OneToMany(
     *   targetEntity="Cms\ElmatBundle\Entity\News",
     *   mappedBy="galeria"
     * )
     * @var \Doctrine\Common\Collections\Collection
     */
    private $news;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Cms\ElmatBundle\Entity\Page",
     *     mappedBy="galeria"
     * )
     */
    private $pages;

    /**
     *
     * @Assert\File(maxSize="6000000")
     */
    public $_file;


    public function __construct() {
        $this->updated_at = new \DateTime();
        $this->zdjecia = new ArrayCollection();
        $this->articles = new ArrayCollection();
        $this->news = new ArrayCollection();
        $this->pages = new ArrayCollection();
        $this->slug = 'nowa-galeria';
    }

    public function __toString() {
        return '' . $this->getNazwa();
    }

    /**
     *
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload() {
        $cms = new Cms();
        $this->setSlug($cms->make_url($this->nazwa));

        $this->updated_at = new \DateTime("now");

        if (null !== $this->_file) {
            // do whatever you want to generate a unique name
            $this->oldFile = $this->getObrazek();

            $this->obrazek = $this->generateObrazekName() . '.' . $this->_file->guessExtension();
        }
    }

    /**
     *
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload() {
        if (null === $this->_file) {
            return;
        }

        // if there is an error when moving the file, an exception will
        // be automatically thrown by move(). This will properly prevent
        // the entity from being persisted to the database on error
        $this->_file->move($this->getUploadRootDir(), $this->obrazek);

        if ($this->oldFile) {
            @unlink($this->getUploadRootDir() . '/' . $this->oldFile);
            @unlink($this->getUploadRootDir() . '/crop-' . $this->oldFile);
        }

        unset($this->_file);
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload() {
        if ($file = $this->getAbsolutePath()) {
            @unlink($file);
        }
    }

    public function getAbsolutePath() {
        return !$this->getObrazek() ? null : $this->getUploadRootDir() . '/' . $this->getObrazek();
    }

    public function getWebPath() {
        return !$this->getObrazek() ? null : $this->getUploadDir() . '/' . $this->getObrazek();
    }

    public function getWebPathCrop() {
        if (file_exists($this->getUploadRootDir() . '/crop-' . $this->getObrazek())) {
            return $this->getUploadDir() . '/crop-' . $this->getObrazek();
        } else {
            return $this->getWebPath();
        }
    }

    public function generateObrazekName() {
        $cms = new Cms();
        return $cms->make_url($this->getNazwa() . ' ' . uniqid());
    }

    public function getUploadRootDir() {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__ . '/../../../../web/' . $this->getUploadDir();
    }

    public function getUploadDir() {
        // get rid of the __DIR__ so it doesn't screw when displaying uploaded doc/image in the view.
        return 'uploads/galeria';
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set nazwa
     *
     * @param string $nazwa
     */
    public function setNazwa($nazwa) {
        $this->nazwa = $nazwa;
    }

    /**
     * Get nazwa
     *
     * @return string
     */
    public function getNazwa() {
        return $this->nazwa;
    }

    /**
     * Set opis
     *
     * @param string $opis
     */
    public function setOpis($opis) {
        $this->opis = $opis;
    }

    /**
     * Get opis
     *
     * @return string
     */
    public function getOpis() {
        return $this->opis;
    }

    /**
     * Set slug
     *
     * @param string $slug
     */
    public function setSlug($slug) {
        $this->slug = $slug;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug() {
        return $this->slug;
    }


    /**
     * Set obrazek
     *
     * @param string $obrazek
     */
    public function setObrazek($obrazek) {
        $this->obrazek = $obrazek;
    }

    /**
     * Get obrazek
     *
     * @return string
     */
    public function getObrazek() {
        return $this->obrazek;
    }


    /**
     * Set updated_at
     *
     * @param \DateTime $updatedAt
     */
    public function setUpdatedAt($updatedAt) {
        $this->updated_at = $updatedAt;
    }

    /**
     * Get updated_at
     *
     * @return \DateTime
     */
    public function getUpdatedAt() {
        return $this->updated_at;
    }

    /**
     * Add zdjecie
     *
     * @param \Cms\ElmatBundle\Entity\GaleriaZdjecie $zdjecie
     * @return Galeria
     */
    public function addZdjecium(GaleriaZdjecie $zdjecie) {
        $zdjecie->setGaleria($this);
        $this->zdjecia[] = $zdjecie;

        return $this;
    }

    /**
     * Remove zdjecie
     *
     * @param \Cms\ElmatBundle\Entity\GaleriaZdjecie $zdjecie
     */
    public function removeZdjecium(GaleriaZdjecie $zdjecie) {
        $this->zdjecia->removeElement($zdjecie);
    }

    /**
     * Get zdjecia
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getZdjecia() {
        return $this->zdjecia;
    }

    /**
     * Add article
     *
     * @param \Cms\ElmatBundle\Entity\Article $article
     * @return Galeria
     */
    public function addArticle(Article $article) {
        $this->articles[] = $article;

        return $this;
    }

    /**
     * Remove article
     *
     * @param \Cms\ElmatBundle\Entity\Article $article
     */
    public function removeArticle(Article $article) {
        $this->articles->removeElement($article);
    }

    /**
     * Get articles
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getArticles() {
        return $this->articles;
    }

    /**
     * Add news
     *
     * @param \Cms\ElmatBundle\Entity\News $news
     * @return Galeria
     */
    public function addNews(News $news) {
        $this->news[] = $news;

        return $this;
    }

    /**
     * Remove news
     *
     * @param \Cms\ElmatBundle\Entity\News $news
     */
    public function removeNews(News $news) {
        $this->news->removeElement($news);
    }

    /**
     * Get news
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNews() {
        return $this->news;
    }

    /**
     * Add page
     *
     * @param \Cms\ElmatBundle\Entity\Page $page
     * @return Galeria
     */
    public function addPage(Page $page) {
        $this->pages[] = $page;

        return $this;
    }

    /**
     * Remove page
     *
     * @param \Cms\ElmatBundle\Entity\Page $page
     */
    public function removePage(Page $page) {
        $this->pages->removeElement($page);
    }

    /**
     * Get pages
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPages() {
        return $this->pages;
    }
}