<?php
namespace Cms\ElmatBundle\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as Orm;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @Orm\Entity()
 * @Orm\Table(name="training_registration")
 * @ORM\HasLifecycleCallbacks();
 */
class TrainingRegistration {

    const STATUS_NIEPOTWIERDZONE = 0;
    const STATUS_POTWIERDZONE    = 1;
    const STATUS_ANULOWANE       = 2;

    const STATUS_PLATNOSCI_NIEOPLACONE        = 0;
    const STATUS_PLATNOSCI_OPLACONE           = 1;
    const STATUS_PLATNOSCI_OPLACONE_CZESCIOWO = 2;

    /**
     * @Orm\Id
     * @Orm\Column(type="integer")
     * @Orm\GeneratedValue
     */
    private $id;


    /**
     * @var \DateTime $updated
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated;

    /**
     * @var \DateTime $created
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $status;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $status_platnosci;

    /**
     * @ORM\Column(type="string", length=10000, nullable=true)
     * @Assert\Length(max=10000)
     */
    private $uwagi;

    /**
     * @ORM\Column(type="string", length=10000, nullable=true)
     * @Assert\Length(max=10000)
     */
    private $uwagi_administracyjne;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="training_registrations")
     * @Assert\NotBlank
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="TrainingDate", inversedBy="training_registrations")
     * @Assert\NotBlank
     */
    protected $training_date;

    /**
     * @ORM\OneToMany(targetEntity="TrainingParticipant", mappedBy="training_registration", cascade={"persist", "remove", "refresh"})
     * @Assert\NotBlank
     */
    protected $training_participants;

    public function __construct() {

        $this->status = self::STATUS_NIEPOTWIERDZONE;
        $this->status_platnosci = self::STATUS_PLATNOSCI_NIEOPLACONE;
        $this->updated = new \DateTime();
        $this->created = new \DateTime();
        $this->training_participants = new ArrayCollection();


    }

    public function __toString() {

        return '';

    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return TrainingRegistration
     */
    public function setUpdated($updated) {
        $this->updated = $updated;

        return $this;
    }

    public function getCreated() {
        return $this->created;
    }

    public function setCreated($created) {
        $this->created = $created;
        return $this;
    }


    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated() {
        return $this->updated;
    }


    /**
     * Set status
     *
     * @param integer $status
     * @return TrainingRegistration
     */
    public function setStatus($status) {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus() {
        return $this->status;
    }

    /**
     * Set status_platnosci
     *
     * @param integer $statusPlatnosci
     * @return TrainingRegistration
     */
    public function setStatusPlatnosci($statusPlatnosci) {
        $this->status_platnosci = $statusPlatnosci;

        return $this;
    }

    /**
     * Get status_platnosci
     *
     * @return integer
     */
    public function getStatusPlatnosci() {
        return $this->status_platnosci;
    }

    /**
     * Set user
     *
     * @param \Cms\ElmatBundle\Entity\User $user
     * @return TrainingRegistration
     */
    public function setUser(User $user = null) {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Cms\ElmatBundle\Entity\User
     */
    public function getUser() {
        return $this->user;
    }

    /**
     * Set training_date
     *
     * @param \Cms\ElmatBundle\Entity\TrainingDate $trainingDate
     * @return TrainingRegistration
     */
    public function setTrainingDate(TrainingDate $trainingDate = null) {
        $this->training_date = $trainingDate;

        return $this;
    }

    /**
     * Get training_date
     *
     * @return \Cms\ElmatBundle\Entity\TrainingDate
     */
    public function getTrainingDate() {
        return $this->training_date;
    }

    /**
     * Add training_participants
     *
     * @param \Cms\ElmatBundle\Entity\TrainingParticipant $trainingParticipants
     * @return TrainingRegistration
     */
    public function addTrainingParticipant(TrainingParticipant $trainingParticipants) {
        $this->training_participants[] = $trainingParticipants;
        $trainingParticipants->setTrainingRegistration($this);

        return $this;
    }

    /**
     * Remove training_participants
     *
     * @param \Cms\ElmatBundle\Entity\TrainingParticipant $trainingParticipants
     */
    public function removeTrainingParticipant(TrainingParticipant $trainingParticipants) {
        $this->training_participants->removeElement($trainingParticipants);
    }

    /**
     * Get training_participants
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTrainingParticipants() {
        return $this->training_participants;
    }

    /**
     * Set uwagi
     *
     * @param string $uwagi
     * @return TrainingRegistration
     */
    public function setUwagi($uwagi) {
        $this->uwagi = $uwagi;

        return $this;
    }

    /**
     * Get uwagi
     *
     * @return string
     */
    public function getUwagi() {
        return $this->uwagi;
    }

    /**
     * Set uwagi_administracyjne
     *
     * @param string $uwagiAdministracyjne
     * @return TrainingRegistration
     */
    public function setUwagiAdministracyjne($uwagiAdministracyjne) {
        $this->uwagi_administracyjne = $uwagiAdministracyjne;

        return $this;
    }

    /**
     * Get uwagi_administracyjne
     *
     * @return string
     */
    public function getUwagiAdministracyjne() {
        return $this->uwagi_administracyjne;
    }


    public function getStatusTxt() {

        $status = '';
        switch ($this->status) {

            case self::STATUS_NIEPOTWIERDZONE:
                $status = 'Niepotwierdzone';
                break;

            case self::STATUS_POTWIERDZONE:
                $status = 'Potwierdzone';
                break;

            case self::STATUS_ANULOWANE:
                $status = 'Anulowane';
                break;

        }

        return $status;

    }

    public function getStatusPlatnosciTxt() {

        $status = '';
        switch ($this->status_platnosci) {

            case self::STATUS_PLATNOSCI_NIEOPLACONE:
                $status = 'Nieopłacone';
                break;

            case self::STATUS_PLATNOSCI_OPLACONE:
                $status = 'Opłacone';
                break;
            case self::STATUS_PLATNOSCI_OPLACONE_CZESCIOWO:
                $status = 'Opłacone częściowo';
                break;


        }


        return $status;
    }


    public function participantsTxt() {

        $txt = '';
        $n = 1;
        foreach ($this->getTrainingParticipants() as $tr) {

            $txt .= $n++ . ':' . $tr->getNazwa() . ' '
                . $tr->getEmail() . ' '
                . $tr->getTelefon() . ', ';


        }

        return $txt;

    }

}