<?php
namespace Cms\ElmatBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity()
 * @ORM\Table(name="offer_group")
 * @ORM\HasLifecycleCallbacks();
 */
class OfferGroup {
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * kolejność sortowania
     * @ORM\Column(type="integer", nullable=true);
     */
    private $kolejnosc;

    /**
     * @ORM\Column(type="string", length=2, nullable=false )
     */
    private $lang;

    /**
     * @Gedmo\Slug(fields={"title"}, updatable=false, unique=false)
     * @ORM\Column(name="slug", type="string", length=255)
     */
    private $slug;

    /**
     * @var \DateTime $created
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @var \DateTime $updated
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updated;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $show_highlights;

    /** @Assert\File(maxSize="6000000") */
    public $_file_image;

    public $_delete_file_image = false;

    /**
     * @ORM\Column(name="image", type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Cms\ElmatBundle\Entity\Article",
     *     mappedBy="offer_group"
     * )
     */
    private $articles;

    /**
     * @ORM\OneToMany(
     *     targetEntity="News",
     *     mappedBy="offer_group"
     * )
     * @ORM\OrderBy({"publishDate" = "DESC"});
     */
    private $news;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Cms\ElmatBundle\Entity\Page",
     *     mappedBy="offer_group"
     * )
     */
    private $pages;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Cms\ElmatBundle\Entity\Product",
     *     mappedBy="offer_group"
     * )
     */
    private $products;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Cms\ElmatBundle\Entity\ProductCategory",
     *     mappedBy="offer_group"
     * )
     */
    private $product_categories;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Cms\ElmatBundle\Entity\SliderPhoto",
     *     mappedBy="offer_group"
     * )
     */
    private $slider_photos;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Cms\ElmatBundle\Entity\ZasobCategory",
     *     mappedBy="offer_group"
     * )
     */
    private $zasob_categories;

    public function __toString() {
        return '' . $this->getTitle();
    }

    public function __construct() {
        $this->kolejnosc = 99;
        $this->lang = 'pl';
        $this->articles = new ArrayCollection();
        $this->news = new ArrayCollection();
        $this->pages = new ArrayCollection();
        $this->products = new ArrayCollection();
        $this->product_categories = new ArrayCollection();
        $this->slider_photos = new ArrayCollection();
        $this->zasob_categories = new ArrayCollection();
    }

    /**
     * tak, wiem ;]
     * @return string
     */
    public function getSymbol() {
        $symbol = '';

        if (preg_match('/structural-cabling/', $this->getSlug())) {
            $symbol = 'structural_cabling';
        }
        if (preg_match('/fiber-optic-cables/', $this->getSlug())) {
            $symbol = 'fiber_optic';
        }
        if (preg_match('/fttx-systems-and-elements/', $this->getSlug())) {
            $symbol = 'fttx_systems';
        }
        if (preg_match('/passive-optical-technology/', $this->getSlug())) {
            $symbol = 'passive_optical';
        }
        if (preg_match('/active-elements-and-systems/', $this->getSlug())) {
            $symbol = 'active_elements';
        }
        if (preg_match('/connectivity/', $this->getSlug())) {
            $symbol = 'connectivity';
        }
        if (preg_match('/distribution/', $this->getSlug())) {
            $symbol = 'distribution';
        }
        if (preg_match('/photonics/', $this->getSlug())) {
            $symbol = 'photonics';
        }

        return $symbol;
    }

    public function isEnabled() {
        $slugsOfEnabledItems = array(
            'structural-cabling',
            'active-elements-and-systems',
//            'fttx-systems-and-elements',
            'passive-optical-technology',
            'connectivity',
            'fiber-optic-cables',
            'distribution',
            'photonics'
        );

        return in_array($this->slug, $slugsOfEnabledItems, true);
    }

    public function getRoute() {
        $symbol = $this->getSymbol();
        $baseRoute = 'cms_elmat_default_homepage';

        if (!empty($symbol)) {
            return "{$baseRoute}_{$symbol}";
        }

        return $baseRoute;
    }


    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function prepareUploadedNames() {
        foreach (array('image') as $_fi) {
            $_file = '_file_' . $_fi;
            if (null !== $this->{$_file}) {
                $this->{'old' . $_file} = $this->getAbsolutePath($_fi);
                $this->{$_fi} = $_fi . '-' . uniqid() . '.' . $this->{$_file}->guessExtension();
            } elseif (true == $this->{'_delete' . $_file}) {
                @unlink($this->getAbsolutePath($_fi));
                $this->{$_fi} = null;
            }
        }
    }

    /**
     *
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function storeUploadedFiles() {
        foreach (array('image') as $_fi) {
            $_file = '_file_' . $_fi;
            if (null === $this->{$_file}) {
                continue;
            }
            $this->{$_file}->move($this->getUploadRootDir(), $this->{$_fi});
            if ($this->{'old' . $_file}) {
                @unlink($this->{'old' . $_file});
            }
            unset($this->{$_file});
        }
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUploadedFiles() {
        if ($file = $this->getAbsolutePath('image')) {
            @unlink($file);
        }
    }


    public function getAbsolutePath($_fil) {
        $file = null;

        switch ($_fil) {
            case 'image':
            default:
                $file = $this->getImage();
        }

        return !$file ? null : $this->getUploadRootDir() . '/' . $file;
    }

    public function getWebPath($_fil = 'image') {
        $file = null;

        switch ($_fil) {
            case 'image':
            default:
                $file = $this->getImage();
        }

        return !$file ? null : $this->getUploadDir() . '/' . $file;
    }

    protected function getUploadRootDir() {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__ . '/../../../../web/' . $this->getUploadDir();
    }

    protected function getUploadDir() {
        return 'uploads/offer_group/' . $this->getId();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return OfferGroup
     */
    public function setTitle($title) {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * Set kolejnosc
     *
     * @param integer $kolejnosc
     * @return OfferGroup
     */
    public function setKolejnosc($kolejnosc) {
        $this->kolejnosc = $kolejnosc;

        return $this;
    }

    /**
     * Get kolejnosc
     *
     * @return integer
     */
    public function getKolejnosc() {
        return $this->kolejnosc;
    }

    /**
     * Set lang
     *
     * @param string $lang
     * @return OfferGroup
     */
    public function setLang($lang) {
        $this->lang = $lang;

        return $this;
    }

    /**
     * Get lang
     *
     * @return string
     */
    public function getLang() {
        return $this->lang;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return OfferGroup
     */
    public function setSlug($slug) {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug() {
        return $this->slug;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return OfferGroup
     */
    public function setCreated($created) {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated() {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return OfferGroup
     */
    public function setUpdated($updated) {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated() {
        return $this->updated;
    }

    /**
     * Set show_highlights
     *
     * @param boolean $showHighlights
     * @return OfferGroup
     */
    public function setShowHighlights($showHighlights) {
        $this->show_highlights = $showHighlights;

        return $this;
    }

    /**
     * Get show_highlights
     *
     * @return boolean
     */
    public function getShowHighlights() {
        return $this->show_highlights;
    }

    /**
     * Set image
     *
     * @param string $image
     * @return Training
     */
    public function setImage($image) {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage() {
        return $this->image;
    }

    /**
     * Add article
     *
     * @param \Cms\ElmatBundle\Entity\Article $article
     * @return OfferGroup
     */
    public function addArticle(Article $article) {
        $this->articles[] = $article;

        return $this;
    }

    /**
     * Remove article
     *
     * @param \Cms\ElmatBundle\Entity\Article $article
     */
    public function removeArticle(Article $article) {
        $this->articles->removeElement($article);
    }

    /**
     * Get articles
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getArticles() {
        return $this->articles;
    }

    /**
     * Add news
     *
     * @param \Cms\ElmatBundle\Entity\News $news
     * @return OfferGroup
     */
    public function addNew(News $news) {
        $this->news[] = $news;

        return $this;
    }

    /**
     * Remove news
     *
     * @param \Cms\ElmatBundle\Entity\News $news
     */
    public function removeNew(News $news) {
        $this->news->removeElement($news);
    }

    /**
     * Get news
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNews() {
        return $this->news;
    }

    /**
     * Add page
     *
     * @param \Cms\ElmatBundle\Entity\Page $page
     * @return OfferGroup
     */
    public function addPage(Page $page) {
        $this->pages[] = $page;

        return $this;
    }

    /**
     * Remove page
     *
     * @param \Cms\ElmatBundle\Entity\Page $page
     */
    public function removePage(Page $page) {
        $this->pages->removeElement($page);
    }

    /**
     * Get pages
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPages() {
        return $this->pages;
    }

    /**
     * Add product
     *
     * @param \Cms\ElmatBundle\Entity\Product $product
     * @return OfferGroup
     */
    public function addProduct(Product $product) {
        $this->products[] = $product;

        return $this;
    }

    /**
     * Remove product
     *
     * @param \Cms\ElmatBundle\Entity\Product $product
     */
    public function removeProduct(Product $product) {
        $this->products->removeElement($product);
    }

    /**
     * Get products
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProducts() {
        return $this->products;
    }

    /**
     * Add product_category
     *
     * @param \Cms\ElmatBundle\Entity\ProductCategory $product_category
     * @return OfferGroup
     */
    public function addProductCategory(ProductCategory $product_category) {
        $this->product_categories[] = $product_category;

        return $this;
    }

    /**
     * Remove product_category
     *
     * @param \Cms\ElmatBundle\Entity\ProductCategory $product_category
     */
    public function removeProductCategory(ProductCategory $product_category) {
        $this->product_categories->removeElement($product_category);
    }

    /**
     * Get product_categories
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProductCategories() {
        return $this->product_categories;
    }

    /**
     * Add slider_photo
     *
     * @param \Cms\ElmatBundle\Entity\SliderPhoto $slider_photo
     * @return OfferGroup
     */
    public function addSliderPhoto(SliderPhoto $slider_photo) {
        $this->slider_photos[] = $slider_photo;

        return $this;
    }

    /**
     * Remove slider_photo
     *
     * @param \Cms\ElmatBundle\Entity\SliderPhoto $slider_photo
     */
    public function removeSliderPhoto(SliderPhoto $slider_photo) {
        $this->slider_photos->removeElement($slider_photo);
    }

    /**
     * Get product_categors
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSliderPhotos() {
        return $this->slider_photos;
    }

    /**
     * Add zasob_category
     *
     * @param \Cms\ElmatBundle\Entity\ZasobCategory $zasob_category
     * @return OfferGroup
     */
    public function addZasobCategory(ZasobCategory $zasob_category) {
        $this->zasob_categories[] = $zasob_category;

        return $this;
    }

    /**
     * Remove zasob_category
     *
     * @param \Cms\ElmatBundle\Entity\ZasobCategory $zasob_category
     */
    public function removeZasobCategory(ZasobCategory $zasob_category) {
        $this->zasob_categories->removeElement($zasob_category);
    }

    /**
     * Get zasob_categors
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getZasobCategories() {
        return $this->zasob_categories;
    }

}