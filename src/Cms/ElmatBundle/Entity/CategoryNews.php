<?php
namespace Cms\ElmatBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="category_news")
 * @ORM\Entity(repositoryClass="Cms\ElmatBundle\Repository\CategoryNewsRepository")
 * @ORM\HasLifecycleCallbacks();
 */
class CategoryNews {
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * kolejność sortowania
     * @ORM\Column(type="integer", nullable=true);
     */
    private $kolejnosc;

    /**
     * @ORM\Column(type="string", length=2, nullable=false )
     */
    private $lang;


    /**
     * @Gedmo\Slug(fields={"title"}, updatable=false, unique=false)
     * @ORM\Column(name="slug", type="string", length=255)
     */
    private $slug;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $show_highlights;

    /**
     * @var \DateTime $created
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @var \DateTime $updated
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updated;

    /**
     * @ORM\OneToMany(targetEntity="News", mappedBy="category")
     * @ORM\OrderBy({"publishDate" = "DESC"})
     *
     */
    protected $news;

    public function __toString() {
        return '' . $this->getTitle();
    }

    public function __construct() {
        $this->lang = 'pl';
        $this->kolejnosc = 99;
        $this->news = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return CategoryNews
     */
    public function setTitle($title) {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * Set kolejnosc
     *
     * @param integer $kolejnosc
     * @return CategoryNews
     */
    public function setKolejnosc($kolejnosc) {
        $this->kolejnosc = $kolejnosc;

        return $this;
    }

    /**
     * Get kolejnosc
     *
     * @return integer
     */
    public function getKolejnosc() {
        return $this->kolejnosc;
    }

    /**
     * Set lang
     *
     * @param string $lang
     * @return CategoryNews
     */
    public function setLang($lang) {
        $this->lang = $lang;

        return $this;
    }

    /**
     * Get lang
     *
     * @return string
     */
    public function getLang() {
        return $this->lang;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return CategoryNews
     */
    public function setSlug($slug) {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug() {
        return $this->slug;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return CategoryNews
     */
    public function setCreated($created) {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated() {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return CategoryNews
     */
    public function setUpdated($updated) {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated() {
        return $this->updated;
    }

    /**
     * Set show_highlights
     *
     * @param boolean $showHighlights
     * @return CategoryNews
     */
    public function setShowHighlights($showHighlights) {
        $this->show_highlights = $showHighlights;

        return $this;
    }

    /**
     * Get show_highlights
     *
     * @return boolean
     */
    public function getShowHighlights() {
        return $this->show_highlights;
    }

    /**
     * Add news
     *
     * @param \Cms\ElmatBundle\Entity\News $news
     * @return CategoryNews
     */
    public function addNews(News $news) {
        $this->news[] = $news;

        return $this;
    }

    /**
     * Remove news
     *
     * @param \Cms\ElmatBundle\Entity\News $news
     */
    public function removeNews(News $news) {
        $this->news->removeElement($news);
    }

    /**
     * Get news
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNews() {
        return $this->news;
    }
}