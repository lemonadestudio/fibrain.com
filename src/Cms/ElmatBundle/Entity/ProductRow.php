<?php

namespace Cms\ElmatBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Cms\ElmatBundle\Helper\Cms;


/**
 * ProductTableRow
 *
 * @ORM\Table(name="product_row")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class ProductRow {

    const TYPE_ROW       = 0;
    const TYPE_SEPARATOR = 1;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="sort", type="smallint", nullable=true)
     */
    private $sort;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255, nullable=true)
     */
    private $image;


    /**
     * @var string
     *
     * @ORM\Column(name="doc_pl", type="string", length=255, nullable=true)
     */
    private $doc_pl;

    /**
     * @var string
     *
     * @ORM\Column(name="doc_en", type="string", length=255, nullable=true)
     */
    private $doc_en;

    /** @Assert\File(maxSize="6000000") */
    public $_file_image;
    /** @Assert\File(maxSize="6000000") */
    public $_file_doc_pl;
    /** @Assert\File(maxSize="6000000") */
    public $_file_doc_en;

    public $_delete_file_image;
    public $_delete_file_doc_pl;
    public $_delete_file_doc_en;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="value1", type="string", length=255, nullable=true)
     */
    private $value1;

    /**
     * @var string
     *
     * @ORM\Column(name="value2", type="string", length=255, nullable=true)
     */
    private $value2;

    /**
     * @var string
     *
     * @ORM\Column(name="value3", type="string", length=255, nullable=true)
     */
    private $value3;

    /**
     * @var string
     *
     * @ORM\Column(name="value4", type="string", length=255, nullable=true)
     */
    private $value4;

    /**
     * @var string
     *
     * @ORM\Column(name="value5", type="string", length=255, nullable=true)
     */
    private $value5;

    /**
     * @var string
     *
     * @ORM\Column(name="value6", type="string", length=255, nullable=true)
     */
    private $value6;

    /**
     * @var string
     *
     * @ORM\Column(name="value7", type="string", length=255, nullable=true)
     */
    private $value7;

    /**
     * @ORM\ManyToOne(targetEntity="Product", inversedBy="rows")
     */
    private $product;

    /**
     * @var \DateTime $created
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created;

    /**
     * @var \DateTime $updated
     *
     * @Gedmo\Timestampable(on="update")
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated;


    /**
     * @ORM\Column(type="integer", nullable=true);
     *
     * @Assert\Choice(choices={"0", "1"})
     */
    private $type;

    /** @ORM\Column(type="string", length=25, nullable=true) */
    private $separator_bgcolor;

    /** @ORM\Column(type="string", length=255, nullable=true) */
    private $separator_text1;

    /** @ORM\Column(type="string", length=255, nullable=true) */
    private $separator_text2;


    /** @ORM\Column(type="string", length=6, nullable=true) */
    private $height;

    /**
     * @var Cms
     */
    private $helper = null;


    public function __construct() {
        $this->type = self::TYPE_ROW;
    }

    /**
     *
     * @return \Cms\ElmatBundle\Helper\Cms
     */
    public function getHelper() {
        if (!$this->helper) {
            $this->helper = new Cms();
        }
        return $this->helper;
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function prepareUploadedNames() {


        foreach (array('image', 'doc_pl', 'doc_en') as $_fi) {
            $_file = '_file_' . $_fi;


            if (null !== $this->{$_file} && $this->{$_file}->isValid()) {

// 			    var_dump($_file, $this->{$_file}->isValid());

                // do whatever you want to generate a unique name
                $this->{'old' . $_file} = $this->getAbsolutePath($_fi);
                $this->{$_fi} = $_fi . '-' . uniqid() . '.' . $this->{$_file}->guessExtension();

            } elseif (true == $this->{'_delete' . $_file}) {

                @unlink($this->getAbsolutePath($_fi));
                $this->{$_fi} = null;

            }

        }
// 		die;
    }

    /**
     *
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function storeUploadedFiles() {

        foreach (array('image', 'doc_pl', 'doc_en') as $_fi) {

            $_file = '_file_' . $_fi;

            if (null === $this->{$_file} || !$this->{$_file}->isValid()) {
                continue;
            }

            $this->{$_file}->move($this->getUploadRootDir(), $this->{$_fi});

            if ($this->{'old' . $_file}) {
                @unlink($this->{'old' . $_file});
            }

            unset($this->{$_file});


        }

    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUploadedFiles() {
        if ($file = $this->getAbsolutePath('image')) {
            @unlink($file);
        }
        if ($file = $this->getAbsolutePath('doc_pl')) {
            @unlink($file);
        }
        if ($file = $this->getAbsolutePath('doc_en')) {
            @unlink($file);
        }
    }


    public function getAbsolutePath($_fil) {
        $file = null;
        switch ($_fil) {
            case 'doc_pl':
                $file = $this->getDocPl();
                break;
            case 'doc_en':
                $file = $this->getDocEn();
                break;
            case 'image':
            default:
                $file = $this->getImage();
        }
        return !$file ? null : $this->getUploadRootDir() . '/' . $file;
    }

    public function getWebPath($_fil) {
        $file = null;
        switch ($_fil) {
            case 'doc_pl':
                $file = $this->getDocPl();
                break;
            case 'doc_en':
                $file = $this->getDocEn();
                break;
            case 'image':
            default:
                $file = $this->getImage();
        }
        return !$file ? null : $this->getUploadDir() . '/' . $file;
    }

    protected function getUploadRootDir() {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__ . '/../../../../web/' . $this->getUploadDir();
    }

    protected function getUploadDir() {
        return 'uploads/produkty_rows/' . $this->getProduct()->getId();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set sort
     *
     * @param integer $sort
     * @return ProductRow
     */
    public function setSort($sort) {
        $this->sort = $sort;

        return $this;
    }

    /**
     * Get sort
     *
     * @return integer
     */
    public function getSort() {
        return $this->sort;
    }

    /**
     * Set image
     *
     * @param string $image
     * @return ProductRow
     */
    public function setImage($image) {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage() {
        return $this->image;
    }

    /**
     * Set doc_pl
     *
     * @param string $docPl
     * @return ProductRow
     */
    public function setDocPl($docPl) {
        $this->doc_pl = $docPl;

        return $this;
    }

    /**
     * Get doc_pl
     *
     * @return string
     */
    public function getDocPl() {
        return $this->doc_pl;
    }

    /**
     * Set doc_en
     *
     * @param string $docEn
     * @return ProductRow
     */
    public function setDocEn($docEn) {
        $this->doc_en = $docEn;

        return $this;
    }

    /**
     * Get doc_en
     *
     * @return string
     */
    public function getDocEn() {
        return $this->doc_en;
    }

    /**
     * Set value1
     *
     * @param string $value1
     * @return ProductRow
     */
    public function setValue1($value1) {
        $this->value1 = $value1;

        return $this;
    }

    /**
     * Get value1
     *
     * @return string
     */
    public function getValue1() {
        return $this->value1;
    }

    /**
     * Set value2
     *
     * @param string $value2
     * @return ProductRow
     */
    public function setValue2($value2) {
        $this->value2 = $value2;

        return $this;
    }

    /**
     * Get value2
     *
     * @return string
     */
    public function getValue2() {
        return $this->value2;
    }

    /**
     * Set value3
     *
     * @param string $value3
     * @return ProductRow
     */
    public function setValue3($value3) {
        $this->value3 = $value3;

        return $this;
    }

    /**
     * Get value3
     *
     * @return string
     */
    public function getValue3() {
        return $this->value3;
    }

    /**
     * Set value4
     *
     * @param string $value4
     * @return ProductRow
     */
    public function setValue4($value4) {
        $this->value4 = $value4;

        return $this;
    }

    /**
     * Get value4
     *
     * @return string
     */
    public function getValue4() {
        return $this->value4;
    }

    /**
     * Set value5
     *
     * @param string $value5
     * @return ProductRow
     */
    public function setValue5($value5) {
        $this->value5 = $value5;

        return $this;
    }

    /**
     * Get value5
     *
     * @return string
     */
    public function getValue5() {
        return $this->value5;
    }

    /**
     * Set value6
     *
     * @param string $value6
     * @return ProductRow
     */
    public function setValue6($value6) {
        $this->value6 = $value6;

        return $this;
    }

    /**
     * Get value6
     *
     * @return string
     */
    public function getValue6() {
        return $this->value6;
    }

    /**
     * Set value7
     *
     * @param string $value7
     * @return ProductRow
     */
    public function setValue7($value7) {
        $this->value7 = $value7;

        return $this;
    }

    /**
     * Get value7
     *
     * @return string
     */
    public function getValue7() {
        return $this->value7;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return ProductRow
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return ProductRow
     */
    public function setDescription($description) {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Set product
     *
     * @param \Cms\ElmatBundle\Entity\Product $product
     * @return ProductRow
     */
    public function setProduct($product = null) {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return \Cms\ElmatBundle\Entity\Product
     */
    public function getProduct() {
        return $this->product;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Product
     */
    public function setCreated($created) {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated() {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return Product
     */
    public function setUpdated($updated) {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated() {
        return $this->updated;
    }


    /**
     * Set type
     *
     * @param integer $type
     * @return ProductRow
     */
    public function setType($type) {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer
     */
    public function getType() {
        return $this->type;
    }

    /**
     * Set separator_bgcolor
     *
     * @param string $separatorBgcolor
     * @return ProductRow
     */
    public function setSeparatorBgcolor($separatorBgcolor) {
        $this->separator_bgcolor = $separatorBgcolor;

        return $this;
    }

    /**
     * Get separator_bgcolor
     *
     * @return string
     */
    public function getSeparatorBgcolor() {
        return $this->separator_bgcolor;
    }

    /**
     * Set separator_text1
     *
     * @param string $separatorText1
     * @return ProductRow
     */
    public function setSeparatorText1($separatorText1) {
        $this->separator_text1 = $separatorText1;

        return $this;
    }

    /**
     * Get separator_text1
     *
     * @return string
     */
    public function getSeparatorText1() {
        return $this->separator_text1;
    }

    /**
     * Set separator_text2
     *
     * @param string $separatorText2
     * @return ProductRow
     */
    public function setSeparatorText2($separatorText2) {
        $this->separator_text2 = $separatorText2;

        return $this;
    }

    /**
     * Get separator_text2
     *
     * @return string
     */
    public function getSeparatorText2() {
        return $this->separator_text2;
    }

    /**
     * Set height
     *
     * @param string $height
     * @return ProductRow
     */
    public function setHeight($height) {
        $this->height = $height;

        return $this;
    }

    /**
     * Get height
     *
     * @return string
     */
    public function getHeight() {
        return $this->getHelper()->parseWidthCss($this->height);
    }
}