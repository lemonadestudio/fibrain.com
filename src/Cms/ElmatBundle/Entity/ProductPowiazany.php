<?php

namespace Cms\ElmatBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 *
 * @ORM\Table(name="product_powiazany");
 * @ORM\Entity();
 * @ORM\HasLifecycleCallbacks();
 */
class ProductPowiazany {
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $kolejnosc;

    /**
     * @ORM\ManyToOne(targetEntity="Product", inversedBy="powiazane")
     */
    private $product;

    /**
     * @ORM\ManyToOne(targetEntity="Product", inversedBy="powiazane_do")
     */
    private $powiazany;

    public function __construct() {
        $this->kolejnosc = 0;
        $this->updated_at = new \DateTime();

    }

    public function __toString() {
        return '' . $this->getId();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set kolejnosc
     *
     * @param integer $kolejnosc
     * @return ProductPowiazany
     */
    public function setKolejnosc($kolejnosc) {
        $this->kolejnosc = $kolejnosc;

        return $this;
    }

    /**
     * Get kolejnosc
     *
     * @return integer
     */
    public function getKolejnosc() {
        return $this->kolejnosc;
    }

    /**
     * Set product
     *
     * @param \Cms\ElmatBundle\Entity\Product $product
     * @return ProductPowiazany
     */
    public function setProduct(Product $product = null) {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return \Cms\ElmatBundle\Entity\Product
     */
    public function getProduct() {
        return $this->product;
    }

    /**
     * Set powiazany
     *
     * @param \Cms\ElmatBundle\Entity\Product $powiazany
     * @return ProductPowiazany
     */
    public function setPowiazany(Product $powiazany = null) {
        $this->powiazany = $powiazany;

        return $this;
    }

    /**
     * Get powiazany
     *
     * @return \Cms\ElmatBundle\Entity\Product
     */
    public function getPowiazany() {
        return $this->powiazany;
    }
}