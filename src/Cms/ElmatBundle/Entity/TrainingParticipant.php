<?php
namespace Cms\ElmatBundle\Entity;


use Doctrine\ORM\Mapping as Orm;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @Orm\Entity()
 * @Orm\Table(name="training_participant")
 * @ORM\HasLifecycleCallbacks();
 */
class TrainingParticipant {
    /**
     * @Orm\Id
     * @Orm\Column(type="integer")
     * @Orm\GeneratedValue
     */
    private $id;

    /**
     * @var \DateTime $updated
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $nazwa;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Email()
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $telefon;

    /**
     * @ORM\ManyToOne(targetEntity="TrainingRegistration", inversedBy="training_participants", cascade={"persist", "refresh"})
     * @Assert\NotBlank
     */
    protected $training_registration;

    public function __construct() {
        $this->updated = new \DateTime();
    }

    public function __toString() {

        return '';

    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return TrainingParticipant
     */
    public function setUpdated($updated) {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated() {
        return $this->updated;
    }

    /**
     * Set nazwa
     *
     * @param string $nazwa
     * @return TrainingParticipant
     */
    public function setNazwa($nazwa) {
        $this->nazwa = $nazwa;

        return $this;
    }

    /**
     * Get nazwa
     *
     * @return string
     */
    public function getNazwa() {
        return $this->nazwa;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return TrainingParticipant
     */
    public function setEmail($email) {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail() {
        return $this->email;
    }

    /**
     * Set telefon
     *
     * @param string $telefon
     * @return TrainingParticipant
     */
    public function setTelefon($telefon) {
        $this->telefon = $telefon;

        return $this;
    }

    /**
     * Get telefon
     *
     * @return string
     */
    public function getTelefon() {
        return $this->telefon;
    }

    /**
     * Set training_registration
     *
     * @param \Cms\ElmatBundle\Entity\TrainingRegistration $trainingRegistration
     * @return TrainingParticipant
     */
    public function setTrainingRegistration(TrainingRegistration $trainingRegistration = null) {
        $this->training_registration = $trainingRegistration;

        return $this;
    }

    /**
     * Get training_registration
     *
     * @return \Cms\ElmatBundle\Entity\TrainingRegistration
     */
    public function getTrainingRegistration() {
        return $this->training_registration;
    }
}