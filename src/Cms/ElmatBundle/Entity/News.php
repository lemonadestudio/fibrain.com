<?php
namespace Cms\ElmatBundle\Entity;


use Cms\ElmatBundle\Helper\Cms;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as Orm;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @Orm\Entity(repositoryClass="Cms\ElmatBundle\Repository\NewsRepository")
 * @Orm\Table(name="news")
 * @Orm\HasLifecycleCallbacks()
 * @Gedmo\SoftDeleteable(fieldName="deletedAt")
 */
class News {
    /**
     * @Orm\Id
     * @Orm\Column(type="integer")
     * @Orm\GeneratedValue
     */
    private $id;

    /**
     * @var string $title
     *
     * @ORM\Column(type="string", length=255);
     * @Assert\NotNull();
     * @Assert\Length(max = 255);
     */
    private $title;

    /**
     * @var \DateTime $deletedAt
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @Gedmo\Slug(fields={"title"}, updatable=false, unique=false)
     * @ORM\Column(type="string", length=255, unique=false);
     */
    private $slug;

    /**
     * @ORM\Column(type="boolean")
     */
    private $published = true;

    /**
     * @var \DateTime $publishDate ;
     * @ORM\Column(type="datetime", nullable=true);
     */
    private $publishDate;

    /**
     * @var string $keywords
     *
     * @ORM\Column(type="string", length=1024, nullable=true);
     * @Assert\Length(max = 1024);
     */
    private $keywords;

    /**
     * @var string $description
     *
     * @ORM\Column(type="string", length=2048, nullable=true);
     * @Assert\Length(max = 2048)
     */
    private $description;

    /**
     * @var string $content
     *
     * @ORM\Column(type="text", nullable=true )
     */
    private $content;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true );
     */
    private $automaticSeo;

    /**
     * @ORM\Column(type="string", length=2, nullable=false )
     */
    private $lang;

    /**
     * @var \DateTime $created
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @var \DateTime $updated
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updated;


    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $highlight_mainpage;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $highlight_category;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $highlights_box_mainpage;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $promocja;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $view_count;


    /** @Assert\File(maxSize="6000000") */
    public $_file_image;

    public $_delete_file_image = false;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="CategoryNews",
     *     inversedBy="news"
     * )
     * @ORM\JoinColumn(onDelete="RESTRICT")
     */
    protected $category;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="OfferGroup",
     *     inversedBy="news"
     * )
     * @ORM\JoinColumn(onDelete="RESTRICT")
     */
    protected $offer_group;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Galeria",
     *     inversedBy="news"
     * )
     * @ORM\JoinColumn(
     *     name="galeria_id",
     *     referencedColumnName="id",
     *     onDelete="SET NULL"
     * )
     */
    protected $galeria;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Plik",
     *     mappedBy="news",
     *     cascade={"persist", "remove"}
     * )
     * @ORM\OrderBy({"kolejnosc" = "ASC"})
     */
    private $pliki;

    public function __construct() {
        $this->published = true;
        $this->automaticSeo = true;
        $this->publishDate = new \DateTime();
        $this->view_count = 0;
        $this->lang = "pl";
        $this->pliki = new ArrayCollection();
    }

    public function __toString() {
        return '' . $this->title;
    }

    public function getRouteParametersArray() {
        return array();
    }

    public function getRoute() {
        return "CmsElmatBundle_news";
    }

    /**
     * @ORM\PrePersist();
     * @ORM\PreUpdate();
     */
    public function setCreatedValue() {
        if ($this->getAutomaticSeo()) {
            // urla
            $cms_helper = new Cms();
            $this->setSlug($cms_helper->make_url($this->getTitle()));

            // description
            $description = strip_tags($this->getContent());
            $description = mb_substr(html_entity_decode($description), 0, 1048);

            // usunięcie nowych linii
            $description = preg_replace('@\v@', ' ', $description);
            // podwójnych białych znaków
            $description = preg_replace('@\h{2,}@', ' ', $description);

            // usunięcie ostatniego, niedokończonego zdania
            $description = preg_replace('@(.*)\..*@', '\1.', $description);

            // trim
            $description = trim($description);

            $this->setDescription($description);

            // keywords
            $keywords_arr = explode(' ', $this->getTitle() . ' ' . $this->getDescription());

            $keywords = array();
            if (is_array($keywords_arr)) {
                foreach ($keywords_arr as $kw) {
                    $kw = trim($kw);
                    $kw = preg_replace('@\.,;\'\"@', '', $kw);
                    if (strlen($kw) >= 4 && !in_array($kw, $keywords)) {
                        $keywords[] = $kw;
                    }
                    if (count($keywords) >= 10) {
                        break;
                    }
                }
            }

            $this->setKeywords(implode(', ', $keywords));
            $this->setAutomaticSeo(false);
        }
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function prepareUploadedNames() {
        foreach (array('image') as $_fi) {
            $_file = '_file_' . $_fi;

            if (null !== $this->{$_file}) {
                // do whatever you want to generate a unique name
                $this->{'old' . $_file} = $this->getAbsolutePath($_fi);
                $this->{'oldcrop' . $_file} = $this->getAbsolutePath($_fi, true);
                $this->{$_fi} = $_fi . '-' . uniqid() . '.' . $this->{$_file}->guessExtension();
            } elseif (true == $this->{'_delete' . $_file}) {
                @unlink($this->getAbsolutePath($_fi));
                @unlink($this->getAbsolutePath($_fi, true));
                $this->{$_fi} = null;
            }
        }
    }

    /**
     *
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function storeUploadedFiles() {
        foreach (array('image') as $_fi) {
            $_file = '_file_' . $_fi;

            if (null === $this->{$_file}) {
                continue;
            }

            $this->{$_file}->move($this->getUploadRootDir(), $this->{$_fi});

            if ($this->{'old' . $_file}) {
                @unlink($this->{'old' . $_file});
            }
            if ($this->{'oldcrop' . $_file}) {
                @unlink($this->{'oldcrop' . $_file});
            }

            unset($this->{$_file});
        }
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUploadedFiles() {
        if ($file = $this->getAbsolutePath('image')) {
            @unlink($file);
        }
        if ($file = $this->getAbsolutePath('image', true)) {
            @unlink($file);
        }
    }

    public function getAbsolutePath($_fil, $crop = false) {
        $file = null;

        switch ($_fil) {
            case 'image':
            default:
                $file = $this->getImage();
        }

        return !$file ? null : $this->getUploadRootDir() . '/' . ($crop ? 'crop-' : '') . $file;
    }

    public function getWebPath($_fil = 'image') {
        $file = null;

        switch ($_fil) {
            case 'image':
            default:
                $file = $this->getImage();
        }

        return !$file ? null : $this->getUploadDir() . '/' . $file;
    }

    public function getWebPathCrop($_fil = 'image') {
        if ($_fil == 'image') {
            if (file_exists($this->getUploadRootDir() . '/crop-' . $this->getImage())) {
                return $this->getUploadDir() . '/crop-' . $this->getImage();
            } else {
                return $this->getWebPath();
            }
        }

    }

    public function getUploadRootDir() {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__ . '/../../../../web/' . $this->getUploadDir();
    }

    protected function getUploadDir() {
        return 'uploads/pages/' . $this->getId();
    }


    /**
     * Set title
     *
     * @param string $title
     */
    public function setTitle($title) {
        $this->title = $title;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * Set slug
     *
     * @param string $slug
     */
    public function setSlug($slug) {
        $this->slug = $slug;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug() {
        return $this->slug;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set published
     *
     * @param boolean $published
     */
    public function setPublished($published) {
        $this->published = $published;
    }

    /**
     * Get published
     *
     * @return boolean
     */
    public function getPublished() {
        return $this->published;
    }


    /**
     * Set publishDate
     *
     * @param \DateTime $publishDate
     */
    public function setPublishDate($publishDate) {
        $this->publishDate = $publishDate;
    }

    /**
     * Get publishDate
     *
     * @return \DateTime
     */
    public function getPublishDate() {
        return $this->publishDate;
    }

    /**
     * Set keywords
     *
     * @param string $keywords
     */
    public function setKeywords($keywords) {
        $this->keywords = $keywords;
    }

    /**
     * Get keywords
     *
     * @return string
     */
    public function getKeywords() {
        return $this->keywords;
    }

    /**
     * Set description
     *
     * @param string $description
     */
    public function setDescription($description) {
        $this->description = $description;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Set content
     *
     * @param string $content
     */
    public function setContent($content) {
        $this->content = $content;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent() {
        return $this->content;
    }

    /**
     * Set automaticSeo
     *
     * @param boolean $automaticSeo
     */
    public function setAutomaticSeo($automaticSeo) {
        $this->automaticSeo = $automaticSeo;
    }

    /**
     * Get automaticSeo
     *
     * @return boolean
     */
    public function getAutomaticSeo() {
        return $this->automaticSeo;
    }

    /**
     * Set lang
     *
     * @param string $lang
     */
    public function setLang($lang) {
        $this->lang = $lang;
    }

    /**
     * Get lang
     *
     * @return string
     */
    public function getLang() {
        return $this->lang;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return News
     */
    public function setCreated($created) {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated() {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return News
     */
    public function setUpdated($updated) {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated() {
        return $this->updated;
    }

    /**
     * Set highlight_mainpage
     *
     * @param boolean $highlightMainpage
     * @return News
     */
    public function setHighlightMainpage($highlightMainpage) {
        $this->highlight_mainpage = $highlightMainpage;

        return $this;
    }

    /**
     * Get highlight_mainpage
     *
     * @return boolean
     */
    public function getHighlightMainpage() {
        return $this->highlight_mainpage;
    }

    /**
     * Set highlight_category
     *
     * @param boolean $highlightCategory
     * @return News
     */
    public function setHighlightCategory($highlightCategory) {
        $this->highlight_category = $highlightCategory;

        return $this;
    }

    /**
     * Get highlight_category
     *
     * @return boolean
     */
    public function getHighlightCategory() {
        return $this->highlight_category;
    }

    /**
     * Set view_count
     *
     * @param integer $viewCount
     * @return News
     */
    public function setViewCount($viewCount) {
        $this->view_count = $viewCount;

        return $this;
    }

    /**
     * Get view_count
     *
     * @return integer
     */
    public function getViewCount() {
        return $this->view_count;
    }

    /**
     * Set highlights_box_mainpage
     *
     * @param boolean $highlightsBoxMainpage
     * @return News
     */
    public function setHighlightsBoxMainpage($highlightsBoxMainpage) {
        $this->highlights_box_mainpage = $highlightsBoxMainpage;

        return $this;
    }

    /**
     * Get highlights_box_mainpage
     *
     * @return boolean
     */
    public function getHighlightsBoxMainpage() {
        return $this->highlights_box_mainpage;
    }

    /**
     * Set image
     *
     * @param string $image
     * @return Article
     */
    public function setImage($image) {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage() {
        return $this->image;
    }

    /**
     * Set promocja
     *
     * @param boolean $promocja
     * @return News
     */
    public function setPromocja($promocja) {
        $this->promocja = $promocja;

        return $this;
    }

    /**
     * Get promocja
     *
     * @return boolean
     */
    public function getPromocja() {
        return $this->promocja;
    }

    /**
     * Set category
     *
     * @param \Cms\ElmatBundle\Entity\CategoryNews $category
     * @return News
     */
    public function setCategory(CategoryNews $category = null) {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \Cms\ElmatBundle\Entity\CategoryNews
     */
    public function getCategory() {
        return $this->category;
    }

    /**
     * Set offer_group
     *
     * @param \Cms\ElmatBundle\Entity\OfferGroup $offerGroup
     * @return News
     */
    public function setOfferGroup(OfferGroup $offerGroup = null) {
        $this->offer_group = $offerGroup;

        return $this;
    }

    /**
     * Get offer_group
     *
     * @return \Cms\ElmatBundle\Entity\OfferGroup
     */
    public function getOfferGroup() {
        return $this->offer_group;
    }

    /**
     * Set galeria
     *
     * @param \Cms\ElmatBundle\Entity\Galeria $galeria
     * @return News
     */
    public function setGaleria(Galeria $galeria = null) {
        $this->galeria = $galeria;

        return $this;
    }

    /**
     * Get galeria
     *
     * @return \Cms\ElmatBundle\Entity\Galeria
     */
    public function getGaleria() {
        return $this->galeria;
    }

    /**
     * Add plik
     *
     * @param \Cms\ElmatBundle\Entity\Plik $plik
     * @return News
     */
    public function addPlikus(Plik $plik) {
        $plik->setNews($this);
        $this->pliki[] = $plik;

        return $this;
    }

    /**
     * Remove plik
     *
     * @param \Cms\ElmatBundle\Entity\Plik $plik
     */
    public function removePlikus(Plik $plik) {
        $this->pliki->removeElement($plik);
    }

    /**
     * Get pliki
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPliki() {
        return $this->pliki;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     * @return News
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime 
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }
}