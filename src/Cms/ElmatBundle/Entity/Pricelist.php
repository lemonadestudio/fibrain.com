<?php
namespace Cms\ElmatBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="Cms\ElmatBundle\Repository\PricelistRepository")
 * @ORM\Table(name="pricelist")
 * @ORM\HasLifecycleCallbacks()
 */
class Pricelist {
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @var string $title
     *
     * @ORM\Column(type="string", length=255);
     * @Assert\NotNull();
     * @Assert\Length(max = 255);
     */
    private $title;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $published = true;

    /**
     * @var string $description
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $content;

    /**
     * @var \DateTime $created
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created;

    /**
     * @var \DateTime $updated
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated;

    /** @Assert\File(maxSize="6000000") */
    public $_file_image;

    public $_delete_file_image = false;

    /** @Assert\File(maxSize="6000000") */
    public $_file_cennik;

    public $_delete_file_cennik = false;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $cennik;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $cennik_filename;

    public function __construct() {

        $this->published = true;

    }

    public function __toString() {

        return '' . $this->getTitle();

    }


    /**
     * @ORM\PrePersist();
     * @ORM\PreUpdate();
     */
    public function setCreatedValue() {

    }


    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function prepareUploadedNames() {


        foreach (array('image', 'cennik') as $_fi) {
            $_file = '_file_' . $_fi;


            if (null !== $this->{$_file}) {

                // do whatever you want to generate a unique name
                $this->{'old' . $_file} = $this->getAbsolutePath($_fi);
                $this->{$_fi} = $_fi . '-' . uniqid() . '.' . $this->{$_file}->guessExtension();
                $this->{$_fi . '_filename'} = $this->{$_file}->getClientOriginalName();

            } elseif (true == $this->{'_delete' . $_file}) {

                @unlink($this->getAbsolutePath($_fi));
                $this->{$_fi} = null;

            }

        }
        // 		die;
    }

    /**
     *
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function storeUploadedFiles() {

        foreach (array('image', 'cennik') as $_fi) {

            $_file = '_file_' . $_fi;

            if (null === $this->{$_file}) {
                continue;
            }

            $this->{$_file}->move($this->getUploadRootDir(), $this->{$_fi});

            if ($this->{'old' . $_file}) {
                @unlink($this->{'old' . $_file});
            }

            unset($this->{$_file});


        }

    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUploadedFiles() {
        if ($file = $this->getAbsolutePath('image')) {
            @unlink($file);
        }

        if ($file = $this->getAbsolutePath('cennik')) {
            @unlink($file);
        }


    }


    public function getAbsolutePath($_fil) {
        $file = null;
        switch ($_fil) {

            case 'cennik':
                $file = $this->getCennik();
                break;

            case 'image':
            default:
                $file = $this->getImage();
        }
        return !$file ? null : $this->getUploadRootDir() . '/' . $file;
    }

    public function getWebPath($_fil = 'image') {
        $file = null;
        switch ($_fil) {

            case 'cennik':
                $file = $this->getCennik();
                break;

            case 'image':
            default:
                $file = $this->getImage();
        }
        return !$file ? null : $this->getUploadDir() . '/' . $file;
    }

    protected function getUploadRootDir() {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__ . '/../../../../web/' . $this->getUploadDir();
    }

    protected function getUploadDir() {
        return 'uploads/cennik/' . $this->getId();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }


    /**
     * Set title
     *
     * @param string $title
     * @return Pricelist
     */
    public function setTitle($title) {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * Set published
     *
     * @param boolean $published
     * @return Pricelist
     */
    public function setPublished($published) {
        $this->published = $published;

        return $this;
    }

    /**
     * Get published
     *
     * @return boolean
     */
    public function getPublished() {
        return $this->published;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return Pricelist
     */
    public function setContent($content) {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent() {
        return $this->content;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Pricelist
     */
    public function setCreated($created) {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated() {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return Pricelist
     */
    public function setUpdated($updated) {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated() {
        return $this->updated;
    }

    /**
     * Set image
     *
     * @param string $image
     * @return Pricelist
     */
    public function setImage($image) {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage() {
        return $this->image;
    }

    /**
     * Set cennik
     *
     * @param string $cennik
     * @return Pricelist
     */
    public function setCennik($cennik) {
        $this->cennik = $cennik;

        return $this;
    }

    /**
     * Get cennik
     *
     * @return string
     */
    public function getCennik() {
        return $this->cennik;
    }

    /**
     * Set cennik_filename
     *
     * @param string $cennikFilename
     * @return Pricelist
     */
    public function setCennikFilename($cennikFilename) {
        $this->cennik_filename = $cennikFilename;

        return $this;
    }

    /**
     * Get cennik_filename
     *
     * @return string
     */
    public function getCennikFilename() {
        return $this->cennik_filename;
    }
}