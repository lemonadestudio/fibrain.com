<?php
namespace Cms\ElmatBundle\Entity;

use Doctrine\ORM\Mapping as Orm;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @Orm\Entity(repositoryClass="Cms\ElmatBundle\Repository\TrainingRepository")
 * @Orm\Table(name="training")
 * @ORM\HasLifecycleCallbacks();
 */
class Training {
    /**
     * @Orm\Id
     * @Orm\Column(type="integer")
     * @Orm\GeneratedValue
     */
    private $id;

    /**
     * @var string $title
     *
     * @ORM\Column(type="string", length=255);
     * @Assert\NotNull();
     * @Assert\Length(max = 255);
     */
    private $title;

    /**
     * @Gedmo\Slug(fields={"title"})
     * @ORM\Column(type="string", length=255, unique=true);
     */
    private $slug;

    /**
     * @ORM\Column(type="boolean")
     */
    private $published = true;

    /**
     *
     * @var \DateTime $publishDate ;
     * @ORM\Column(type="datetime", nullable=true);
     */
    private $publishDate;

    /**
     * @var string $keywords
     *
     * @ORM\Column(type="string", length=1024, nullable=true);
     * @Assert\Length(max = 1024);
     */
    private $keywords;

    /**
     * @var string $description
     *
     * @ORM\Column(type="string", length=2048, nullable=true);
     * @Assert\Length(max = 2048)
     */
    private $description;

    /**
     * @var string $content
     *
     * @ORM\Column(type="text", nullable=true )
     */
    private $content;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true );
     */
    private $automaticSeo;

    /**
     * @ORM\Column(type="string", length=2, nullable=false )
     */
    private $lang;

    /**
     * @ORM\ManyToOne(targetEntity="TrainingCategory", inversedBy="trainings")
     */
    protected $category;

    /**
     * @ORM\OneToMany(targetEntity="TrainingDate", mappedBy="training")
     */
    protected $training_dates;

    /** @Assert\File(maxSize="6000000") */
    public $_file_image;

    public $_delete_file_image = false;

    /**
     * @ORM\Column(name="image", type="string", length=255, nullable=true)
     */
    private $image;

    public function __construct() {
        $this->published = true;
        $this->automaticSeo = true;
        $this->publishDate = new \DateTime();
        $this->lang = "pl";
        $this->training_dates = new ArrayCollection();

    }

    public function __toString() {

        return '' . $this->title;

    }

    public function getRouteParametersArray() {
        // todo: reimplement
        return array();
    }

    public function getRoute() {
        return "cms_elmat_training_show";
    }


    /**
     * @ORM\PostPersist();
     *
     */
    function setPostCreatedValue() {

    }

    /**
     * @ORM\PrePersist();
     * @ORM\PreUpdate();
     */
    public function setCreatedValue() {
        if ($this->getAutomaticSeo()) {
            // description
            $description = strip_tags($this->getContent());
            $description = mb_substr(html_entity_decode($description), 0, 2048);

            // usunięcie ostatniego, niedokończonego zdania

            $description = preg_replace('@(.*)\..*@', '\1', $description);

            $this->setDescription($description);

            // keywords
            $keywords_arr = explode(' ', $this->getTitle() . ' ' . $this->getDescription());

            $keywords = array();
            if (is_array($keywords_arr)) {
                foreach ($keywords_arr as $kw) {
                    $kw = trim($kw);
                    $kw = preg_replace('@\.,;\'\"@', '', $kw);
                    if (strlen($kw) >= 4 && !in_array($kw, $keywords)) {
                        $keywords[] = $kw;
                    }
                    if (count($keywords) >= 10) {
                        break;
                    }
                }
            }

            $this->setKeywords(implode(', ', $keywords));
            $this->setAutomaticSeo(false);

        }
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set category
     *
     * @param \Cms\ElmatBundle\Entity\TrainingCategory $category
     */
    public function setCategory(TrainingCategory $category) {
        $this->category = $category;
    }

    /**
     * Get category
     *
     * @return \Cms\ElmatBundle\Entity\TrainingCategory
     */
    public function getCategory() {
        return $this->category;
    }


    /**
     * Set title
     *
     * @param string $title
     */
    public function setTitle($title) {
        $this->title = $title;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * Set slug
     *
     * @param string $slug
     */
    public function setSlug($slug) {
        $this->slug = $slug;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug() {
        return $this->slug;
    }

    /**
     * Set published
     *
     * @param boolean $published
     */
    public function setPublished($published) {
        $this->published = $published;
    }

    /**
     * Get published
     *
     * @return boolean
     */
    public function getPublished() {
        return $this->published;
    }

    /**
     * Set publishDate
     *
     * @param \DateTime $publishDate
     */
    public function setPublishDate($publishDate) {
        $this->publishDate = $publishDate;
    }

    /**
     * Get publishDate
     *
     * @return \DateTime
     */
    public function getPublishDate() {
        return $this->publishDate;
    }

    /**
     * Set keywords
     *
     * @param string $keywords
     */
    public function setKeywords($keywords) {
        $this->keywords = $keywords;
    }

    /**
     * Get keywords
     *
     * @return string
     */
    public function getKeywords() {
        return $this->keywords;
    }

    /**
     * Set description
     *
     * @param string $description
     */
    public function setDescription($description) {
        $this->description = $description;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Set content
     *
     * @param string $content
     */
    public function setContent($content) {
        $this->content = $content;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent() {
        return $this->content;
    }

    /**
     * Set automaticSeo
     *
     * @param boolean $automaticSeo
     */
    public function setAutomaticSeo($automaticSeo) {
        $this->automaticSeo = $automaticSeo;
    }

    /**
     * Get automaticSeo
     *
     * @return boolean
     */
    public function getAutomaticSeo() {
        return $this->automaticSeo;
    }

    /**
     * Set lang
     *
     * @param string $lang
     */
    public function setLang($lang) {
        $this->lang = $lang;
    }

    /**
     * Get lang
     *
     * @return string
     */
    public function getLang() {
        return $this->lang;
    }

    /**
     * Set image
     *
     * @param string $image
     * @return Training
     */
    public function setImage($image) {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage() {
        return $this->image;
    }

    /**
     * Add training_dates
     *
     * @param \Cms\ElmatBundle\Entity\TrainingDate $trainingDates
     */
    public function addTrainingDate(TrainingDate $trainingDates) {
        $this->training_dates[] = $trainingDates;
    }

    /**
     * Remove training_dates
     *
     * @param \Cms\ElmatBundle\Entity\TrainingDate $trainingDates
     */
    public function removeTrainingDate(TrainingDate $trainingDates) {
        $this->training_dates->removeElement($trainingDates);
    }

    /**
     * Get training_dates
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTrainingDates() {
        return $this->training_dates;
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function prepareUploadedNames() {
        foreach (array('image') as $_fi) {
            $_file = '_file_' . $_fi;
            if (null !== $this->{$_file}) {
                $this->{'old' . $_file} = $this->getAbsolutePath($_fi);
                $this->{$_fi} = $_fi . '-' . uniqid() . '.' . $this->{$_file}->guessExtension();
            } elseif (true == $this->{'_delete' . $_file}) {
                @unlink($this->getAbsolutePath($_fi));
                $this->{$_fi} = null;
            }
        }
    }

    /**
     *
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function storeUploadedFiles() {

        foreach (array('image') as $_fi) {

            $_file = '_file_' . $_fi;
            if (null === $this->{$_file}) {
                continue;
            }
            $this->{$_file}->move($this->getUploadRootDir(), $this->{$_fi});
            if ($this->{'old' . $_file}) {
                @unlink($this->{'old' . $_file});
            }
            unset($this->{$_file});
        }

    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUploadedFiles() {
        if ($file = $this->getAbsolutePath('image')) {
            @unlink($file);
        }

    }

    public function getAbsolutePath($_fil) {
        $file = null;
        switch ($_fil) {

            case 'image':
            default:
                $file = $this->getImage();
        }
        return !$file ? null : $this->getUploadRootDir() . '/' . $file;
    }

    public function getWebPath($_fil = 'image') {
        $file = null;
        switch ($_fil) {

            case 'image':
            default:
                $file = $this->getImage();
        }
        return !$file ? null : $this->getUploadDir() . '/' . $file;
    }

    protected function getUploadRootDir() {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__ . '/../../../../web/' . $this->getUploadDir();
    }

    protected function getUploadDir() {
        return 'uploads/szkolenie/' . $this->getId();
    }
}