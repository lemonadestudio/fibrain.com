<?php

namespace Cms\ElmatBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping\UniqueConstraint;
use Sonata\UserBundle\Entity\BaseUser as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;


/**
 * @ORM\Entity(repositoryClass="Cms\ElmatBundle\Repository\UserRepository")
 * @ORM\Table(name="user")
 * @ORM\HasLifecycleCallbacks()
 *
 * @UniqueEntity("username")
 * @UniqueEntity("email")
 */
class User extends BaseUser {

    public static $wojewodztwa = array(
        'dolnośląskie' => 'dolnośląskie',
        'kujawsko-pomorskie' => 'kujawsko-pomorskie',
        'lubelskie' => 'lubelskie', 'lubuskie' => 'lubuskie',
        'łódzkie' => 'łódzkie', 'małopolskie' => 'małopolskie',
        'mazowieckie' => 'mazowieckie', 'opolskie' => 'opolskie',
        'podkarpackie' => 'podkarpackie', 'podlaskie' => 'podlaskie',
        'pomorskie' => 'pomorskie', 'śląskie' => 'śląskie',
        'świętokrzyskie' => 'świętokrzyskie',
        'warmińsko-mazurskie' => 'warmińsko-mazurskie',
        'wielkopolskie' => 'wielkopolskie',
        'zachodniopomorskie' => 'zachodniopomorskie',
    );

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    protected $id;

    /** @ORM\Column(type="string", length=255, nullable=true) @Assert\NotBlank */
    protected $firma;

    /** @ORM\Column(type="string", length=255, nullable=true) @Assert\NotBlank */
    protected $ulica;

    /** @ORM\Column(type="string", length=255, nullable=true) @Assert\NotBlank */
    protected $kod_pocztowy;

    /** @ORM\Column(type="string", length=255, nullable=true) @Assert\NotBlank */
    protected $miasto;

    /** @ORM\Column(type="string", length=255, nullable=true) @Assert\NotBlank */
    protected $wojewodztwo;

    /** @ORM\Column(type="string", length=255, nullable=true) @Assert\NotBlank */
    protected $nip;

    /** @ORM\Column(type="string", length=255, nullable=true) @Assert\NotBlank */
    protected $telefon;

    /** @ORM\Column(type="string", length=255, nullable=true) */
    protected $fax;

    /** @ORM\Column(type="boolean", nullable=true) */
    protected $firma_integrator;

    /** @ORM\Column(type="boolean", nullable=true) */
    protected $firma_instalator;

    /** @ORM\Column(type="boolean", nullable=true) */
    protected $firma_handlowa;

    /** @ORM\Column(type="boolean", nullable=true) */
    protected $firma_projektant;

    /** @ORM\Column(type="boolean", nullable=true) */
    protected $firma_tv_kablowa;

    /** @ORM\Column(type="boolean", nullable=true) */
    protected $firma_isp;

    /** @ORM\Column(type="string", length=255, nullable=true) */
    protected $firma_inny;

    /** @ORM\Column(type="boolean", nullable=true) */
    protected $oferta_elektroenergetyka;

    /** @ORM\Column(type="boolean", nullable=true) */
    protected $oferta_wireless;

    /** @ORM\Column(type="boolean", nullable=true) */
    protected $oferta_urzadzenia_aktywne;

    /** @ORM\Column(type="boolean", nullable=true) */
    protected $oferta_swiatlowody;

    /** @ORM\Column(type="boolean", nullable=true) */
    protected $oferta_sieci;

    /** @ORM\Column(type="boolean", nullable=true) */
    protected $oferta_szafy;

    /** @ORM\Column(type="boolean", nullable=true) */
    protected $oferta_miejskie_sieci;

    /** @ORM\Column(type="boolean", nullable=true) */
    protected $info_staly_klient;

    /** @ORM\Column(type="boolean", nullable=true) */
    protected $info_wyszukiwarka;

    /** @ORM\Column(type="boolean", nullable=true) */
    protected $info_reklamy;

    /** @ORM\Column(type="boolean", nullable=true) */
    protected $info_prasa_codzienna;

    /** @ORM\Column(type="boolean", nullable=true) */
    protected $info_prasa_specjalistyczna;

    /** @ORM\Column(type="boolean", nullable=true) */
    protected $info_znajomi;

    /** @ORM\Column(type="boolean", nullable=true) */
    protected $info_subskrypcja;

    /** @ORM\Column(type="boolean", nullable=true) */
    protected $firma_instalator_sieci_niskopradowe;

    /** @ORM\Column(type="boolean", nullable=true) */
    protected $firma_samorzad_terytorialny;

    /** @ORM\Column(type="boolean", nullable=true) */
    protected $firma_administrator_sieci;

    /** @ORM\Column(type="boolean", nullable=true) */
    protected $oferta_akcesoria_swiatlowodowe;

    /** @ORM\Column(type="boolean", nullable=true) */
    protected $oferta_mikrokanalizacja;

    /** @ORM\Column(type="boolean", nullable=true) */
    protected $oferta_kable_napowietrzne_airtrack;

    /** @ORM\Column(type="boolean", nullable=true) */
    protected $oferta_technologia_fttx;

    /** @ORM\Column(type="boolean", nullable=true) */
    protected $oferta_technologia_gpon;

    /** @ORM\Column(type="boolean", nullable=true) */
    protected $oferta_sieci_miejskie;

    /** @ORM\Column(type="boolean", nullable=true) */
    protected $oferta_osprzet_instalacyjny;

    /** @ORM\Column(type="boolean", nullable=true) */
    protected $oferta_osprzet_pomiarowy;

    /** @ORM\Column(type="boolean", nullable=true) */
    protected $oferta_okablowanie_strukturalne;

    /** @ORM\Column(type="boolean", nullable=true) */
    protected $oferta_inne;

    /** @ORM\Column(type="boolean", nullable=true) */
    protected $info_inne;


    /**
     * @ORM\Column(type="string", length=1024, nullable=true)
     * @Assert\MaxLength(1000)
     **/
    private $password_reset_token;

    /**
     * @ORM\OneToMany(targetEntity="TrainingRegistration", mappedBy="user")
     * @Assert\NotBlank
     */
    protected $training_registrations;

    public function __construct() {
        parent::__construct();
        $this->training_registrations = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer $id
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set firma
     *
     * @param string $firma
     * @return User
     */
    public function setFirma($firma) {
        $this->firma = $firma;

        return $this;
    }

    /**
     * Get firma
     *
     * @return string
     */
    public function getFirma() {
        return $this->firma;
    }

    /**
     * Set ulica
     *
     * @param string $ulica
     * @return User
     */
    public function setUlica($ulica) {
        $this->ulica = $ulica;

        return $this;
    }

    /**
     * Get ulica
     *
     * @return string
     */
    public function getUlica() {
        return $this->ulica;
    }

    /**
     * Set kod_pocztowy
     *
     * @param string $kodPocztowy
     * @return User
     */
    public function setKodPocztowy($kodPocztowy) {
        $this->kod_pocztowy = $kodPocztowy;

        return $this;
    }

    /**
     * Get kod_pocztowy
     *
     * @return string
     */
    public function getKodPocztowy() {
        return $this->kod_pocztowy;
    }

    /**
     * Set miasto
     *
     * @param string $miasto
     * @return User
     */
    public function setMiasto($miasto) {
        $this->miasto = $miasto;

        return $this;
    }

    /**
     * Get miasto
     *
     * @return string
     */
    public function getMiasto() {
        return $this->miasto;
    }

    /**
     * Set wojewodztwo
     *
     * @param string $wojewodztwo
     * @return User
     */
    public function setWojewodztwo($wojewodztwo) {
        $this->wojewodztwo = $wojewodztwo;

        return $this;
    }

    /**
     * Get wojewodztwo
     *
     * @return string
     */
    public function getWojewodztwo() {
        return $this->wojewodztwo;
    }

    /**
     * Set nip
     *
     * @param string $nip
     * @return User
     */
    public function setNip($nip) {
        $this->nip = $nip;

        return $this;
    }

    /**
     * Get nip
     *
     * @return string
     */
    public function getNip() {
        return $this->nip;
    }

    /**
     * Set telefon
     *
     * @param string $telefon
     * @return User
     */
    public function setTelefon($telefon) {
        $this->telefon = $telefon;

        return $this;
    }

    /**
     * Get telefon
     *
     * @return string
     */
    public function getTelefon() {
        return $this->telefon;
    }

    /**
     * Set fax
     *
     * @param string $fax
     * @return User
     */
    public function setFax($fax) {
        $this->fax = $fax;

        return $this;
    }

    /**
     * Get fax
     *
     * @return string
     */
    public function getFax() {
        return $this->fax;
    }


    /**
     * Set firma_integrator
     *
     * @param boolean $firmaIntegrator
     * @return User
     */
    public function setFirmaIntegrator($firmaIntegrator) {
        $this->firma_integrator = $firmaIntegrator;

        return $this;
    }

    /**
     * Get firma_integrator
     *
     * @return boolean
     */
    public function getFirmaIntegrator() {
        return $this->firma_integrator;
    }

    /**
     * Set firma_instalator
     *
     * @param boolean $firmaInstalator
     * @return User
     */
    public function setFirmaInstalator($firmaInstalator) {
        $this->firma_instalator = $firmaInstalator;

        return $this;
    }

    /**
     * Get firma_instalator
     *
     * @return boolean
     */
    public function getFirmaInstalator() {
        return $this->firma_instalator;
    }

    /**
     * Set firma_handlowa
     *
     * @param boolean $firmaHandlowa
     * @return User
     */
    public function setFirmaHandlowa($firmaHandlowa) {
        $this->firma_handlowa = $firmaHandlowa;

        return $this;
    }

    /**
     * Get firma_handlowa
     *
     * @return boolean
     */
    public function getFirmaHandlowa() {
        return $this->firma_handlowa;
    }

    /**
     * Set firma_projektant
     *
     * @param boolean $firmaProjektant
     * @return User
     */
    public function setFirmaProjektant($firmaProjektant) {
        $this->firma_projektant = $firmaProjektant;

        return $this;
    }

    /**
     * Get firma_projektant
     *
     * @return boolean
     */
    public function getFirmaProjektant() {
        return $this->firma_projektant;
    }

    /**
     * Set firma_tv_kablowa
     *
     * @param boolean $firmaTvKablowa
     * @return User
     */
    public function setFirmaTvKablowa($firmaTvKablowa) {
        $this->firma_tv_kablowa = $firmaTvKablowa;

        return $this;
    }

    /**
     * Get firma_tv_kablowa
     *
     * @return boolean
     */
    public function getFirmaTvKablowa() {
        return $this->firma_tv_kablowa;
    }

    /**
     * Set firma_isp
     *
     * @param boolean $firmaIsp
     * @return User
     */
    public function setFirmaIsp($firmaIsp) {
        $this->firma_isp = $firmaIsp;

        return $this;
    }

    /**
     * Get firma_isp
     *
     * @return boolean
     */
    public function getFirmaIsp() {
        return $this->firma_isp;
    }

    /**
     * Set firma_inny
     *
     * @param string $firmaInny
     * @return User
     */
    public function setFirmaInny($firmaInny) {
        $this->firma_inny = $firmaInny;

        return $this;
    }

    /**
     * Get firma_inny
     *
     * @return string
     */
    public function getFirmaInny() {
        return $this->firma_inny;
    }

    /**
     * Set oferta_elektroenergetyka
     *
     * @param boolean $ofertaElektroenergetyka
     * @return User
     */
    public function setOfertaElektroenergetyka($ofertaElektroenergetyka) {
        $this->oferta_elektroenergetyka = $ofertaElektroenergetyka;

        return $this;
    }

    /**
     * Get oferta_elektroenergetyka
     *
     * @return boolean
     */
    public function getOfertaElektroenergetyka() {
        return $this->oferta_elektroenergetyka;
    }

    /**
     * Set oferta_wireless
     *
     * @param boolean $ofertaWireless
     * @return User
     */
    public function setOfertaWireless($ofertaWireless) {
        $this->oferta_wireless = $ofertaWireless;

        return $this;
    }

    /**
     * Get oferta_wireless
     *
     * @return boolean
     */
    public function getOfertaWireless() {
        return $this->oferta_wireless;
    }

    /**
     * Set oferta_urzadzenia_aktywne
     *
     * @param boolean $ofertaUrzadzeniaAktywne
     * @return User
     */
    public function setOfertaUrzadzeniaAktywne($ofertaUrzadzeniaAktywne) {
        $this->oferta_urzadzenia_aktywne = $ofertaUrzadzeniaAktywne;

        return $this;
    }

    /**
     * Get oferta_urzadzenia_aktywne
     *
     * @return boolean
     */
    public function getOfertaUrzadzeniaAktywne() {
        return $this->oferta_urzadzenia_aktywne;
    }

    /**
     * Set oferta_swiatlowody
     *
     * @param boolean $ofertaSwiatlowody
     * @return User
     */
    public function setOfertaSwiatlowody($ofertaSwiatlowody) {
        $this->oferta_swiatlowody = $ofertaSwiatlowody;

        return $this;
    }

    /**
     * Get oferta_swiatlowody
     *
     * @return boolean
     */
    public function getOfertaSwiatlowody() {
        return $this->oferta_swiatlowody;
    }

    /**
     * Set oferta_sieci
     *
     * @param boolean $ofertaSieci
     * @return User
     */
    public function setOfertaSieci($ofertaSieci) {
        $this->oferta_sieci = $ofertaSieci;

        return $this;
    }

    /**
     * Get oferta_sieci
     *
     * @return boolean
     */
    public function getOfertaSieci() {
        return $this->oferta_sieci;
    }

    /**
     * Set oferta_szafy
     *
     * @param boolean $ofertaSzafy
     * @return User
     */
    public function setOfertaSzafy($ofertaSzafy) {
        $this->oferta_szafy = $ofertaSzafy;

        return $this;
    }

    /**
     * Get oferta_szafy
     *
     * @return boolean
     */
    public function getOfertaSzafy() {
        return $this->oferta_szafy;
    }

    /**
     * Set oferta_miejskie_sieci
     *
     * @param boolean $ofertaMiejskieSieci
     * @return User
     */
    public function setOfertaMiejskieSieci($ofertaMiejskieSieci) {
        $this->oferta_miejskie_sieci = $ofertaMiejskieSieci;

        return $this;
    }

    /**
     * Get oferta_miejskie_sieci
     *
     * @return boolean
     */
    public function getOfertaMiejskieSieci() {
        return $this->oferta_miejskie_sieci;
    }

    /**
     * Set info_staly_klient
     *
     * @param boolean $infoStalyKlient
     * @return User
     */
    public function setInfoStalyKlient($infoStalyKlient) {
        $this->info_staly_klient = $infoStalyKlient;

        return $this;
    }

    /**
     * Get info_staly_klient
     *
     * @return boolean
     */
    public function getInfoStalyKlient() {
        return $this->info_staly_klient;
    }

    /**
     * Set info_wyszukiwarka
     *
     * @param boolean $infoWyszukiwarka
     * @return User
     */
    public function setInfoWyszukiwarka($infoWyszukiwarka) {
        $this->info_wyszukiwarka = $infoWyszukiwarka;

        return $this;
    }

    /**
     * Get info_wyszukiwarka
     *
     * @return boolean
     */
    public function getInfoWyszukiwarka() {
        return $this->info_wyszukiwarka;
    }

    /**
     * Set info_reklamy
     *
     * @param boolean $infoReklamy
     * @return User
     */
    public function setInfoReklamy($infoReklamy) {
        $this->info_reklamy = $infoReklamy;

        return $this;
    }

    /**
     * Get info_reklamy
     *
     * @return boolean
     */
    public function getInfoReklamy() {
        return $this->info_reklamy;
    }

    /**
     * Set info_prasa_codzienna
     *
     * @param boolean $infoPrasaCodzienna
     * @return User
     */
    public function setInfoPrasaCodzienna($infoPrasaCodzienna) {
        $this->info_prasa_codzienna = $infoPrasaCodzienna;

        return $this;
    }

    /**
     * Get info_prasa_codzienna
     *
     * @return boolean
     */
    public function getInfoPrasaCodzienna() {
        return $this->info_prasa_codzienna;
    }

    /**
     * Set info_prasa_specjalistyczna
     *
     * @param boolean $infoPrasaSpecjalistyczna
     * @return User
     */
    public function setInfoPrasaSpecjalistyczna($infoPrasaSpecjalistyczna) {
        $this->info_prasa_specjalistyczna = $infoPrasaSpecjalistyczna;

        return $this;
    }

    /**
     * Get info_prasa_specjalistyczna
     *
     * @return boolean
     */
    public function getInfoPrasaSpecjalistyczna() {
        return $this->info_prasa_specjalistyczna;
    }

    /**
     * Set info_znajomi
     *
     * @param boolean $infoZnajomi
     * @return User
     */
    public function setInfoZnajomi($infoZnajomi) {
        $this->info_znajomi = $infoZnajomi;

        return $this;
    }

    /**
     * Get info_znajomi
     *
     * @return boolean
     */
    public function getInfoZnajomi() {
        return $this->info_znajomi;
    }

    /**
     * Set info_subskrypcja
     *
     * @param boolean $infoSubskrypcja
     * @return User
     */
    public function setInfoSubskrypcja($infoSubskrypcja) {
        $this->info_subskrypcja = $infoSubskrypcja;

        return $this;
    }

    /**
     * Get info_subskrypcja
     *
     * @return boolean
     */
    public function getInfoSubskrypcja() {
        return $this->info_subskrypcja;
    }


    /**
     * Set password_reset_token
     *
     * @param string $passwordResetToken
     * @return User
     */
    public function setPasswordResetToken($passwordResetToken) {
        $this->password_reset_token = $passwordResetToken;

        return $this;
    }

    /**
     * Get password_reset_token
     *
     * @return string
     */
    public function getPasswordResetToken() {
        return $this->password_reset_token;
    }


    /**
     * Add training_registrations
     *
     * @param \Cms\ElmatBundle\Entity\TrainingRegistration $trainingRegistrations
     * @return TrainingDate
     */
    public function addTrainingRegistration(TrainingRegistration $trainingRegistrations) {
        $this->training_registrations[] = $trainingRegistrations;

        return $this;
    }

    /**
     * Remove training_registrations
     *
     * @param \Cms\ElmatBundle\Entity\TrainingRegistration $trainingRegistrations
     */
    public function removeTrainingRegistration(TrainingRegistration $trainingRegistrations) {
        $this->training_registrations->removeElement($trainingRegistrations);
    }

    /**
     * Get training_registrations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTrainingRegistrations() {
        return $this->training_registrations;
    }


    public function setFirmaInstalatorSieciNiskopradowe($firmaInstalator) {
        $this->firma_instalator_sieci_niskopradowe = $firmaInstalator;

        return $this;
    }


    public function getFirmaInstalatorSieciNiskopradowe() {
        return $this->firma_instalator_sieci_niskopradowe;
    }


    public function getFirmaSamorzadTerytorialny() {
        return $this->firma_samorzad_terytorialny;
    }

    public function getFirmaAdministratorSieci() {
        return $this->firma_administrator_sieci;
    }

    public function getOfertaAkcesoriaSwiatlowodowe() {
        return $this->oferta_akcesoria_swiatlowodowe;
    }

    public function getOfertaMikrokanalizacja() {
        return $this->oferta_mikrokanalizacja;
    }

    public function getOfertaKableNapowietrzneAirtrack() {
        return $this->oferta_kable_napowietrzne_airtrack;
    }

    public function getOfertaTechnologiaFttx() {
        return $this->oferta_technologia_fttx;
    }

    public function getOfertaTechnologiaGpon() {
        return $this->oferta_technologia_gpon;
    }

    public function getOfertaSieciMiejskie() {
        return $this->oferta_sieci_miejskie;
    }

    public function getOfertaOsprzetInstalacyjny() {
        return $this->oferta_osprzet_instalacyjny;
    }

    public function getOfertaOsprzetPomiarowy() {
        return $this->oferta_osprzet_pomiarowy;
    }

    public function getOfertaOkablowanieStrukturalne() {
        return $this->oferta_okablowanie_strukturalne;
    }

    public function getOfertaInne() {
        return $this->oferta_inne;
    }

    public function getInfoInne() {
        return $this->info_inne;
    }

    public function setFirmaSamorzadTerytorialny($firma_samorzad_terytorialny) {
        $this->firma_samorzad_terytorialny = $firma_samorzad_terytorialny;
        return $this;
    }

    public function setFirmaAdministratorSieci($firma_administrator_sieci) {
        $this->firma_administrator_sieci = $firma_administrator_sieci;
        return $this;
    }


    public function setOfertaAkcesoriaSwiatlowodowe($oferta_akcesoria_swiatlowodowe) {
        $this->oferta_akcesoria_swiatlowodowe = $oferta_akcesoria_swiatlowodowe;
        return $this;
    }

    public function setOfertaMikrokanalizacja($oferta_mikrokanalizacja) {
        $this->oferta_mikrokanalizacja = $oferta_mikrokanalizacja;
        return $this;
    }

    public function setOfertaKableNapowietrzneAirtrack($oferta_kable_napowietrzne_airtrack) {
        $this->oferta_kable_napowietrzne_airtrack = $oferta_kable_napowietrzne_airtrack;
        return $this;
    }

    public function setOfertaTechnologiaFttx($oferta_technologia_fttx) {
        $this->oferta_technologia_fttx = $oferta_technologia_fttx;
        return $this;
    }

    public function setOfertaTechnologiaGpon($oferta_technologia_gpon) {
        $this->oferta_technologia_gpon = $oferta_technologia_gpon;
        return $this;
    }


    public function setOfertaSieciMiejskie($oferta_sieci_miejskie) {
        $this->oferta_sieci_miejskie = $oferta_sieci_miejskie;
        return $this;
    }

    public function setOfertaOsprzetInstalacyjny($oferta_osprzet_instalacyjny) {
        $this->oferta_osprzet_instalacyjny = $oferta_osprzet_instalacyjny;
        return $this;
    }

    public function setOfertaOsprzetPomiarowy($oferta_osprzet_pomiarowy) {
        $this->oferta_osprzet_pomiarowy = $oferta_osprzet_pomiarowy;
        return $this;
    }

    public function setOfertaOkablowanieStrukturalne($oferta_okablowanie_strukturalne) {
        $this->oferta_okablowanie_strukturalne = $oferta_okablowanie_strukturalne;
        return $this;
    }

    public function setOfertaInne($oferta_inne) {
        $this->oferta_inne = $oferta_inne;
        return $this;
    }

    public function setInfoInne($info_inne) {
        $this->info_inne = $info_inne;
        return $this;
    }

    public function set($key, $val) {
        if (property_exists($this, $key)) {
            $method_name = 'set' . str_replace(' ', '', ucwords(str_replace('_', ' ', $key)));
            if (method_exists($this, $method_name)) {
                $this->{$method_name}($val);
            }
        }
    }


}