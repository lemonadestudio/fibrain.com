<?php

namespace Cms\ElmatBundle\Entity;

use Cms\ElmatBundle\Model\NewsletterMessageRecipientStatus;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 *
 * @ORM\Table(name="newsletter_message_recipient")
 * @ORM\Entity(repositoryClass="Cms\ElmatBundle\Repository\NewsletterMessageRecipientRepository")
 * @ORM\HasLifecycleCallbacks();
 */
class NewsletterMessageRecipient {
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $sentDate;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private $status;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @ORM\ManyToOne(targetEntity="NewsletterMessage", inversedBy="recipients")
     */
    private $newsletterMessage;

    /**
     * @ORM\ManyToOne(targetEntity="NewsletterSubscriber", inversedBy="messageRecipients")
     */
    private $newsletterSubscriber;

    public function __construct() {
        $this->createdAt = new \DateTime();
    }

    public function getStatusDescription() {
        switch ($this->status) {
            case NewsletterMessageRecipientStatus::SENT:
                return 'wysłano';
            case NewsletterMessageRecipientStatus::READY_TO_SEND:
                return 'oczekuje na wysłanie';
            case NewsletterMessageRecipientStatus::INVALID_EMAIL:
                return 'Nieprawidłowy adres email subskrybenta. Wysyłka pominięta.';
            case NewsletterMessageRecipientStatus::NO_ACCOUNT:
                return 'Brak konta w systemie powiązanego z subskrybentem. Wysyłka pominięta.';
            case NewsletterMessageRecipientStatus::MAILER_ERROR:
                return 'Błąd podczas wysyłania maila. Oczekuje na ponowną wysyłkę.';
            case NewsletterMessageRecipientStatus::MESSAGE_COMPOSE_ERROR:
                return 'Błąd podczas tworzenia wiadomości. Oczekuje na ponowną wysyłkę.';
        }
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set sentDate
     *
     * @param \DateTime $sentDate
     * @return NewsletterMessageRecipient
     */
    public function setSentDate($sentDate) {
        $this->sentDate = $sentDate;

        return $this;
    }

    /**
     * Get sentDate
     *
     * @return \DateTime
     */
    public function getSentDate() {
        return $this->sentDate;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return NewsletterMessageRecipient
     */
    public function setStatus($status) {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus() {
        return $this->status;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return NewsletterMessageRecipient
     */
    public function setCreatedAt($createdAt) {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt() {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return NewsletterMessageRecipient
     */
    public function setUpdatedAt($updatedAt) {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt() {
        return $this->updatedAt;
    }

    /**
     * Set newsletterMessage
     *
     * @param \Cms\ElmatBundle\Entity\NewsletterMessage $newsletterMessage
     * @return NewsletterMessageRecipient
     */
    public function setNewsletterMessage(NewsletterMessage $newsletterMessage = null) {
        $this->newsletterMessage = $newsletterMessage;

        return $this;
    }

    /**
     * Get newsletterMessage
     *
     * @return \Cms\ElmatBundle\Entity\NewsletterMessage
     */
    public function getNewsletterMessage() {
        return $this->newsletterMessage;
    }

    /**
     * Set newsletterSubscriber
     *
     * @param \Cms\ElmatBundle\Entity\NewsletterSubscriber $newsletterSubscriber
     * @return NewsletterMessageRecipient
     */
    public function setNewsletterSubscriber(NewsletterSubscriber $newsletterSubscriber = null) {
        $this->newsletterSubscriber = $newsletterSubscriber;

        return $this;
    }

    /**
     * Get newsletterSubscriber
     *
     * @return \Cms\ElmatBundle\Entity\NewsletterSubscriber
     */
    public function getNewsletterSubscriber() {
        return $this->newsletterSubscriber;
    }
}