<?php

namespace Cms\ElmatBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as Orm;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @Orm\Entity(repositoryClass="Cms\ElmatBundle\Repository\TrainingDateRepository")
 * @Orm\Table(name="training_date")
 * @ORM\HasLifecycleCallbacks();
 */
class TrainingDate {
    /**
     * @Orm\Id
     * @Orm\Column(type="integer")
     * @Orm\GeneratedValue
     */
    private $id;

    /**
     * @var string $title
     *
     * @ORM\Column(type="string", length=255);
     * @Assert\MaxLength(255);
     * @Assert\NotBlank
     */
    private $price;

    /**
     * @ORM\Column(type="date", nullable=true);
     * @Assert\NotBlank
     */
    private $startDate;

    /**
     * @ORM\Column(type="date", nullable=true);
     * @Assert\NotBlank
     */
    private $endDate;

    /**
     * @ORM\Column(type="boolean")
     */
    private $published = true;

    /**
     * @var \DateTime $publishDate;
     *
     * @ORM\Column(type="datetime", nullable=true);
     */
    private $publishDate;

    /**
     * @ORM\Column(type="string", length=2, nullable=false)
     * @Assert\NotNull
     * @Assert\NotBlank
     */
    private $lang;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="City",
     *     inversedBy="training_dates"
     * )
     * @Assert\NotBlank
     */
    protected $city;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Training",
     *     inversedBy="training_dates"
     * )
     * @Assert\NotBlank
     */
    protected $training;

    /**
     * @ORM\OneToMany(
     *     targetEntity="TrainingRegistration",
     *     mappedBy="training_date"
     * )
     * @Assert\NotBlank
     */
    protected $training_registrations;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $brak_miejsc = false;

    public function __construct() {
        $this->published = true;
        $this->automaticSeo = true;
        $this->brak_miejsc = false;
        $this->publishDate = new \DateTime();
        $this->lang = "pl";

        $this->training_participants = new ArrayCollection();
        $this->training_registrations = new ArrayCollection();
    }

    public function __toString() {
        return '';
    }

    public function getRouteParametersArray() {
        $arr = array();

        return $arr;
    }

    public function getRoute() {
    }

    /**
     * @ORM\PrePersist();
     * @ORM\PreUpdate();
     */
    public function setCreatedValue() {
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set price
     *
     * @param string $price
     */
    public function setPrice($price) {
        $this->price = $price;
    }

    /**
     * Get price
     *
     * @return string
     */
    public function getPrice() {
        return $this->price;
    }

    /**
     * Set startDate
     *
     * @param |DateTime $startDate
     */
    public function setStartDate($startDate) {
        $this->startDate = $startDate;
    }

    /**
     * Get startDate
     *
     * @return |DateTime
     */
    public function getStartDate() {
        return $this->startDate;
    }

    /**
     * Set endDate
     *
     * @param |DateTime $endDate
     */
    public function setEndDate($endDate) {
        $this->endDate = $endDate;
    }

    /**
     * Get endDate
     *
     * @return |DateTime
     */
    public function getEndDate() {
        return $this->endDate;
    }

    /**
     * Set published
     *
     * @param boolean $published
     */
    public function setPublished($published) {
        $this->published = $published;
    }

    /**
     * Get published
     *
     * @return boolean
     */
    public function getPublished() {
        return $this->published;
    }

    /**
     * Set publishDate
     *
     * @param |DateTime $publishDate
     */
    public function setPublishDate($publishDate) {
        $this->publishDate = $publishDate;
    }

    /**
     * Get publishDate
     *
     * @return |DateTime
     */
    public function getPublishDate() {
        return $this->publishDate;
    }

    /**
     * Set lang
     *
     * @param string $lang
     */
    public function setLang($lang) {
        $this->lang = $lang;
    }

    /**
     * Get lang
     *
     * @return string
     */
    public function getLang() {
        return $this->lang;
    }

    /**
     * Set city
     *
     * @param \Cms\ElmatBundle\Entity\City $city
     */
    public function setCity(City $city) {
        $this->city = $city;
    }

    /**
     * Get city
     *
     * @return \Cms\ElmatBundle\Entity\City
     */
    public function getCity() {
        return $this->city;
    }

    /**
     * Set training
     *
     * @param \Cms\ElmatBundle\Entity\Training $training
     */
    public function setTraining(Training $training) {
        $this->training = $training;
    }

    /**
     * Get training
     *
     * @return \Cms\ElmatBundle\Entity\Training
     */
    public function getTraining() {
        return $this->training;
    }

    /**
     * Add training_registrations
     *
     * @param \Cms\ElmatBundle\Entity\TrainingRegistration $trainingRegistrations
     * @return TrainingDate
     */
    public function addTrainingRegistration(TrainingRegistration $trainingRegistrations) {
        $this->training_registrations[] = $trainingRegistrations;

        return $this;
    }

    /**
     * Remove training_registrations
     *
     * @param \Cms\ElmatBundle\Entity\TrainingRegistration $trainingRegistrations
     */
    public function removeTrainingRegistration(TrainingRegistration $trainingRegistrations) {
        $this->training_registrations->removeElement($trainingRegistrations);
    }

    /**
     * Get training_registrations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTrainingRegistrations() {
        return $this->training_registrations;
    }

    public function getBrakMiejsc() {
        return $this->brak_miejsc;
    }

    public function setBrakMiejsc($brak_miejsc) {
        $this->brak_miejsc = $brak_miejsc;
        return $this;
    }

    public function getSaMiejsca() {
        return !$this->getBrakMiejsc();
    }


}