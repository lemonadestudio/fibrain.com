<?php

namespace Cms\ElmatBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="Cms\ElmatBundle\Repository\SubscriptionRepository")
 * @ORM\Table(name="subscription")
 * @ORM\HasLifecycleCallbacks()
 */
class Subscription {
    const STATUS_NIEAKTYWNY = 0;
    const STATUS_AKTYWNY = 1;

    /**
     * @Orm\Id
     * @Orm\Column(type="integer")
     * @Orm\GeneratedValue
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(max=255)
     * @Assert\Email()
     * @Assert\NotBlank()
     */
    protected $email;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(max=255)
     */
    protected $confirmationToken;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $status;


    /**
     * @var \DateTime $created
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $created;

    /**
     * @var \DateTime $updated
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $updated;


    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function generateToken() {

        if (!$this->getConfirmationToken()) {

            $token = sha1(rand(1, 99999999)
                . time()
                . $this->getEmail()
                . time()
                . md5(time()
                    . $this->getEmail()
                    . rand(1, 99999999))
            );
            $this->setConfirmationToken($token);

        }

    }

    public function __construct() {
        $this->status = self::STATUS_NIEAKTYWNY;
    }

    public function __toString() {
        return '' . $this->getEmail();
    }


    public static function getStatusChoices() {
        return array(
            Subscription::STATUS_AKTYWNY => 'Aktywny',
            Subscription::STATUS_NIEAKTYWNY => 'Nieaktywny'
        );
    }

    public function getStatusTxt() {
        $arr = Subscription::getStatusChoices();

        return $arr[$this->getStatus()];
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Subscription
     */
    public function setEmail($email) {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail() {
        return $this->email;
    }

    /**
     * Set confirmationToken
     *
     * @param string $confirmationToken
     * @return Subscription
     */
    public function setConfirmationToken($confirmationToken) {
        $this->confirmationToken = $confirmationToken;

        return $this;
    }

    /**
     * Get confirmationToken
     *
     * @return string
     */
    public function getConfirmationToken() {
        return $this->confirmationToken;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Subscription
     */
    public function setStatus($status) {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus() {
        return $this->status;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Subscription
     */
    public function setCreated($created) {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated() {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return Subscription
     */
    public function setUpdated($updated) {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated() {
        return $this->updated;
    }
}