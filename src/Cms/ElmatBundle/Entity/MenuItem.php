<?php

namespace Cms\ElmatBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;

use Doctrine\ORM\Mapping as ORM;

/**
 * @Orm\Entity(repositoryClass="Cms\ElmatBundle\Repository\MenuItemRepository")
 * @Orm\Table(name="menu_item")
 */
class MenuItem {
    /**
     * @var integer $id
     * @Orm\Id
     * @Orm\Column(type="integer")
     * @Orm\GeneratedValue
     */
    private $id;

    /**
     * @var string $anchor
     * @ORM\Column(type="string", length=255, nullable=true);
     */
    private $anchor;

    /**
     * @var string $title
     * @ORM\Column(type="string", length=255, nullable=true);
     */
    private $title;

    /**
     * @var string $type
     * @ORM\Column(type="string", length=255, nullable=true);
     */
    private $type;

    /**
     * @var string $url
     * @ORM\Column(type="string", length=1024, nullable=true);
     */
    private $url;

    /**
     * @var string $location
     * @ORM\Column(type="string", length=255, nullable=true);
     */
    private $location;

    /**
     * @var integer $sortOrder
     * @ORM\Column(type="integer", nullable=true);
     * @deprecated
     */
    private $sortOrder;

    /**
     *
     * @var string
     * @ORM\Column(type="string", length=1024, nullable=true);
     */
    private $route;

    /**
     *
     * @var string
     * @ORM\Column(type="string", length=1024, nullable=true);
     */
    private $routeParameters;

    /**
     * @var array $attributes
     * @ORM\Column(type="string", length=255, nullable=true);
     */
    private $attributes;

    /**
     * @var integer $left
     * @ORM\Column(type="integer", nullable=true);
     */
    private $lft;

    /**
     * @var integer $right
     * @ORM\Column(type="integer", nullable=true);
     */
    private $rgt;

    /**
     * @var integer $depth
     * @ORM\Column(type="integer", nullable=true);
     */
    private $depth;

    /**
     * @ORM\ManyToOne(targetEntity="MenuItem", inversedBy="childItems")
     * @ORM\JoinColumn(name="parentItem_id", referencedColumnName="id")
     */
    private $parentItem;

    /**
     * @ORM\OneToMany(targetEntity="MenuItem", mappedBy="parentItem", cascade={"persist"})
     * @ORM\OrderBy({"lft" = "ASC"})
     */
    private $childItems;

    public function __construct() {
        $this->childItems = new ArrayCollection();
        $this->depth = 1;
        $this->lft = 400;
        $this->rgt = 400;
    }

    public function __toString() {
        return '' . $this->getAnchor();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set anchor
     *
     * @param string $anchor
     */
    public function setAnchor($anchor) {
        $this->anchor = $anchor;
    }

    /**
     * Get anchor
     *
     * @return string
     */
    public function getAnchor() {
        return $this->anchor;
    }

    /**
     * Set title
     *
     * @param string $title
     */
    public function setTitle($title) {
        $this->title = $title;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * Set type
     *
     * @param string $type
     */
    public function setType($type) {
        $this->type = $type;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType() {
        return $this->type;
    }

    /**
     * Set url
     *
     * @param string $url
     */
    public function setUrl($url) {
        $this->url = $url;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl() {
        return $this->url;
    }

    /**
     * Set location
     *
     * @param string $location
     */
    public function setLocation($location) {
        $this->location = $location;
    }

    /**
     * Get location
     *
     * @return string
     */
    public function getLocation() {
        return $this->location;
    }

    /**
     * Set sortOrder
     *
     * @param integer $sortOrder
     */
    public function setSortOrder($sortOrder) {
        $this->sortOrder = $sortOrder;
    }

    /**
     * Get sortOrder
     *
     * @return integer
     */
    public function getSortOrder() {
        return $this->sortOrder;
    }

    /**
     * Set route
     *
     * @param string $route
     */
    public function setRoute($route) {
        $this->route = $route;
    }

    /**
     * Get route
     *
     * @return string
     */
    public function getRoute() {
        return $this->route;
    }

    /**
     * Set routeParameters
     *
     * @param string $routeParameters
     */
    public function setRouteParameters($routeParameters) {
        $this->routeParameters = $routeParameters;
    }

    /**
     * Get routeParameters
     *
     * @return string
     */
    public function getRouteParameters() {
        return $this->routeParameters;
    }

    public function getRouteParametersArray() {
        return (array)json_decode($this->getRouteParameters());
    }

    /**
     * Set attributes
     *
     * @param string $attributes
     */
    public function setAttributes($attributes) {
        $this->attributes = $attributes;
    }

    /**
     * Get attributes
     *
     * @return string
     */
    public function getAttributes() {
        return $this->attributes;
    }

    /**
     * Set lft
     *
     * @param integer $lft
     */
    public function setLft($lft) {
        $this->lft = $lft;
    }

    /**
     * Get lft
     *
     * @return integer
     */
    public function getLft() {
        return $this->lft;
    }

    /**
     * Set rgt
     *
     * @param integer $rgt
     */
    public function setRgt($rgt) {
        $this->rgt = $rgt;
    }

    /**
     * Get rgt
     *
     * @return integer
     */
    public function getRgt() {
        return $this->rgt;
    }

    /**
     * Set depth
     *
     * @param integer $depth
     */
    public function setDepth($depth) {
        $this->depth = $depth;
    }

    /**
     * Get depth
     *
     * @return integer
     */
    public function getDepth() {
        return $this->depth;
    }

    /**
     * Set parentItem
     *
     * @param \Cms\ElmatBundle\Entity\MenuItem $parentItem
     */
    public function setParentItem(MenuItem $parentItem) {
        $this->parentItem = $parentItem;
    }

    /**
     * Get parentItem
     *
     * @return \Cms\ElmatBundle\Entity\MenuItem
     */
    public function getParentItem() {
        return $this->parentItem;
    }

    /**
     * Add childItems
     *
     * @param \Cms\ElmatBundle\Entity\MenuItem $childItems
     */
    public function addMenuItem(MenuItem $childItems) {
        $this->childItems[] = $childItems;
    }

    /**
     * Get childItems
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChildItems() {
        return $this->childItems;
    }
}