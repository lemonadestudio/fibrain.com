<?php

namespace Cms\ElmatBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="Cms\ElmatBundle\Repository\NewsletterRepository")
 * @ORM\Table(name="newsletter")
 */
class Newsletter {

    // nowo dodany newsletter
    const STATUS_NOWY = 0;

    // wysyłka w toku
    const STATUS_AKTYWNY = 1;

    // wysyłka zakończona
    const STATUS_ZAKONCZONO = 2;

    /**
     * @Orm\Id
     * @Orm\Column(type="integer")
     * @Orm\GeneratedValue
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(max=255)
     */
    protected $tytul;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    protected $tresc;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    protected $tresc_txt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $data_wysylki;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $status;

    /** @ORM\Column(type="integer", nullable=true) */
    protected $count_to_send;

    /** @ORM\Column(type="integer", nullable=true) */
    protected $count_sent;

    /**
     * @var \DateTime $created
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $created;

    /**
     * @var \DateTime $updated
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $updated;

    /**
     * @ORM\OneToMany(targetEntity="NewsletterSending", mappedBy="newsletter")
     * @ORM\OrderBy({"data" = "ASC"})
     */
    protected $sendings;

    /** @ORM\Column(type="boolean", nullable=true) */
    protected $tylko_z_kontem;

    /**
     * @ORM\Column(type="text", nullable=true )
     */
    protected $lista_reczna;


    /** @ORM\Column(type="boolean", nullable=true) */
    protected $oferta_swiatlowody;

    /** @ORM\Column(type="boolean", nullable=true) */
    protected $oferta_akcesoria_swiatlowodowe;

    /** @ORM\Column(type="boolean", nullable=true) */
    protected $oferta_mikrokanalizacja;

    /** @ORM\Column(type="boolean", nullable=true) */
    protected $oferta_kable_napowietrzne_airtrack;

    /** @ORM\Column(type="boolean", nullable=true) */
    protected $oferta_technologia_fttx;

    /** @ORM\Column(type="boolean", nullable=true) */
    protected $oferta_technologia_gpon;

    /** @ORM\Column(type="boolean", nullable=true) */
    protected $oferta_urzadzenia_aktywne;

    /** @ORM\Column(type="boolean", nullable=true) */
    protected $oferta_sieci_miejskie;

    /** @ORM\Column(type="boolean", nullable=true) */
    protected $oferta_osprzet_instalacyjny;

    /** @ORM\Column(type="boolean", nullable=true) */
    protected $oferta_osprzet_pomiarowy;

    /** @ORM\Column(type="boolean", nullable=true) */
    protected $oferta_okablowanie_strukturalne;


    public function __construct() {
        $this->status = self::STATUS_NOWY;
        $this->sendings = new ArrayCollection();
        $this->data_wysylki = new \DateTime();
        $this->count_sent = 0;
        $this->count_to_send = 0;
    }

    public function __toString() {
        return '' . $this->getTytul();
    }


    public static function getStatusChoices() {
        return array(
            Newsletter::STATUS_NOWY => 'Nowy',
            Newsletter::STATUS_AKTYWNY => 'W trakcie wysyłki',
            Newsletter::STATUS_ZAKONCZONO => 'Zakończono',

        );
    }

    public function getStatusTxt() {
        $arr = self::getStatusChoices();

        return $arr[$this->getStatus()];
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set tytul
     *
     * @param string $tytul
     * @return Newsletter
     */
    public function setTytul($tytul) {
        $this->tytul = $tytul;

        return $this;
    }

    /**
     * Get tytul
     *
     * @return string
     */
    public function getTytul() {
        return $this->tytul;
    }

    /**
     * Set tresc
     *
     * @param string $tresc
     * @return Newsletter
     */
    public function setTresc($tresc) {
        $this->tresc = $tresc;

        return $this;
    }

    /**
     * Get tresc
     *
     * @return string
     */
    public function getTresc() {
        return $this->tresc;
    }

    /**
     * Set tresc_txt
     *
     * @param string $trescTxt
     * @return Newsletter
     */
    public function setTrescTxt($trescTxt) {
        $this->tresc_txt = $trescTxt;

        return $this;
    }

    /**
     * Get tresc_txt
     *
     * @return string
     */
    public function getTrescTxt() {
        return $this->tresc_txt;
    }

    /**
     * Set data_wysylki
     *
     * @param \DateTime $dataWysylki
     * @return Newsletter
     */
    public function setDataWysylki($dataWysylki) {
        $this->data_wysylki = $dataWysylki;

        return $this;
    }

    /**
     * Get data_wysylki
     *
     * @return \DateTime
     */
    public function getDataWysylki() {
        return $this->data_wysylki;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Newsletter
     */
    public function setStatus($status) {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus() {
        return $this->status;
    }

    /**
     * Set count_to_send
     *
     * @param integer $countToSend
     * @return Newsletter
     */
    public function setCountToSend($countToSend) {
        $this->count_to_send = $countToSend;

        return $this;
    }

    /**
     * Get count_to_send
     *
     * @return integer
     */
    public function getCountToSend() {
        return $this->count_to_send;
    }

    /**
     * Set count_sent
     *
     * @param integer $countSent
     * @return Newsletter
     */
    public function setCountSent($countSent) {
        $this->count_sent = $countSent;

        return $this;
    }

    /**
     * Get count_sent
     *
     * @return integer
     */
    public function getCountSent() {
        return $this->count_sent;
    }

    /**
     * Add sendings
     *
     * @param \Cms\ElmatBundle\Entity\NewsletterSending $sendings
     * @return Newsletter
     */
    public function addSending(NewsletterSending $sendings) {
        $this->sendings[] = $sendings;

        return $this;
    }

    /**
     * Remove sendings
     *
     * @param \Cms\ElmatBundle\Entity\NewsletterSending $sendings
     */
    public function removeSending(NewsletterSending $sendings) {
        $this->sendings->removeElement($sendings);
    }

    /**
     * Get sendings
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSendings() {
        return $this->sendings;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Newsletter
     */
    public function setCreated($created) {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated() {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return Newsletter
     */
    public function setUpdated($updated) {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated() {
        return $this->updated;
    }

    /**
     * Set tylko_z_kontem
     *
     * @param boolean $tylkoZKontem
     * @return Newsletter
     */
    public function setTylkoZKontem($tylkoZKontem) {
        $this->tylko_z_kontem = $tylkoZKontem;

        return $this;
    }

    /**
     * Get tylko_z_kontem
     *
     * @return boolean
     */
    public function getTylkoZKontem() {
        return $this->tylko_z_kontem;
    }

    public function getListaReczna() {
        return $this->lista_reczna;
    }

    public function setListaReczna($lista_reczna) {
        $this->lista_reczna = $lista_reczna;
        return $this;
    }


    /**
     * Set oferta_swiatlowody
     *
     * @param boolean $ofertaSwiatlowody
     * @return User
     */
    public function setOfertaSwiatlowody($ofertaSwiatlowody) {
        $this->oferta_swiatlowody = $ofertaSwiatlowody;

        return $this;
    }

    /**
     * Get oferta_swiatlowody
     *
     * @return boolean
     */
    public function getOfertaSwiatlowody() {
        return $this->oferta_swiatlowody;
    }


    /**
     * Set oferta_urzadzenia_aktywne
     *
     * @param boolean $ofertaUrzadzeniaAktywne
     * @return User
     */
    public function setOfertaUrzadzeniaAktywne($ofertaUrzadzeniaAktywne) {
        $this->oferta_urzadzenia_aktywne = $ofertaUrzadzeniaAktywne;

        return $this;
    }

    /**
     * Get oferta_urzadzenia_aktywne
     *
     * @return boolean
     */
    public function getOfertaUrzadzeniaAktywne() {
        return $this->oferta_urzadzenia_aktywne;
    }


    public function getOfertaAkcesoriaSwiatlowodowe() {
        return $this->oferta_akcesoria_swiatlowodowe;
    }

    public function getOfertaMikrokanalizacja() {
        return $this->oferta_mikrokanalizacja;
    }

    public function getOfertaKableNapowietrzneAirtrack() {
        return $this->oferta_kable_napowietrzne_airtrack;
    }

    public function getOfertaTechnologiaFttx() {
        return $this->oferta_technologia_fttx;
    }

    public function getOfertaTechnologiaGpon() {
        return $this->oferta_technologia_gpon;
    }

    public function getOfertaSieciMiejskie() {
        return $this->oferta_sieci_miejskie;
    }

    public function getOfertaOsprzetInstalacyjny() {
        return $this->oferta_osprzet_instalacyjny;
    }

    public function getOfertaOsprzetPomiarowy() {
        return $this->oferta_osprzet_pomiarowy;
    }

    public function getOfertaOkablowanieStrukturalne() {
        return $this->oferta_okablowanie_strukturalne;
    }


    public function setOfertaAkcesoriaSwiatlowodowe($oferta_akcesoria_swiatlowodowe) {
        $this->oferta_akcesoria_swiatlowodowe = $oferta_akcesoria_swiatlowodowe;
        return $this;
    }

    public function setOfertaMikrokanalizacja($oferta_mikrokanalizacja) {
        $this->oferta_mikrokanalizacja = $oferta_mikrokanalizacja;
        return $this;
    }

    public function setOfertaKableNapowietrzneAirtrack($oferta_kable_napowietrzne_airtrack) {
        $this->oferta_kable_napowietrzne_airtrack = $oferta_kable_napowietrzne_airtrack;
        return $this;
    }

    public function setOfertaTechnologiaFttx($oferta_technologia_fttx) {
        $this->oferta_technologia_fttx = $oferta_technologia_fttx;
        return $this;
    }

    public function setOfertaTechnologiaGpon($oferta_technologia_gpon) {
        $this->oferta_technologia_gpon = $oferta_technologia_gpon;
        return $this;
    }


    public function setOfertaSieciMiejskie($oferta_sieci_miejskie) {
        $this->oferta_sieci_miejskie = $oferta_sieci_miejskie;
        return $this;
    }

    public function setOfertaOsprzetInstalacyjny($oferta_osprzet_instalacyjny) {
        $this->oferta_osprzet_instalacyjny = $oferta_osprzet_instalacyjny;
        return $this;
    }

    public function setOfertaOsprzetPomiarowy($oferta_osprzet_pomiarowy) {
        $this->oferta_osprzet_pomiarowy = $oferta_osprzet_pomiarowy;
        return $this;
    }

    public function setOfertaOkablowanieStrukturalne($oferta_okablowanie_strukturalne) {
        $this->oferta_okablowanie_strukturalne = $oferta_okablowanie_strukturalne;
        return $this;
    }
}