<?php

namespace Cms\ElmatBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="email_template")
 * @ORM\HasLifecycleCallbacks()
 */
class EmailTemplate {

    /**
     * @var string
     * @ORM\Id
     * @ORM\Column(type="string", length=255,  nullable=false, unique=true)
     * @Assert\NotBlank
     */
    protected $name;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $topic;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    protected $content;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    protected $content_txt;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    protected $description;


    public function __toString() {
        return '' . $this->name;
    }

    public function __construct() {

    }

    /**
     * @ORM\PrePersist();
     * @ORM\PreUpdate();
     */
    public function setCreatedValue() {
        $content_html = str_replace(array('&oacute;', '&Oacute;'), array('ó', 'Ó'), $this->getContent());

        if ($this->getContentTxt() == '') {

            $content_txt = strip_tags($content_html);

            $this->setContentTxt($content_txt);
        }

        $this->setContent($content_html);
    }

    /**
     * Set name
     *
     * @param string $name
     * @return EmailTemplate
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set topic
     *
     * @param string $topic
     * @return EmailTemplate
     */
    public function setTopic($topic) {
        $this->topic = $topic;

        return $this;
    }

    /**
     * Get topic
     *
     * @return string
     */
    public function getTopic() {
        return $this->topic;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return EmailTemplate
     */
    public function setContent($content) {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent() {
        return $this->content;
    }

    /**
     * Set content_txt
     *
     * @param string $contentTxt
     * @return EmailTemplate
     */
    public function setContentTxt($contentTxt) {
        $this->content_txt = $contentTxt;

        return $this;
    }

    /**
     * Get content_txt
     *
     * @return string
     */
    public function getContentTxt() {
        return $this->content_txt;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return EmailTemplate
     */
    public function setDescription($description) {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription() {
        return $this->description;
    }
}