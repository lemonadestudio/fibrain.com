<?php

namespace Cms\ElmatBundle\Entity;

use Cms\ElmatBundle\Helper\Cms;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="Cms\ElmatBundle\Repository\ProductRepository")
 * @ORM\Table(name="product")
 * @ORM\HasLifecycleCallbacks()
 * @Gedmo\SoftDeleteable(fieldName="deletedAt")
 */
class Product {

    const TYPE_normalny      = 'normalny';
    const TYPE_grupa_tabelka = 'grupa_tabelka';
    const TYPE_grupa_lista   = 'grupa_lista';

    /**
     * @var Cms
     */
    private $helper = null;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @var string $title
     *
     * @ORM\Column(type="string", length=255);
     * @Gedmo\Translatable
     * @Assert\NotNull();
     * @Assert\Length(max = 255);
     */
    private $title;

    /**
     * @var \DateTime $deletedAt
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @var string $title
     *
     * @ORM\Column(type="string", length=255, nullable=true);
     * @Assert\Length(max = 255);
     */
    private $code;

    /**
     * @ORM\Column(type="boolean")
     */
    private $published = true;

    /**
     * @var \DateTime $publishDate ;
     *
     * @ORM\Column(type="datetime", nullable=true);
     */
    private $publishDate;

    /**
     * @var string $content
     *
     * @Gedmo\Translatable
     * @ORM\Column(type="text", nullable=true )
     */
    private $content;

    /**
     * @var string $description
     *
     * @ORM\Column(type="string", length=2048, nullable=true)
     * @Gedmo\Translatable
     * @Assert\Length(max = 2048)
     */
    private $description;

    /**
     * @var string $keywords
     *
     * @ORM\Column(type="string", length=1024, nullable=true)
     * @Gedmo\Translatable
     * @Assert\Length(max = 1024)
     */
    private $keywords;

    /**
     * @Gedmo\Translatable
     * @Gedmo\Slug(fields={"title"})
     * @ORM\Column(type="string", length=255, unique=false);
     */
    private $slug;

    /**
     *
     * @var boolean
     * @ORM\Column(type="boolean", nullable=true );
     */
    private $automaticSeo;

    /**
     * @var \DateTime $created
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created;

    /**
     * @var \DateTime $updated
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated;

    /** @Assert\File(maxSize="6000000") */
    public $_file_image;

    public $_delete_file_image;

    /** @Assert\File(maxSize="6000000") */
    public $_file_image_thumb;

    public $_delete_file_image_thumb;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @ORM\Column(name="image_thumb", type="string", length=255, nullable=true)
     */
    private $image_thumb;


    /** @ORM\Column(type="string", length=60, nullable=true) */
    private $col_name;
    /** @ORM\Column(type="string", length=60, nullable=true) */
    private $col_image;
    /** @ORM\Column(type="string", length=60, nullable=true) */
    private $col_doc;

    /** @ORM\Column(type="string", length=60, nullable=true) */
    private $col1;
    /** @ORM\Column(type="string", length=60, nullable=true) */
    private $col2;
    /** @ORM\Column(type="string", length=60, nullable=true) */
    private $col3;
    /** @ORM\Column(type="string", length=60, nullable=true) */
    private $col4;
    /** @ORM\Column(type="string", length=60, nullable=true) */
    private $col5;
    /** @ORM\Column(type="string", length=60, nullable=true) */
    private $col6;
    /** @ORM\Column(type="string", length=60, nullable=true) */
    private $col7;

    /** @ORM\Column(type="string", length=6, nullable=true) */
    private $col_name_width;
    /** @ORM\Column(type="string", length=6, nullable=true) */
    private $col_image_width;
    /** @ORM\Column(type="string", length=6, nullable=true) */
    private $col_doc_width;

    /** @ORM\Column(type="string", length=6, nullable=true) */
    private $col1_width;
    /** @ORM\Column(type="string", length=6, nullable=true) */
    private $col2_width;
    /** @ORM\Column(type="string", length=6, nullable=true) */
    private $col3_width;
    /** @ORM\Column(type="string", length=6, nullable=true) */
    private $col4_width;
    /** @ORM\Column(type="string", length=6, nullable=true) */
    private $col5_width;
    /** @ORM\Column(type="string", length=6, nullable=true) */
    private $col6_width;
    /** @ORM\Column(type="string", length=6, nullable=true) */
    private $col7_width;

    /**
     * @ORM\Column(type="string", length=30, nullable=true);
     *
     * @Assert\Choice(choices={"normalny", "grupa_tabelka", "grupa_lista"})
     */
    private $type;

    /** @ORM\Column(type="boolean", nullable=true) */
    private $box_produkty_subpage;

    /**
     * @ORM\ManyToOne(targetEntity="ProductCategory", inversedBy="products")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $category;

    /**
     * @ORM\ManyToOne(targetEntity="OfferGroup", inversedBy="products")
     * @ORM\JoinColumn(onDelete="RESTRICT")
     */
    private $offer_group;

    /**
     * @ORM\ManyToOne(targetEntity="Product", inversedBy="products_based_on")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $base_product;

    /**
     * @ORM\OneToMany(targetEntity="Product", mappedBy="base_product", cascade={"persist", "remove"}, orphanRemoval=true)
     * @ORM\OrderBy({"publishDate" = "DESC"});
     */
    private $products_based_on;

    /**
     * @ORM\OneToMany(targetEntity="ProductRow", mappedBy="product", cascade={"persist", "remove"})
     * @ORM\OrderBy({"sort" = "ASC"});
     */
    private $rows;

    /**
     * @ORM\OneToMany(targetEntity="ProductTab", mappedBy="product", cascade={"persist", "remove"})
     * @ORM\OrderBy({"sort" = "ASC"});
     */
    private $tabs;

    /**
     * @ORM\OneToMany(targetEntity="ProductZdjecie", mappedBy="product", cascade={"persist", "remove"})
     * @ORM\OrderBy({"kolejnosc" = "ASC"})
     */
    private $zdjecia;

    /**
     * @ORM\OneToMany(targetEntity="ProductPowiazany", mappedBy="product",  cascade={"persist", "remove"})
     * @ORM\OrderBy({"kolejnosc" = "ASC"})
     */
    private $powiazane;

    /**
     * @ORM\OneToMany(targetEntity="ProductPowiazany", mappedBy="powiazany", cascade={"persist", "remove"})
     * @ORM\OrderBy({"kolejnosc" = "ASC"})
     */
    private $powiazane_do;

    /**
     * @ORM\OneToMany(targetEntity="Plik", mappedBy="product", cascade={"persist", "remove"})
     * @ORM\OrderBy({"kolejnosc" = "ASC"})
     */
    private $pliki;

    public function __construct() {
        $this->published = true;
        $this->automaticSeo = true;
        $this->publishDate = new \DateTime();

        $this->rows = new ArrayCollection();
        $this->tabs = new ArrayCollection();
        $this->pliki = new ArrayCollection();
        $this->zdjecia = new ArrayCollection();
        $this->powiazane = new ArrayCollection();
        $this->powiazane_do = new ArrayCollection();
        $this->products_based_on = new ArrayCollection();

        $this->helper = new Cms();
    }

    public function __toString() {
        return '' . $this->getTitle();
    }

    /**
     *
     * @return \Cms\ElmatBundle\Helper\Cms
     */
    public function getHelper() {
        if (!$this->helper) {
            $this->helper = new Cms();
        }
        return $this->helper;
    }

    /**
     * @ORM\PrePersist();
     * @ORM\PreUpdate();
     */
    public function setCreatedValue() {
        if ($this->getAutomaticSeo()) {
            // description
            $description = strip_tags($this->getContent());
            $description = mb_substr(html_entity_decode($description), 0, 1048);

            // usunięcie nowych linii
            $description = preg_replace('@\v@', ' ', $description);
            // podwójnych białych znaków
            $description = preg_replace('@\h{2,}@', ' ', $description);

            // usunięcie ostatniego, niedokończonego zdania
            $description = preg_replace('@(.*)\..*@', '\1.', $description);

            // trim
            $description = trim($description);

            // keywords
            $keywords_arr = explode(' ', $this->getTitle() . ' ' . $this->getDescription());

            $keywords = array();
            if (is_array($keywords_arr)) {
                foreach ($keywords_arr as $kw) {
                    $kw = trim($kw);
                    $kw = preg_replace('@\.,;\'\"@', '', $kw);
                    if (strlen($kw) >= 4 && !in_array($kw, $keywords)) {
                        $keywords[] = $kw;
                    }
                    if (count($keywords) >= 10) {
                        break;
                    }
                }
            }

            $this->setDescription($description);
            $this->setKeywords(implode(', ', $keywords));
            $this->setAutomaticSeo(false);
        }
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function prepareUploadedNames() {
        foreach (array('image', 'image_thumb') as $_fi) {
            $_file = '_file_' . $_fi;

            if (null !== $this->{$_file}) {
                // do whatever you want to generate a unique name
                $this->{'old' . $_file} = $this->getAbsolutePath($_fi);
                $this->{'oldcrop' . $_file} = $this->getAbsolutePath($_fi, true);
                $this->{$_fi} = $_fi . '-' . uniqid() . '.' . $this->{$_file}->guessExtension();
            } elseif (true == $this->{'_delete' . $_file}) {
                @unlink($this->getAbsolutePath($_fi));
                @unlink($this->getAbsolutePath($_fi, true));
                $this->{$_fi} = null;
            }
        }
    }


    /**
     *
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function storeUploadedFiles() {
        foreach (array('image', 'image_thumb') as $_fi) {
            $_file = '_file_' . $_fi;

            if (null === $this->{$_file}) {
                continue;
            }

            $this->{$_file}->move($this->getUploadRootDir(), $this->{$_fi});

            if ($this->{'old' . $_file}) {
                @unlink($this->{'old' . $_file});
            }
            if ($this->{'oldcrop' . $_file}) {
                @unlink($this->{'oldcrop' . $_file});
            }

            unset($this->{$_file});
        }
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUploadedFiles() {
        if ($file = $this->getAbsolutePath('image')) {
            @unlink($file);
        }
        if ($file = $this->getAbsolutePath('image', true)) {
            @unlink($file);
        }

        if ($file = $this->getAbsolutePath('image_thumb')) {
            @unlink($file);
        }
        if ($file = $this->getAbsolutePath('image_thumb', true)) {
            @unlink($file);
        }
    }


    public function getAbsolutePath($_fil, $crop = false) {
        $file = null;

        switch ($_fil) {
            case 'image_thumb':
                $file = $this->getImageThumb();
                break;

            case 'image':
            default:
                $file = $this->getImage();
                break;
        }

        return !$file ? null : $this->getUploadRootDir() . '/' . ($crop ? 'crop-' : '') . $file;
    }

    public function getWebPath($_fil = 'image') {
        $file = null;

        switch ($_fil) {
            case 'image_thumb':
                $file = $this->getImageThumb();
                break;

            case 'image':
            default:
                $file = $this->getImage();
                break;
        }

        return !$file ? null : $this->getUploadDir() . '/' . $file;
    }

    public function getWebPathCrop($_fil = 'image') {
        if ($_fil == 'image') {
            if (file_exists($this->getUploadRootDir() . '/crop-' . $this->getImage())) {
                return $this->getUploadDir() . '/crop-' . $this->getImage();
            } else {
                return $this->getWebPath();
            }
        }
        if ($_fil == 'image_thumb') {
            if (file_exists($this->getUploadRootDir() . '/crop-' . $this->getImageThumb())) {
                return $this->getUploadDir() . '/crop-' . $this->getImageThumb();
            } else {
                return $this->getWebPath('image_thumb');
            }
        }
    }

    public function getUploadRootDir() {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__ . '/../../../../web/' . $this->getUploadDir();
    }

    public function getUploadDir() {
        if ($this->getBaseProduct()) {
            return $this->getBaseProduct()->getUploadDir();
        }
        return 'uploads/produkty/' . $this->getId();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Product
     */
    public function setTitle($title) {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle() {
        if ($this->getBaseProduct()) {
            return $this->getBaseProduct()->getTitle();
        }
        return $this->title;
    }

    /**
     * Set published
     *
     * @param boolean $published
     * @return Product
     */
    public function setPublished($published) {
        $this->published = $published;

        return $this;
    }

    /**
     * Get published
     *
     * @return boolean
     */
    public function getPublished() {
        if ($this->getBaseProduct()) {
            return $this->getBaseProduct()->getPublished();
        }
        return $this->published;
    }

    /**
     * Set publishDate
     *
     * @param \DateTime $publishDate
     * @return Product
     */
    public function setPublishDate($publishDate) {
        $this->publishDate = $publishDate;

        return $this;
    }

    /**
     * Get publishDate
     *
     * @return \DateTime
     */
    public function getPublishDate() {
        if ($this->getBaseProduct()) {
            return $this->getBaseProduct()->getPublishDate();
        }
        return $this->publishDate;
    }

    /**
     * Set keywords
     *
     * @param string $keywords
     * @return Product
     */
    public function setKeywords($keywords) {
        $this->keywords = $keywords;

        return $this;
    }

    /**
     * Get keywords
     *
     * @return string
     */
    public function getKeywords() {
        if ($this->getBaseProduct()) {
            return $this->getBaseProduct()->getKeywords();
        }
        return $this->keywords;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Product
     */
    public function setDescription($description) {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription() {
        if ($this->getBaseProduct()) {
            return $this->getBaseProduct()->getDescription();
        }
        return $this->description;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return Product
     */
    public function setContent($content) {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent() {
        if ($this->getBaseProduct()) {
            return $this->getBaseProduct()->getContent();
        }
        return $this->content;
    }

    /**
     * Set automaticSeo
     *
     * @param boolean $automaticSeo
     * @return Product
     */
    public function setAutomaticSeo($automaticSeo) {
        $this->automaticSeo = $automaticSeo;

        return $this;
    }

    /**
     * Get automaticSeo
     *
     * @return boolean
     */
    public function getAutomaticSeo() {
        if ($this->getBaseProduct()) {
            return $this->getBaseProduct()->getAutomaticSeo();
        }

        return $this->automaticSeo;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Product
     */
    public function setCreated($created) {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated() {
        if ($this->getBaseProduct()) {
            return $this->getBaseProduct()->getCreated();
        }
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return Product
     */
    public function setUpdated($updated) {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated() {
        if ($this->getBaseProduct()) {
            return $this->getBaseProduct()->getUpdated();
        }
        return $this->updated;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Product
     */
    public function setType($type) {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType() {
        if ($this->getBaseProduct()) {
            return $this->getBaseProduct()->getType();
        }
        return $this->type;
    }

    /**
     * Add row
     *
     * @param \Cms\ElmatBundle\Entity\ProductRow $row
     * @return Product
     */
    public function addRow(ProductRow $row) {
        $row->setProduct($this);
        $this->rows[] = $row;

        return $this;
    }

    /**
     * Remove row
     *
     * @param \Cms\ElmatBundle\Entity\ProductRow $row
     */
    public function removeRow(ProductRow $row) {
        $this->rows->removeElement($row);
    }

    /**
     * Get rows
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRows() {
        if ($this->getBaseProduct()) {
            return $this->getBaseProduct()->getRows();
        }
        return $this->rows;
    }

    /**
     * Set col1
     *
     * @param string $col1
     * @return Product
     */
    public function setCol1($col1) {
        $this->col1 = $col1;

        return $this;
    }

    /**
     * Get col1
     *
     * @return string
     */
    public function getCol1() {
        if ($this->getBaseProduct()) {
            return $this->getBaseProduct()->getCol1();
        }
        return $this->col1;
    }

    /**
     * Set col2
     *
     * @param string $col2
     * @return Product
     */
    public function setCol2($col2) {
        $this->col2 = $col2;

        return $this;
    }

    /**
     * Get col2
     *
     * @return string
     */
    public function getCol2() {
        if ($this->getBaseProduct()) {
            return $this->getBaseProduct()->getCol2();
        }
        return $this->col2;
    }

    /**
     * Set col3
     *
     * @param string $col3
     * @return Product
     */
    public function setCol3($col3) {
        $this->col3 = $col3;

        return $this;
    }

    /**
     * Get col3
     *
     * @return string
     */
    public function getCol3() {
        if ($this->getBaseProduct()) {
            return $this->getBaseProduct()->getCol3();
        }
        return $this->col3;
    }

    /**
     * Set col4
     *
     * @param string $col4
     * @return Product
     */
    public function setCol4($col4) {
        $this->col4 = $col4;

        return $this;
    }

    /**
     * Get col4
     *
     * @return string
     */
    public function getCol4() {
        if ($this->getBaseProduct()) {
            return $this->getBaseProduct()->getCol4();
        }
        return $this->col4;
    }

    /**
     * Set col5
     *
     * @param string $col5
     * @return Product
     */
    public function setCol5($col5) {
        $this->col5 = $col5;

        return $this;
    }

    /**
     * Get col5
     *
     * @return string
     */
    public function getCol5() {
        if ($this->getBaseProduct()) {
            return $this->getBaseProduct()->getCol5();
        }
        return $this->col5;
    }

    /**
     * Set col6
     *
     * @param string $col6
     * @return Product
     */
    public function setCol6($col6) {
        $this->col6 = $col6;

        return $this;
    }

    /**
     * Get col6
     *
     * @return string
     */
    public function getCol6() {
        if ($this->getBaseProduct()) {
            return $this->getBaseProduct()->getCol6();
        }
        return $this->col6;
    }

    /**
     * Set col7
     *
     * @param string $col7
     * @return Product
     */
    public function setCol7($col7) {
        $this->col7 = $col7;

        return $this;
    }

    /**
     * Get col7
     *
     * @return string
     */
    public function getCol7() {
        if ($this->getBaseProduct()) {
            return $this->getBaseProduct()->getCol7();
        }
        return $this->col7;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Product
     */
    public function setSlug($slug) {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug() {
        if ($this->getBaseProduct()) {
            return $this->getBaseProduct()->getSlug();
        }
        return $this->slug;
    }

    /**
     * Set category
     *
     * @param \Cms\ElmatBundle\Entity\ProductCategory $category
     * @return Product
     */
    public function setCategory(ProductCategory $category = null) {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \Cms\ElmatBundle\Entity\ProductCategory
     */
    public function getCategory() {
        return $this->category;
    }

    public function getCategoryNametreeFull() {

        if (!$this->getCategory()) {
            return '';
        }

        return $this->getCategory()->getNametreeFull();

    }

    /**
     * Set image
     *
     * @param string $image
     * @return Product
     */
    public function setImage($image) {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage() {
        if ($this->getBaseProduct()) {
            return $this->getBaseProduct()->getImage();
        }
        return $this->image;
    }

    /**
     * Set image_thumb
     *
     * @param string $image_thumb
     * @return Product
     */
    public function setImageThumb($image_thumb) {
        $this->image_thumb = $image_thumb;

        return $this;
    }

    /**
     * Get image_thumb
     *
     * @return string
     */
    public function getImageThumb() {
        if ($this->getBaseProduct()) {
            return $this->getBaseProduct()->getImageThumb();
        }
        return $this->image_thumb;
    }

    /**
     * Set col_name
     *
     * @param string $colName
     * @return Product
     */
    public function setColName($colName) {
        $this->col_name = $colName;

        return $this;
    }

    /**
     * Get col_name
     *
     * @return string
     */
    public function getColName() {
        if ($this->getBaseProduct()) {
            return $this->getBaseProduct()->getColName();
        }
        return $this->col_name;
    }

    /**
     * Set col_image
     *
     * @param string $colImage
     * @return Product
     */
    public function setColImage($colImage) {
        $this->col_image = $colImage;

        return $this;
    }

    /**
     * Get col_image
     *
     * @return string
     */
    public function getColImage() {
        if ($this->getBaseProduct()) {
            return $this->getBaseProduct()->getColImage();
        }
        return $this->col_image;
    }

    /**
     * Set col_doc
     *
     * @param string $colDoc
     * @return Product
     */
    public function setColDoc($colDoc) {
        $this->col_doc = $colDoc;

        return $this;
    }

    /**
     * Get col_doc
     *
     * @return string
     */
    public function getColDoc() {
        if ($this->getBaseProduct()) {
            return $this->getBaseProduct()->getColDoc();
        }
        return $this->col_doc;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return Product
     */
    public function setCode($code) {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode() {
        if ($this->getBaseProduct()) {
            return $this->getBaseProduct()->getCode();
        }
        return $this->code;
    }

    /**
     * Add tab
     *
     * @param \Cms\ElmatBundle\Entity\ProductTab $tab
     * @return Product
     */
    public function addTab(ProductTab $tab) {
        $tab->setProduct($this);
        $this->tabs[] = $tab;

        return $this;
    }

    /**
     * Remove tab
     *
     * @param \Cms\ElmatBundle\Entity\ProductTab $tab
     */
    public function removeTab(ProductTab $tab) {
        $this->tabs->removeElement($tab);
    }

    /**
     * Get tabs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTabs() {
        if ($this->getBaseProduct()) {
            return $this->getBaseProduct()->getTabs();
        }
        return $this->tabs;
    }

    /**
     * Set box_produkty_subpage
     *
     * @param boolean $boxProduktySubpage
     * @return Product
     */
    public function setBoxProduktySubpage($boxProduktySubpage) {
        $this->box_produkty_subpage = $boxProduktySubpage;

        return $this;
    }

    /**
     * Get box_produkty_subpage
     *
     * @return boolean
     */
    public function getBoxProduktySubpage() {
        if ($this->getBaseProduct()) {
            return $this->getBaseProduct()->getBoxProduktySubpage();
        }
        return $this->box_produkty_subpage;
    }

    /**
     * Set offer_group
     *
     * @param \Cms\ElmatBundle\Entity\OfferGroup $offerGroup
     * @return Product
     */
    public function setOfferGroup(OfferGroup $offerGroup = null) {
        $this->offer_group = $offerGroup;

        return $this;
    }

    /**
     * Get offer_group
     *
     * @return \Cms\ElmatBundle\Entity\OfferGroup
     */
    public function getOfferGroup() {
//         if($this->getBaseProduct()) {
//             return $this->getBaseProduct()->getOfferGroup();
//         }
        return $this->offer_group;
    }

    /**
     * Add zdjecie
     *
     * @param \Cms\ElmatBundle\Entity\ProductZdjecie $zdjecie
     * @return Product
     */
    public function addZdjecium(ProductZdjecie $zdjecie) {
        $zdjecie->setProduct($this);
        $this->zdjecia[] = $zdjecie;

        return $this;
    }

    /**
     * Remove zdjecie
     *
     * @param \Cms\ElmatBundle\Entity\ProductZdjecie $zdjecie
     */
    public function removeZdjecium(ProductZdjecie $zdjecie) {
        $this->zdjecia->removeElement($zdjecie);
    }

    /**
     * Get zdjecia
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getZdjecia() {
        if ($this->getBaseProduct()) {
            return $this->getBaseProduct()->getZdjecia();
        }
        return $this->zdjecia;
    }

    /**
     * Add powiazane
     *
     * @param \Cms\ElmatBundle\Entity\ProductPowiazany $powiazane
     * @return Product
     */
    public function addPowiazane(ProductPowiazany $powiazane) {
        $powiazane->setProduct($this);
        $this->powiazane[] = $powiazane;

        return $this;
    }

    /**
     * Remove powiazane
     *
     * @param \Cms\ElmatBundle\Entity\ProductPowiazany $powiazane
     */
    public function removePowiazane(ProductPowiazany $powiazane) {
        $this->powiazane->removeElement($powiazane);
    }

    /**
     * Get powiazane
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPowiazane() {
        if ($this->getBaseProduct()) {
            return $this->getBaseProduct()->getPowiazane();
        }
        return $this->powiazane;
    }

    /**
     * Add powiazane_do
     *
     * @param \Cms\ElmatBundle\Entity\ProductPowiazany $powiazaneDo
     * @return Product
     */
    public function addPowiazaneDo(ProductPowiazany $powiazaneDo) {
        $this->powiazane_do[] = $powiazaneDo;
        $powiazaneDo->setProduct($this);
        return $this;
    }

    /**
     * Remove powiazane_do
     *
     * @param \Cms\ElmatBundle\Entity\ProductPowiazany $powiazaneDo
     */
    public function removePowiazaneDo(ProductPowiazany $powiazaneDo) {
        $this->powiazane_do->removeElement($powiazaneDo);
    }

    /**
     * Get powiazane_do
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPowiazaneDo() {
        if ($this->getBaseProduct()) {
            return $this->getBaseProduct()->getPowiazaneDo();
        }
        return $this->powiazane_do;
    }

    /**
     * Add plik
     *
     * @param \Cms\ElmatBundle\Entity\Plik $plik
     * @return Product
     */
    public function addPlikus(Plik $plik) {
        $plik->setProduct($this);
        $this->pliki[] = $plik;
        return $this;
    }

    /**
     * Remove plik
     *
     * @param \Cms\ElmatBundle\Entity\Plik $plik
     */
    public function removePlikus(Plik $plik) {
        $this->pliki->removeElement($plik);
    }

    /**
     * Get pliki
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPliki() {
        if ($this->getBaseProduct()) {
            return $this->getBaseProduct()->getPliki();
        }
        return $this->pliki;
    }

    /**
     * Set col_name_width
     *
     * @param string $colNameWidth
     * @return Product
     */
    public function setColNameWidth($colNameWidth) {
        $this->col_name_width = $colNameWidth;

        return $this;
    }

    /**
     * Get col_name_width
     *
     * @return string
     */
    public function getColNameWidth() {
        if ($this->getBaseProduct()) {
            return $this->getBaseProduct()->getColNameWidth();
        }
        return $this->getHelper()->parseWidthCss($this->col_name_width);
    }

    /**
     * Set col_image_width
     *
     * @param string $colImageWidth
     * @return Product
     */
    public function setColImageWidth($colImageWidth) {
        $this->col_image_width = $colImageWidth;

        return $this;
    }

    /**
     * Get col_image_width
     *
     * @return string
     */
    public function getColImageWidth() {
        if ($this->getBaseProduct()) {
            return $this->getBaseProduct()->getColImageWidth();
        }
        return $this->getHelper()->parseWidthCss($this->col_image_width);
    }

    /**
     * Set col_doc_width
     *
     * @param string $colDocWidth
     * @return Product
     */
    public function setColDocWidth($colDocWidth) {
        $this->col_doc_width = $colDocWidth;

        return $this;
    }

    /**
     * Get col_doc_width
     *
     * @return string
     */
    public function getColDocWidth() {
        if ($this->getBaseProduct()) {
            return $this->getBaseProduct()->getColDocWidth();
        }
        return $this->getHelper()->parseWidthCss($this->col_doc_width);
    }

    /**
     * Set col1_width
     *
     * @param string $col1Width
     * @return Product
     */
    public function setCol1Width($col1Width) {
        $this->col1_width = $col1Width;

        return $this;
    }

    /**
     * Get col1_width
     *
     * @return string
     */
    public function getCol1Width() {
        if ($this->getBaseProduct()) {
            return $this->getBaseProduct()->getCol1Width();
        }

        return $this->getHelper()->parseWidthCss($this->col1_width);
    }

    /**
     * Set col2_width
     *
     * @param string $col2Width
     * @return Product
     */
    public function setCol2Width($col2Width) {
        $this->col2_width = $col2Width;

        return $this;
    }

    /**
     * Get col2_width
     *
     * @return string
     */
    public function getCol2Width() {
        if ($this->getBaseProduct()) {
            return $this->getBaseProduct()->getCol2Width();
        }
        return $this->getHelper()->parseWidthCss($this->col2_width);
    }

    /**
     * Set col3_width
     *
     * @param string $col3Width
     * @return Product
     */
    public function setCol3Width($col3Width) {
        $this->col3_width = $col3Width;

        return $this;
    }

    /**
     * Get col3_width
     *
     * @return string
     */
    public function getCol3Width() {
        if ($this->getBaseProduct()) {
            return $this->getBaseProduct()->getCol3Width();
        }
        return $this->getHelper()->parseWidthCss($this->col3_width);
    }

    /**
     * Set col4_width
     *
     * @param string $col4Width
     * @return Product
     */
    public function setCol4Width($col4Width) {
        $this->col4_width = $col4Width;

        return $this;
    }

    /**
     * Get col4_width
     *
     * @return string
     */
    public function getCol4Width() {
        if ($this->getBaseProduct()) {
            return $this->getBaseProduct()->getCol4Width();
        }
        return $this->getHelper()->parseWidthCss($this->col4_width);
    }

    /**
     * Set col5_width
     *
     * @param string $col5Width
     * @return Product
     */
    public function setCol5Width($col5Width) {

        $this->col5_width = $col5Width;

        return $this;
    }

    /**
     * Get col5_width
     *
     * @return string
     */
    public function getCol5Width() {
        if ($this->getBaseProduct()) {
            return $this->getBaseProduct()->getCol5Width();
        }
        return $this->getHelper()->parseWidthCss($this->col5_width);
    }

    /**
     * Set col6_width
     *
     * @param string $col6Width
     * @return Product
     */
    public function setCol6Width($col6Width) {
        $this->col6_width = $col6Width;

        return $this;
    }

    /**
     * Get col6_width
     *
     * @return string
     */
    public function getCol6Width() {
        if ($this->getBaseProduct()) {
            return $this->getBaseProduct()->getCol6Width();
        }
        return $this->getHelper()->parseWidthCss($this->col6_width);
    }

    /**
     * Set col7_width
     *
     * @param string $col7Width
     * @return Product
     */
    public function setCol7Width($col7Width) {
        $this->col7_width = $col7Width;

        return $this;
    }

    /**
     * Get col7_width
     *
     * @return string
     */
    public function getCol7Width() {
        if ($this->getBaseProduct()) {
            return $this->getBaseProduct()->getCol7Width();
        }
        return $this->getHelper()->parseWidthCss($this->col7_width);
    }

    /**
     * Set base_product
     *
     * @param \Cms\ElmatBundle\Entity\Product $baseProduct
     * @return Product
     */
    public function setBaseProduct(Product $baseProduct = null) {
        $this->base_product = $baseProduct;

        return $this;
    }

    /**
     * Get base_product
     *
     * @return \Cms\ElmatBundle\Entity\Product
     */
    public function getBaseProduct() {
        return $this->base_product;
    }

    /**
     * Add products_based_on
     *
     * @param \Cms\ElmatBundle\Entity\Product $productsBasedOn
     * @return Product
     */
    public function addProductsBasedOn(Product $productsBasedOn) {
        $this->products_based_on[] = $productsBasedOn;

        return $this;
    }

    public function setProductsBasedOn($productsBasedOn) {
        foreach ($productsBasedOn as $prod) {
            $this->addProductsBasedOn($prod);
        }

        return $this;
    }

    /**
     * Remove products_based_on
     *
     * @param \Cms\ElmatBundle\Entity\Product $productsBasedOn
     */
    public function removeProductsBasedOn(Product $productsBasedOn) {
        $this->products_based_on->removeElement($productsBasedOn);
    }

    /**
     * Get products_based_on
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProductsBasedOn() {
        return $this->products_based_on;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     * @return Product
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime 
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }
}