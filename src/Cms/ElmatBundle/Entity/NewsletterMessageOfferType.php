<?php

namespace Cms\ElmatBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 *
 * @ORM\Table(name="newsletter_message_offer_type")
 * @ORM\Entity(repositoryClass="Cms\ElmatBundle\Repository\NewsletterMessageOfferTypeRepository")
 * @ORM\HasLifecycleCallbacks();
 */
class NewsletterMessageOfferType {
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $type;

    /**
     * @ORM\ManyToOne(targetEntity="NewsletterMessage", inversedBy="offerTypes")
     */
    private $newsletterMessage;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param integer $type
     * @return NewsletterMessageOfferType
     */
    public function setType($type) {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer
     */
    public function getType() {
        return $this->type;
    }

    /**
     * Set newsletterMessage
     *
     * @param \Cms\ElmatBundle\Entity\NewsletterMessage $newsletterMessage
     * @return NewsletterMessageOfferType
     */
    public function setNewsletterMessage(NewsletterMessage $newsletterMessage = null) {
        $this->newsletterMessage = $newsletterMessage;

        return $this;
    }

    /**
     * Get newsletterMessage
     *
     * @return \Cms\ElmatBundle\Entity\NewsletterMessage
     */
    public function getNewsletterMessage() {
        return $this->newsletterMessage;
    }
}