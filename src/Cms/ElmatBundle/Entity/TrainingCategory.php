<?php
namespace Cms\ElmatBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @Gedmo\Tree(type="nested")
 * @ORM\Table(name="category_training")
 * @ORM\Entity(repositoryClass="Cms\ElmatBundle\Repository\TrainingCategoryRepository")
 * @ORM\HasLifecycleCallbacks();
 */
class TrainingCategory {
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @Gedmo\Slug(fields={"title"})
     * @ORM\Column(name="slug", type="string", length=255)
     */
    private $slug;

    /**
     * @ORM\Column(type="string", length=1024, nullable=true)
     * @Assert\Length(max = 1024)
     */
    private $keywords;

    /**
     * @ORM\Column(type="string", length=2048, nullable=true)
     * @Assert\Length(max = 2048)
     */
    private $description;

    /**
     * @ORM\Column(type="boolean", nullable=true );
     */
    private $automaticSeo;

    /**
     * @ORM\Column(type="text", nullable=true )
     */
    private $content;

    /**
     * @Gedmo\TreeLeft
     * @ORM\Column(name="lft", type="integer")
     */
    private $lft;

    /**
     * @Gedmo\TreeLevel
     * @ORM\Column(name="lvl", type="integer")
     */
    private $lvl;

    /**
     * @Gedmo\TreeRight
     * @ORM\Column(name="rgt", type="integer")
     */
    private $rgt;

    /**
     * @Gedmo\TreeRoot
     * @ORM\Column(name="root", type="integer", nullable=true)
     */
    private $root;

    /**
     * @Gedmo\TreeParent
     * @ORM\ManyToOne(targetEntity="TrainingCategory", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $parent;

    /**
     * @ORM\OneToMany(targetEntity="TrainingCategory", mappedBy="parent")
     * @ORM\OrderBy({"lft" = "ASC"})
     */
    private $children;

    /**
     * @ORM\OneToMany(targetEntity="Training", mappedBy="category")
     * @ORM\OrderBy({"publishDate" = "DESC"})
     */
    protected $trainings;


    /** @Assert\File(maxSize="6000000") */
    public $_file_image;

    public $_delete_file_image = false;

    /**
     * @ORM\Column(name="image", type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @var \DateTime $updated
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated;

    /**
     * kolejność sortowania
     * @ORM\Column(type="integer", nullable=true);
     */
    private $kolejnosc;


    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Assert\MaxLength(100)
     */
    private $color;


    /**
     * @ORM\PostPersist();
     */
    public function setPostCreatedValue() {

    }

    /**
     * @ORM\PrePersist();
     * @ORM\PreUpdate();
     */
    public function setCreatedValue() {
        if ($this->getAutomaticSeo()) {
            // description
            $description = strip_tags($this->getContent());
            $description = mb_substr(html_entity_decode($description), 0, 2048);

            // usunięcie ostatniego, niedokończonego zdania

            $description = preg_replace('@(.*)\..*@', '\1', $description);

            $this->setDescription($description);

            // keywords
            $keywords_arr = explode(' ', $this->getTitle() . ' ' . $this->getDescription());

            $keywords = array();
            if (is_array($keywords_arr)) {
                foreach ($keywords_arr as $kw) {
                    $kw = trim($kw);
                    $kw = preg_replace('@\.,;\'\"@', '', $kw);
                    if (strlen($kw) >= 4 && !in_array($kw, $keywords)) {
                        $keywords[] = $kw;
                    }
                    if (count($keywords) >= 10) {
                        break;
                    }
                }
            }

            $this->setKeywords(implode(', ', $keywords));
            $this->setAutomaticSeo(false);

        }
    }


    public function __toString() {
        return '' . $this->getTitle();
    }

    public function getNametree() {

        $pref = '';
        for ($i = 0; $i < $this->lvl; $i++) {
            $pref .= '-';
        }
        $pref = preg_replace('/-$/', ' ↳', $pref);
        $pref = preg_replace('/-/', " · · ·", $pref);

        return $pref . $this->getTitle();
    }

    public function __construct() {
        $this->automaticSeo = true;
        $this->children = new ArrayCollection();
        $this->trainings = new ArrayCollection();
        $this->updated = new \DateTime();
        $this->kolejnosc = 0;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     */
    public function setTitle($title) {
        $this->title = $title;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * Set slug
     *
     * @param string $slug
     */
    public function setSlug($slug) {
        $this->slug = $slug;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug() {
        return $this->slug;
    }

    /**
     * Set lft
     *
     * @param integer $lft
     */
    public function setLft($lft) {
        $this->lft = $lft;
    }

    /**
     * Get lft
     *
     * @return integer
     */
    public function getLft() {
        return $this->lft;
    }

    /**
     * Set lvl
     *
     * @param integer $lvl
     */
    public function setLvl($lvl) {
        $this->lvl = $lvl;
    }

    /**
     * Get lvl
     *
     * @return integer
     */
    public function getLvl() {
        return $this->lvl;
    }

    /**
     * Set rgt
     *
     * @param integer $rgt
     */
    public function setRgt($rgt) {
        $this->rgt = $rgt;
    }

    /**
     * Get rgt
     *
     * @return integer
     */
    public function getRgt() {
        return $this->rgt;
    }

    /**
     * Set root
     *
     * @param integer $root
     */
    public function setRoot($root) {
        $this->root = $root;
    }

    /**
     * Get root
     *
     * @return integer
     */
    public function getRoot() {
        return $this->root;
    }

    /**
     * Set parent
     *
     * @param \Cms\ElmatBundle\Entity\TrainingCategory $parent
     */
    public function setParent(TrainingCategory $parent) {
        $this->parent = $parent;
    }

    /**
     * Get parent
     *
     * @return \Cms\ElmatBundle\Entity\TrainingCategory
     */
    public function getParent() {
        return $this->parent;
    }

    /**
     * Add children
     *
     * @param \Cms\ElmatBundle\Entity\TrainingCategory $children
     */
    public function addTrainingCategory(TrainingCategory $children) {
        $this->children[] = $children;
    }

    /**
     * Add children
     *
     * @param \Cms\ElmatBundle\Entity\TrainingCategory $children
     * @return TrainingCategory
     */
    public function addChildren(TrainingCategory $children) {
        $this->children[] = $children;

        return $this;
    }

    /**
     * Remove children
     *
     * @param \Cms\ElmatBundle\Entity\TrainingCategory $children
     */
    public function removeChildren(TrainingCategory $children) {
        $this->children->removeElement($children);
    }

    /**
     * Get children
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChildren() {
        return $this->children;
    }

    /**
     * Add training
     *
     * @param \Cms\ElmatBundle\Entity\Training $training
     */
    public function addTraining(Training $training) {
        $this->trainings[] = $training;
    }

    /**
     * Remove training
     *
     * @param \Cms\ElmatBundle\Entity\Training $training
     */
    public function removeTraining(Training $training) {
        $this->trainings->removeElement($training);
    }

    /**
     * Get trainings
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTrainings() {
        return $this->trainings;
    }


    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function prepareUploadedNames() {
        foreach (array('image') as $_fi) {
            $_file = '_file_' . $_fi;
            if (null !== $this->{$_file}) {
                $this->{'old' . $_file} = $this->getAbsolutePath($_fi);
                $this->{$_fi} = $_fi . '-' . uniqid() . '.' . $this->{$_file}->guessExtension();
            } elseif (true == $this->{'_delete' . $_file}) {
                @unlink($this->getAbsolutePath($_fi));
                $this->{$_fi} = null;
            }
        }
    }

    /**
     *
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function storeUploadedFiles() {

        foreach (array('image') as $_fi) {

            $_file = '_file_' . $_fi;
            if (null === $this->{$_file}) {
                continue;
            }
            $this->{$_file}->move($this->getUploadRootDir(), $this->{$_fi});
            if ($this->{'old' . $_file}) {
                @unlink($this->{'old' . $_file});
            }
            unset($this->{$_file});
        }

    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUploadedFiles() {
        if ($file = $this->getAbsolutePath('image')) {
            @unlink($file);
        }

    }

    public function getAbsolutePath($_fil) {
        $file = null;
        switch ($_fil) {

            case 'image':
            default:
                $file = $this->getImage();
        }
        return !$file ? null : $this->getUploadRootDir() . '/' . $file;
    }

    public function getWebPath($_fil = 'image') {
        $file = null;
        switch ($_fil) {

            case 'image':
            default:
                $file = $this->getImage();
        }
        return !$file ? null : $this->getUploadDir() . '/' . $file;
    }

    protected function getUploadRootDir() {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__ . '/../../../../web/' . $this->getUploadDir();
    }

    protected function getUploadDir() {
        return 'uploads/kategorie_szkolen/' . $this->getId();
    }

    /**
     * Set image
     *
     * @param string $image
     * @return TrainingCategory
     */
    public function setImage($image) {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage() {
        return $this->image;
    }

    /**
     * Set keywords
     *
     * @param string $keywords
     * @return TrainingCategory
     */
    public function setKeywords($keywords) {
        $this->keywords = $keywords;

        return $this;
    }

    /**
     * Get keywords
     *
     * @return string
     */
    public function getKeywords() {
        return $this->keywords;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return TrainingCategory
     */
    public function setDescription($description) {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return TrainingCategory
     */
    public function setContent($content) {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent() {
        return $this->content;
    }

    /**
     * Set automaticSeo
     *
     * @param boolean $automaticSeo
     * @return TrainingCategory
     */
    public function setAutomaticSeo($automaticSeo) {
        $this->automaticSeo = $automaticSeo;

        return $this;
    }

    /**
     * Get automaticSeo
     *
     * @return boolean
     */
    public function getAutomaticSeo() {
        return $this->automaticSeo;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return TrainingCategory
     */
    public function setUpdated($updated) {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated() {
        return $this->updated;
    }

    /**
     * Set kolejnosc
     *
     * @param integer $kolejnosc
     * @return TrainingCategory
     */
    public function setKolejnosc($kolejnosc) {
        $this->kolejnosc = $kolejnosc;

        return $this;
    }

    /**
     * Get kolejnosc
     *
     * @return integer
     */
    public function getKolejnosc() {
        return $this->kolejnosc;
    }

    /**
     * Set color
     *
     * @param string $color
     * @return TrainingCategory
     */
    public function setColor($color) {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color
     *
     * @return string
     */
    public function getColor() {
        return $this->color;
    }
}