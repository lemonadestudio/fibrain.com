<?php

namespace Cms\ElmatBundle\Entity;

use Sonata\UserBundle\Entity\BaseGroup as BaseGroup;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Cms\ElmatBundle\Repository\UserGroupRepository")
 * @ORM\Table(name="user_group")
 * @ORM\HasLifecycleCallbacks()
 */
class UserGroup extends BaseGroup {
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * Get id
     *
     * @return integer $id
     */
    public function getId() {
        return $this->id;
    }
}