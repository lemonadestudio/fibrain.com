<?php

namespace Cms\ElmatBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="newsletter_sending")
 */
class NewsletterSending {
    const STATUS_SENT                 = 0;
    const STATUS_ERROR                = 1;
    const STATUS_POMINIETY_BRAK_KONTA = 2;
    const STATUS_POMINIETY_FILTRY     = 3;

    /**
     * @Orm\Id
     * @Orm\Column(type="integer")
     * @Orm\GeneratedValue
     */
    private $id;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $data;

    /** @ORM\Column(type="integer", nullable=true) */
    protected $status;

    /** @ORM\ManyToOne(targetEntity="Newsletter", inversedBy="sendings" ) */
    protected $newsletter;

    /** @ORM\ManyToOne(targetEntity="Subscription" ) */
    protected $subscription;

    /**
     * @var \DateTime $created
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $created;

    /**
     * @var \DateTime $updated
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $updated;

    public function __construct() {
        $this->status = self::STATUS_SENT;
    }


    public static function getStatusChoices() {
        return array(
            NewsletterSending::STATUS_ERROR => 'Błąd',
            NewsletterSending::STATUS_SENT => 'OK',
            NewsletterSending::STATUS_POMINIETY_BRAK_KONTA => 'Pomijam - brak konta',
            NewsletterSending::STATUS_POMINIETY_FILTRY => 'Pomijam - niepasujące filtry',
        );
    }

    public function getStatusTxt() {

        $arr = self::getStatusChoices();

        return $arr[$this->getStatus()];
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set data
     *
     * @param \DateTime $data
     * @return NewsletterSending
     */
    public function setData($data) {
        $this->data = $data;

        return $this;
    }

    /**
     * Get data
     *
     * @return \DateTime
     */
    public function getData() {
        return $this->data;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return NewsletterSending
     */
    public function setStatus($status) {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus() {
        return $this->status;
    }

    /**
     * Set newsletter
     *
     * @param \Cms\ElmatBundle\Entity\Newsletter $newsletter
     * @return NewsletterSending
     */
    public function setNewsletter(Newsletter $newsletter = null) {
        $this->newsletter = $newsletter;

        return $this;
    }

    /**
     * Get newsletter
     *
     * @return \Cms\ElmatBundle\Entity\Newsletter
     */
    public function getNewsletter() {
        return $this->newsletter;
    }

    /**
     * Set subscription
     *
     * @param \Cms\ElmatBundle\Entity\Subscription $subscription
     * @return NewsletterSending
     */
    public function setSubscription(Subscription $subscription = null) {
        $this->subscription = $subscription;

        return $this;
    }

    /**
     * Get subscription
     *
     * @return \Cms\ElmatBundle\Entity\Subscription
     */
    public function getSubscription() {
        return $this->subscription;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return NewsletterSending
     */
    public function setCreated($created) {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated() {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return NewsletterSending
     */
    public function setUpdated($updated) {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated() {
        return $this->updated;
    }
}