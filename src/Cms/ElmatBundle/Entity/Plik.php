<?php

namespace Cms\ElmatBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 *
 * @ORM\Table(name="plik");
 * @ORM\Entity();
 * @ORM\HasLifecycleCallbacks();
 */
class Plik {
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=1024, nullable=true)
     */
    private $sciezka;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nazwa;

    /**
     * @ORM\Column(type="string", length=5, nullable=true)
     */
    private $rozszerzenie;

    /**
     * @ORM\Column(type="integer",  nullable=true)
     */
    private $rozmiar;


    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $kolejnosc;

    /**
     * @ORM\ManyToOne(targetEntity="Product", inversedBy="pliki")
     */
    private $product;

    /**
     * @ORM\ManyToOne(targetEntity="News", inversedBy="pliki")
     */
    private $news;

    /**
     * @ORM\ManyToOne(targetEntity="Article", inversedBy="pliki")
     */
    private $article;

    /**
     * @ORM\ManyToOne(targetEntity="Page", inversedBy="pliki")
     */
    private $page;

    /**
     * @ORM\OneToOne(targetEntity="Zasob", mappedBy="plik")
     */
    private $zasob;

    /**
     * @var |DateTime $updated
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated;

    /**
     * @var |DateTime $created
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created;


    public function __construct() {
        $this->kolejnosc = 0;
        $this->updated = new \DateTime();
        $this->created = new \DateTime();

    }

    public function __toString() {
        return $this->getNazwa();
    }


    public function getNazwaPobierz() {
        $nazwa = $this->getNazwa() . ' (' . number_format($this->getRozmiarMB(), 2, ',', '') . ' MB)';

        return $nazwa;
    }

    public function getNazwaRoz() {
        return $this->getNazwa() . '.' . $this->getRozszerzenie();
    }

    public function getRozmiarMBFormat() {
        return number_format($this->getRozmiarMB(), 2, ',', '') . ' MB';
    }


    public function getRozmiarMB() {
        return $this->getRozmiar() / (1024 * 1024);
    }

    public function getRozmiarKB() {
        return $this->getRozmiar() / (1024 * 1024);
    }

    public function getPelnaSciezka() {
        return __DIR__ . '/../../../../web' . $this->getSciezka();
    }


    /**
     * @ORM\PreUpdate()
     * @ORM\PrePersist()
     */
    public function preSave() {
//         $this->nazwa = Urlizer::utf8ToAscii($this->nazwa, ' ' );;
//         $this->rozszerzenie =  Urlizer::transliterate($this->rozszerzenie, '' );
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set sciezka
     *
     * @param string $sciezka
     * @return Plik
     */
    public function setSciezka($sciezka) {
        $this->sciezka = $sciezka;

        return $this;
    }


    /**
     * Get sciezka
     *
     * @return string
     */
    public function getSciezka() {
        return $this->sciezka;
    }

    /**
     * Set nazwa
     *
     * @param string $nazwa
     * @return Plik
     */
    public function setNazwa($nazwa) {
        $this->nazwa = $nazwa;

        return $this;
    }

    /**
     * Get nazwa
     *
     * @return string
     */
    public function getNazwa() {
        return $this->nazwa;
    }

    /**
     * Set rozszerzenie
     *
     * @param string $rozszerzenie
     * @return Plik
     */
    public function setRozszerzenie($rozszerzenie) {
        $this->rozszerzenie = $rozszerzenie;

        return $this;
    }

    /**
     * Get rozszerzenie
     *
     * @return string
     */
    public function getRozszerzenie() {
        return $this->rozszerzenie;
    }

    /**
     * Set rozmiar
     *
     * @param integer $rozmiar
     * @return Plik
     */
    public function setRozmiar($rozmiar) {
        $this->rozmiar = $rozmiar;

        return $this;
    }

    /**
     * Get rozmiar
     *
     * @return integer
     */
    public function getRozmiar() {
        return $this->rozmiar;
    }

    /**
     * Set kolejnosc
     *
     * @param integer $kolejnosc
     * @return Plik
     */
    public function setKolejnosc($kolejnosc) {
        $this->kolejnosc = $kolejnosc;

        return $this;
    }

    /**
     * Get kolejnosc
     *
     * @return integer
     */
    public function getKolejnosc() {
        return $this->kolejnosc;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return Plik
     */
    public function setUpdated($updated) {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated() {
        return $this->updated;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Plik
     */
    public function setCreated($created) {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated() {
        return $this->created;
    }

    /**
     * Set product
     *
     * @param \Cms\ElmatBundle\Entity\Product $product
     * @return Plik
     */
    public function setProduct(Product $product = null) {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return \Cms\ElmatBundle\Entity\Product
     */
    public function getProduct() {
        return $this->product;
    }

    /**
     * Set news
     *
     * @param \Cms\ElmatBundle\Entity\News $news
     * @return Plik
     */
    public function setNews(News $news = null) {
        $this->news = $news;

        return $this;
    }

    /**
     * Get news
     *
     * @return \Cms\ElmatBundle\Entity\News
     */
    public function getNews() {
        return $this->news;
    }

    /**
     * Set article
     *
     * @param \Cms\ElmatBundle\Entity\Article $article
     * @return Plik
     */
    public function setArticle(Article $article = null) {
        $this->article = $article;

        return $this;
    }

    /**
     * Get article
     *
     * @return \Cms\ElmatBundle\Entity\Article
     */
    public function getArticle() {
        return $this->article;
    }

    /**
     * Set page
     *
     * @param \Cms\ElmatBundle\Entity\Page $page
     * @return Plik
     */
    public function setPage(Page $page = null) {
        $this->page = $page;

        return $this;
    }

    /**
     * Get page
     *
     * @return \Cms\ElmatBundle\Entity\Page
     */
    public function getPage() {
        return $this->page;
    }

    /**
     * Set zasob
     *
     * @param \Cms\ElmatBundle\Entity\Zasob $zasob
     * @return Plik
     */
    public function setZasob(Zasob $zasob = null) {
        $this->zasob = $zasob;

        return $this;
    }

    /**
     * Get zasob
     *
     * @return \Cms\ElmatBundle\Entity\Zasob
     */
    public function getZasob() {
        return $this->zasob;
    }
}