<?php
namespace Cms\ElmatBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="Cms\ElmatBundle\Repository\ZasobRepository")
 * @ORM\Table(name="zasob")
 * @ORM\HasLifecycleCallbacks()
 * @Gedmo\SoftDeleteable(fieldName="deletedAt")
 */
class Zasob {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @var string $title
     *
     * @ORM\Column(type="string", length=255);
     * @Assert\NotNull();
     * @Assert\Length(max = 255);
     */
    private $title;

    /**
     * @var \DateTime $deletedAt
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @ORM\Column(type="boolean")
     */
    private $published = true;

    /**
     *
     * @var \DateTime $publishDate ;
     * @ORM\Column(type="datetime", nullable=true);
     */
    private $publishDate;

    /**
     * @var string $description
     *
     * @ORM\Column(type="string", length=2048, nullable=true)
     * @Assert\Length(max = 2048)
     */
    private $description;

    /**
     * @var \DateTime $created
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created;

    /**
     * @var \DateTime $updated
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated;


    /** @Assert\File(maxSize="6000000") */
    public $_file_image;

    public $_delete_file_image = false;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $highlight_mainpage;

    /**
     * @ORM\ManyToOne(targetEntity="ZasobCategory", inversedBy="zasoby")
     */
    protected $category;

    /**
     * @ORM\OneToOne(targetEntity="Plik", inversedBy="zasob", cascade={"persist", "remove"})
     * @ORM\OrderBy({"kolejnosc" = "ASC"})
     */
    private $plik;

    public function __construct() {
        $this->published = true;
        $this->automaticSeo = true;
        $this->publishDate = new \DateTime();
        $this->locale = 'pl';
        $this->plik = new Plik();
        $this->plik->setRozszerzenie('pdf');
    }

    public function __toString() {
        return '' . $this->getTitle();
    }

    /**
     * @ORM\PrePersist();
     * @ORM\PreUpdate();
     */
    public function setCreatedValue() {

    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function prepareUploadedNames() {
        foreach (array('image') as $_fi) {
            $_file = '_file_' . $_fi;
            if (null !== $this->{$_file}) {
                $this->{'old' . $_file} = $this->getAbsolutePath($_fi);
                $this->{'oldcrop' . $_file} = $this->getAbsolutePath($_fi, true);
                $this->{$_fi} = $_fi . '-' . uniqid() . '.' . $this->{$_file}->guessExtension();
            } elseif (true == $this->{'_delete' . $_file}) {
                @unlink($this->getAbsolutePath($_fi));
                @unlink($this->getAbsolutePath($_fi, true));
                $this->{$_fi} = null;
            }
        }
    }

    /**
     *
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function storeUploadedFiles() {

        foreach (array('image') as $_fi) {

            $_file = '_file_' . $_fi;
            if (null === $this->{$_file}) {
                continue;
            }
            $this->{$_file}->move($this->getUploadRootDir(), $this->{$_fi});
            if ($this->{'old' . $_file}) {
                @unlink($this->{'old' . $_file});
            }

            if ($this->{'oldcrop' . $_file}) {
                @unlink($this->{'oldcrop' . $_file});
            }

            unset($this->{$_file});
        }

    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUploadedFiles() {
        if ($file = $this->getAbsolutePath('image')) {
            @unlink($file);
        }

    }


    public function getAbsolutePath($_fil, $crop = false) {
        $file = null;
        switch ($_fil) {

            case 'image':
            default:
                $file = $this->getImage();
        }
        return !$file ? null : $this->getUploadRootDir() . '/' . ($crop ? 'crop-' : '') . $file;
    }

    public function getWebPath($_fil = 'image') {
        $file = null;
        switch ($_fil) {

            case 'image':
            default:
                $file = $this->getImage();
        }
        return !$file ? null : $this->getUploadDir() . '/' . $file;
    }

    public function getWebPathCrop() {
        if (file_exists($this->getUploadRootDir() . '/crop-' . $this->getImage())) {
            return $this->getUploadDir() . '/crop-' . $this->getImage();
        } else {
            return $this->getWebPath();
        }
    }

    public function getUploadRootDir() {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__ . '/../../../../web/' . $this->getUploadDir();
    }

    public function getUploadDir() {
        return 'uploads/zasoby/' . $this->getId();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }


    /**
     * Set title
     *
     * @param string $title
     * @return Zasob
     */
    public function setTitle($title) {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * Set published
     *
     * @param boolean $published
     * @return Zasob
     */
    public function setPublished($published) {
        $this->published = $published;

        return $this;
    }

    /**
     * Get published
     *
     * @return boolean
     */
    public function getPublished() {
        return $this->published;
    }

    /**
     * Set publishDate
     *
     * @param \DateTime $publishDate
     * @return Zasob
     */
    public function setPublishDate($publishDate) {
        $this->publishDate = $publishDate;

        return $this;
    }

    /**
     * Get publishDate
     *
     * @return \DateTime
     */
    public function getPublishDate() {
        return $this->publishDate;
    }


    /**
     * Set description
     *
     * @param string $description
     * @return Zasob
     */
    public function setDescription($description) {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription() {
        return $this->description;
    }


    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Zasob
     */
    public function setCreated($created) {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated() {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return Zasob
     */
    public function setUpdated($updated) {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated() {
        return $this->updated;
    }


    /**
     * Set image
     *
     * @param string $image
     * @return Zasob
     */
    public function setImage($image) {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage() {
        return $this->image;
    }


    /**
     * Set highlight_mainpage
     *
     * @param boolean $highlightMainpage
     * @return Zasob
     */
    public function setHighlightMainpage($highlightMainpage) {
        $this->highlight_mainpage = $highlightMainpage;

        return $this;
    }

    /**
     * Get highlight_mainpage
     *
     * @return boolean
     */
    public function getHighlightMainpage() {
        return $this->highlight_mainpage;
    }


    /**
     * Set category
     *
     * @param \Cms\ElmatBundle\Entity\ZasobCategory $category
     */
    public function setCategory(ZasobCategory $category) {
        $this->category = $category;
    }

    /**
     * Get category
     *
     * @return \Cms\ElmatBundle\Entity\ZasobCategory
     */
    public function getCategory() {
        return $this->category;
    }


    /**
     * Set plik
     *
     * @param \Cms\ElmatBundle\Entity\Plik $plik
     * @return Zasob
     */
    public function setPlik(Plik $plik = null) {
        $this->plik = $plik;

        return $this;
    }

    /**
     * Get plik
     *
     * @return \Cms\ElmatBundle\Entity\Plik
     */
    public function getPlik() {
        return $this->plik;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     * @return Zasob
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime 
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }
}