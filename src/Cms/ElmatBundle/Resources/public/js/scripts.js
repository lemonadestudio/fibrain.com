
var main_slider_interval = null;

$(function(){

	$('.gallery_photo').colorbox({
		rel: '.gallery_photo',
		maxWidth: '90%',
		maxHeight: '90%',
		// transition:"fade",
		// slideshow: true,
		// slideshowAuto: false
        onComplete: function() {
            var currentPhoto = $(this).attr('data-current');
            var totalPhotos = $(this).attr('data-total');

            if (currentPhoto > 0 && totalPhotos > 0) {
                $('#cboxCurrent').html("Image "+currentPhoto+" of "+totalPhotos);
            }
        },
    });

	$('.bc-controlls .bc-left').click(function(){
		zmieniarkaZmien($(this).parents('.box-carousel'), 1);
	});

	$('.bc-controlls .bc-right').click(function(){
		zmieniarkaZmien($(this).parents('.box-carousel'), -1);
	});

	$('.bc3-controlls .bc3-right').click(function(){
		sliderSlide($(this).parents('.box-carousel3'), -1);
	});

	$('.bc3-controlls .bc3-left').click(function(){
		sliderSlide($(this).parents('.box-carousel3'), 1);
	});


	$('#highlights_changer a').click(function(){
		rel_id = $(this).attr('rel');

		$('.highlight-main, #highlights_changer > a').removeClass('active');

		$(this).addClass('active');
		$('#' + rel_id).addClass('active');
	});



	$('.main-slider .controls .ctrl').click(function(){
		_par = $(this).parent();
		_zm = _par.parent();
		_curr_idx = _zm.attr('data-curr');
		_idx = $(this).attr('data-idx');
		_diff = _idx - _curr_idx;
		zmieniarkaZmien(_zm, _diff, adjustMainSliderControlls);
		mainSliderStart();

	});

	mainSliderStart();

	sliderSlide($('#hp_rozw .box-carousel3'), 0);
	sliderSlide($('#hp_usl .box-carousel3'), 0);

//	if(items_count > 0) {
//
//		curr_item = items_count -1;
//		zmieniarkaZmien();
//		zmien_timer = setInterval(function(){
//			 zmieniarkaZmien();
//		}, 3000);
//
//	}

	adjustTopMenu();


    /*
    var top_search_keyword_default = $("#top_search_keyword").val();
    $("#top_search_keyword").focus(function(){
       if($(this).val()==top_search_keyword_default)
       {
            $(this).val("");
       }
    });
    $("#top_search_keyword").blur(function(){
       if($(this).val()=="")
       {
            $(this).val(top_search_keyword_default);
       }
    });
    */

	var footer_newsletter_email_default = $("#footer_newsletter_email").val();
    $("#footer_newsletter_email").focus(function(){
       if($(this).val()==footer_newsletter_email_default)
       {
            $(this).val("");
       }
    });
    $("#footer_newsletter_email").blur(function(){
       if($(this).val()=="")
       {
            $(this).val(footer_newsletter_email_default);
       }
    });

    $('.prod-fold-elements .prod-fold-elem .title').click(function(){
    	$(this).parent().toggleClass('folded');
    });

    $(document).on("click", "#terminy_calendar .controls .changedate", function(){
    	if($(this).hasClass('inactive'))
    		return;

    	_par = $(this).parent();
    	var _date = '';

    	if($(this).hasClass('lft')) {
    		_date = _par.attr('data-prev');
    	} else {
    		_date = _par.attr('data-next');
    	}

    	$('#terminy_calendar').addClass('loading');

    	$.ajax({
    		url: url_training_calendar,
    		data: {
    			date: _date
    		},
    		success: function(ret) {
    			$('#terminy_calendar').html(ret);
    			$('#terminy_calendar').removeClass('loading')
    		}
    	});

    });

    $(document).on('click', '#terminy_calendar .calendar .termin', function(){

    	sz = $(this).attr('data-szkolenie');
    	sz = sz?sz:'';

    	d = $(this).attr('data-data');
    	d = d?d:'';

    	var params = {
    			setfilter: 1,
    			date: d,
    			szkolenie: sz
    	};

    	window.location = url_training_terminy + '?' + $.param(params);

    });
    
    var menu_show_timeout = null;
    var menu_hide_timeout = null;
    
//    $(document).on('mouseenter', '#container #top #menu-top > li.drop', function(e) {
//        e.stopPropagation();
//    });
    
    
    $(document).on('mouseenter', '#container #top #menu-top > li.drop', function(e){
        e.stopPropagation();
        
        if( $(this).hasClass('active') ) {
            window.clearTimeout(menu_show_timeout);
            window.clearTimeout(menu_hide_timeout);
            return;
        }
        
        if( $('#container #top #menu-top > li.drop.active').size() == 0 ) {
            $(this).addClass('active');
            
        } else {
            var liel = $(this);
 
            window.clearTimeout(menu_hide_timeout);
            window.clearTimeout(menu_show_timeout);
            menu_show_timeout = window.setTimeout(function(){

               $('#container #top #menu-top > li.drop').removeClass('active');
               liel.addClass('active');

            }, 150);
        }
        
        
        
    });
    
    $(document).on('mouseleave', '#container #top #menu-top > li.drop', function(e){
        e.stopPropagation();
        
        window.clearTimeout(menu_hide_timeout);
        menu_hide_timeout = window.setTimeout(function(){

            $('#container #top #menu-top > li.drop').removeClass('active');
        }, 150);
    });
    
    $(document).on('mousemove', '#container #top #menu-top > li.drop > .submenu', function(e){
       
        window.clearTimeout(menu_hide_timeout);
        window.clearTimeout(menu_show_timeout);
        
         e.stopPropagation();
    });
    
    // b. scripts
    $.each($('.product-web > .row'), function() {
        var height = 0;
        $.each($(this).find('.item'), function() {
            if ($(this).height() > height) {
                height = $(this).height();
            }
        });
        $(this).find('.item').height(height);
    });
    var product_articles = 0;
    $.each($('.product-articles > .item'), function() {
        if ($(this).height() > product_articles) {
            product_articles = $(this).height();
        }
    });
    $('.product-articles > .item').height(product_articles);
    var promotion_articles = 0;
    $.each($('.news-promotions > .item'), function() {
        if ($(this).height() > promotion_articles) {
            promotion_articles = $(this).height();
        }
    });
    $('.news-promotions > .item').height(promotion_articles);
//    var suplies_articles = 0;
//    $.each($('.news-suplies > .item'), function() {
//        if ($(this).height() > suplies_articles) {
//            suplies_articles = $(this).height();
//        }
//    });
//    $('.news-suplies > .item').height(suplies_articles);
    var slider_box = 0;
    $.each($('.events-box > .slider > .item'), function() {
        if ($(this).height() > slider_box) {
            slider_box = $(this).height();
        }
    });
    $('.events-box > .slider').height(slider_box);
    var slider_fibrain = 0;
    $.each($('.news-products > .slider > .item'), function() {
        if ($(this).height() > slider_fibrain) {
            slider_fibrain = $(this).height();
        }
    });
    $('.news-products > .slider').height(slider_fibrain);
    var slider_news = 0;
    $.each($('.news-box > .slider > .item'), function() {
        if ($(this).height() > slider_news) {
            slider_news = $(this).height();
        }
    });
    var slider_realization = 0;
    $.each($('.realization-box > .slider > .item'), function() {
        if ($(this).height() > slider_realization) {
            slider_realization = $(this).height();
        }
    });
    if (slider_news > slider_realization) {
        $('.news-box > .slider').height(slider_news);
        $('.realization-box > .slider').height(slider_news);
    } else {
        $('.news-box > .slider').height(slider_realization);
        $('.realization-box > .slider').height(slider_realization);
    }
    $.each($('.tech-articles > .row'), function() {
        var height = 0;
        $.each($(this).find('.item'), function() {
            if ($(this).height() > height) {
                height = $(this).height();
            }
        });
        $(this).find('.item').height(height);
    });
    
    $(document).ready(function() {
        $("select").select2();
    });
    
    $('.news .news-title li').on('click', function(e, f){
        var $this = $(this);
        var $categories = $this.parents('.news').find('.category');
        
        $this.parents('ul').find('li').removeClass('selected');
        $this.addClass('selected');
        
        $categories.hide();
        $($categories.get($this.index())).show();
    });
    
    $('.sideBar').on({
        mouseenter: function () {
            if ($(this).hasClass('left'))
                $(this).stop().animate({left: 0}, 500);
            else if ($(this).hasClass('right'))
                $(this).stop().animate({right: 0}, 500);
        },
        mouseleave: function () {
            var animWidth = $(this).attr('data-anim-width');

            if ($(this).hasClass('left'))
                $(this).stop().animate({left: animWidth * -1}, 500);
            else if ($(this).hasClass('right'))
                $(this).stop().animate({right: animWidth * -1}, 500);
        }
    });

    $('.newsletterWithFacebook').on('submit', '.newsletter', function(e){
        e.preventDefault();
        
        var $this = $(this);
        
        $.ajax({
            url: $this.attr('action'),
            type: 'POST',
            data: $this.serialize(),
            success: function(response) {
                var $container = $('.newsletterWithFacebook');
                
                $container.find('.newsletter').remove();
                $container.find('.content').prepend(response);
                $container.find('input[type="text"]').val('');
            },
            error: function() {
                alert('Wystąpił. Proszę spróbować ponownie.');
            }
        });
    });
});

function adjustTopMenu() {

	ul_0 = $('#menu_main > ul');
	li_1 = $('#menu_main > ul > li');
	ul_0_pos = ul_0.position();
	li_1.each(function(){

		ul_1 = $(this).find('ul.menu_level_1');

		if(ul_1.size() > 0) {

			li_1 = ul_1.children();
			if(li_1.size() >= 3) {
				ul_1.addClass('two-column');
				for(i=0; i<li_1.size(); i++) {
					__li = li_1.eq(i);
					__li.addClass(i%2 == 0 ? 'left' : 'right');
				}
			} else {
				ul_1.addClass('one-column');
			}
		}

		ul_1 = $(this).find('ul.menu_level_1');

		if(ul_1.size() > 0) {


			_pos = $(this).position();
			_off = $(this).offset();
			_ul_pos = ul_1.position();
			_wid = ul_1.width();
			// console.log(ul_0_pos.left, _pos.left, _off.left, _ul_pos.left, _wid, ul_0_pos.left + _pos.left + _wid);



			if(ul_0_pos.left + _pos.left + _wid > 937) {
				 ul_1.css('left', (937 - (ul_0_pos.left + _pos.left + _wid)) + 'px');
			}


		}

	});

}

function mainSliderStart() {
	clearInterval(main_slider_interval);
	main_slider_interval = setInterval(function(){
		 zmieniarkaZmien($('.main-slider'), 1, adjustMainSliderControlls);
	}, cms_config.ust_zmieniarka_strona_glowna_interwal);
}

function adjustMainSliderControlls() {

	_curr_idx = $('.main-slider').attr('data-curr');
	$('.main-slider .controls .ctrl').removeClass('current')
	.eq(_curr_idx).addClass('current');

}

function cloneFormRow(o)
{
    var new_o = o.prev(".form-row").clone(true);
    o.prev(".form-row").removeClass("last");
    o.before(new_o);


}


function zmieniarkaZmien(carousel, step, callback) {

	var next_item;
	var curr_item;
	var prev_item;
	var items = null;
	var items_count = 0;
	var zmien_timer = null;

	if(typeof step == 'undefined') {
		step = 1;
	}

	curr_item = parseInt(carousel.attr('data-curr'));
	if(isNaN(curr_item)) {
		curr_item = 0;
	}
	items = carousel.find('.box-item');
	items_count = items.size();

	if(items_count <= 1 )
		return;

//	if (zmien_timer != null) {
//		clearInterval(zmien_timer);
//		zmien_timer = setInterval(function(){
//			 zmieniarkaZmien();
//		}, 3000);
//	}

	curr_item = (curr_item + items_count + step) % items_count;
	next_item = (curr_item + 1) % items_count;
	prev_item = (curr_item + items_count - 1) % items_count;

	items.removeClass('current');
	items.slice(curr_item, curr_item + 1).addClass('current');

	items.removeClass('next');
	items.slice(next_item, next_item + 1).addClass('next');

	items.removeClass('prev');
	items.slice(prev_item, prev_item + 1).addClass('prev');

	carousel.attr('data-curr', curr_item);

	if(typeof callback == 'function') {
		callback();
	}

}

function sliderSlide(carousel, step) {

	var item_width = 205;
	var items = null;
	var items_count = 0;
	var min_pos = 0;
	var max_pos = 0;
	var slider = null;

	items = carousel.find('.box-uitem');
	items_count = items.size();

	if(items_count == 0)
		return ;

	min_pos = -((items_count-3) * 205) ;

	slider = carousel.find('.bc3-slider');

	pos = slider.position().left;

	next_pos = pos + step * item_width;

	if(next_pos < min_pos)
		next_pos = min_pos;

	if(next_pos > max_pos)
		next_pos = max_pos;

	slider.css('left', next_pos + 'px');

	carousel.find('.bc3-right').show();
	carousel.find('.bc3-left').show();

	if(next_pos == min_pos)
		carousel.find('.bc3-right').hide();

	if(next_pos == max_pos)
		carousel.find('.bc3-left').hide();

}

function clearFields(selector) {

	item = $(selector);

	item.find('input[type="text"], input[type="password"], input[type="email"], select').val('');
	item.find('input[type="checkbox"]').removeAttr('checked');

}



