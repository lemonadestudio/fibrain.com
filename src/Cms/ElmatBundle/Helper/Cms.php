<?php

namespace Cms\ElmatBundle\Helper;

use Gedmo\Sluggable\Util\Urlizer;
use Cms\ElmatBundle\Model\Domain;
use Symfony\Component\DependencyInjection\ContainerInterface;

class Cms {

    /**
     * @var ContainerInterface
     */
    private $container = null;

    public function __construct(ContainerInterface $container = null) {
        $this->container = $container;
    }

    public static function getOfferGroups() {
        $offer_groups = array(
            array(
                'slug' => 'structural-cabling',
                'symbol' => 'structural-elmat',
                'symbol2' => 'structural_cabling',
                'role' => 'ROLE_GRUPA_STRUCTURAL',
            ),
            array(
                'slug' => 'fiber-optic-cables',
                'symbol' => 'fiber-elmat',
                'symbol2' => 'fiber_optic',
                'role' => 'ROLE_GRUPA_FIBER'
            ),
            array(
                'slug' => 'fttx-systems-and-elements',
                'symbol' => 'fttx-elmat',
                'symbol2' => 'fttx_systems',
                'role' => 'ROLE_GRUPA_FTTX'
            ),
            array(
                'slug' => 'passive-optical-technology',
                'symbol' => 'passive-elmat',
                'symbol2' => 'passive_optical',
                'role' => 'ROLE_GRUPA_PASSIVE'
            ),
            array(
                'slug' => 'active-elements-and-systems',
                'symbol' => 'active-elmat',
                'symbol2' => 'active_elements',
                'role' => 'ROLE_GRUPA_ACTIVE'
            ),
            array(
                'slug' => 'connectivity',
                'symbol' => 'connectivity-elmat',
                'symbol2' => 'connectivity',
                'role' => 'ROLE_GRUPA_CONNECTIVITY'
            ),
            array(
                'slug' => 'distribution',
                'symbol' => 'distribution-elmat',
                'symbol2' => 'distribution',
                'role' => 'ROLE_GRUPA_DISTRIBUTION'
            ),
            array(
                'slug' => 'photonics',
                'symbol' => 'photonics-elmat',
                'symbol2' => 'photonics',
                'role' => 'ROLE_GRUPA_PHOTONICS'
            ),
        );

        return $offer_groups;
    }


    public function make_url($slug, $strategy = 'default') {

        $options = array(
            'separator' => '-'
        );

        // kod zaczerpnięty z Gedmo Doctrine Extensions

        // build the slug
        // Step 1: transliteration, changing 北京 to 'Bei Jing'
        $slug = call_user_func_array(
            array('Gedmo\Sluggable\Util\Urlizer', 'transliterate'),
            array($slug, $options['separator'])
        );
        // Step 2: urlization (replace spaces by '-' etc...)

        $slug = Urlizer::urlize($slug, $options['separator']);


        // stylize the slug
        switch ($strategy) {
            case 'camel':
                $slug = preg_replace_callback('/^[a-z]|' . $options['separator'] . '[a-z]/smi', function ($m) {
                    return strtoupper($m[0]);
                }, $slug);
                break;

            case 'lower':
                if (function_exists('mb_strtolower')) {
                    $slug = mb_strtolower($slug);
                } else {
                    $slug = strtolower($slug);
                }
                break;

            case 'upper':
                if (function_exists('mb_strtoupper')) {
                    $slug = mb_strtoupper($slug);
                } else {
                    $slug = strtoupper($slug);
                }
                break;

            default:
                // leave it as is
                break;
        }

        return $slug;
    }


    public function _old_make_url($string, $strategy = 'default') {


        if (!in_array($strategy, array('default'))) {
            $strategy = 'default';
        }

        $clean = $string;

        switch ($strategy) {

            case 'default':

                $delimiter = '-';

                $string = str_replace(
                    array('Ą', 'Ć', 'Ę', 'Ł', 'Ń', 'Ó', 'Ś', 'Ź', 'Ż', 'ą', 'ć', 'ę', 'ł', 'ń', 'ó', 'ś', 'ź', 'ż'),
                    array('a', 'c', 'e', 'l', 'n', 'o', 's', 'z', 'z', 'a', 'c', 'e', 'l', 'n', 'o', 's', 'z', 'z'),
                    $string);
                $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $string);
                $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
                $clean = strtolower(trim($clean, '-'));
                $clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);


                break;

            default:
                break;

        }

        return $clean;

    }


    /**
     * Zwraca domeny z konfiguracji cms
     */
    public function domains() {

        $domains = $this->container->parameters['cms_elmat.domains'];

        return $domains;

    }

    /**
     * Zwraca informacjie o aktualnej domenie
     * @return Domain
     */
    public function domain() {

        $domains = $this->domains();
        $domain = $this->container->get('request')->attributes->get('_domain');
        return new Domain($domains[$domain]);

    }


    public function parseWidthCss($width_css) {
        $width_css = trim($width_css);
        if ($width_css) {
            $ret = '';
            $unit = 'px';
            if (preg_match('/%/', $width_css)) {
                $unit = '%';
            } elseif (preg_match('/em/', $width_css)) {
                $unit = 'em';
            }
            $ret = intval($width_css) . $unit;
        } else {
            $ret = $width_css;
        }

        return $ret;
    }


}