<?php

namespace Cms\ElmatBundle\Listener;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\EventDispatcher\Event;


class DomainListener {

    /**
     * ustawia parametr _domain dla requestu
     * na podstawie dostępnych opcji ( jeżeli domena nie jest skonfigurowana
     * - zostanie wyrzucony wyjątek
     * @param Event $event
     * @throws NotFoundHttpException
     */
    public function onDomainParse(Event $event) {
        $request = $event->getRequest();
        $session = $request->getSession();

        $parameters = $event->getDispatcher()->getContainer()->parameters;

        // skonfigurowane domeny
        $domeny = $parameters['cms_elmat.domains'];

        $host = $request->getHost();

        $symbol = '';
        $match = false;
        $match_domena = false;
        foreach ($domeny as $k => $domena) {
            if ($domena['domain'] == $host || 'www.' . $domena['domain'] == $host) {
                $match = $domena['domain'];
                $match_domena = $domena;
                break;
            }
        }

        if ($match) {
            $request->attributes->set('_domain', $match);
            $request->attributes->set('_domain_symbol', $match_domena['symbol']);

            $_symbol = $match_domena['symbol'];
            $_symbol2 = '';

            $slug = '';
            $og_id = 0;

            switch ($_symbol) {
                case 'structural-elmat':
                    $og_id = 1;
                    $slug = 'structural-cabling';
                    $_symbol2 = 'structural_cabling';
                    break;
                case 'fiber-elmat':
                    $og_id = 2;
                    $slug = 'fiber-optic-cables';
                    $_symbol2 = 'fiber_optic';
                    break;
                case 'fttx-elmat':
                    $og_id = 3;
                    $slug = 'fttx-systems-and-elements';
                    $_symbol2 = 'fttx_systems';
                    break;
                case 'passive-elmat':
                    $og_id = 4;
                    $slug = 'passive-optical-technology';
                    $_symbol2 = 'passive_optical';
                    break;
                case 'active-elmat':
                    $og_id = 5;
                    $slug = 'active-elements-and-systems';
                    $_symbol2 = 'active_elements';
                    break;
                case 'connectivity-elmat':
                    $og_id = 6;
                    $slug = 'connectivity';
                    $_symbol2 = 'connectivity';
                    break;
                case 'distribution-elmat':
                    $og_id = 7;
                    $slug = 'distribution';
                    $_symbol2 = 'distribution';
                    break;
                case 'photonics-elmat':
                    $og_id = 8;
                    $slug = 'photonics';
                    $_symbol2 = 'photonics';
                    break;
            }

            $request->attributes->set('_domain_symbol_id', $og_id);
            $request->attributes->set('_domain_symbol_slug', $slug);
            $request->attributes->set('_domain_symbol2', $_symbol2);
        } else {
            // throw new NotFoundHttpException('Invalid domain. Check configuration');
        }

        $request->attributes->set('_domain_cookies', $parameters['host_cookies']);
        $request->attributes->set('_domain_elmat', $parameters['host_elmat']);

        //$session->set('_domain', $request->getHost());
    }

}
