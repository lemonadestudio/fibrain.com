<?php

namespace Cms\ElmatBundle\Util;

use Cms\ElmatBundle\Entity\EmailTemplate;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;


class EmailTemplates {

	/**
	 * @var EntityManager
	 */
	protected $em;

	/**
	 * @var EntityRepository
	 */
	protected $repo;

	protected $email_templates = array();

	protected $is_loaded = false;

	protected $default_email_templates = array(

	        'rejestracja_klienta_potwierdzenie_email' => array(
	                'description' => '
    	                <p>Wysyłane do klienta w celu potwierdzenia jego adresu Email.</p>
    	                <p>Dostępne zmienne:</p>
    	                <ul>

    	                    <li>{AKTYWACJA_URL} - Link do potwierdzenia adresu Email</li>
	                        <li>{LOGIN} - Login podany w formularzu</li>
	                        <li>{EMAIL} - Email podany w formularzu</li>
	                        <li>{FIRMA} - Nazwa firmy podana w formularzu</li>
	                        <li>{KOD_POCZTOWY} - Kod pocztowy podany w formularzu</li>
	                        <li>{MIASTO} - Miasto podane w formularzu</li>
	                        <li>{WOJEWODZTWO} - Województwo wybrane w formularzu</li>
	                        <li>{NIP} - NIP podany w formularzu</li>
	                        <li>{TELEFON} - Telefon podany w formularzu</li>
	                        <li>{FAX} - Fax podany w formularzu</li>

    	                </ul>
	                ',
	                'topic' => 'Potwierdzenie rejestracji klienta w systemie Elmat.pl',
	                'content' => '
	                    <p>Dziękujemy za rejestrację w naszym systemie</p>
	                    <p>Do aktywacji konta potrzebna jest weryfikacja adresu e-mail podanego podczas rejestracji</p>
	                    <p>Kliknij w ten link: <a href="{AKTYWACJA_URL}">{AKTYWACJA_URL}</a> aby potwierdzić swój adres e-mail</p>
	                    <p>Aby uzyskać dostęp do cenników konieczna jest również akceptacja Administratora</p>
	                    <p>Dane konta:</p>
	                    <ul>
	                        <li>Login: {LOGIN}</li>
	                        <li>Email: {EMAIL}</li>
	                        <li>Nazwa firmy: {FIRMA}</li>
	                        <li>Kod pocztowy: {KOD_POCZTOWY}</li>
	                        <li>Miasto: {MIASTO}</li>
	                        <li>Województwo: {WOJEWODZTWO}</li>
	                        <li>NIP: {NIP}</li>
	                        <li>Telefon: {TELEFON}</li>
	                        <li>Fax: {FAX}</li>
	                    </ul>
	                    ',
	                 ),

	        'rejestracja_klienta_nowy_klient_powiadomienie' => array(
	                'description' => '
    	                <p>Wysyłane do administracji w celu poinformowania o założeniu i potwierdzeniu nowego konta.</p>
    	                <p>Nowe konto można zablokować lub dodać odpowiednie uprawnienia np do cenników.</p>
    	                <ul>
    	                    <li>{EDYCJA_URL} - Link do edycji konta użytkownika</li>
	                        <li>{LOGIN} - Login podany w formularzu</li>
	                        <li>{EMAIL} - Email podany w formularzu</li>
	                        <li>{FIRMA} - Nazwa firmy podana w formularzu</li>
	                        <li>{KOD_POCZTOWY} - Kod pocztowy podany w formularzu</li>
	                        <li>{MIASTO} - Miasto podane w formularzu</li>
	                        <li>{WOJEWODZTWO} - Województwo wybrane w formularzu</li>
	                        <li>{NIP} - NIP podany w formularzu</li>
	                        <li>{TELEFON} - Telefon podany w formularzu</li>
	                        <li>{FAX} - Fax podany w formularzu</li>

    	                </ul>
	                ',
	                'topic' => 'Utworzono nowe konto klienta w systemie Elmat.pl',
	                'content' => '
	                    <p>Utworzono nowe konto, klient potwierdził adres email.</p>
	                    <p>Aby przejść do edycji kliknij w link: <a href="{EDYCJA_URL}">{EDYCJA_URL}</a></p>
	                    <p>Dane nowego konta:</p>
	                    <ul>
	                        <li>Login: {LOGIN}</li>
	                        <li>Email: {EMAIL}</li>
	                        <li>Nazwa firmy: {FIRMA}</li>
	                        <li>Kod pocztowy: {KOD_POCZTOWY}</li>
	                        <li>Miasto: {MIASTO}</li>
	                        <li>Województwo: {WOJEWODZTWO}</li>
	                        <li>NIP: {NIP}</li>
	                        <li>Telefon: {TELEFON}</li>
	                        <li>Fax: {FAX}</li>
	                    </ul>
	                    ',
	        ),



	        'newsletter_domyslny_szablon' => array(
	                'description' => '
    	                <p>Domyślny szablon newsletera używany przy dodawaniu nowego newslettera.</p>
    	                <p>Dostępne zmienne:.</p>
    	                <ul>
    	                    <li>{URL_REZYGNACJA} - link do wypisania się z newsletera</li>
    	                </ul>
	                ',
	                'topic' => 'Newsletter Elmat.pl',
	                'content' => '
	                    <p>Treść domyślna newsletera.</p>
	                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit,
	                        sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
	                        Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
	                        Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
	                        Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
	                <p><hr /></p>
	                <p>Aby wypisać się z newslettera kliknij w poniższy link: <a href="{URL_REZYGNACJA}">{URL_REZYGNACJA}</a></p>
	                    ',
	        ),

	        'newsletter_aktywacja_email' => array(
	                'description' => '
    	                <p>Email wysyłany w celu aktywacji subskrypcji newslettera</p>
    	                <p>Dostępne zmienne:.</p>
    	                <ul>
    	                    <li>{AKTYWACJA_URL} - link do aktywacji adresu się z newsletera</li>
    	                </ul>
	                ',
	                'topic' => 'Aktywacja subskrypcji newsletera Elmat.pl',
	                'content' => '
	                    <p>Dziękujemy za dokonanie subskrypcji.</p>
	                    <p>Do aktywacji konta potrzebna jest weryfikacja adresu e-mail </p>
	                <p><hr /></p>
	                <p>Aby potwierdzić proszę kliknąć w poniższy link: <a href="{AKTYWACJA_URL}">{AKTYWACJA_URL}</a></p>
	                    ',
	        ),
        
            'resetowanie_hasla_email' => array(
	                'description' => '
    	                <p>Email z linkiem do zresetowania hasla</p>
    	                <p>Dostępne zmienne:</p>
    	                <ul>
    	                    <li>{URL_ZMIANA_HASLA} - link do resetowania hasła</li>
                            <li>{IP} - IP z którego wysłane żądanie zmiany hasła</li>
    	                </ul>
	                ',
	                'topic' => 'Resetowanie hasła do konta Elmat.pl',
	                'content' => '
	                    <p>Na stronie Elmat.pl została użyta opcja resetowania hasła do konta.</p>
	                    <p>Jeżeli nie używałeś tej opcji zignoruj tę wiadomość.</p>
	                <p><hr /></p>
	                <p>Aby zresetować hasło proszę kliknąć w poniższy link i wprowadzić nowe hasło: <a href="{URL_ZMIANA_HASLA}">{URL_ZMIANA_HASLA}</a></p>
                    <p>Żądanie resetowania hasła zostało wykonane z adresu IP: {IP}</p>
	                    ',
	        ),
        
        
	        'rejestracja_klienta_na_szkolenie_powiadomienie' => array(
	                'description' => '
    	                <p>Wysyłane do administracji w celu poinformowania o nowej rejestracji na szkolenie.</p>
    	                <p>Rejestrację można potwierdzić, anulować, zmienić status jej płatności itp..</p>
                        <p>Dostępne zmienne:</p>
    	                <ul>
    	                    <li>{EDYCJA_URL} - Link do podglądu / edycji rejestracji</li>
                            <li>{SZKOLENIE} - Nazwa szkolenia</li>
                            <li>{TERMIN} - Termin szkolenia</li>
                            <li>{FIRMA} - Nazwa firmy</li>
	                        <li>{EMAIL} - Email klienta podany przy rejestracji</li>
	                        <li>{TELEFON} - Telefon klienta podany przy rejestracji</li>
                                <li>{ULICA} - Ulica klienta </li>   
                                <li>{KOD_POCZTOWY} - Kod pocztowy klienta</li>
                                <li>{MIASTO} - Miasto klienta</li>
                                <li>{NIP} - NIP klienta</li>
                                <li>{FAX} - fax klienta</li>
                            <li>{LICZBA_UCZESTNIKOW} - liczba uczestników</li>
                            <li>{LISTA_UCZESTNIKOW} - lista uczestników</li>
                            <li>{KOSZT_OSOBA}</li>
                            <li>{KOSZT_CALKOWITY}</li>
                            <li>{UWAGI}</li> - uwagi podane przez klienta
                            
    	                </ul>
	                ',
	                'topic' => 'Nowa rejestracja na szkolenie  "{SZKOLENIE}". Termin: {TERMIN} ',
	                'content' => '
	                    <p>Nowa rejestracja na szkolenie. </p>
                        <p>Szkolenie: {SZKOLENIE}</p>
                        <p>Termin: {TERMIN}</p>
                        
                        <p>Dane firmy:<br />
                        {FIRMA}<br />
                        {ULICA}<br />
                        {KOD_POCZTOWY} {MIASTO}<br />
                        NIP: {NIP}<br />
                        tel. {TELEFON}<br />
                        email: {EMAIL}<br />
                        fax: {FAX}
                        </p>
                        
                        <p>Uczestnicy: {LICZBA_UCZESTNIKOW}</p>
                        <p>Uwagi: {UWAGI}</p>
                        <p>Cena / osobę: {KOSZT_OSOBA}</p>
                        <p>Cena całkowita: {KOSZT_CALKOWITY}</p>
                        
                        <p>Lista uczestnikow:</p>
                        <p>{LISTA_UCZESTNIKOW}</p>
                        <p></p>
	                    <p>Aby przejść do edycji kliknij w link: <a href="{EDYCJA_URL}">{EDYCJA_URL}</a></p>
	                   
	                    ',
	        ),

    );



	public function __construct(EntityManager $em) {

		$this->em = $em;

	}


	/**
	 * @param string $name Name of the email_template.
	 * @return string|null Value of the email_template.
	 * @throws \RuntimeException If email_template is not defined.
	 */
	public function get($name, $default_content = '', $default_topic = '') {

	    $this->all();

		if(array_key_exists($name, $this->email_templates)) {
			return $this->email_templates[$name];
		}

		$_templ = new EmailTemplate();
		$_templ->setName($name);
		$_templ->setContent($default_content);
		$_templ->setTopic($default_topic);
		$this->em->persist($_templ);
		$this->em->flush();

		$this->email_templates[$name] = $_templ;

		return $_templ;

	}

	public function getParsed($name, $values = array()) {

	    $template = $this->get($name);

	    $parsed_template = new EmailTemplate();

	    $topic = $template->getTopic();
	    $content = $template->getContent();
	    $content_txt = $template->getContentTxt();

	    foreach($values as $key => $val) {

	        $topic = str_replace('{'.$key.'}', $val, $topic);
	        $content = str_replace('{'.$key.'}', $val, $content);
	        $content_txt = str_replace('{'.$key.'}', $val, $content_txt);


	    }

	    $parsed_template->setTopic($topic);
	    $parsed_template->setContent($content);
	    $parsed_template->setContentTxt($content_txt);

	    return $parsed_template;


	}

	/**
	 * @return string[] with key => value
	 */
	public function all() {

		$email_templates = array();

		if($this->is_loaded) {
			return $this->email_templates;
		}

		foreach ($this->getRepo()->findAll() as $email_template) {
			$email_templates[$email_template->getName()] = $email_template;
		}


		foreach($this->default_email_templates as $_name => $_def_email_template) {
            if(!array_key_exists($_name, $email_templates)) {
                $_templ = new EmailTemplate();
                $_templ->setName($_name);
                $_templ->setDescription($_def_email_template['description']);
                $_templ->setContent($_def_email_template['content']);
                $_templ->setTopic($_def_email_template['topic']);
                $this->em->persist($_templ);
                $this->em->flush();
                $email_templates[$_name] = $_templ;
            }
		}

		$this->email_templates = $email_templates;
		$this->is_loaded = true;;

		return $email_templates;
	}

	/**
	 * @return EntityRepository
	 */
	protected function getRepo() {
		if ($this->repo === null) {
			$this->repo = $this->em->getRepository(get_class(new EmailTemplate()));
		}

		return $this->repo;
	}

}
