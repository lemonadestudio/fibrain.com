<?php

namespace Cms\ElmatBundle\Model;

class OfferType {

    //$oferta_swiatlowody
    const OFFER_FIBER_CABLES = 1;
    
    //$oferta_akcesoria_swiatlowodowe
    const OFFER_FIBER_ACCESSORIES = 2;
    
    //$oferta_mikrokanalizacja
    const OFFER_MICRO_CANALIZATION = 3;
    
    //$oferta_kable_napowietrzne_airtrack
    const OFFER_AIRTRACK_CABLES = 4;
    
    //$oferta_technologia_fttx
    const OFFER_TECHNOLOGY_FTTX = 5;
    
    //$oferta_technologia_gpon
    const OFFER_TECHNOLOGY_GPON = 6;
    
    //$oferta_urzadzenia_aktywne
    const OFFER_ACTIVE_DEVICES = 7;
    
    //$oferta_sieci_miejskie
    const OFFER_CITY_NETWORK = 8;
    
    //$oferta_osprzet_instalacyjny
    const OFFER_INSTALLATION_EQUIPMENT = 9;
    
    //$oferta_osprzet_pomiarowy
    const OFFER_MEASURING_EQUIPMENT = 10;
    
    //$oferta_okablowanie_strukturalne
    const OFFER_STRUCTURED_CABLING = 11;
    
    private static $offerToUserColumnMap = array(
        self::OFFER_FIBER_CABLES => 'oferta_swiatlowody',
        self::OFFER_FIBER_ACCESSORIES => 'oferta_akcesoria_swiatlowodowe',
        self::OFFER_MICRO_CANALIZATION => 'oferta_mikrokanalizacja',
        self::OFFER_AIRTRACK_CABLES => 'oferta_kable_napowietrzne_airtrack',
        self::OFFER_TECHNOLOGY_FTTX => 'oferta_technologia_fttx',
        self::OFFER_TECHNOLOGY_GPON => 'oferta_technologia_gpon',
        self::OFFER_ACTIVE_DEVICES => 'oferta_urzadzenia_aktywne',
        self::OFFER_CITY_NETWORK => 'oferta_sieci_miejskie',
        self::OFFER_INSTALLATION_EQUIPMENT => 'oferta_osprzet_instalacyjny',
        self::OFFER_MEASURING_EQUIPMENT => 'oferta_osprzet_pomiarowy',
        self::OFFER_STRUCTURED_CABLING => 'oferta_okablowanie_strukturalne'
    );
    
    private static $offerToNameMap = array(
        self::OFFER_FIBER_CABLES => 'Kable światłowodowe',
        self::OFFER_FIBER_ACCESSORIES => 'Akcesoria światłowodowe',
        self::OFFER_MICRO_CANALIZATION => 'Mikorkanalizacja MetroJet',
        self::OFFER_AIRTRACK_CABLES => 'Kable napowietrzne AirTrack',
        self::OFFER_TECHNOLOGY_FTTX => 'Technologia FTTx',
        self::OFFER_TECHNOLOGY_GPON => 'Technologia GPON',
        self::OFFER_ACTIVE_DEVICES => 'Urządzenia aktywne',
        self::OFFER_CITY_NETWORK => 'Rozwiązania dla sieci miejskich',
        self::OFFER_INSTALLATION_EQUIPMENT => 'Osprzęt instalacyjny',
        self::OFFER_MEASURING_EQUIPMENT => 'Osprzęt pomiarowy',
        self::OFFER_STRUCTURED_CABLING => 'Okablowanie strukturalne'
    );

    public static function getAllTypes() {
        return self::$offerToNameMap;
    }

    public static function exists($offerId) {
        return array_key_exists($offerId, self::$offerToNameMap);
    }

    public static function getDescription($offerId) {
        if (!self::exists($offerId)) {
            return null;
        }

        return self::$offerToNameMap[$offerId];
    }

    public static function getMappedUserOfferColumn($offerId) {
        if (array_key_exists($offerId, self::$offerToUserColumnMap)) {
            return self::$offerToUserColumnMap[$offerId];
        }
        
        return null;
    }

}
